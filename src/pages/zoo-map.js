import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import styles from "./zoo-map.module.css"
import MapJSON from "../../public/maps/kczoo.json"
import { renderAsText } from "../utility"
import Loading from "../components/loading"
import { generateActivitiesByDate } from "../utility"

const locations = {
  "Alligator Alley": {
    x: 0.3938,
    y: 0.246,
  },
  "Auditorium in the Main Lobby": {
    x: 0.2955,
    y: 0.2387,
  },
  "Camel Feeding Deck near Australia": {
    x: 0.47384224506292993,
    y: 0.20275159360965092,
  },
  "Cheetah Habitat in East Africa": {
    x: 0.635870192863268,
    y: 0.5740475402986631,
  },
  "Chimpanzee Habitat in East Africa": {
    x: 0.8727335720056761,
    y: 0.7926309404506658,
  },
  "Demonstration Area at Elephant Expedition": {
    x: 0.46053816837998645,
    y: 0.4208926920282344,
  },
  "Outside of the Koala Building": {
    x: 0.38480194017782043,
    y: 0.27959491905228545,
  },
  "Lower Level at Orangutan Canopy": {
    x: 0.3942718558723594,
    y: 0.15660154841436694,
  },
  "Discovery Barn": {
    x: 0.4372,
    y: 0.2709,
  },
  "Helzberg Penguin Plaza": {
    x: 0.38526388728453087,
    y: 0.3160847445928181,
  },
  "Hippo Habitat in East Africa": {
    x: 0.8581244947449832,
    y: 0.6883548962865025,
  },
  "Indoor Viewing at Polar Bear Passage": {
    x: 0.34594063979718687,
    y: 0.23968678485198566,
  },
  "Indoor Viewing at the Lion Habitat in East Africa": {
    x: 0.7394040882317774,
    y: 0.7741985311961422,
  },
  "Inside Tropics building": {
    x: 0.3278092158450574,
    y: 0.3837142154139777,
  },
  "Lorikeet Encounters near Endangered Species Carousel": {
    x: 0,
    y: 0,
  },
  "Matschie's Tree Kangaroo habitat in Australia": {
    x: 0.5326,
    y: 0.1333,
  },
  "On the deck at Gorilla Habitat": {
    x: 0.4661046310195809,
    y: 0.60637739206478,
  },
  "Painted Dog Habitat in East Africa": {
    x: 0.820418062131536,
    y: 0.5422381331411501,
  },
  "Rhino Habitat in East Africa": {
    x: 0.8040766832196532,
    y: 0.7565094250474746,
  },
  "Sea Lion Habitat near Tuxedo Grill": {
    x: 0.32659660468881424,
    y: 0.33971780140697955,
  },
  "Snakes building near Australia": {
    x: 0.48458251530194835,
    y: 0.12464603798029324,
  },
  "Stingray Bay": {
    x: 0.4093,
    y: 0.289,
  },
  "Swamp Monkey habitat near Stingray Bay": {
    x: 0.4093,
    y: 0.289,
  },
  "Tiger Habitat on Tiger Trail": {
    x: 0.44341147938530084,
    y: 0.10205435248992095,
  },
  "Viewing Deck near Treetops Café in East Africa": {
    x: 0,
    y: 0,
  },
  "Tropics Event Pavilion": {
    x: 0.2625,
    y: 0.4351,
  },
}

class Map extends React.Component {
  constructor(props) {
    super(props)

    let activities = generateActivitiesByDate(
      props.data.prismic.allActivitys.edges
    )

    this.state = {
      animalsIncomplete: this.props.data.prismic.allAnimal_details.edges,
      animals: null,
      attractionsIncomplete: this.props.data.prismic.allMap_attractions.edges,
      attractions: null,
      activitiesIncomplete: activities,
      activities: [],
    }

    this.combinePrismicAndJSON = this.combinePrismicAndJSON.bind(this)
    this.mapplicRef = React.createRef()
  }

  loadAnimals(cursor) {
    this.props.prismic
      .load({
        variables: { cursor: cursor },
        query, // (optional)
        fragments: [], // (optional)
      })
      .then(res => {
        this.setState({
          animalsIncomplete: this.state.animalsIncomplete.concat(
            res.data.allAnimal_details.edges
          ),
        })
        if (res.data.allAnimal_details.pageInfo.hasNextPage) {
          this.loadAnimals(res.data.allAnimal_details.pageInfo.endCursor)
        } else {
          this.setState({ animals: this.state.animalsIncomplete })
        }
      })
      .catch(function(err) {
        // This will fix your error since you are now handling the error thrown by your first catch block
        console.log(err.message)
      })
  }

  loadAttractions(cursor) {
    this.props.prismic
      .load({
        variables: { cursor: cursor },
        query, // (optional)
        fragments: [], // (optional)
      })
      .then(res => {
        this.setState({
          attractionsIncomplete: this.state.attractionsIncomplete.concat(
            res.data.allMap_attractions.edges
          ),
        })
        if (res.data.allMap_attractions.pageInfo.hasNextPage) {
          this.loadAttractions(res.data.allMap_attractions.pageInfo.endCursor)
        } else {
          this.setState({ attractions: this.state.attractionsIncomplete })
        }
      })
      .catch(function(err) {
        // This will fix your error since you are now handling the error thrown by your first catch block
        console.log(err.message)
      })
  }

  loadActivities(cursor) {
    this.props.prismic
      .load({
        variables: { cursor: cursor },
        query, // (optional)
        fragments: [], // (optional)
      })
      .then(res => {
        this.setState({
          activitiesIncomplete: this.state.activitiesIncomplete.concat(
            generateActivitiesByDate(res.data.allActivitys.edges)
          ),
        })
        if (res.data.allActivitys.pageInfo.hasNextPage) {
          this.loadActivities(res.data.allActivitys.pageInfo.endCursor)
        } else {
          this.setState({ activities: this.state.activitiesIncomplete })
        }
      })
      .catch(function(err) {
        // This will fix your error since you are now handling the error thrown by your first catch block
        console.log(err.message)
      })
  }

  combinePrismicAndJSON(mapJson, prismicAnimals, prismicAttractions) {
    console.log({ mapJson, prismicAnimals, prismicAttractions })

    prismicAnimals = prismicAnimals.filter(animal => {
      return animal.node.map_id !== null
    })

    prismicAnimals.forEach(animal => {
      mapJson.levels[0].locations = mapJson.levels[0].locations.map(
        mapAnimal => {
          if (animal.node.map_id === mapAnimal.id) {
            var _tmpAnimal = {
              title: animal.node.map_tooltip_title
                ? renderAsText(animal.node.map_tooltip_title)
                : mapAnimal.title,
              description: animal.node.map_tooltip_description
                ? renderAsText(animal.node.map_tooltip_description)
                : mapAnimal.description,
              thumbnail: animal.node.wide_images
                ? animal.node.wide_images["Map-Thumb"].url
                : mapAnimal.thumbnail,
            }

            return Object.assign(mapAnimal, _tmpAnimal)
          } else {
            return mapAnimal
          }
        }
      )
    })

    prismicAttractions.forEach(attraction => {
      mapJson.levels[0].locations = mapJson.levels[0].locations.map(
        mapAttraction => {
          if (attraction.node.attraction_map_id === mapAttraction.id) {
            var _tmpAttraction = {
              title: attraction.node.attraction_tooltip_title
                ? renderAsText(attraction.node.attraction_tooltip_title)
                : mapAttraction.title,
              description: attraction.node.attraction_tooltip_description
                ? renderAsText(attraction.node.attraction_tooltip_description)
                : mapAttraction.description,
              thumbnail: attraction.node.attraction_map_thumbnail
                ? attraction.node.attraction_map_thumbnail.url
                : mapAttraction.thumbnail,
            }

            return Object.assign(mapAttraction, _tmpAttraction)
          } else {
            return mapAttraction
          }
        }
      )
    })

    this.state.activities.forEach(d => {
      let id = `activity-${d.uid}`

      if (mapJson.levels[0].locations.filter(d => d.id === id).length === 0) {
        mapJson.levels[0].locations.push(
          Object.assign({}, locations[d.location], {
            title: renderAsText(d.title),
            id: id,
            description: d.description,
            pin: "hidden",
            category: "activity",
            fill: "#41af49",
            action: "tooltip",
          })
        )
      }
    })

    return mapJson
  }

  componentDidMount() {
    let {
      hasNextPage,
      endCursor,
    } = this.props.data.prismic.allAnimal_details.pageInfo
    let {
      hasNextPage: attractionsHasNextPage,
      endCursor: attractionsEndCursor,
    } = this.props.data.prismic.allMap_attractions.pageInfo

    //Check if there is more than one page of animals
    if (hasNextPage) {
      this.loadAnimals(endCursor)
    } else {
      this.setState({ animals: this.state.animalsIncomplete })
    }

    //Check if there is more than one page of attractions
    if (attractionsHasNextPage) {
      this.loadAttractions(attractionsEndCursor)
    } else {
      this.setState({ attractions: this.state.attractionsIncomplete })
    }

    //Check if there is more than one page of activities
    if (this.props.data.prismic.allActivitys.pageInfo.hasNextPage) {
      this.loadActivities(
        this.props.data.prismic.allActivitys.pageInfo.endCursor
      )
    } else {
      this.setState({ activities: this.state.activitiesIncomplete })
    }
  }

  componentWillUnmount() {
    window.$(this.mapplicRef.current).mapplic("destroy")
  }

  componentDidUpdate() {
    if (this.state.animals && this.state.attractions) {
      const height = window.innerHeight - 100

      console.log({ MapJSON })

      window.$(this.mapplicRef.current).mapplic({
        source: this.combinePrismicAndJSON(
          MapJSON,
          this.state.animals,
          this.state.attractions
        ),
        height: `${height}px`,
        mapfill: true,
        sidebar: true,
        alphabetic: true,
        thumbholder: false,
        marker: "hidden",
        minimap: false,
        fullscreen: true,
        zoommargin: 0,
        maxscale: 3,
        basecolor: "#fff",
      })
    }
  }

  render() {
    const doc = this.props.data.prismic.allMap_landing_pages.edges
      .slice(0, 1)
      .pop()
    console.log("MAP doc -->", doc)
    if (!doc) return null
    const { meta_title, meta_description } = doc.node
    console.log("SOURCE FOR MAP", this.state.source)

    return (
      <Layout title="Zoo Map" section="visit" {...this.props}>
        <SEO
          title={meta_title}
          description={meta_description}
          canonical={this.props.location.origin + this.props.location.pathname}
        />
        <div className={styles.top}></div>
        <div ref={this.mapplicRef} className={styles.mapplicContainer}></div>
        <div>
          {!this.state.animals && (
            <div className={styles.mapSpinnerContainer}>
              <div id="lottieAnimation">
                <Loading />
              </div>
            </div>
          )}
        </div>
      </Layout>
    )
  }
}

export default Map

export const query = graphql`
  query ZooMapQuery($cursor: String) {
    prismic {
      allMap_landing_pages {
        edges {
          node {
            meta_description
            meta_title
          }
        }
      }
      allMap_attractions(after: $cursor) {
        edges {
          node {
            attraction_map_id
            attraction_tooltip_title
            attraction_tooltip_description
            attraction_map_thumbnail
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
      }
      allAnimal_details(after: $cursor) {
        edges {
          node {
            wide_images
            map_id
            map_tooltip_description
            map_tooltip_title
            animal_name
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
      }
      allActivitys(after: $cursor) {
        edges {
          node {
            title
            description
            _meta {
              id
              tags
              type
              uid
            }
            activity_times_are_active
            activity_times {
              duration
              duration_time_units
              frequency
              location
              start_am_pm
              start_hour
              start_minute
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
      }
    }
  }
`
