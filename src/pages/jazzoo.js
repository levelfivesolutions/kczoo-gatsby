import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import Hero from "../components/jazzoo/hero"
import Slicer from "../components/slice-r/slice-r"
import {
  renderAsHtml,
  renderAsText,
  linkResolver,
  ColorToHex,
} from "../utility"
import styles from "./jazzoo.module.css"
import SectionBlock from "../components/section-block"
import JazzooNavigation from "../components/jazzoo/navigation"
import Fringe from "../components/jazzoo/fringe"
import Title from "../components/title"
import JazzooCarousel from "../components/jazzoo/carousel"
import CalendarDay from "../components/calendar-day"
import JazzooFooter from "../components/jazzoo/footer"
import { Carousel } from "react-bootstrap"
import { CarouselItem } from "react-bootstrap"
import moment from "moment"
import Button from "../components/button/button-link"
import { Link } from "gatsby"
import Columns from "../components/jazzoo/columns"

const JazzzooLanding = props => {
  const { data, location } = props
  const doc = data.prismic.allJazzoo_homs.edges.slice(0, 1).pop()

  const [imagesPerSlide, setImagesPerSlide] = useState(1)
  const [slides, setSlides] = useState(null)
  const [images, setImages] = useState(null)

  console.log("DOC", doc)

  function debounce(fn, ms) {
    let timer
    return _ => {
      clearTimeout(timer)
      timer = setTimeout(_ => {
        timer = null
        fn.apply(this, arguments)
      }, ms)
    }
  }

  function setNumOfImagesPerSlide() {
    const width = typeof window !== "undefined" ? window.innerWidth : ""
    if (width >= 1184) {
      setImagesPerSlide(4)
    } else if (width >= 976) {
      setImagesPerSlide(3)
    } else if (width >= 752) {
      setImagesPerSlide(2)
    } else {
      setImagesPerSlide(1)
    }
  }

  useEffect(() => {
    const debouncedHandleResize = debounce(function handleResize() {
      setNumOfImagesPerSlide()
    }, 100)

    setNumOfImagesPerSlide()

    window.addEventListener("resize", debouncedHandleResize)

    return _ => {
      window.removeEventListener("resize", debouncedHandleResize)
    }
  })

  useEffect(() => {
    let tempSlides = []
    let i,
      j,
      temporary,
      chunk = imagesPerSlide
    for (i = 0, j = images.length; i < j; i += chunk) {
      temporary = images.slice(i, i + chunk)
      tempSlides.push(temporary)
    }
    setSlides(tempSlides)
  }, [images, imagesPerSlide])

  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    body,
    body_text,
    title,
    date,
    date_description,
    carousel,
    jazzoo_photo_carousel,
    past_photos_link,
    event_detail_area,
    jazzoo_navigation_get_tickets_link,
    jazzoo_navigation_get_tickets_text,
    get_tickets_text,
    get_tickets_link,
    event_detail_right_heading,
    event_detail_right_caption,
    event_detail_left_heading,
    event_detail_right_body,
    event_detail_left_body,
    event_detail_left_caption,
  } = doc.node

  if (!images) {
    setImages(jazzoo_photo_carousel)
  }

  const dateFormat = new Intl.DateTimeFormat("en-US", {
    weekday: "long",
    month: "long",
    day: "2-digit",
    year: "numeric",
  })

  const formattedDate = dateFormat.format(
    new Date(date).setDate(new Date(date).getDate() + 1)
  )

  return (
    <Layout title={meta_title || title} section="activities" {...props}>
      {console.log(meta_title, "meta title")}
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero pageId={doc.node._meta.id} />
      <JazzooNavigation
        id={_meta.id}
        location={location}
        ticketLinkText={renderAsText(jazzoo_navigation_get_tickets_text)}
        ticketLink={jazzoo_navigation_get_tickets_link?.url}
        ticketLinkActive="true"
      />
      <Fringe>
        <SectionBlock classList={styles.jazzooSection1}>
          <Title color="Jazzoo 2022 Green" className={styles.title}>
            {title}
          </Title>
          <div className="container mb-4">
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <div className={styles.carouselContainer}>
                  <JazzooCarousel images={carousel} />
                </div>
                {renderAsHtml(body_text)}
                <div className="d-flex flex-column flex-sm-row align-items-center justify-content-center">
                  {
                    get_tickets_link &&
                    <Button
                    size="large"
                    color="Jazzoo 2022 Maroon"
                    url={get_tickets_link.url}
                    text={renderAsText(get_tickets_text)}
                    />
                  }
                  &nbsp; &nbsp; &nbsp;
                  <Button
                    size="large"
                    color="Jazzoo 2022 Maroon"
                    link="/jazzoo/sponsor-levels"
                    text="Become a Sponsor"
                  />
                </div>
              </div>
            </div>
          </div>
          {/* <div className="container">
            <div className="row">
              <div className="offset-md-1 col-md-10">
                {body
                  .filter(d => d.type === "cta")
                  .map(slice => {
                    return slice.fields.length > 0 ? (
                      <Slicer key={slice.__typename} data={slice} />
                    ) : null
                  })}
                {body
                  .filter(d => d.type === "info_callout")
                  .map(slice => {
                    return <Slicer key={slice.__typename} data={slice} />
                  })}
              </div>
            </div>
          </div> */}
        </SectionBlock>
        <div className={`${styles.zigzagSection}`}>
          {//
          // Carousel
          //
          event_detail_area === true ? (
            <div className="container my-3">
              <div className="row d-flex align-items-center justify-content-center">
                <h3
                  style={{
                    fontSize: "24px",
                    marginBottom: 24,
                    marginTop: 14,
                    fontFamily: "Catalpa",
                    fontWeight: "700",
                    color: "#2B4D20",
                    lineHeight: "32px",
                  }}
                >
                  Past Jazzoo Photos
                </h3>
              </div>
              <div className="row">
                <div className="col">
                  <Carousel>
                    {slides &&
                      slides.map(slide => {
                        console.log("slide", slide)
                        return (
                          <Carousel.Item>
                            <div className="row">
                              {slide.map(image => (
                                <div className="col-12 d-flex justify-content-center col-md-6 col-lg-4 col-xl-3">
                                  <div style={{ paddingBottom: 10 }}>
                                    <img
                                      style={{
                                        boxShadow: "0 3px 6px rgba(0,0,0,.5)",
                                      }}
                                      src={
                                        image.jazzoo_photo_carousel_image.url
                                      }
                                      alt={
                                        image.jazzoo_photo_carousel_image.alt
                                      }
                                    />
                                  </div>
                                </div>
                              ))}
                            </div>
                          </Carousel.Item>
                        )
                      })}
                  </Carousel>
                  <div
                    className="d-flex justify-content-center"
                    style={{ marginTop: 24 }}
                  >
                    <Button
                      color="Tangerine"
                      isInverse
                      text="See All Past Photos"
                      link={linkResolver(past_photos_link)}
                    />
                  </div>
                </div>
              </div>
            </div>
          ) : (
            //
            // Event Details
            //
            <div className="container my-5">
              <div className="row d-flex">
                <div className="col-12 col-md">
                  <div className="d-flex flex-row flex-md-column flex-lg-row">
                    <div className="mb-3 mb-lg-0 d-flex align-items-center align-items-md-start ">
                      <div>
                        <CalendarDay
                          date={date}
                          color="Jazzoo 2022 Maroon"
                          size="extra-large"
                        />
                      </div>
                    </div>
                    <div className="ml-4 pl-1 pl-md-0 pl-lg-1 ml-md-0 ml-lg-4">
                      <p
                        style={{
                          fontFamily: "Catalpa",
                          fontSize: "24px",
                          fontWeight: "700",
                          color: "#2B4D20",
                          lineHeight: "32px",
                          marginBottom: "8px",
                        }}
                        className={styles.greyText}
                      >
                        {renderAsText(event_detail_left_heading)}
                      </p>
                      <p
                        className={styles.greyText}
                        style={{
                          fontFamily: "DINOTBold",
                          fontSize: "24px",
                          fontWeight: "bold",
                          lineHeight: "31px",
                        }}
                      >
                        {formattedDate}
                      </p>
                      <p style={{ fontSize: "18px", lineHeight: "23px" }}>
                        <div className={styles.eventDetailLeftBody}>
                          {renderAsHtml(event_detail_left_body)}
                        </div>
                      </p>
                    </div>
                  </div>
                  <div
                    style={{ marginTop: "25px" }}
                    className={styles.finePrint}
                  >
                    {renderAsText(event_detail_left_caption)}
                  </div>
                </div>
                <div className="col-12 col-md-auto d-flex flex-row my-4 my-md-0 flex-md-column align-items-center justify-content-around">
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                  <div aria-hidden="true" className={styles.dividerDot}></div>
                </div>
                <div className="col-12 col-md">
                  <p
                    style={{
                      fontFamily: "Catalpa",
                      fontSize: "24px",
                      fontWeight: "700",
                      color: "#2B4D20",
                      lineHeight: "32px",
                      marginBottom: "8px",
                    }}
                    className={styles.greyText}
                  >
                    {renderAsText(event_detail_right_heading)}
                  </p>
                  {renderAsHtml(event_detail_right_body)}
                  <div className={styles.finePrint}>
                    {renderAsText(event_detail_right_caption)}
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
        <SectionBlock classList={styles.jazzooSection2}>
          {body
            .filter(d => d.type === "column")
            .map(slice => {
              return (
                <Columns
                  classList={styles.noBackground}
                  key={slice.__typename}
                  sliceData={slice}
                />
              )
            })}
          <div className="container" style={{ marginBottom: "180px" }}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                {body
                  .filter(d => d.type !== "column")
                  .map(slice => {
                    return (
                      <Slicer
                        classList={styles.noBackground}
                        key={slice.__typename}
                        data={slice}
                      />
                    )
                  })}
              </div>
            </div>
          </div>
        </SectionBlock>
      </Fringe>
      <JazzooFooter />
    </Layout>
  )
}

export default JazzzooLanding

export const query = graphql`
  {
    prismic {
      allJazzoo_homs {
        edges {
          node {
            _meta {
              id
            }
            wide_image
            small_image
            fringe_left
            fringe_right
            title
            date
            date_description
            body_text
            meta_title
            meta_description
            event_detail_area
            carousel {
              image
            }
            body {
              ... on PRISMIC_Jazzoo_homBodyInfo_callout {
                type
                label
                primary {
                  info_description
                }
                fields {
                  info_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                  }
                  info_link_title
                }
              }
              ... on PRISMIC_Jazzoo_homBodyColumn {
                type
                label
                primary {
                  column_section_title
                }
                fields {
                  color
                  column_image
                  column_text
                  column_title
                  plu
                  plu_description
                  column_cta_label
                  column_cta {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                  }
                }
              }
              ... on PRISMIC_Jazzoo_homBodyCta {
                type
                label
                fields {
                  icon
                  color
                  cta_label
                  cta_text
                  cta_title
                  cta_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC_Form {
                      hero_image
                      title
                      _linkType
                      form_id
                      _meta {
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Jazzoo_landing_page {
                      _linkType
                      _meta {
                        tags
                        type
                        uid
                        id
                      }
                    }
                    ... on PRISMIC_Jazzoo_hom {
                      _meta {
                        uid
                        type
                        tags
                      }
                      _linkType
                    }
                    ... on PRISMIC_Jazzoo_sponsor_levels {
                      _linkType
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC_Jazzoo_sponsors {
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                      _linkType
                    }
                    ... on PRISMIC_Jazzoo_grid_layout {
                      _linkType
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC_Jazzoo_volunteers {
                      _linkType
                      _meta {
                        uid
                        type
                        tags
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_Jazzoo_homBodyMultiple_link_cta {
                type
                label
                fields {
                  multiple_link_cta_text
                  multiple_link_cta_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC_Form {
                      hero_image
                      title
                      _linkType
                      form_id
                      _meta {
                        id
                        type
                        uid
                      }
                    }
                    ... on PRISMIC_Jazzoo_landing_page {
                      _linkType
                      _meta {
                        tags
                        type
                        uid
                        id
                      }
                    }
                    ... on PRISMIC_Jazzoo_hom {
                      _meta {
                        uid
                        type
                        tags
                      }
                      _linkType
                    }
                    ... on PRISMIC_Jazzoo_sponsor_levels {
                      _linkType
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC_Jazzoo_sponsors {
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                      _linkType
                    }
                    ... on PRISMIC_Jazzoo_grid_layout {
                      _linkType
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC_Jazzoo_volunteers {
                      _linkType
                      _meta {
                        uid
                        type
                        tags
                      }
                    }
                  }
                }
                primary {
                  multiple_link_cta_description
                }
              }
            }
            event_detail_area
            past_photos_link {
              ... on PRISMIC_Photo_library {
                _meta {
                  type
                  uid
                  id
                  tags
                }
              }
              _linkType
            }
            jazzoo_photo_carousel {
              jazzoo_photo_carousel_image
            }
            jazzoo_navigation_get_tickets_text
            jazzoo_navigation_get_tickets_link {
              ... on PRISMIC__ExternalLink {
                target
                _linkType
                url
              }
            }
            get_tickets_text
            get_tickets_link {
              ... on PRISMIC__ExternalLink {
                target
                _linkType
                url
              }
            }
            event_detail_right_heading
            event_detail_right_caption
            event_detail_left_heading
            event_detail_right_body
            event_detail_left_body
            event_detail_left_caption
          }
        }
      }
    }
  }
`
