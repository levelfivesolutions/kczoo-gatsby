import React from "react";
import { graphql } from 'gatsby';
//import { Link } from "gatsby";
import { filterArrayByType, renderLinkUrl } from '../utility.js';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import SectionBlock from '../components/section-block';
import CardLanding from '../components/card-landing';
import CampExperience from '../components/camp-experience/camp-experience'
import { getPrice, linkResolver  } from '../utility'


const PremierExperiences = (props) => {
	let { data, location } = props;
	console.log('>>>', data);

	const doc = data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
	console.log('doc -->', doc);
	if (!doc) return null;
	
  const { wide_header_size, title, feature, summary, meta_title, meta_description, body, _meta } = doc.node;
	const fotzPluBlocks = filterArrayByType(body, 'feature_block_with_plu_and_fotz')[0].fields;

	return (
		<Layout title={ title } section="animals" { ...props }>
			<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
      <Hero image={ wide_header_size } pageId={ _meta.id } />
      <TitleBar 
      	title={ title } 
      	description={ summary }
      />
      { fotzPluBlocks &&
			fotzPluBlocks.map((d, i) => {
					return (
						<SectionBlock key={ i }>
							<div className="container">
								<div className="row">
									<div className="col-md-12">
										<CampExperience
											color={ d.color }
											image={ d.wide_image }
											title={ d.title }
											richDescription={ d.description }
											timeRange={ d.additional_text }
											prices={[
												{
													price: getPrice([], d.plu_price, 0),
													description: d.plu_description
												}
											]}
											btnText={ d.cta_label }
											btnLink={ d.cta_link }
										/>
									</div>
                  {
                    console.log("asdf", d.cta_link, d.cta_link)
                  }
								</div>
							</div>
						</SectionBlock>
					);
				})
			}        
		</Layout>
	);
}

export default PremierExperiences;

export const query = graphql`
{
  prismic {
    allLanding_page_hero_blocks(uid: "premier-experiences") {
      edges {
        node {
          _meta {
            id
            tags
            type
            uid
          }
          meta_title
          meta_description
          wide_header_size
          title
          summary
          feature
          button_label
          cta {
            ... on PRISMIC__ExternalLink {
              _linkType
              url
            }
          }
          body {
            ... on PRISMIC_Landing_page_hero_blockBodyFeature_block_with_plu_and_fotz {
              type
              label
              fields {
                additional_text
                color
                cta_label
                description
                plu_description
                plu_price
                title: title1
                wide_image
                cta_link {
                  ... on PRISMIC__ImageLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC__FileLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC__ExternalLink {
                    target
                    _linkType
                    url
                  }
                  ... on PRISMIC_Form {
                    _linkType
                    _meta {
                      id
                      uid
                      type
                      tags
                    }
                  }
                  ... on PRISMIC_Detail {
                    _linkType
                    _meta {
                      id
                      uid
                      type
                      tags
                    }
                  }
                }
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyFeature_block {
              type
              label
              fields {
                callout_description
                callout_image
                callout_title
                color_name
                cta_text
                cta: cta1 {
                  ... on PRISMIC_Special_exhibit {
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
`

