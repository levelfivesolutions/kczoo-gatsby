import React from 'react';
import { graphql } from 'gatsby';
import { RichText } from 'prismic-reactjs';

export const query = graphql`
  {
		prismic {
			allHome_pages {
				edges {
					node {
						hero_title
						hero_description
					}
				}
			}
		}
  }
`

export default function Test({ data }) {
	const doc = data.prismic.allHome_pages.edges.slice(0,1).pop();
	console.log('doc', doc);
	if (!doc) return null;
	return (
		<>
			<h1>{RichText.asText(doc.node.hero_title)}</h1>
			<h2>{doc.node.hero_description}</h2>
		</>
	);
}
