import React from "react"
import { graphql } from "gatsby"
import { linkResolver } from "../utility"
import Layout from "../components/layout/layout"
import { Helmet } from "react-helmet"
import BodyText from "../components/body-text"

const docQuery = graphql`
  query docQuery($docId: String) {
    prismic {
      _allDocuments(id: $docId) {
        edges {
          node {
            _meta {
              type
              tags
              uid
              id
            }
          }
        }
      }
    }
  }
`

class Preview extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      doc: null,
      docId: null,
    }
  }

  load(docId) {
    console.log("prismic???", this.props.prismic, this.props)
    if (this.props.prismic) {
      this.props.prismic
        .load({
          variables: { id: docId },
          query: docQuery, // (optional)
        })
        .then(res => {
          console.log(">>>>>> LOADED", res)
          this.setState({ doc: res.data })
        })
        .catch(err => {
          console.error("Error doing thing", err)
        })
    }
  }

  componentDidMount() {
    let { location, prismic } = this.props

    if (typeof location === "undefined") return null

    let query = new URLSearchParams(location.search)
    console.log(this.props, "thisprops")

    const documentId = query.get("documentId")

    console.log("docId", documentId)
    this.load(documentId)
    this.setState({ docId: documentId })
  }

  render() {
    const redirectUrl = linkResolver(this.state.doc) || "/"

    console.log("doc", this.state.doc)
    return (
      <>
        <Helmet>
          {/* <meta http-equiv="Refresh" content={`0; url='${redirectUrl}'"`} /> */}
        </Helmet>
        <div style={{ padding: "40px" }}>
          <a href={redirectUrl}>
            {this.state.docId} {redirectUrl} If you are not automatically
            redirected, click here.
          </a>
        </div>
      </>
    )
  }
}

export default Preview
