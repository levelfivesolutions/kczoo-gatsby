import React from "react";
import { graphql } from 'gatsby';
//import { Link } from "gatsby";
import { filterArrayByType } from '../utility.js';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import SectionBlock from '../components/section-block';
import CardLanding from '../components/card-landing';
import AddOnBlock from "../components/add-on/add-on-block.js";

const BirthdaysLanding = (props) => {
	let { data, location } = props;
	console.log('>>>', data);
																							 
	const doc = data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
	console.log('doc -->', doc);
	if (!doc) return null;

	const { wide_header_size, title, cta, summary, button_label, body, meta_title, meta_description } = doc.node;
	
	const featuredItems = filterArrayByType(body, "feature_block").pop().fields;
	//const secondaryItems = filterArrayByType(body, "column").pop().fields;
		
	return (
		<Layout title={ title } section="rent" { ...props }>
			<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
      <Hero image={ wide_header_size } pageId={ doc.node._meta.id } />
      <TitleBar 
      	title={ title } 
      	description={ summary }
        linkText={ button_label }
        link={ cta } 
      />
      {
				featuredItems.map((d, i) => {
					return (
						<SectionBlock key={ i }>
							<div className="container">
								<div className="row">
									<div className="col-md-12">
										<CardLanding
											color={ d.color_name }
											image={ d.callout_image }
											season=''
											year=''
											minAge=''
											maxAge=''
											title={ d.callout_title }
											description={ d.callout_description }
											urlText={ d.cta_text }
											link={ d.cta }
										/>
									</div>
								</div>
							</div>
						</SectionBlock>
					);
				})
      }
      <SectionBlock>
        <div className="container">
          <div className="row">
            <div className="col">
              <AddOnBlock dark body={body} />
            </div>
          </div>
        </div>
      </SectionBlock>
		</Layout>
	);
}

export default BirthdaysLanding;

export const query = graphql`
{
  prismic {
    allLanding_page_hero_blocks(uid: "birthdays") {
      edges {
        node {
          _meta {
						id
						tags
						type
						uid
          }
          meta_title
          meta_description
          wide_header_size
          title
          summary
          feature
          body_text
          button_label
          cta {
						_linkType
						... on PRISMIC__Document {
							_meta {
								id
								tags
								type
								uid
							}
						}
						... on PRISMIC__ExternalLink {
							_linkType
							url
						}
						... on PRISMIC__FileLink {
							_linkType
							url
						}
          }
          body {
            ... on PRISMIC_Landing_page_hero_blockBodyFeature_block {
              type
              label
              fields {
                callout_description
                callout_image
                callout_title
                color_name
                cta_text
                cta:cta1 {
									_linkType
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC__FileLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC_Detail {
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                }
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyAddOns {
              type
              label
              fields {
                addOn_title
                description
                icon_selector
              }
              primary {
                subheading
                title1
              }
            }
          }
        }
      }
    }
  }
}

`

