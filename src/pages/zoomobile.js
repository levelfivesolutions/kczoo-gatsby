import React, { useState, useEffect } from "react";
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import SectionBlock from '../components/section-block';
import CampExperience from '../components/camp-experience/camp-experience';
import CallOutBlock from "../components/call-out/call-out-block";
import { filterArrayByType } from '../utility';
import { getItems } from '../galaxy';
import { getPrice } from '../utility';
import styles from './homeschool.module.css';

const ZoomobileLanding = (props) => {
  const [items, setItems] = useState(0);

  useEffect(() => {
    getItems( (res) => {
    	setItems(res) 
    });
  }, []);
	
	const { data, location } = props;
	console.log('>>>', data);
	
	const doc = data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
	console.log('doc -->', doc);
	if (!doc) return null;
	
  const { wide_header_size, title, feature, summary, body, meta_title, meta_description, button_label, cta } = doc.node;
	
	let featuredItems = filterArrayByType(body, 'feature_block_with_plu_and_fotz')[0].fields;
	console.log('featuredItems', JSON.stringify(featuredItems));

	return (
		<Layout title={ title } section="education" { ...props }>
			<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
      <Hero image={ wide_header_size } pageId={doc.node._meta.id}/>
      <TitleBar 
      	title={ title } 
      	description={ summary }
      	calloutDescription={ feature }
      	link={ cta }
      	linkText={ button_label }
      />
      {
				featuredItems.map((d, i) => {
					
					return (
						<SectionBlock key={ i }>
							<div className="container">
								<div className="row">
									<div className="col-md-12">
                      <CampExperience
                        color={ d.color }
                        image={ d.wide_image }
                        title={ d.title }
                        richDescription={ d.description }
                        timeRange={ d.additional_text }
                        prices={[
													{
														price: getPrice(items, d.plu_price, 0),
														description: d.plu_description
													}
												]}
                        btnText={ d.cta_label }
                        btnLink={ d.cta_link }
                      />
									</div>
								</div>
							</div>
						</SectionBlock>
					);
				})
			}
			<CallOutBlock body={ body } />
		</Layout>
	);
}

export default ZoomobileLanding;

export const query = graphql`
{
  prismic {
    allLanding_page_hero_blocks(uid: "zoomobile") {
      edges {
        node {
          _meta {
            id
            tags
            type
            uid
          }
          meta_title
          meta_description
          wide_header_size
          title
          summary
          feature
          cta {
            _linkType
            ... on PRISMIC__ExternalLink {
              _linkType
              url
            }
            ... on PRISMIC_Form {
              _linkType
              _meta {
                id
                tags
                type
                uid
              }
            }
          }
          button_label
          body_text
          body {
            ... on PRISMIC_Landing_page_hero_blockBodyFeature_block_with_plu_and_fotz {
              type
              label
              fields {
                additional_text
                color
                cta_label
                cta_link {
                  _linkType
                  ... on PRISMIC_Detail {
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                }
                description
                plu_description
                plu_price
                title:title1
                wide_image
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyDetailed_callout {
              type
              label
              fields {
                title:title1
                text:text1
                image:image1
                color
                cta_label
                link {
                  _linkType
                  ... on PRISMIC__FileLink {
                    _linkType
                    url
                  }
                }
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyAlert {
              type
              label
              primary {
                alert_title
                alerttext
                color
                icon_selector
              }
            }
          }
        }
      }
    }
  }
}
`
