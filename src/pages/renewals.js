import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import TitleBar from "../components/renewals/titlebar-landing.js"
import Hero from "../components/hero-landing/hero-landing"
import styles from "./renewals.module.css"
import RenewalCards from "../components/renewals/renewal-card"
import { renderAsHtml } from "../utility"

const MembershipRenewals = props => {
  const { data, location } = props
  const doc = data.prismic.renewal_page

  return (
    <Layout
      title={doc.title}
      parent={{
        title: "Memberships",
        url: "/memberships",
      }}
      {...props}
    >
      <SEO
        title={doc.meta_title}
        description={doc.meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero image={doc.hero.url} pageId={doc._meta.id} />
      <TitleBar title={doc.title} description={doc.description} />
      <section>
        <div className="container">
          <div className="row d-flex justify-content-center">
            <div className={`col-10 ${styles.bodyText}`}>
              {renderAsHtml(doc.body_text)}
            </div>
            <RenewalCards cards={doc.renewal_types} />
          </div>
        </div>
      </section>
      <div className={styles.footerText}>
        {renderAsHtml(doc.renewal_footer_text)}
      </div>
    </Layout>
  )
}

export default MembershipRenewals

export const MembershipRenewalQuery = graphql`
  query MemberShipRenewalQuery {
    prismic {
      renewal_page(lang: "en-us", uid: "renew") {
        _meta {
          uid
          tags
          type
        }
        title
        description
        hero
        body_text
        meta_description
        meta_title
        renewal_footer_text
        renewal_types {
          color
          renewal_button_text
          renewal_option_description
          renewal_option_link {
            ... on PRISMIC__ExternalLink {
              _linkType
              url
            }
          }
          renewal_option_message
          renewal_option_title
        }
      }
    }
  }
`
