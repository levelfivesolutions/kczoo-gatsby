import React, { useState, useEffect } from "react";
import { graphql } from 'gatsby';
//import { Link } from "gatsby";
import { filterArrayByType, getPrice } from '../utility';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import SectionBlock from '../components/section-block';
import CampExperience from '../components/camp-experience/camp-experience';
import { getItems } from '../galaxy';
import CampSeasonal from '../components/camp-seasonal/camp-seasonal';
import CallOutBlock from "../components/call-out/call-out-block";
import styles from './day-camps-landing.module.css';

const DayCampLanding = (props) => {
	const { data, location } = props;
	const doc = data.prismic.allDay_camps_landing_pages.edges.slice(0,1).pop();
  const [items, setItems] = useState(0);

  useEffect(() => {
    getItems( (res) => {
    	setItems(res) 
    });
  }, []);

	console.log("DOC", doc);
	if (!doc) return null;
	const { wide_header_size, title, subhead_description, body, meta_title="", meta_description } = doc.node;

	var activeCamps = filterArrayByType( body, "camp_feature_callout")[0].fields;
	var inactiveCamps = filterArrayByType( body, "column")[0].fields;
	console.log("CAMPASSSSS ", inactiveCamps);

	return (
		<Layout title={ title } section="camps" { ...props }>
			<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
			<Hero image={ wide_header_size } pageId={doc.node._meta.id}/>
			<TitleBar title={ title } description={ subhead_description } />
			{
				activeCamps.map((d, i) => {
					return (
						<SectionBlock>
							<div className="container">
									<div className="row">
									<div className="col-md-12">
										<CampExperience
											color={ d.color_name }
											image={d.callout_image}
											title={d.callout_title}
											richDescription={d.description}
											ageRange={d.age_range}
											startDate={d.date_start}
											endDate={d.date_end}
											timeRange={ d.time_range }
											prices={[
												{
													price: getPrice(items, d.reg_plu_price),
													description: d.reg_plu_price_description
												},
												{
													price: getPrice(items, d.fotz_plu_price),
													description: d.fotz_plu_price_description
												}
											]}
											btnText={ d.cta_label }
											url={`/camp/${d.cta._meta.uid}`}
											/>
									</div>
									</div>
							</div>
						</SectionBlock>

					);
				})
			}
			{
				inactiveCamps &&
				<SectionBlock>
					<div className="container">
						<h2 className={styles.sectionTitle}>More Seasonal Camps</h2>
						<div className="row">
							<div className="col-md-10 offset-md-1">
								<div className="row v-20-s">
									{
										inactiveCamps.map((d, i) => {
											return (
												<div className="col-md-4">
													<CampSeasonal
														color={d.color}
														image={d.column_image}
														title={d.column_title}
														description={d.column_text}
														ageRange={ d.age_range }
														season={ d.season }
														link={ d.cta_link }
													/>
												</div>
											);
										})
									}
								</div>
							</div>
						</div>
					</div>
				</SectionBlock>
			}

			<SectionBlock>
					<div className="container">
						<div className="row v-20-s" >
								<CallOutBlock body={body} />
						</div>
					</div>
			</SectionBlock>
		</Layout>
	);
}

export default DayCampLanding

export const query = graphql`
{
    prismic {
      allDay_camps_landing_pages {
        edges {
          node {    
            body {
              ... on PRISMIC_Day_camps_landing_pageBodyCamp_feature_callout {
                type
                fields {
                  age_range
                  callout_image
                  callout_title
                  color_name
                  cta_label
                  description
                  fotz_plu_price
                  fotz_plu_price_description
                  reg_plu_price
                  reg_plu_price_description
                  time_range
                  cta {
                    ... on PRISMIC_Camp {
                      _meta {
                        uid
                        id
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_Day_camps_landing_pageBodyColumn {
                type
                fields {
                  age_range
                  column_image
                  column_text
                  column_title
                  season
                  color
                  cta_label
									cta_link {
										_linkType
										... on PRISMIC__Document {
											_meta {
												id
												tags
												type
												uid
											}
										}
										... on PRISMIC__ExternalLink {
											_linkType
											url
										}
									}
                }
              }
              ... on PRISMIC_Day_camps_landing_pageBodyDetailed_callout {
                type
                label
                fields {
                  body_text
                  button_label
                  image1
                  title1
                  color
                  link:cta1 {
										_linkType
										... on PRISMIC__Document {
											_meta {
												id
												tags
												type
												uid
											}
										}
										... on PRISMIC__ExternalLink {
											_linkType
											url
										}
                  }
                }
              }
            }
            wide_header_size
            meta_title
            meta_description
            subhead_description
            title
            _meta {
							id
							type
							uid
							tags
            }        
          }
        }
      }
    }
  }  
`
