import React from "react";
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import styles from './corporate-meetings.module.css';
import { filterArrayByType, renderAsText } from '../utility'
import CorporateMeetingRoom from "../components/corporate-meeting/corporate-meeting-room";
import AddOnBlock from "../components/add-on/add-on-block";
import SectionBlock from "../components/section-block";

class PicnicsLanding extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
		const doc = this.props.data.prismic.allPurrfect_picnics_landing_pages.edges.slice(0,1).pop();
    if (!doc) return null;
    const { button_label, callout_text, wide_header_size, meta_description, cta_link, meta_title, subhead_description, body, title, _meta } = doc.node;
  
    var rooms = [];
    rooms = this.props.data.prismic.allPurrfect_picnics_locations.edges.map(d => { return (d.node) });
    
    return (
      <Layout title={ title } section="rent" { ...this.props }>
        <SEO title={ meta_title } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
        <Hero image={ wide_header_size } pageId={ _meta.id } />
        <TitleBar 
        	title={ title } 
        	link={ cta_link } 
        	linkText={ button_label } 
        	description={ subhead_description } 
        	calloutTitle={ callout_text }
        />
        {
          rooms.map((d, i) => {
            
            return (
              <SectionBlock key={ i } id={ d._meta.id } className={styles.section}>
                <div className="container">
                  <div className="row">
                    <div className="col-md-12">
                      <CorporateMeetingRoom
                        color={ d.body && filterArrayByType( d.body , "color_picker")[0].primary.color_name }
                        image={ d.wide_image }
                        title={ d.meeting_room_name }
                        description={d.body_text}
                        btnText="Book Your Picnic"
                        url={ `/form/purrfect-picnic-request?preferredLocation=${ renderAsText(d.meeting_room_name).replace(/ /g,'-') }` }
                      />
                    </div>
                  </div>
                </div>
              </SectionBlock>

            );
          })
        }
        <section className={ styles.section }>
          <AddOnBlock dark body={body} />
        </section>

      </Layout>
    );
  }
}

export default PicnicsLanding;

export const query = graphql`
{
  prismic {
    allPurrfect_picnics_landing_pages {
      edges {
        node {
          _meta {
            id
          }          
          body {
            ... on PRISMIC_Purrfect_picnics_landing_pageBodyAddOns1 {
              type
              label
              fields {
                addOn_text
                addOn_title
                icon_selector
              }
              primary {
                subheading
                title1
              }
            }
          }
          button_label
          callout_text
          wide_header_size
          meta_description
          meta_title
          subhead_description
          title
          cta_link {
						_linkType
            ... on PRISMIC__ExternalLink {
              url
            }
          }
        }
      }
    }
    allPurrfect_picnics_locations {
      edges {
        node {
          body {
            ... on PRISMIC_Purrfect_picnics_locationBodyColor_picker {
              type
              primary {
                color_name
              }
            }
          }
          body_text
          cta_label
          wide_image
          meeting_room_name
          _meta {
            id
          }
        }
      }
    }
  }
}
`

