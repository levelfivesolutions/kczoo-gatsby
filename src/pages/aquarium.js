import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import Hero from "../components/hero-landing/hero-landing"
import SlicerR from "../components/slice-r/slice-r"
import CTA from "../components/cta/cta"
import { renderAsHtml, renderAsText, filterArrayByType } from "../utility"
import styles from "./aquarium-landing.module.css"
import AquariumNavigation from "../components/aquarium-navigation/aquarium-navigation"

const AquariumLanding = props => {
  const { data, location } = props
  const doc = data.prismic.allAquarium_homes.edges.slice(0, 1).pop()

  console.log("DOC", doc)
  if (!doc) return null
  const {
    aquarium_summary,
    body_text,
    meta_description,
    meta_title,
    title,
    wide_image,
    body,
  } = doc.node

  console.log("Aquarium Body", body)

  const footer = filterArrayByType(body, "reused_image_text_block")
  const columns = filterArrayByType(body, "column")
  const banner = filterArrayByType(body, "cta")

  return (
    <Layout title={title} section="camps" {...props}>
      <SEO
        title={renderAsText(title)}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero image={wide_image} pageId={doc.node._meta.id} />
      <TitleBar title={title} description={aquarium_summary} />
      <AquariumNavigation location={location} />
      <div className="container text-center">
        {banner.length > 0 && (
          <div className={styles.banner}>
            {banner.map(slice => {
              return <SlicerR data={slice} />
            })}
          </div>
        )}
        <div
          className="row d-flex justify-content-center"
          style={{ margin: "80px 0" }}
        >
          <div className={`col-10 ${styles.bodyText}`}>
            {renderAsHtml(body_text)}
          </div>
        </div>
      </div>
      {columns.map(slice => {
        return <SlicerR key={slice.__typename} data={slice} />
      })}
      {footer.map(slice => {
        return <SlicerR key={slice.__typename} data={slice} />
      })}
    </Layout>
  )
}

export default AquariumLanding

export const query = graphql`
  {
    prismic {
      allAquarium_homes {
        edges {
          node {
            aquarium_summary
            _meta {
              id
            }
            body {
              ... on PRISMIC_Aquarium_homeBodyDetailed_callout {
                type
                label
                fields {
                  body_text
                  title1
                  image1
                  cta1 {
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                    _linkType
                    ... on PRISMIC_Aquarium_site_plan {
                      _meta {
                        uid
                        tags
                        type
                        id
                      }
                      _linkType
                    }
                    ... on PRISMIC_Detail {
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                    }
                  }
                  color
                  button_label
                }
              }
              ... on PRISMIC_Aquarium_homeBodyDetailed_callout2 {
                type
                label
                fields {
                  color
                  cta_label
                  image1
                  link {
                    ... on PRISMIC__ImageLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      name
                      size
                      url
                    }
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC_Aquarium_site_plan {
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                      _linkType
                    }
                    ... on PRISMIC_Detail {
                      _linkType
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                    }
                  }
                  second_cta_label
                  second_link {
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__ImageLink {
                      _linkType
                      url
                    }
                  }
                  text1
                  title1
                }
              }
              ... on PRISMIC_Aquarium_homeBodyReused_image_text_block {
                type
                label
                primary {
                  inner_block {
                    _linkType
                    ... on PRISMIC_Reusable_image_text_block {
                      image_text_image
                      image_text_title
                      _linkType
                      image_text_color
                      image_text_background_image
                      image_text_link_text
                      image_text_link {
                        ... on PRISMIC__ImageLink {
                          _linkType
                          url
                        }
                        ... on PRISMIC__FileLink {
                          _linkType
                          url
                        }
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                        ... on PRISMIC_Detail {
                          _linkType
                          _meta {
                            id
                            uid
                            tags
                            type
                          }
                        }
                        ... on PRISMIC_Aquarium_donations {
                          _linkType
                          _meta {
                            id
                            uid
                            tags
                            type
                          }
                        }
                        ... on PRISMIC_Form {
                          _linkType
                          _meta {
                            uid
                            id
                            tags
                            type
                          }
                        }
                        _linkType
                      }
                      image_text_text
                      _meta {
                        type
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Aquarium_site_plan {
                      _linkType
                      _meta {
                        uid
                        tags
                        type
                        id
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_Aquarium_homeBodyColumn {
                type
                label
                fields {
                  color
                  column_image
                  column_text
                  column_title
                  column_cta_label
                  column_cta {
                    ... on PRISMIC__ImageLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC_Aquarium_donations {
                      _linkType
                      _meta {
                        uid
                        id
                      }
                    }
                    ... on PRISMIC_Aquarium_site_plan {
                      _linkType
                      _meta {
                        uid
                        type
                        id
                      }
                    }
                    ... on PRISMIC_Detail {
                      _linkType
                      _meta {
                        id
                        uid
                        type
                        tags
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_Aquarium_homeBodyCta {
                type
                label
                fields {
                  color
                  cta_label
                  cta_link {
                    _linkType
                    ... on PRISMIC_Aquarium_donations {
                      _linkType
                      _meta {
                        id
                        type
                        uid
                      }
                    }
                  }
                  cta_text
                  cta_title
                  icon
                }
              }
            }
            body_text
            meta_description
            meta_title
            spot_image
            tall_image
            title
            wide_image
          }
        }
      }
    }
  }
`
