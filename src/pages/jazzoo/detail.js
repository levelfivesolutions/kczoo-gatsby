import React from "react"
import { graphql } from "gatsby"
import moment from "moment"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/jazzoo/hero"
import Slicer from "../../components/slice-r/slice-r"
import { renderAsHtml, renderImageUrl } from "../../utility"
import styles from "./detail.module.css"
import SectionBlock from "../../components/section-block"
import JazzooNavigation from "../../components/jazzoo/navigation"
import Fringe from "../../components/jazzoo/fringe"
import Title from "../../components/title"
import CalendarDay from "../../components/calendar-day"
import JazzooFooter from "../../components/jazzoo/footer"

const JazzzooDetail = props => {
  const { data, location } = props
  console.log("data ->", data)
  const doc = data.prismic.allJazzoo_event_details.edges.slice(0, 1).pop()

  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    title,
    image,
    when,
    when_description,
    where,
    attire,
    body,
  } = doc.node

  return (
    <Layout
      title={meta_title || title}
      parent={{
        title: "Jazzoo",
        url: "/jazzoo",
      }}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero pageId={doc.node._meta.id} />
      <JazzooNavigation id={_meta.id} location={location} />
      <Fringe>
        <SectionBlock>
          <Title color="Jazzoo 2022 Green" className={styles.title}>
            {title}
          </Title>
          <div className="container">
            <div className="row">
              <div className="offset-md-1 col-md-7">
                <h2 className={styles.sectionTitle}>WHEN</h2>
                <div className={styles.calendarEvent}>
                  <div className={styles.calendarDay}>
                    <CalendarDay date={when} color="Pumpkin" size="large" />
                  </div>
                  <div className={styles.calendarDesc}>
                    <div>{moment(when).format("dddd, LL")}</div>
                    <div>
                      <small>{renderAsHtml(when_description)}</small>
                    </div>
                  </div>
                </div>
                <div>
                  <h2 className={styles.sectionTitle}>WHERE</h2>
                  {renderAsHtml(where)}
                  <h2 className={styles.sectionTitle}>ATTIRE</h2>
                  {renderAsHtml(attire)}
                </div>
              </div>
              <div className={`col-md-3`}>
                <img
                  src={renderImageUrl(image)}
                  alt={image.alt}
                  className="img-fluid"
                />
              </div>
            </div>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                {body
                  .filter(d => d.type === "info_callout")
                  .map(slice => {
                    return (
                      <div className="mt-3">
                        <Slicer key={slice.__typename} data={slice} />
                      </div>
                    )
                  })}
                {body
                  .filter(d => d.type === "cta")
                  .map(slice => {
                    return (
                      <div className="mt-5">
                        <Slicer
                          key={slice.__typename}
                          data={slice}
                          className={`mt-4 ${styles.jazzoo2022BottomPadding}`}
                        />
                      </div>
                    )
                  })}
              </div>
            </div>
          </div>
        </SectionBlock>
      </Fringe>
      <JazzooFooter />
    </Layout>
  )
}

export default JazzzooDetail

export const query = graphql`
  {
    prismic {
      allJazzoo_event_details {
        edges {
          node {
            meta_title
            meta_description
            title
            image
            when
            when_description
            where
            attire
            body {
              ... on PRISMIC_Jazzoo_event_detailBodyInfo_callout {
                type
                label
                primary {
                  info_description
                }
                fields {
                  info_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                  }
                  info_link_title
                }
              }
              ... on PRISMIC_Jazzoo_event_detailBodyCta {
                type
                label
                fields {
                  color
                  cta_label
                  cta_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                  }
                  cta_text
                  cta_title
                  icon
                }
              }
            }
            _meta {
              id
              uid
              type
              tags
            }
          }
        }
      }
    }
  }
`
