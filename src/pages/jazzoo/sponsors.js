import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/jazzoo/hero"
import Slicer from "../../components/slice-r/slice-r"
import {
  renderAsHtml,
  renderImageUrl,
  ColorToHex,
  linkResolver,
} from "../../utility"
import styles from "./sponsors.module.scss"
import SectionBlock from "../../components/section-block"
import JazzooNavigation from "../../components/jazzoo/navigation"
import Fringe from "../../components/jazzoo/fringe"
import Titlebar from "../../components/jazzoo/titlebar"
import JazzooFooter from "../../components/jazzoo/footer"
import Icon from "../../components/icon/icon"
import SmartLink from "../../components/smart-link"
import Button from "../../components/button/button-link"

function Separator({ icon, name, color, className }) {
  const style = {
    backgroundColor: ColorToHex(color),
  }
  return (
    <div className={`${styles.separator} ${className}`}>
      <div className={styles.separatorCenter}>
        <div className={styles.iconContainer} style={style}>
          <Icon icon={icon} width="25" color="White" />
        </div>
        <h6 className={styles.separatorTitle}>{name}</h6>
      </div>
    </div>
  )
}

const JazzzooSponsors = props => {
  const { data, location } = props
  console.log("data ->", data)
  const doc = data.prismic.allJazzoo_sponsorss.edges.slice(0, 1).pop()
  console.log("DOC", doc)

  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    title,
    introduction,
    presenting_sponsor,
    presenting_sponsor_link,
    introduction_button,
    introduction_button_text,
    sponsors_tier_1,
    sponsors_tier_2,
    sponsors_tier_3,
    sponsors_tier_4,
    sponsors_tier_5,
    sponsors_tier_6,
    sponsors_tier_7,
  } = doc.node

  return (
    <Layout
      title={meta_title || title}
      parent={{
        title: "Jazzoo",
        url: "/jazzoo",
      }}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero pageId={doc.node._meta.id} />
      <JazzooNavigation id={_meta.id} location={location} />
      <Fringe>
        <Titlebar
          title={title}
          className={styles.title}
          color="Jazzoo 2022 Green"
          introduction={introduction}
        />
        {
          introduction_button && introduction_button_text && 
          <div className="container">
          <div className="row">
            <div className="col-12 d-flex justify-content-center">
              {
                console.log("INTRODUCTION", introduction_button)
              }
            <Button
                color="Jazzoo 2022 Maroon"
                text={introduction_button_text}
                link={introduction_button}
              />
            </div>
          </div>
        </div>
              }
        <div className="container">
          {presenting_sponsor && (
            <section className={styles.section}>
              <Separator icon="hippo" name="HIPPO" color="Jazzoo 2021 Orange" />
              <div className="row">
                <div className="col-md-4 offset-md-4 d-flex justify-content-center">
                  <SmartLink link={presenting_sponsor_link}>
                    <img
                      src={renderImageUrl(presenting_sponsor)}
                      alt={presenting_sponsor?.alt || ""}
                      className="img-fluid"
                    />
                  </SmartLink>
                </div>
              </div>
            </section>
          )}
          {sponsors_tier_1?.length > 0 && (
            <section className={styles.section}>
              <div className="row">
                <div className="offset-md-1 col-md-10">
                  <Separator
                    icon="elephant"
                    name="ELEPHANT"
                    color="Jazzoo 2021 Green"
                  />
                  <div className="row">
                    {sponsors_tier_1.map(d => {
                      return (
                        <div className="col-md-4 d-flex align-items-center">
                          <SmartLink link={d.link}>
                            <img
                              src={renderImageUrl(d.logo)}
                              alt={d.logo.alt}
                              className="img-fluid"
                            />
                          </SmartLink>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </section>
          )}
          {sponsors_tier_2?.length > 0 && (
            <section className={styles.section}>
              <div className="row">
                <div className="offset-md-1 col-md-10">
                  <Separator
                    icon="baboon"
                    name="BABOON"
                    color="Jazzoo 2021 Blue"
                    className="mb-3"
                  />
                  <div className={styles.columns}>
                    {sponsors_tier_2.map(d => {
                      return (
                        <div className={styles.linkWrapper}>
                          <SmartLink link={d.link}>{d.name}</SmartLink>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </section>
          )}
          {sponsors_tier_3?.length > 0 && (
            <section className={styles.section}>
              <div className="row">
                <div className="offset-md-1 col-md-10">
                  <Separator
                    icon="lion"
                    name="LION"
                    color="Jazzoo 2021 Red"
                    className="mb-3"
                  />
                  <div className={styles.columns}>
                    {sponsors_tier_3.map(d => {
                      return (
                        <div className={styles.linkWrapper}>
                          <SmartLink link={d.link}>{d.name}</SmartLink>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </section>
          )}
          {sponsors_tier_4?.length > 0 && (
            <section className={styles.section}>
              <div className="row">
                <div className="offset-md-1 col-md-10">
                  <Separator
                    icon="giraffe"
                    name="GIRAFFE"
                    color="Jazzoo 2021 Light Green"
                    className="mb-3"
                  />
                  <div className={styles.columns}>
                    {sponsors_tier_4.map(d => {
                      return (
                        <div className={styles.linkWrapper}>
                          <SmartLink link={d.link}>{d.name}</SmartLink>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </section>
          )}
          {sponsors_tier_5?.length > 0 && (
            <section className={styles.section}>
              <div className="row">
                <div className="offset-md-1 col-md-10">
                  <Separator
                    icon="monkey"
                    name="CHIMP"
                    color="Jazzoo 2021 Orange"
                    className="mb-3"
                  />
                  <div className={styles.columns}>
                    {sponsors_tier_5.map(d => {
                      return (
                        <div className={styles.linkWrapper}>
                          <SmartLink link={d.link}>{d.name}</SmartLink>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </section>
          )}
          {sponsors_tier_6?.length > 0 && (
            <section className={styles.section}>
              <div className="row">
                <div className="offset-md-1 col-md-10">
                  <Separator
                    icon="flamingo"
                    name="FLAMINGO"
                    color="Jazzoo 2021 Green"
                    className="mb-3"
                  />
                  <div className={styles.columns}>
                    {sponsors_tier_6.map(d => {
                      return (
                        <div className={styles.linkWrapper}>
                          <SmartLink link={d.link}>{d.name}</SmartLink>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </section>
          )}
          {sponsors_tier_7?.length > 0 && (
            <section className={styles.section}>
              <div className="row">
                <div className="offset-md-1 col-md-10">
                  <Separator
                    icon="gift"
                    name="IN KIND SPONSORS"
                    color="Jazzoo 2021 Blue"
                    className="mb-3"
                  />
                  <div className={styles.columns}>
                    {sponsors_tier_7.map(d => {
                      return (
                        <div className={styles.linkWrapper}>
                          <SmartLink link={d.link}>{d.name}</SmartLink>
                        </div>
                      )
                    })}
                  </div>
                </div>
              </div>
            </section>
          )}
        </div>
      </Fringe>
      <JazzooFooter />
    </Layout>
  )
}

export default JazzzooSponsors

export const query = graphql`
{
  prismic {
    allJazzoo_sponsorss {
      edges {
        node {
          _meta {
            id
            tags
            type
            uid
          }
          meta_title
          meta_description
          title
          introduction
          introduction_button {
            _linkType
            ... on PRISMIC__ExternalLink {
              _linkType
              url
              target
            }
            ... on PRISMIC_Jazzoo_sponsor_levels {
              _linkType
              _meta {
                id
                tags
                type
                uid
              }
            }
          }
          introduction_button_text
          presenting_sponsor
          presenting_sponsor_link {
            _linkType
            ... on PRISMIC__ExternalLink {
              _linkType
              url
            }
            ... on PRISMIC__Document {
              _meta {
                id
                tags
                type
                uid
              }
            }
          }
          presenting_sponsor_name
          sponsors_tier_1 {
            logo
            name
            link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
          }
          sponsors_tier_2 {
            logo
            name
            link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
          }
          sponsors_tier_3 {
            name
            link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
          }
          sponsors_tier_4 {
            name
            link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
          }
          sponsors_tier_5 {
            name
            link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
          }
          sponsors_tier_6 {
            name
            link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
          }
          sponsors_tier_7 {
            name
            link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
          }
        }
      }
    }
  }
}

`
