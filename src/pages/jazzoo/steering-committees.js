import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/jazzoo/hero"
import Slicer from "../../components/slice-r/slice-r"
import { renderAsHtml, renderImageUrl } from "../../utility"
import styles from "./steering-committees.module.scss"
import SectionBlock from '../../components/section-block'
import JazzooNavigation from "../../components/jazzoo/navigation"
import Fringe from "../../components/jazzoo/fringe"
import Titlebar from "../../components/jazzoo/titlebar"
import BodyText from "../../components/body-text"
import JazzooFooter from "../../components/jazzoo/footer"

const JazzzooSponsors = props => {
    const { data, location } = props
    console.log('data ->', data)
    const doc = data.prismic.allJazzoo_steering_committees.edges.slice(0, 1).pop()

    console.log("DOC", doc)
    if (!doc) return null
    const {
        _meta,
        meta_title,
        meta_description,
        title,
        body_text,
        image,
        image_caption
    } = doc.node

    return (
        <Layout parent={{
            title: "Jazzoo",
            url: "/jazzoo",
          }} title={meta_title || title} section="activities" {...props}>
            <SEO
                title={meta_title}
                description={meta_description}
                canonical={location.origin + location.pathname}
            />
            <Hero pageId={doc.node._meta.id} />
            <JazzooNavigation id={_meta.id} location={location} />
            <Fringe>
                <Titlebar title={title} />
                <div className={styles.contentWrapper}>
                    <div className="container">
                        <div className="row">
                            <div className="offset-md-1 col-md-7">
                                {renderAsHtml(body_text)}
                            </div>
                            <div className="col-md-3">
                                <figure>
                                    <img src={renderImageUrl(image)} alt={image.alt} className={`img-fluid ${styles.image}`} />
                                    <figcaption className={styles.imageCaption}>{image_caption}</figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </Fringe>
            <JazzooFooter />
        </Layout>
    )
}

export default JazzzooSponsors

export const query = graphql`
{
  prismic {
    allJazzoo_steering_committees {
        edges {
          node {
                _meta {
                    id
                    tags
                    type
                    uid
                }
                image
                image_caption
                title
                introduction
                body_text
            }
        }
      }
    }
}
`
