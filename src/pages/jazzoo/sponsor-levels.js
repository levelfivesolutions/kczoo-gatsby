import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/jazzoo/hero"
import Slicer from "../../components/slice-r/slice-r"
import { renderAsText, renderImageUrl } from "../../utility"
import styles from "./sponsor-levels.module.scss"
import SectionBlock from "../../components/section-block"
import JazzooNavigation from "../../components/jazzoo/navigation"
import Fringe from "../../components/jazzoo/fringe"
import Titlebar from "../../components/jazzoo/titlebar"
import JazzooFooter from "../../components/jazzoo/footer"
import Button from "../../components/button/button-link.js"

const JazzzooSponsorLevels = props => {
  const { data, location } = props
  console.log("data ->", data)
  const doc = data.prismic.allJazzoo_sponsor_levelss.edges.slice(0, 1).pop()

  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    title,
    introduction,
    introduction_cta_text,
    introduction_cta_link,
    chart_title,
    chart_image,
  } = doc.node

  return (
    <Layout
      title={meta_title || title}
      parent={{
        title: "Jazzoo",
        url: "/jazzoo",
      }}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero pageId={doc.node._meta.id} />
      <JazzooNavigation id={_meta.id} location={location} />
      <Fringe>
        <SectionBlock paddingTop={false} shading="even" >
        <Titlebar
          title={title}
          color="Jazzoo 2022 Green"
          introduction={introduction}
        />
        {introduction_cta_link && (
          <div className={styles.buttonContainer}>
            <div className="container">
              <Button
                color="Jazzoo 2022 Maroon"
                text={introduction_cta_text}
                link={introduction_cta_link}
              />
            </div>
          </div>
        )}
</SectionBlock>
        <SectionBlock shading="even">
          <div className="container" style={{ paddingBottom: 100 }}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <h2 className={styles.sectionTitle}>
                  {renderAsText(chart_title)}
                </h2>
                <img
                  src={renderImageUrl(chart_image)}
                  alt={chart_image.alt}
                  className={`img-fluid ${styles.image}`}
                />
              </div>
            </div>
          </div>
        </SectionBlock>
      </Fringe>
      <JazzooFooter />
    </Layout>
  )
}

export default JazzzooSponsorLevels

export const query = graphql`
  {
    prismic {
      allJazzoo_sponsor_levelss {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            title
            introduction
            introduction_cta_link {
              _linkType
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
                target
              }
            }
            introduction_cta_text
            chart_title
            chart_image
          }
        }
      }
    }
  }
`
