import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/jazzoo/hero"
import Slicer from "../../components/slice-r/slice-r"
import { renderAsHtml, renderImageUrl } from "../../utility"
import styles from "./volunteers.module.scss"
import SectionBlock from "../../components/section-block"
import JazzooNavigation from "../../components/jazzoo/navigation"
import Fringe from "../../components/jazzoo/fringe"
import Titlebar from "../../components/jazzoo/titlebar"
import BodyText from "../../components/body-text"
import JazzooFooter from "../../components/jazzoo/footer"

const JazzzooSponsors = props => {
  const { data, location } = props
  console.log("data ->", data)
  const doc = data.prismic.allJazzoo_volunteerss.edges.slice(0, 1).pop()

  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    title,
    body_copy,
    image,
    body,
  } = doc.node

  return (
    <Layout
      title={meta_title || title}
      parent={{
        title: "Jazzoo",
        url: "/jazzoo",
      }}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero pageId={doc.node._meta.id} />
      <JazzooNavigation id={_meta.id} location={location} />
      <Fringe>
        <Titlebar title={title} />
        <div className={styles.contentWrapper}>
          <div className="container">
            <div className="row">
              <div className="offset-md-1 col-md-7">
                {renderAsHtml(body_copy)}
              </div>
              <div className="col-md-3">
                <img
                  src={renderImageUrl(image)}
                  alt={image.alt}
                  className={`img-fluid ${styles.image}`}
                />
              </div>
            </div>
          </div>
        </div>
        {body
          .filter(d => d.type === "simple_columns")
          .map(slice => {
            return (
              <Slicer
                classList={styles.jazzoo2022BottomPadding}
                key={slice.__typename}
                data={slice}
                inset={false}
                shading="even"
              />
            )
          })}
      </Fringe>
      <JazzooFooter />
    </Layout>
  )
}

export default JazzzooSponsors

export const query = graphql`
  {
    prismic {
      allJazzoo_volunteerss {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            title
            body_copy
            image
            body {
              ... on PRISMIC_Jazzoo_volunteersBodySimple_columns {
                type
                label
                fields {
                  simple_columns_color
                  simple_columns_body_text
                  simple_columns_cta {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                  }
                  simple_columns_cta_label
                }
              }
            }
          }
        }
      }
    }
  }
`
