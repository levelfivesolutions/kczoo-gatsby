import React from "react"
import { graphql } from "gatsby"
import { RichText } from "prismic-reactjs"
import moment from "moment"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import groupBy from "lodash.groupby"
import sortBy from "lodash.sortby"
import Hero from "../components/hero-landing/hero-landing.js"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import Button from "../components/button/button-link.js"
import SpecialEvent from "../components/special-event"
import styles from "./events.module.css"
import { renderAsText } from "../utility"

const multiplier = 3

class SpecialEvents extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      max: multiplier,
      events: this.props.data.prismic.allSpecial_events.edges,
    }

    this.handleClickShowMore = this.handleClickShowMore.bind(this)
  }
  loadEvents(cursor) {
    this.props.prismic
      .load({
        variables: { cursor: cursor },
        query, // (optional)
        fragments: [], // (optional)
      })
      .then(res => {
        console.log("LOADED", res.data)
        this.setState({
          events: this.state.events.concat(res.data.allSpecial_events.edges),
        })
        if (res.data.allSpecial_events.pageInfo.hasNextPage) {
          this.loadEvents(res.data.allSpecial_events.pageInfo.endCursor)
        }
      })
      .catch(function(err) {
        // This will fix your error since you are now handling the error thrown by your first catch block
        console.log(err.message)
      })
  }
  componentDidMount() {
    let {
      hasNextPage,
      endCursor,
    } = this.props.data.prismic.allSpecial_events.pageInfo
    if (hasNextPage) {
      this.loadEvents(endCursor)
    }
  }
  handleClickShowMore(data) {
    this.setState({ max: this.state.max + multiplier })
  }
  render() {
    console.log("data -->", this.props.data)

    const doc = this.props.data.prismic.allSpecial_events_landing_pages.edges
      .slice(0, 1)
      .pop()
    console.log("doc -->", doc)
    if (!doc) return null

    const {
      image,
      title,
      meta_title,
      meta_description,
      header_summary,
    } = doc.node

    let { max } = this.state

    let events = this.state.events.filter(
      d => !d.node._meta.tags.includes("test")
    )
    console.log("events", events)

    // BUILD EVENT TIMES
    let eventTimes = []
    let rightNow = moment()

    // In case there is no end time, fall off by start time
    events.forEach(event => {
      if (event.node.activity_time) {
        event.node.activity_time.forEach(time => {
          if (time.start_time) {
            let trueEndTime =
              time.end_time != "undefined" ? time.end_time : time.start_time
            if (moment(trueEndTime).isSameOrAfter(rightNow)) {
              var firstDate = moment(trueEndTime)
              var secondDate = moment()
              var yearDiff = firstDate.diff(secondDate, "year")
              var monthDiff = firstDate.diff(secondDate, "month")
              var dayDiff = firstDate.diff(secondDate, "day")
              var hourDiff = firstDate.diff(secondDate, "hour")
              var minuteDiff = firstDate.diff(secondDate, "minute")
              console.log(
                `Event: ${renderAsText(
                  event.node.title
                )} ends at: ${trueEndTime}`
              )
              console.log(
                `Event: ${renderAsText(
                  event.node.title
                )} ends in, ${yearDiff}y, ${monthDiff}m, ${dayDiff}d, ${hourDiff %
                  24}h, ${minuteDiff % 60}m`
              )
              eventTimes.push(
                Object.assign({}, time, {
                  title: event.node.title,
                  spot_images: event.node.spot_images,
                  uid: event.node._meta.uid,
                  isBigEvent: event.node._meta.tags.includes("Big Event"),
                })
              )
            }
          }
        })
      }
    })
    console.log("eventTimes", eventTimes)

    let eventsByMonths = groupBy(eventTimes, d => {
      let month = moment(d.start_time).month()
      let year = moment(d.start_time).year()
      console.log(d.start_time, year, month)
      return moment([year, month]).format()
    })
    console.log("eventsByMonths -->", eventsByMonths)

    let months = sortBy(Object.keys(eventsByMonths))
    months.forEach(function(d) {
      console.log("KEY", d)
    })

    let showMore = null
    if (max < Object.keys(eventsByMonths).length) {
      showMore = (
        <div className={styles.showMoreContainer}>
          <Button
            text="Show more"
            color="Zoo Green"
            onClick={this.handleClickShowMore}
          />
        </div>
      )
    }

    return (
      <Layout title={title} section="activities" {...this.props}>
        <SEO
          title={meta_title}
          canonical={this.props.location.origin + this.props.location.pathname}
          description={meta_description}
        />
        <Hero image={image} pageId={doc.node._meta.id} />
        <TitleBar title={title} description={header_summary} />
        {months.slice(0, max).map((key, i) => {
          console.log("* KEY", key)
          return (
            <section className={styles.section} key={i}>
              <div className="container">
                <h2 className={styles.sectionTitle}>
                  {moment(key).format("MMMM YYYY")}
                </h2>
                <div className="row">
                  {sortBy(eventsByMonths[key], d => d.start_time).map(
                    (d, j) => {
                      let endDate = ""
                      if (d.number_of_days && d.number_of_days > 1) {
                        endDate = moment(d.start_time)
                          .add(d.number_of_days - 1, "days")
                          .format()
                      }
                      let url
                      if (d.uid === "jazzoo") {
                        url = "/jazzoo"
                      } else if (d.isBigEvent) {
                        url = `/${d.uid}`
                      } else {
                        url = `/event/${d.uid}`
                      }
                      return (
                        <div className="col-md-3" key={j}>
                          <SpecialEvent
                            title={RichText.asText(d.title)}
                            image={d.spot_images}
                            startDate={d.start_time}
                            endDate={endDate}
                            url={url}
                          />
                        </div>
                      )
                    }
                  )}
                </div>
              </div>
            </section>
          )
        })}
        {showMore}
      </Layout>
    )
  }
}

export default SpecialEvents

export const query = graphql`
  query SpecialEventsQuery($cursor: String) {
    prismic {
      allSpecial_events_landing_pages {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            image
            title
            header_summary
            meta_description
            meta_title
          }
        }
      }
      allSpecial_events(after: $cursor) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            title
            spot_images
            activity_time {
              start_time
              end_time
              number_of_days
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
      }
    }
  }
`
