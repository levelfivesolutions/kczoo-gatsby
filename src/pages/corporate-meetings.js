import React from "react";
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import styles from './corporate-meetings.module.css';
import { getItems } from '../galaxy'
import { filterArrayByType, renderAsText, getPrice } from '../utility'
import CorporateMeetingRoom from "../components/corporate-meeting/corporate-meeting-room";
import AddOnBlock from "../components/add-on/add-on-block";

class CorporateMeetingLanding extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
			items: []
    }
  }

  componentDidMount() {
		getItems( (res) => {
			this.setState( { items: res } )
		});
	}

  render() {
    const doc = this.props.data.prismic.allCorporate_meetings_landing_pages.edges.slice(0,1).pop();
    if (!doc) return null;
    const { wide_image, title, subhead_description, meta_title, meta_description, cta_link, callout_text, body, button_label } = doc.node;
  
    var rooms = [];
    rooms = this.props.data.prismic.allCorporate_meeting_room_sections.edges.map(d => { return (d.node) });
    
    return (
      <Layout title={ title } section="rent" { ...this.props }>
        <SEO title={ meta_title } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
        <Hero image={ wide_image } pageId={doc.node._meta.id}/>
        <TitleBar 
        	title={ title } 
        	description={ subhead_description } 
        	calloutTitle={ callout_text } 
        	link={ cta_link } 
        	linkText={ button_label } 
        />
        {
          rooms.map((d, i) => {
            console.log("GADSFADSFADSF ", d.plu_price);
            return (
              <section key={ i } className={styles.section}>
                <div className="container">
                  <div className="row">
                    <div className="col-md-12">
                      <CorporateMeetingRoom
                        color={ d.body && filterArrayByType( d.body , "color_picker")[0].primary.color_name }
                        image={ d.wide_image }
                        title={ d.meeting_room_name }
                        description={d.body_text}
                        price={ getPrice( this.state.items, d.plu_price) }
                        quantifier="Rental Price"
                        id={ d._meta.id }
                        btnText="Book Your Meeting"
                        url={ `/form/memorable-meeting-request-form?preferredLocation=${ renderAsText(d.meeting_room_name).replace(/ /g,'-') }` }
                      />
                    </div>
                  </div>
                </div>
              </section>
            );
          })
        }
        <section className={ styles.section }>
          <AddOnBlock dark body={ body } />
        </section>

      </Layout>
    );
  }
}

export default CorporateMeetingLanding;

export const query = graphql`
{
  prismic {
    allCorporate_meeting_room_sections {
      edges {
        node {
          _meta{
            id
          }
          body {
            ... on PRISMIC_Corporate_meeting_room_sectionBodyColor_picker {
              type
              primary {
                color_name
              }
            }
            ... on PRISMIC_Corporate_meeting_room_sectionBodyBackground_images {
              type
              primary {
                select_background_image
              }
            }
          }
          wide_image
          meeting_room_name
          body_text
          plu_price
          cta_label
        }
      }
    }
    allCorporate_meetings_landing_pages {
      edges {
        node {
          _meta {
						id
						tags
						type
						uid
          }          
          meta_title
          meta_description
          wide_image
          title
          subhead_description
          callout_text
          button_label
          thumbnail_image1
          summary
          _linkType
          cta_link {
						_linkType
						... on PRISMIC__Document {
							_meta {
								id
								tags
								type
								uid
							}
						}
						... on PRISMIC__ExternalLink {
							_linkType
							url
						}
						... on PRISMIC__FileLink {
							_linkType
							url
						}
          }
          body {
            ... on PRISMIC_Corporate_meetings_landing_pageBodyAlert {
              type
              primary {
                alert_rich_text
                alert_text
                alert_title
                icon_selector
                color
              }
            }
            ... on PRISMIC_Corporate_meetings_landing_pageBodySimple_callout {
              type
              fields {
                button_label
                callout_title
                icon_selector
              }
            }
            ... on PRISMIC_Corporate_meetings_landing_pageBodyAddOns1 {
              type
              primary {
                subheading
                title1
              }
              fields {
                addOn_text
                description
                addOn_title
                icon_selector
              }
            }
          }
        }
      }
    }
  }
}
`

