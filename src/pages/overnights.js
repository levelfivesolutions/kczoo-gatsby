import React from "react";
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import styles from './day-camps-landing.module.css';
import CampExperience from '../components/camp-experience/camp-experience';
import { RichText } from 'prismic-reactjs';
import { filterArrayByType, linkResolver } from '../utility';
import { getItems } from '../galaxy';
import SectionBlock from "../components/section-block";
import AddOnBlock from "../components/add-on/add-on-block"

class DayCampLanding extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
    }
  }

  componentDidMount() {
		getItems( (res) => {
			this.setState( { items: res } )
		});
	} 
  
  render() {
		const doc = this.props.data.prismic.allOvernights_landing_pages.edges.slice(0,1).pop();
    if (!doc) return null;
    const { wide_image, title, subhead_description, body, meta_title, meta_description, _meta } = doc.node;
  
    var overnightCamps = filterArrayByType(body, "detailed_callouts")[0];

    return (
      <Layout title={ title } section="camps" { ...this.props }>
        <SEO title={ meta_title } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
        <Hero image={ wide_image } pageId={ _meta.id }/>
        <TitleBar 
        	title={ title } 
        	description={ subhead_description } 
        />
        {
          overnightCamps.fields.map((d, i) => {
            return (
              <section key={ i } className={styles.section}>
                <div className="container">
                  <div className="row">
                    <div className="col-md-12">
                      <CampExperience
                        color={ d.color_name }
                        image={ d.callout_image }
                        title={d.callout_title}
                        richDescription={ d.feature_block_rich_text}
                        description={ d.feature_block_key_text }
                        btnText={ d.cta_text }
                        url={linkResolver(d.cta)}                      />
                    </div>
                  </div>
                </div>
              </section>

            );
          })
        }

        <SectionBlock >
          <div className="container">
            <div className="row">
              <div className="col-12">
                <AddOnBlock body={ body } />
              </div>
            </div>
          </div>
        </SectionBlock>

      </Layout>
    );
  }
}

export default DayCampLanding

export const query = graphql`
{
    prismic {
      allOvernights_landing_pages {
        edges {
          node {
            meta_title
            meta_description
            wide_image
            title
            subhead_description
            _meta {
              id
            }            
            body {
              ... on PRISMIC_Overnights_landing_pageBodyDetailed_callouts {
                type
                label
                fields {
                  callout_image
                  callout_title
                  feature_block_key_text
                  feature_block_rich_text
                  cta_text
                  color_name
                  cta {
                    ... on PRISMIC_Detail {
                      _meta {
                        tags
                        id
                        uid
                        type
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
