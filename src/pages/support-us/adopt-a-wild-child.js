import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
//import { Link } from "gatsby";
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import TitleBar from "../../components/titlebar-landing/titlebar-landing.js"
import GiftCallOut from "../../components/gift-call-out/gift-call-out.js"
import Hero from "../../components/hero-landing/hero-landing"
import HeroAlt from "../../components/hero-detail"
import SlicerR from "../../components/slice-r/slice-r"
import styles from "../memberships-landing.module.css"
import Button from "../../components/button/button-link"
import {
  ColorToHex,
  filterArrayByType,
  filterArrayByTypeName,
  renderAsText,
  renderAsHtml,
  renderCheckmark,
  getPrice,
  renderLinkUrl,
  linkResolver,
} from "../../utility"
import { getItems, getOnlineItems, getJacksonClayItems } from "../../galaxy"
import { Link } from "@reach/router"
import { Modal } from "react-bootstrap"

const MembershipsLanding = props => {
  const { data, location } = props

  const doc = data.prismic.allAdopt_a_wild_childs.edges.slice(0, 1).pop()

  console.log("doc -->", doc)

  if (!doc) return null
  const {
    iap_row_10_heading,
    iap_row_11_heading,
    iap_row_1_heading,
    iap_row_2_heading,
    iap_row_3_heading,
    iap_row_4_heading,
    iap_row_5_heading,
    iap_row_6_heading,
    iap_row_7_heading,
    iap_row_8_heading,
    iap_row_9_heading,
    iap_section_description,
    iap_section_title,
    individual_adoption_packages,
    title,
    wide_image,
    body_text,
    optional_image,
    meta_description,
    meta_title,
    feature_background_color,
    feature_body_text,
    feature_cta_label,
    feature_image,
    feature_price,
    feature_price_detail,
    feature_cta_link,
    feature_header_intro,
    feature_header,
    feature_header_outro,
    body,
    cso_row_1_heading,
    cso_row_2_heading,
    cso_row_3_heading,
    cso_row_4_heading,
    cso_row_5_heading,
    cso_row_6_heading,
    cso_section_description,
    cso_section_title,
    community_and_social_adoption_packages,
  } = doc.node

  let backgroundStyle = {
    backgroundImage: 'url(/images/backgrounds/background-leaf.svg)',
    backgroundPosition: 'center top',
    backgroundRepeat: 'no-repeat',
    padding: '70px 0'
  };

  let featureBackgroundStyle = {
    backgroundColor: ColorToHex(feature_background_color),
    paddingTop: '70px',
    paddingBottom: '70px',
    color: '#fff'
  }

  const rowLabels = [
    iap_row_1_heading,
    iap_row_2_heading,
    iap_row_3_heading,
    iap_row_4_heading,
    iap_row_5_heading,
    iap_row_6_heading,
    iap_row_7_heading,
    iap_row_8_heading,
    iap_row_9_heading,
    iap_row_10_heading,
    iap_row_11_heading,
  ]

  const csoRowLabels = [
    cso_row_1_heading,
    cso_row_2_heading,
    cso_row_3_heading,
    cso_row_4_heading,
    cso_row_5_heading,
    cso_row_6_heading
  ]

  return (
    <Layout title={title} section="support-us" {...props}>
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
       <HeroAlt image={wide_image} pageId={doc.node._meta.id} />

      {/* <TitleBar
        title={title}
        description={description}
        calloutTitle={callout_heading}
        calloutDescription={callout_description}
      /> */}

      <div className="row no-gutters" style={backgroundStyle}>
        <div className="container">
          <div className="row">
            {title &&
              <div className="offset-lg-1 col-lg-10">
                <h1 className={styles.adoptTitle}>{renderAsText(title)}</h1>
              </div>
            }
            {body_text &&
              <div className="offset-lg-1 col-lg-7">
                {renderAsHtml(body_text)}
              </div>
            }
            <div className="offset-lg-0 col-lg-3 offset-md-3 col-md-6 offset-sm-2 col-sm-8">
              <div>
                {optional_image && (
                  <div>
                    <img src={optional_image.url} alt={optional_image.alt} className="rounded mb-2 w-100" />
                    {/* <p className="text-center mb-5">{image_label}</p> */}
                  </div>
                )}
              </div>
            </div>
          </div>
          {body &&
            <div className="row">
              <div className="offset-lg-1 col-lg-10 mt-4">
                {body.map(slice => {
                  return <SlicerR data={slice} />
                })}
              </div>
            </div>
          }
        </div>
      </div>
      {feature_header &&
        <div className="row no-gutters" style={featureBackgroundStyle}>
          <div className="container">
            <div className="row">
              <div className="col-lg-5 text-center mb-5">
                {feature_image &&
                  <img src={feature_image.url} alt={feature_image.alt} />
                }
                <div className={styles.adoptFeaturePriceWrapper}>
                  {feature_price &&
                    <div className={styles.adoptFeaturePrice}>
                      {feature_price}
                    </div>
                  }
                  {feature_price_detail &&
                    <div className={styles.adoptFeaturePriceDetail}>
                      {feature_price_detail}
                    </div>
                  }
                </div>
                {feature_cta_link && 
                  <div className="text-center">
                    <Button
                      text={feature_cta_label}
                      link={feature_cta_link}
                      color={feature_background_color}
                      isInverse={true}
                    />
                  </div>
                }
              </div>
              <div className="col-lg-7">
                <div className="text-center mb-5">
                  {feature_header_intro && 
                    <div className={styles.adoptFeatureHeaderIntro}>
                      {feature_header_intro}
                    </div>
                  }
                  {feature_header && 
                    <h1 className={styles.adoptFeatureHeader}>
                      {feature_header}
                    </h1>
                  }
                  {feature_header_outro && 
                    <div className={styles.adoptFeatureHeaderOutro}>
                      {feature_header_outro}
                    </div>
                  }
                </div>
                {feature_body_text && 
                  <div className={styles.adoptFeatureBody}>
                    {renderAsHtml(feature_body_text)}
                  </div>
                }
              </div>
            </div>
          </div>
        </div>
      }
      <section className={styles.section}>
        {/*  = = = = = = = = = = = = = = = == = = = = = = = = = = = == = = = = = = = == = = = = = = = = == = = = == = 
               ********************************************* START DESKTOP VIEW **********************************************
               = = = = = = =  = ==  = == = = == = = = = =  = = = == = =  = == = = = == = ==  = = =  = == =  = = =  = = =  =*/}
        <div className="d-md-none d-none d-sm-none d-lg-block container">
          <div className="row">
            {/* 
              The top-left most cell with Adoption Package details
            */}
            <div className="col">
              <div>
                <h2 className={styles.ticketComparisonSectionTitle}>
                  {renderAsText(iap_section_title)}
                </h2>
                <p className={styles.ticketComparisonSectionDescription}>
                  {renderAsText(iap_section_description)}
                </p>
              </div>
            </div>

            {/* Loops through the columns just to make the headers */}
            {Object.keys(individual_adoption_packages).map(d => {
              let adoptionPackage = individual_adoption_packages[d]
              return (
                <div
                  className={`col ${styles.membershipDetailHeaderContainer}`}
                  key={Math.random(0, 9999999)}
                >
                  <div
                    className={styles.membershipDetailHeader}
                    style={{
                      backgroundColor: ColorToHex(
                        adoptionPackage.iap_color || "Tangerine"
                      ),
                    }}
                  >
                    <div
                      className={styles.membershipDetailHeaderHeading}
                      style={{ padding: 0, height: "16px" }}
                    ></div>
                    <div className={styles.membershipDetailHeaderBody}>
                      <p
                        className={styles.ticketPrices}
                        style={{ marginBottom: "25px" }}
                      >
                        <sup style={{ fontSize: "28px" }}>$</sup>
                        {adoptionPackage.iap_price}
                      </p>
                      <Button
                        link={adoptionPackage.iap_link}
                        isInverse
                        color={adoptionPackage.iap_color}
                        text={renderAsText(adoptionPackage.iap_link_text)}
                      />
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
          {/* End loop to create header row */}

          {/* Start Loop to create table rows */}
          {rowLabels.map((rowLabel, i) => {
            let rowNum = i + 1
            return (
              // Create the row
              <div
                className={` row ${styles.comparisonRow}`}
                key={Math.random(0, 9999999)}
              >
                {/* Create the row heading */}
                <div
                  className={`col d-flex ${styles.comparisonRowTextContainer}`}
                  style={{ padding: "15px 15px 15px", }}
                >
                  {renderAsText(rowLabel)}
                </div>
                {/* Loop to create a column for each package in the row */}
                {Object.keys(individual_adoption_packages).map(d => {
                  let adoptionPackage = individual_adoption_packages[d]
                  return (
                    <div className={`col d-flex ${styles.comparisonCol}`}>
                      <div
                        className="d-flex flex-grow-1"
                        style={{
                          backgroundColor: ColorToHex(
                            adoptionPackage.iap_color
                          ),
                          borderBottomRightRadius: i === rowLabels.length -1 ? "5px" : "",
                          borderBottomLeftRadius: i === rowLabels.length -1 ? "5px" : ""
                        }}
                      >
                        <div className={styles.comparisonItemSM}>
                          {adoptionPackage["iap_row_" + rowNum] === true ||
                          adoptionPackage["iap_row_" + rowNum] === false
                            ? renderCheckmark(
                                adoptionPackage["iap_row_" + rowNum]
                              )
                            : renderAsText(
                                adoptionPackage["iap_row_" + rowNum]
                              )}
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            )
          })}

          {/* End loop to create table rows */}
        </div>
        {/* * * * * * * * * *
                          End Desktop View
                        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */}

        {/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
                        START MOBILE VIEW
                      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */}

        {/* The mobile section description */}
        <div className="col-12 mb-3 d-lg-none d-xl-none">
          <div>
            <h2 className={styles.ticketComparisonSectionTitle}>
              {renderAsText(iap_section_title)}
            </h2>
            <p className={styles.ticketComparisonSectionDescription}>
              {renderAsHtml(iap_section_description)}
            </p>
          </div>
        </div>

        {/* Loop to create each individual card */}
        {Object.keys(individual_adoption_packages).map(d => {
          let adoptionPackage = individual_adoption_packages[d]

          // return the card
          return (
            // The card wrapper
            <div class="col-12 d-lg-none d-xl-none" style={{ marginBottom: '50px'}}> 

              {/* The header */}
              <div
                className={`${styles.membershipDetailHeaderContainer}`}
                key={Math.random(0, 9999999)}
              >
                <div
                  className={styles.membershipDetailHeader}
                  style={{
                    backgroundColor: ColorToHex(
                      adoptionPackage.iap_color || "Tangerine"
                    ),
                  }}
                >
                  <div
                    className={styles.membershipDetailHeaderHeading}
                    style={{ padding: 0, height: "16px" }}
                  ></div>
                  <div className={styles.membershipDetailHeaderBody}
                   style={{ marginBottom: "25px" }}>
                    <p
                      className={styles.ticketPrices}
                      style={{ marginBottom: "25px" }}
                    >
                      <sup style={{ fontSize: "28px" }}>$</sup>
                      {adoptionPackage.iap_price}
                    </p>
                    <Button
                      link={adoptionPackage.iap_link}
                      isInverse
                      color={adoptionPackage.iap_color}
                      text={renderAsText(adoptionPackage.iap_link_text)}
                    />
                  </div>
                </div>
              </div> 
              {/* end header, begin rows */}


              {rowLabels.map((rowLabel, i) => {
                let rowNum = i + 1

                if ( !adoptionPackage["iap_row_" + rowNum] &&
                renderAsText(adoptionPackage["iap_row_" + rowNum]) == "" ||
                adoptionPackage["iap_row_" + rowNum] == false) return null;

                // return the row to the card
                return (
                  <div
                    className={`m-0 row ${styles.comparisonRow}`}
                    style={{
                      backgroundColor: ColorToHex(adoptionPackage.iap_color),
                    }}
                  >
                    <div className={`col-12 ${styles.comparisonCol} p-0`}>
                      <div className="d-flex flex-grow-1">
                        <div
                          className={` ${styles.comparisonItemSM} justify-content-between`}
                        >
                          <p className={`m-0 ${styles.mobileComparisonLabel}`}>
                            {renderAsText(rowLabel)}
                          </p>
                          <p className="m-0 text-right" style={{minWidth: "25px"}}>
                            {adoptionPackage["iap_row_" + rowNum] === true ||
                            adoptionPackage["iap_row_" + rowNum] === false
                              ? renderCheckmark(
                                  adoptionPackage["iap_row_" + rowNum]
                                )
                              : renderAsText(
                                  adoptionPackage["iap_row_" + rowNum]
                                )}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          )
        })}
      </section>

      {/* 
      ===============================================================================================
      * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
                                                CSO SECTION
      * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
      ===============================================================================================
       */}
       <section className={styles.section}>
        {/*  = = = = = = = = = = = = = = = == = = = = = = = = = = = == = = = = = = = == = = = = = = = = == = = = == = 
               ********************************************* START DESKTOP VIEW **********************************************
               = = = = = = =  = ==  = == = = == = = = = =  = = = == = =  = == = = = == = ==  = = =  = == =  = = =  = = =  =*/}
        <div className="d-md-none d-none d-sm-none d-lg-block container">
          <div className="row">
            {/* 
              The top-left most cell with Adoption Package details
            */}
            <div className="col">
              <div>
                <h2 className={styles.ticketComparisonSectionTitle}>
                  {renderAsText(cso_section_title)}
                </h2>
                <p className={styles.ticketComparisonSectionDescription}>
                  {renderAsText(cso_section_description)}
                </p>
              </div>
            </div>

            {/* Loops through the columns just to make the headers */}
            {Object.keys(community_and_social_adoption_packages).map(d => {
              let adoptionPackage = community_and_social_adoption_packages[d]
              return (
                <div
                  className={`col ${styles.membershipDetailHeaderContainer}`}
                  key={Math.random(0, 9999999)}
                >
                  <div
                    className={styles.membershipDetailHeader}
                    style={{
                      backgroundColor: ColorToHex(
                        adoptionPackage.cso_color || "Tangerine"
                      ),
                    }}
                  >
                    <div
                      className={styles.membershipDetailHeaderHeading}
                      style={{ padding: 0, height: "16px" }}
                    ></div>
                    <div className={styles.membershipDetailHeaderBody}>
                      <p
                        className={styles.ticketPrices}
                        style={{ marginBottom: "25px" }}
                      >
                        <sup style={{ fontSize: "28px" }}>$</sup>
                        {adoptionPackage.cso_price}
                      </p>
                      <Button
                        link={adoptionPackage.cso_link}
                        isInverse
                        color={adoptionPackage.cso_color}
                        text={renderAsText(adoptionPackage.cso_link_text)}
                      />
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
          {/* End loop to create header row */}

          {/* Start Loop to create table rows */}
          {csoRowLabels.map((rowLabel, i) => {
            let rowNum = i + 1
            return (
              // Create the row
              <div
                className={` row ${styles.comparisonRow}`}
                key={Math.random(0, 9999999)}
              >
                {/* Create the row heading */}
                <div
                  className={`col d-flex ${styles.comparisonRowTextContainer}`}
                  style={{ padding: "15px 15px 15px", }}
                >
                  {renderAsText(rowLabel)}
                </div>
                {/* Loop to create a column for each package in the row */}
                {Object.keys(community_and_social_adoption_packages).map(d => {
                  let adoptionPackage = community_and_social_adoption_packages[d]
                  return (
                    <div className={`col d-flex ${styles.comparisonCol}`}>
                      <div
                        className="d-flex flex-grow-1"
                        style={{
                          backgroundColor: ColorToHex(
                            adoptionPackage.cso_color
                          ),
                        }}
                      >
                        <div className={styles.comparisonItemSM}>
                          {adoptionPackage["cso_row_" + rowNum] === true ||
                          adoptionPackage["cso_row_" + rowNum] === false
                            ? renderCheckmark(
                                adoptionPackage["cso_row_" + rowNum]
                              )
                            : renderAsText(
                                adoptionPackage["cso_row_" + rowNum]
                              )}
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            )
          })}

          {/* End loop to create table rows */}
        </div>
        {/* * * * * * * * * *
                          End Desktop View
                        * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */}

        {/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
                        START MOBILE VIEW
                      * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */}

        {/* The mobile section description */}
        <div className="col-12 mb-3 d-lg-none d-xl-none">
          <div>
            <h2 className={styles.ticketComparisonSectionTitle}>
              {renderAsText(cso_section_title)}
            </h2>
            <p className={styles.ticketComparisonSectionDescription}>
              {renderAsHtml(cso_section_description)}
            </p>
          </div>
        </div>

        {/* Loop to create each individual card */}
        {Object.keys(community_and_social_adoption_packages).map(d => {
          let adoptionPackage = community_and_social_adoption_packages[d]

          // return the card
          return (
            // The card wrapper
            <div class="col-12 d-lg-none d-xl-none" style={{ marginBottom: '50px'}}> 

              {/* The header */}
              <div
                className={`${styles.membershipDetailHeaderContainer}`}
                key={Math.random(0, 9999999)}
              >
                <div
                  className={styles.membershipDetailHeader}
                  style={{
                    backgroundColor: ColorToHex(
                      adoptionPackage.cso_color || "Tangerine"
                    ),
                  }}
                >
                  <div
                    className={styles.membershipDetailHeaderHeading}
                    style={{ padding: 0, height: "16px" }}
                  ></div>
                  <div className={styles.membershipDetailHeaderBody}
                   style={{ marginBottom: "25px" }}>
                    <p
                      className={styles.ticketPrices}
                      style={{ marginBottom: "25px" }}
                    >
                      <sup style={{ fontSize: "28px" }}>$</sup>
                      {adoptionPackage.cso_price}
                    </p>
                    <Button
                        link={adoptionPackage.cso_link}
                        isInverse
                        color={adoptionPackage.cso_color}
                        text={renderAsText(adoptionPackage.cso_link_text)}
                      />
                  </div>
                </div>
              </div> 
              {/* end header, begin rows */}


              {csoRowLabels.map((rowLabel, i) => {
                let rowNum = i + 1

                // Return nothing if the row is empty on mobile
                if ( !adoptionPackage["cso_row_" + rowNum] &&
                renderAsText(adoptionPackage["cso_row_" + rowNum]) == "" ||
                adoptionPackage["cso_row_" + rowNum] == false) return null;

                // return the row to the card
                return (
                  <div
                    className={`m-0 row ${styles.comparisonRow}`}
                    style={{
                      backgroundColor: ColorToHex(adoptionPackage.cso_color),
                      borderBottomRightRadius: rowNum === csoRowLabels.length ? "5px" : "",
                      borderBottomLeftRadius: rowNum === csoRowLabels.length ? "5px" : ""
                    }}
                  >
                    <div className={`col-12 ${styles.comparisonCol} p-0`}>
                      <div className="d-flex flex-grow-1">
                        <div
                          className={` ${styles.comparisonItemSM} justify-content-between`}
                        >
                          <p className={`m-0 ${styles.mobileComparisonLabel}`}>
                            {renderAsText(rowLabel)}
                          </p>
                            <p className="m-0 text-right" style={{minWidth: "25px"}}>
                            {adoptionPackage["cso_row_" + rowNum] === true ||
                            adoptionPackage["cso_row_" + rowNum] === false
                              ? renderCheckmark(
                                  adoptionPackage["cso_row_" + rowNum]
                                )
                              : renderAsText(
                                  adoptionPackage["cso_row_" + rowNum]
                                )}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                )
              })}
            </div>
          )
        })}
      </section>
    </Layout>
  )
}

export default MembershipsLanding

export const query = graphql`
{
  prismic {
    allAdopt_a_wild_childs {
      edges {
        node {
          body_text
          meta_description
          meta_title
          optional_image
          wide_image
          title
          feature_body_text
          feature_cta_label
          feature_image
          feature_price
          feature_price_detail
          feature_cta_link {
            _linkType
            ... on PRISMIC__ExternalLink {
              target
              _linkType
              url
            }
            ... on PRISMIC__FileLink {
              _linkType
              name
              url
            }
            ... on PRISMIC__ImageLink {
              _linkType
              url
              name
            }
            ... on PRISMIC_Form {
              _linkType
              _meta {
                uid
                id
                tags
                type
              }
            }
          }
          feature_background_color
          body {
            ... on PRISMIC_Adopt_a_wild_childBodyCta {
              type
              label
              fields {
                color
                cta_label
                cta_link {
                  _linkType
                  ... on PRISMIC__ExternalLink {
                    target
                    _linkType
                    url
                  }
                  ... on PRISMIC__FileLink {
                    _linkType
                    name
                    url
                  }
                  ... on PRISMIC__ImageLink {
                    _linkType
                    name
                    url
                  }
                  ... on PRISMIC_Form {
                    _linkType
                    _meta {
                      uid
                      id
                    }
                  }
                  ... on PRISMIC_Landing_page_hero_block {
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                }
                cta_text
                cta_title
                icon
              }
            }
          }
          feature_header
          feature_header_intro
          feature_header_outro
          iap_row_10_heading
          iap_row_11_heading
          _linkType
          _meta {
            firstPublicationDate
            uid
            tags
            type
          }
          iap_row_1_heading
          iap_row_2_heading
          iap_row_3_heading
          iap_row_4_heading
          iap_row_5_heading
          iap_row_6_heading
          iap_row_7_heading
          iap_row_8_heading
          iap_row_9_heading
          iap_section_description
          iap_section_title
          individual_adoption_packages {
            iap_color
            iap_link_text
            iap_price
            iap_row_1
            iap_row_11
            iap_row_10
            iap_row_2
            iap_row_3
            iap_row_4
            iap_row_5
            iap_row_6
            iap_row_7
            iap_row_8
            iap_row_9
            iap_link {
              ... on PRISMIC__ImageLink {
                _linkType
                url
              }
              ... on PRISMIC__FileLink {
                _linkType
                url
              }
              ... on PRISMIC__ExternalLink {
                target
                _linkType
                url
              }
              ... on PRISMIC_Detail {
                _linkType
                _meta {
                  id
                  uid
                  tags
                  type
                }
              }
              ... on PRISMIC_Form {
                _linkType
                _meta {
                  uid
                  id
                  tags
                  type
                }
              }
              _linkType
            }
          }
          cso_row_1_heading
          cso_row_2_heading
          cso_row_3_heading
          cso_row_4_heading
          cso_row_5_heading
          cso_row_6_heading
          cso_section_description
          cso_section_title
          community_and_social_adoption_packages {
            cso_color
            cso_link_text
            cso_price
            cso_row_1
            cso_row_2
            cso_row_3
            cso_row_4
            cso_row_5
            cso_row_6
            cso_link {
              ... on PRISMIC__ImageLink {
                _linkType
                url
              }
              ... on PRISMIC__FileLink {
                _linkType
                url
              }
              ... on PRISMIC__ExternalLink {
                target
                _linkType
                url
              }
              ... on PRISMIC_Detail {
                _linkType
                _meta {
                  id
                  uid
                  tags
                  type
                }
              }
              ... on PRISMIC_Form {
                _linkType
                _meta {
                  uid
                  id
                  tags
                  type
                }
              }
              _linkType
            }
          }
        }
      }
    }
  }
}

`
