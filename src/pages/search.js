import React from 'react';
import { graphql, Link } from 'gatsby';
//import { RichText, renderImageUrl } from 'prismic-reactjs';
import { linkResolver } from '../utility.js';
//import { sortedUniq } from 'lodash';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import Title from "../components/title";
import Hero from '../components/hero-detail';
import BodyText from "../components/body-text";
import Button from '../components/button/button-link.js';
import AnimalModal from '../components/animal-modal';
import styles from './search.module.css';

const filterDefault = { label: 'Filter Results', value: '' };
const animalDefault = {
	animal_name: '', 
	animal_description: '',
	tall_images: null,
	wide_images: null
};

/*
class MenuItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		};

    this.handleClick = this.handleClick.bind(this);
  }
	handleClick(ev) {
		ev.preventDefault();
    this.props.onChange({ label: this.props.label, value: this.props.value });
  }	
  render() {
		return (
			<a className={`dropdown-item ${ styles.dropdownItem }`} href="#" onClick={ this.handleClick }>
				{ filterDefault.label === this.props.label ? 'None' : this.props.label }
			</a>
		)
	}
}
*/

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			animal: animalDefault,
			filter: filterDefault,
			searchResults: [],
			searchResultsRaw: [],
			pageInfo: null,
			page: 0,
			tags: []
		};

		//this.handleChangeFilter = this.handleChangeFilter.bind(this);
		this.handleClickAnimalLearnMore = this.handleClickAnimalLearnMore.bind(this);
  }
	
  componentDidMount() {
		window.$('#animalModal').on('hidden.bs.modal', () => {
			this.setState({ animal: animalDefault });
		});
		
		if (this.props.location.state && this.props.location.state.searchValue !== '') {
			this.props.prismic.load({
				variables: { search: this.props.location.state.searchValue },
				query, // (optional)
				fragments: [], // (optional)
			})
			.then(res => { 
				console.log('LOADED', res.data);
				
				// REMOVE CONSERVATION PARTNERSHIPS -- EXTERNAL LINKS
				let conservationProjects = [];
				if (res.data.allConservation_projects.edges.length > 0) {
					conservationProjects = res.data.allConservation_projects.edges.filter(d => !d.node._meta.tags.includes('conservation partnerships'));
				}
				
				let results = [
					...res.data.allHours_location_parking_landing_pages.edges, 
					...res.data.allTickets_and_pricing_landing_pages.edges,
					...res.data.allSpecial_events_landing_pages.edges,
					...res.data.allSpecial_events.edges, 
					...res.data.allSpecial_experiences.edges, 
					...res.data.allSpecial_exhibits.edges,
					...res.data.allActivitys.edges, 
					...res.data.allAnimal_cam_details.edges, 
					...res.data.allAnimal_details.edges, 
					...res.data.allCamps.edges, 
					...conservationProjects, 
					...res.data.allCorporate_meetings_landing_pages.edges, 
					...res.data.allDetails.edges, 
					...res.data.allDetail_with_plus.edges, 
					...res.data.allLanding_page_hero_blocks.edges, 
					...res.data.allMap_landing_pages.edges,  
					...res.data.allMembership_landing_pages.edges, 
					...res.data.allOvernights_landing_pages.edges, 
					...res.data.allPurrfect_picnics_landing_pages.edges,
					...res.data.allScout_details.edges, 
					...res.data.allWeddings_landing_pages.edges
				];
				
				this.setState({
					//tags: sortedUniq(tags),
					searchResultsRaw: res.data,
					searchResults: results
				}); 
			});			
		}
	}
	
	handleClickAnimalLearnMore(d) {
		this.setState({ animal: d });
		window.$('#animalModal').modal('show');
	}
	/*
  handleChangeFilter(d) {
    this.props.prismic.load({
      variables: { search: this.props.location.state.searchValue, tags: [d.value] },
      query, // (optional)
      fragments: [], // (optional)
    })
		.then(res => { 
			console.log('LOADED', res.data);
			
			let results = [
				...res.data.allLanding_page_hero_blocks.edges, 
				...res.data.allActivitys.edges, 
				...res.data.allAnimal_cam_details.edges, 
				...res.data.allAnimal_details.edges, 
				...res.data.allCamps.edges, 
				...res.data.allConservation_projects.edges, 
				...res.data.allCorporate_meetings_landing_pages.edges, 
				...res.data.allDetails.edges, 
				...res.data.allDetail_with_plus.edges, 
				...res.data.allHours_location_parking_landing_pages.edges, 
				...res.data.allMap_landing_pages.edges,  
				...res.data.allMembership_landing_pages.edges, 
				...res.data.allOvernights_landing_pages.edges, 
				...res.data.allPurrfect_picnics_landing_pages.edges,
				...res.data.allScout_details.edges, 
				...res.data.allSpecial_events_landing_pages.edges,
				...res.data.allSpecial_events.edges, 
				...res.data.allSpecial_experiences.edges, 
				...res.data.allSpecial_exhibits.edges,
				...res.data.allTickets_and_pricing_landing_pages.edges,
				...res.data.allWeddings_landing_pages.edges
			];
			this.setState({
				filter: d,
				//tags: sortedUniq(tags),
				searchResultsRaw: res.data,
				searchResults: results,
				pageInfo: res.data.allSpecial_experiences.pageInfo
			}); 
		});
	}
	*/
  render() {
		console.log('props >>>', this.props);
		const { location } = this.props;
		const { searchResults, tags } = this.state;
		console.log('searchResults', searchResults, tags);

		const doc = this.props.data.prismic.allSearchs.edges.slice(0,1).pop();
		console.log('doc -->', doc);
		if (!doc) return null;
		const { meta_description, meta_title, search_hero_image } = doc.node;
		
		return (
			<Layout title="Search" { ...this.props }>
				<SEO title={ meta_title } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
				<Hero image={ search_hero_image } />
				<BodyText>
					<div className="container">
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<Title className="mb-0">Search</Title>
							</div>
						</div>
					</div>
				</BodyText>
				<nav className={`navbar navbar-expand-lg ${ styles.filterBar }`}>
					<div className="container">
						<div><strong>Searched for:</strong> "{ location.state ? location.state.searchValue : '' }" ({ searchResults.length } results)</div>
						{/*
							<ul className="navbar-nav ml-auto">
								<li className="nav-item">
									<ul className="navbar-nav">
										<li className={`nav-item dropdown ${ styles.dropdown }`}>
											<a className={`nav-link dropdown-toggle ${ styles.filterActivityToggle }`} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												{ this.state.filter.label }<span className={ `${ styles.iconChevronDownWhite } ml-3` }></span>
											</a>
											<div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
												<MenuItem key={99999} label={ filterDefault.label } value={ filterDefault.value } onChange={ this.handleChangeFilter } />
												{
													tags.map((d, i) => <MenuItem key={ i } label={ d } value={ d } onChange={ this.handleChangeFilter } />)
												}
											</div>
										</li>
									</ul>
								</li>
							</ul>							
						*/}
					</div>
				</nav>	
				<div className={ styles.contentArea }>
					{
						searchResults &&
						searchResults.map(d => {
							console.log('->', d);
							if (d.node._meta.type === 'animal_detail') {
								return (
									<div className={ styles.searchResult }>
										<div className="container">
											<div className="row">
												<div className={`col-lg-2 col-md-3 ${ styles.searchImageCol }`}>
													{ 
														d.node.wide_images && d.node.wide_images['Animal-Grid-Small'] &&
														<img src={ d.node.wide_images['Animal-Grid-Small'].url } alt="" className="img-fluid" />
													}
												</div>
												<div className={`col-lg-8 col-md-6 ${ styles.searchBodyCol }`}>
													<h2 className={ styles.searchResultTitle } onClick={ ev => this.handleClickAnimalLearnMore(d.node) }>
														{ d.node.animal_name }
													</h2>
													<div>{ d.node.animal_description }</div>
												</div>
												<div className={`col-lg-2 col-md-3 ${ styles.searchLinkCol }`}>
													<Button text="Learn More >" onClick={ ev => this.handleClickAnimalLearnMore(d.node) } />
												</div>
											</div>
										</div>
									</div>
								);
							} else {
								let imageSrc = '';
								if (d.node.spot_image) {
									imageSrc = d.node.spot_image.url;
								} else if (d.node.wide_image) {
									if (d.node.wide_image['Search-Schedule-Thumb']) {
										imageSrc = d.node.wide_image['Search-Schedule-Thumb'].url;
									} else if (d.node.wide_image['searchschedule-thumb']) {
										imageSrc = d.node.wide_image['searchschedule-thumb'].url;
									} else if (d.node.wide_image['Search-Thumb']) {
										imageSrc = d.node.wide_image['Search-Thumb'].url;
									}
								}
								return (
									<div className={ styles.searchResult }>
										<div className="container">
											<div className="row">
												<div className={`col-lg-2 col-md-3 ${ styles.searchImageCol }`}>
													{ 
														imageSrc && <img src={ imageSrc } alt="" className="img-fluid" />
													}
												</div>
												<div className={`col-lg-8 col-md-6 ${ styles.searchBodyCol }`}>
													<h2 className={ styles.searchResultTitle }><Link to={ linkResolver(d.node._meta) }>{ d.node.meta_title }</Link></h2>
													<div>{ d.node.meta_description }</div>
												</div>
												<div className={`col-lg-2 col-md-3 ${ styles.searchLinkCol }`}>
													<Button text="Learn More >" link={ d.node } />
												</div>
											</div>
										</div>
									</div>
								);
							}
						})
					}
				</div>
				{/*
					this.state.pageInfo.hasNextPage &&
					<div className={ styles.showMoreContainer }>
						<Button text="Show More" onClick={ this.handleClickLoadMore } />
					</div>
				*/}
				<AnimalModal animal={ this.state.animal } />
			</Layout>
		)
	}
}

export default Search;

export const query = graphql`
	query SearchQuery($search: String, $tags: [String!]) {
		prismic {
			allSearchs {
				edges {
					node {
						meta_description
						meta_title
						search_hero_image
					}
				}
			}
			allLanding_page_hero_blocks(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						wide_image:wide_header_size
						meta_title
						meta_description
					}
				}
			}
			allActivitys(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
						wide_image:wide_images
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allAnimal_cam_details(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						wide_image
						meta_title
						meta_description
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allAnimal_details(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_meta {
							id
							tags
							type
							uid
						}
						animal_cam {
							... on PRISMIC_Animal_cam_detail {
								_linkType
								_meta {
									id
									tags
									type
									uid
								}
							}
							... on PRISMIC_Detail {
								_linkType
								_meta {
									id
									tags
									type
									uid
								}
							}
						}
						animal_description
						animal_name
						animal_scientific_name
						animal_type
						button_link {
							... on PRISMIC_Detail {
								_linkType
								_meta {
									id
									tags
									type
									uid
								}
								title
								summary:summary1
							}
						}
						location
						meta_description
						meta_title
						tall_images
						wide_images
						map_id
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allCamps(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
						spot_image
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allConservation_projects(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
						spot_image
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allCorporate_meetings_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						wide_image
						meta_title
						meta_description
					}
				}
			}
			allDetail_with_plus(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
						spot_image
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allDetails(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						wide_image
						meta_title
						meta_description
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allHours_location_parking_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						wide_image
						meta_title
						meta_description
					}
				}
			}
			allMap_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
					}
				}
			}
			allMembership_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
					}
				}
			}
			allOvernights_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						wide_image
						meta_title
						meta_description
					}
				}
			}
			allPurrfect_picnics_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						wide_image:wide_header_size
						meta_title
						meta_description
					}
				}
			}
			allScout_details(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
						spot_image
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allSpecial_events_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
					}
				}
			}
			allSpecial_events(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
						spot_image:spot_images
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allSpecial_exhibits(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allSpecial_experiences(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						spot_image
						meta_title
						meta_description
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
			allTickets_and_pricing_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						wide_image
						meta_title
						meta_description
					}
				}
			}
			allWeddings_landing_pages(lang: "en-us", fulltext: $search, tags: $tags) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
						wide_image
					}
				}
			}
		}
	}
`
