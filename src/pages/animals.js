import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import Hero from "../components/hero-landing/hero-landing.js"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import Button from "../components/button/button-link.js"
import { renderAsText, renderImageUrl, linkResolver } from "../utility.js"
import styles from "./animals.module.css"

class GalleryTile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      //imageUrl: '',
      isMobile: false,
    }

    this.handleClick = this.handleClick.bind(this)
    //this.handleChangeScreenWidth = this.handleChangeScreenWidth.bind(this);
  }
  /*
	handleChangeScreenWidth(ev) {
		const { data } = this.props;
		
		if (ev.matches) {
			if (!this.state.isMobile || this.state.imageUrl === '') {
				this.setState({ isMobile: true, imageUrl: renderImageUrl(data.wide_images['Animal-Grid-Small']) });
			}
			
		} else if (this.state.isMobile || this.state.imageUrl === '') {
				let image = data.rows > 1 ? data.tall_images : data.wide_images['Animal-Grid-Small'];
				let imageUrl = image ? renderImageUrl(image) : '';
				this.setState({ isMobile: false, imageUrl: imageUrl }); 
		}
	}
	componentDidMount() {
		this.match = window.matchMedia(`(max-width: 768px)`);
		this.handleChangeScreenWidth(this.match);
	this.match.addListener(this.handleChangeScreenWidth);
  }
  componentWillUnmount() {
	this.match.removeListener(this.handleChangeScreenWidth);
  }	
	*/
  handleClick() {
    console.log(">", this.props.data)
    this.props.onClick(this.props.data)
  }
  render() {
    let { data } = this.props
    console.log("DATA", data)
    let image = data.wide_images && data.wide_images["Animal-Grid-Small"]
    if (
      typeof window !== "undefined" &&
      window.matchMedia("(min-width: 768px)").matches
    ) {
      if (data.rows > 1) {
        image = data.tall_images
        if (data.tall_images && data.columns === 3) {
          image = data.tall_images["Animal-Grid-Tall-Skinny"]
        }
      } else if (data.wide_images && data.columns > 3) {
        image = data.wide_images["wide-fat-short"]
      }
    }

    if (!image) {
      let placeholder = "tile-rollover-small.png"
      if (
        typeof window !== "undefined" &&
        window.matchMedia("(min-width: 768px)").matches
      ) {
        placeholder = data.placeholder
      }
      let style = {
        backgroundImage: `url(/images/${placeholder})`,
        gridRow: `span ${data.rows}`,
        gridColumn: `span ${data.columns}`,
      }
      return (
        <div className={styles.galleryTile} style={style}>
          <div
            className={styles.wholePageEditButton}
            data-wio-id={data._meta ? data._meta.id : null}
          ></div>
          <div className={styles.galleryTileOverlayNoImage}>
            <div className={styles.galleryTileContent}>
              <h3 className={styles.galleryTileTitleNoImage}>
                {data.animal_name}
              </h3>
              <h4 className={styles.galleryTileNoImageAvail}>
                No Image Available
              </h4>
              <div className="mt-1">
                <Button text="Learn More" onClick={this.handleClick} />
              </div>
            </div>
          </div>
        </div>
      )
    } else {
      let style = {
        backgroundImage: `url(${renderImageUrl(image)})`,
        gridRow: `span ${data.rows}`,
        gridColumn: `span ${data.columns}`,
      }
      return (
        <div className={styles.galleryTile} style={style}>
          <div
            className={styles.wholePageEditButton}
            data-wio-id={data._meta ? data._meta.id : null}
          ></div>
          <div className={styles.galleryTileOverlay}>
            <div className={styles.galleryTileContent}>
              <h3 className={styles.galleryTileTitle}>{data.animal_name}</h3>
              <div className="mt-1">
                <Button text="Learn More" onClick={this.handleClick} />
              </div>
            </div>
          </div>
        </div>
      )
    }
  }
}

const animalTypes = [
  { label: "All", value: "" },
  { label: "Amphibians", value: "Amphibian" },
  { label: "Birds", value: "Bird" },
  { label: "Fish", value: "Fish" },
  { label: "Mammals", value: "Mammal" },
  { label: "Reptiles", value: "Reptile" },
]

class FilterItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}

    this.handleClick = this.handleClick.bind(this)
  }
  handleClick(ev) {
    ev.preventDefault()
    this.props.onChange({ label: this.props.label, value: this.props.value })
  }
  render() {
    let { active, label, value } = this.props
    let classes =
      active === value
        ? `nav-link ${styles.navLink} ${styles.navLinkActive}`
        : `nav-link ${styles.navLink}`

    return (
      <li className={`${styles.navItem} nav-item`}>
        <a className={classes} href="#" onClick={this.handleClick}>
          {label}
        </a>
      </li>
    )
  }
}

const grid = [
  { columns: 4, rows: 2, placeholder: "tile-rollover-tall-fat.png" },
  { columns: 6, rows: 1, placeholder: "tile-rollover-short-wide.png" },
  { columns: 3, rows: 1, placeholder: "tile-rollover-small.png" },
  { columns: 3, rows: 2, placeholder: "tile-rollover-tall-skinny.png" },
  { columns: 3, rows: 1, placeholder: "tile-rollover-small.png" },
  { columns: 4, rows: 2, placeholder: "tile-rollover-tall-fat.png" },
  { columns: 3, rows: 1, placeholder: "tile-rollover-small.png" },
  { columns: 3, rows: 1, placeholder: "tile-rollover-small.png" },
  { columns: 6, rows: 1, placeholder: "tile-rollover-short-wide.png" },
  { columns: 4, rows: 2, placeholder: "tile-rollover-tall-fat.png" },
  { columns: 3, rows: 1, placeholder: "tile-rollover-small.png" },
  { columns: 3, rows: 1, placeholder: "tile-rollover-small.png" },
]

const animalTemplate = {
  animal_name: "",
  animal_description: "",
  tall_images: null,
  wide_images: null,
}

class AnimalsLanding extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      animals: props.data.prismic.allAnimal_details.edges,
      animalCurrent: animalTemplate,
      pageInfo: props.data.prismic.allAnimal_details.pageInfo,
      animalType: "",
    }

    this.handleChangeFilter = this.handleChangeFilter.bind(this)
    this.handleClickLearnMore = this.handleClickLearnMore.bind(this)
    this.handleClickLoadMore = this.handleClickLoadMore.bind(this)
  }
  handleClickLoadMore() {
    console.log("Click", this.state.pageInfo.endCursor)
    let tags = this.state.animalType === "" ? null : [this.state.animalType]
    this.props.prismic
      .load({
        variables: { cursor: this.state.pageInfo.endCursor, tags: tags },
        query, // (optional)
        fragments: [], // (optional)
      })
      .then(res => {
        console.log("LOADED", res.data)
        this.setState({
          animals: this.state.animals.concat(res.data.allAnimal_details.edges),
          pageInfo: res.data.allAnimal_details.pageInfo,
        })
      })
      .catch(e => {
        console.log(e)
      })
  }
  handleChangeFilter(animalType) {
    console.log("animalType", animalType)
    this.setState({ animalType: animalType.value })
    let tags = animalType.value === "" ? null : [animalType.value]
    this.props.prismic
      .load({
        variables: { tags: tags },
        query, // (optional)
        fragments: [], // (optional)
      })
      .then(res => {
        console.log("LOADED", res.data.allAnimal_details.edges.length, res.data)
        this.setState({
          animals: res.data.allAnimal_details.edges,
          pageInfo: res.data.allAnimal_details.pageInfo,
        })
      })
      .catch(e => {
        console.log(e)
      })
  }
  handleClickLearnMore(data) {
    this.setState({ animalCurrent: data })
    window.$("#animalModal").modal("show")
  }
  handleClickModalHide() {
    window.$("#animalModal").modal("hide")
  }
  componentDidMount() {
    window.$("#animalModal").on("hidden.bs.modal", () => {
      this.setState({ animalCurrent: animalTemplate })
    })
  }
  render() {
    console.log("props ->", this.props)
    const doc = this.props.data.prismic.allAnimals_landing_pages.edges
      .slice(0, 1)
      .pop()
    console.log("Animals doc -->", doc)
    if (!doc) return null

    const { animals, animalCurrent } = this.state

    /*
			BUILD FILTERS
		*/

    let filterItems = animalTypes.map((d, i) => (
      <FilterItem
        key={i}
        label={d.label}
        value={d.value}
        active={this.state.animalType}
        onChange={this.handleChangeFilter}
      />
    ))

    // BUILD GRID DATA
    let j = 0
    let animalsGrid = animals.map((d, i) => {
      let gridItem = Object.assign({}, d.node, grid[j])

      j++

      // RESET GRID BACK TO THE BEGINNING
      if (j === grid.length) {
        j = 0
      }

      return gridItem
    })

    const {
      meta_title,
      meta_description,
      image,
      title,
      summary,
      information,
      _meta,
    } = doc.node

    return (
      <Layout title={title} section="animals" {...this.props}>
        <SEO
          title={meta_title}
          canonical={this.props.location.origin + this.props.location.pathname}
          description={meta_description}
        />
        <Hero image={image} pageId={_meta.id} />
        <TitleBar
          title={title}
          description={summary}
          calloutDescription={information}
        />
        <nav className={`navbar navbar-expand-lg ${styles.filterBar}`}>
          <div className="container">
            <ul className={`navbar-nav ${styles.navbarNav}`}>{filterItems}</ul>
          </div>
        </nav>
        <div className={styles.galleryContainer}>
          <div className="container">
            <div className="col-lg-10 offset-lg-1 col-md-12">
              <div className={styles.gallery}>
                {animalsGrid.map((d, i) => {
                  //console.log('ANIMAL ->', d);
                  return (
                    <GalleryTile
                      key={i}
                      data={d}
                      onClick={this.handleClickLearnMore}
                    />
                  )
                })}
              </div>
              {this.state.pageInfo.hasNextPage && (
                <div className={styles.showMoreContainer}>
                  <Button text="Show More" onClick={this.handleClickLoadMore} />
                </div>
              )}
            </div>
          </div>
        </div>
        <div id="animalModal" className="modal" tabIndex="-1" role="dialog">
          <div
            className={`${styles.modalDialog} modal-dialog modal-lg`}
            role="document"
          >
            <img
              src={
                animalCurrent.tall_images
                  ? renderImageUrl(
                      animalCurrent.tall_images["Animals-Overlay"] ||
                        animalCurrent.tall_images
                    )
                  : ""
              }
              className={styles.modalImage}
              alt=""
            />
            <div className={`modal-content ${styles.modalContent}`}>
              <div className={`modal-header ${styles.modalHeader}`}>
                <h5 className={`modal-title ${styles.modalTitle}`}>
                  {animalCurrent.animal_name}
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className={`modal-body ${styles.modalBody}`}>
                <p>{animalCurrent.animal_description}</p>
                <div className={styles.modalIconBar}>
                  <Link
                    to={`/zoo-map${
                      animalCurrent.map_id
                        ? "?location=" + renderAsText(animalCurrent.map_id)
                        : ""
                    }`}
                    onClick={ev => this.handleClickModalHide()}
                  >
                    <span className={`${styles.iconMap} mr-2`}></span>Find on
                    Map
                  </Link>
                  {animalCurrent.animal_cam && (
                    <Link
                      to={linkResolver(animalCurrent.animal_cam._meta)}
                      onClick={ev => this.handleClickModalHide()}
                    >
                      <span className={`${styles.iconCam} mr-2`}></span>View Cam
                    </Link>
                  )}
                  {animalCurrent.donate_cta && (
                    <Link
                      to={linkResolver(animalCurrent.donate_cta._meta)}
                      onClick={ev => this.handleClickModalHide()}
                    >
                      <span className={`${styles.iconHeart} mr-2`}></span>Donate
                      Today!
                    </Link>
                  )}
                </div>
                {animalCurrent.button_link && (
                  <div className={styles.cta}>
                    <div className={styles.ctaBody}>
                      <h6 className={styles.ctaTitle}>
                        {renderAsText(animalCurrent.button_link.title)}
                      </h6>
                      <div>{animalCurrent.button_link.summary}</div>
                    </div>
                    <Button
                      text="Learn More"
                      isInverse={true}
                      link={animalCurrent.button_link}
                      onClick2={this.handleClickModalHide}
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default AnimalsLanding

export const query = graphql`
  query AnimalsLandingQuery($cursor: String, $tags: [String!]) {
    prismic {
      allAnimals_landing_pages(uid: "animals") {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            title
            summary
            image
            cta_label
            cta {
              _linkType
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__FileLink {
                _linkType
                url
              }
            }
            meta_title
            meta_description
          }
        }
      }
      allAnimal_details(
        tags: $tags
        sortBy: animal_name_ASC
        first: 12
        after: $cursor
      ) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            animal_cam {
              ... on PRISMIC_Animal_cam_detail {
                _linkType
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC_Detail {
                _linkType
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            animal_description
            animal_name
            animal_scientific_name
            animal_type
            button_link {
              ... on PRISMIC_Detail {
                _linkType
                _meta {
                  id
                  tags
                  type
                  uid
                }
                title
                summary: summary1
              }
            }
            location
            meta_description
            meta_title
            tall_images
            wide_images
            map_id
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
      }
    }
  }
`
