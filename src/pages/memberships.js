import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
//import { Link } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import GiftCallOut from "../components/gift-call-out/gift-call-out.js"
import Hero from "../components/hero-landing/hero-landing"
import styles from "./memberships-landing.module.css"
import Button from "../components/button/button-link"
import {
  ColorToHex,
  filterArrayByType,
  filterArrayByTypeName,
  renderAsText,
  renderAsHtml,
  renderCheckmark,
  getPrice,
  renderLinkUrl,
} from "../utility"
import { getItems, getOnlineItems, getJacksonClayItems } from "../galaxy"
import { Link } from "@reach/router"
import { Modal } from "react-bootstrap"
import { linkResolver } from "gatsby-source-prismic-graphql"

const MembershipsLanding = props => {
  const { data, location } = props
  const [items, setItems] = useState(0)
  const [onlinePriceItems, setOnlineItems] = useState(0)
  const [jacksonClayItems, setJacksonClayItems] = useState(0)
  const [openModals, setOpenModals] = useState({})
  let neededModals = []

  useEffect(() => {
    // getItems( (res) => {
    //   setItems(res)
    // });
    // getOnlineItems( (res) => {
    //   setOnlineItems(res)
    // });
    // getJacksonClayItems( (res) => {
    //   setJacksonClayItems(res)
    // });
  }, [])

  // Used to check a gift checkbox when pushing enter on the label
  // Provide access for keyboard users
  function checkBoxOnEnter(id) {
    const checkbox = document.getElementById(id)
    checkbox.checked = !checkbox.checked
  }

  /**
   * @param id - the id of the modal to show
   */
  function showDynamicModal(id) {
    let newModal = { ...openModals }
    newModal[id] = true
    setOpenModals({ ...newModal })
  }

  /**
   * @param id - the id of the modal to hide
   */
  function hideDynamicModal(id) {
    let newModal = { ...openModals }
    newModal[id] = false
    setOpenModals({ ...newModal })
  }

  const doc = data.prismic.allMembership_landing_pages.edges.slice(0, 1).pop()
  console.log("doc -->", doc)
  if (!doc) return null
  const {
    hero,
    description,
    callout_heading,
    membership_renewal_link_text,
    membership_renewal_link,
    title,
    meta_title,
    meta_description,
    callout_description,
    special_experience,
    body,
  } = doc.node

  var membershipSections = filterArrayByType(
    body,
    "membership_comparison_section"
  )

  let giftCallout = null
  let giftCalloutSlice = filterArrayByType(body, "gift_callout")[0]
  if (giftCalloutSlice) {
    giftCallout = (
      <GiftCallOut
        giftCalloutBody={body}
        giftCalloutColorPicker={
          giftCalloutSlice.primary.gift_callout_color_name
        }
        giftCalloutLabel={giftCalloutSlice.primary.gift_callout_label}
        giftCalloutTitle={giftCalloutSlice.primary.gift_callout_title}
        giftCalloutDescription={
          giftCalloutSlice.primary.gift_callout_description
        }
        giftCalloutImage={giftCalloutSlice.primary.gift_callout_image.url}
        giftCalloutButtonOption2={
          giftCalloutSlice.primary.gift_callout_button_option_2_text &&
          giftCalloutSlice.primary.gift_callout_button_option_2_text
        }
        giftCalloutButtonOption3={
          giftCalloutSlice.primary.gift_callout_button_option_3_text &&
          giftCalloutSlice.primary.gift_callout_button_option_3_text
        }
        giftCalloutButtonOption2Link={
          giftCalloutSlice.primary.gift_callout_button_option_2_link &&
          giftCalloutSlice.primary.gift_callout_button_option_2_link.url
        }
        giftCalloutButtonOption3Link={
          giftCalloutSlice.primary.gift_callout_button_option_3_link &&
          giftCalloutSlice.primary.gift_callout_button_option_3_link.url
        }
        giftCalloutDropdownText={
          giftCalloutSlice.primary.gift_callout_dropdown_text &&
          giftCalloutSlice.primary.gift_callout_dropdown_text
        }
        giftCalloutButtonText={
          giftCalloutSlice.primary.gift_callout_button_text
        }
        giftCalloutButtonLink={
          giftCalloutSlice.primary.gift_callout_button_link.url
        }
      />
    )
  }

  return (
    <Layout title={title} section="support-us" {...props}>
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero image={hero} pageId={doc.node._meta.id} />

      <TitleBar
        title={title}
        description={description}
        calloutTitle={callout_heading}
        calloutDescription={callout_description}
        link={linkResolver(membership_renewal_link)}
        linkText={membership_renewal_link_text}
      />

      {giftCallout}

      {membershipSections.map((d, i) => {
        var mDetail = []

        mDetail[0] = d.primary.m_detail_1
        mDetail[1] = d.primary.m_detail_2
        if (d.primary.m_detail_3) {
          mDetail[2] = d.primary.m_detail_3
        }
        console.log("mdetail", mDetail)

        return (
          <section className={styles.section}>
            {/*  = = = = = = = = = = = = = = = == = = = = = = = = = = = == = = = = = = = == = = = = = = = = == = = = == = 
                 ********************************************* START DESKTOP VIEW **********************************************
                 = = = = = = =  = ==  = == = = == = = = = =  = = = == = =  = == = = = == = ==  = = =  = == =  = = =  = = =  =*/}
            {/* Header up till the "payment options" row */}
            <div className="d-md-none d-none d-sm-none d-lg-block container">
              <div className="row">
                <div className="col-12 col-md-3 mb-3">
                  <div>
                    <h2 className={styles.ticketComparisonSectionTitle}>
                      {renderAsText(d.primary.membership_types)}
                    </h2>
                    <p className={styles.ticketComparisonSectionDescription}>
                      {renderAsText(d.primary.membership_type_description)}
                    </p>
                    {/* { console.log("d.primary.link", d.primary.link,(d.primary.link._linkType === "Link.web" || d.primary.link._linkType === "Link.file") ? d.primary.link.url : "#" )} */}
                    <a
                      className={styles.ticketComparisonSectionLink}
                      target="_blank"
                      href={
                        d.primary.link._linkType === "Link.web" ||
                        d.primary.link._linkType === "Link.file"
                          ? d.primary.link.url
                          : "#"
                      }
                    >
                      {renderAsText(d.primary.link_text)}
                    </a>
                  </div>
                </div>
                {mDetail.map((detail, j) => {
                  return (
                    <div
                      className={`col-4 col-md-3 ${styles.membershipDetailHeaderContainer}`}
                    >
                      <div
                        className={styles.wholePageEditButton}
                        data-wio-id={detail._meta.id}
                      ></div>
                      <div
                        className={styles.membershipDetailHeader}
                        style={{
                          backgroundColor: ColorToHex(
                            detail.body
                              ? filterArrayByType(
                                  detail.body,
                                  "color_picker"
                                )[0].primary.color_name
                              : "Tangerine"
                          ),
                          borderTopWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderRightWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderLeftWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderBottomWidth:
                            detail.membership_best_value === true ? 0 : "",
                          borderColor:
                            detail.membership_best_value === true ? "red" : "",
                          borderStyle:
                            detail.membership_best_value === true
                              ? "solid"
                              : "",
                        }}
                      >
                        <div
                          className={styles.membershipBestValueHeaderContainer}
                          style={{
                            display:
                              detail.membership_best_value === true
                                ? "block"
                                : "none",
                          }}
                        >
                          {renderAsText(detail.membership_best_value_text)}
                        </div>
                        <h3 className={styles.membershipDetailHeaderHeading}>
                          {renderAsText(detail.title)}
                        </h3>
                        <div className={styles.membershipDetailHeaderBody}>
                          <p className={styles.ticketPricing}>
                            {detail.above_price_text ? (
                              detail.above_price_text
                            ) : (
                              <span>&nbsp;</span>
                            )}
                          </p>
                          <p className={styles.ticketPrices}>
                            {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail"
                              ? getPrice(onlinePriceItems, detail.plu, 0)
                              : getPrice(jacksonClayItems, detail.plu, 0)}
                          </p>
                          {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail" &&
                            detail.full_plu_price && (
                              <p className={styles.ticketPricesXS}>
                                <strike>
                                  {getPrice(items, detail.full_price_plu, 0)}
                                </strike>
                              </p>
                            )}
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>

              {/* Payment options row */}
              <div className="row">
                <div className="col-12 col-md-3"></div>
                {mDetail.map((detail, j) => {
                  return (
                    <div
                      className={`col-4 col-md-3 ${styles.membershipDetailHeaderContainer}`}
                    >
                      <div
                        className={styles.membershipDetailHeader}
                        style={{
                          borderRadius: 0,
                          backgroundColor: ColorToHex(
                            detail.body
                              ? filterArrayByType(
                                  detail.body,
                                  "color_picker"
                                )[0].primary.color_name
                              : "Tangerine"
                          ),
                          borderTopWidth: 0,
                          borderRightWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderLeftWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderBottomWidth:
                            detail.membership_best_value === true ? 0 : "",
                          borderColor:
                            detail.membership_best_value === true ? "red" : "",
                          borderStyle:
                            detail.membership_best_value === true
                              ? "solid"
                              : "",
                        }}
                      >
                        {detail.payment_plan_info_text &&
                          renderAsText(detail.payment_plan_info_text) != "" && (
                            <div style={{ textAlign: "center" }}>
                              <span
                                tabIndex={0}
                                className={styles.spanLinkWhite}
                                onClick={() =>
                                  showDynamicModal(detail._meta.id)
                                }
                              >
                                or pay with monthly installments
                              </span>

                              {/* Save the modal info for later creation */}
                              <span style={{ display: "none" }}>
                                {neededModals.push({
                                  id: detail._meta.id,
                                  heading:
                                    renderAsText(detail.title) +
                                    " Payment Plan",
                                  text: renderAsHtml(
                                    detail.payment_plan_info_text
                                  ),
                                })}
                              </span>
                            </div>
                          )}
                      </div>
                    </div>
                  )
                })}
              </div>

              {/* Everything after the payment options row */}
              <div className="row">
                <div className="col-12 col-md-3 "></div>
                {mDetail.map((detail, j) => {
                  return (
                    <div
                      className={`col-4 col-md-3 ${styles.membershipDetailHeaderContainer}`}
                    >
                      <div
                        className={styles.membershipDetailHeader}
                        style={{
                          borderRadius: 0,
                          backgroundColor: ColorToHex(
                            detail.body
                              ? filterArrayByType(
                                  detail.body,
                                  "color_picker"
                                )[0].primary.color_name
                              : "Tangerine"
                          ),
                          borderTopWidth: 0,
                          borderRightWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderLeftWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderBottomWidth:
                            detail.membership_best_value === true ? 0 : "",
                          borderColor:
                            detail.membership_best_value === true ? "red" : "",
                          borderStyle:
                            detail.membership_best_value === true
                              ? "solid"
                              : "",
                        }}
                      >
                        <div className={styles.membershipDetailHeaderBody}>
                          <div className={styles.ticketOptionBlockFooter}>
                            {detail.membership_dropdown == null && (
                              <Button
                                color={
                                  detail.body
                                    ? filterArrayByType(
                                        detail.body,
                                        "color_picker"
                                      )[0].primary.color_name
                                    : "Tangerine"
                                }
                                isInverse
                                text={detail.cta_text || "Join Today!"}
                                url={
                                  mDetail[0].__typename ===
                                  "PRISMIC_Membership_detail"
                                    ? detail.cta_link.url
                                    : detail.membership_cta.url
                                }
                                size="large"
                              />
                            )}
                            {detail.membership_dropdown != null && (
                              <div className={`${styles.giftDropdown}`}>
                                <div className={styles.joinGiftContainer}>
                                  <input
                                    id={`giftToggle${detail._meta.id}`}
                                    className={styles.giftToggle}
                                    type="checkbox"
                                  />
                                  {/* First Button for if normal (not gift) */}
                                  <div className={styles.joinBox}>
                                    <button
                                      className={`btn dropdown-toggle ${styles.giftDropdownToggle}`}
                                      style={{
                                        color: ColorToHex(
                                          detail.body
                                            ? filterArrayByType(
                                                detail.body,
                                                "color_picker"
                                              )[0].primary.color_name
                                            : "Tangerine"
                                        ),
                                      }}
                                      isInverse
                                      type="button"
                                      id="dropdownMenuButton"
                                      data-toggle="dropdown"
                                      aria-haspopup="true"
                                      aria-expanded="false"
                                    >
                                      {detail.cta_text}
                                    </button>
                                    <div
                                      className={`${styles.giftDropdownMenu} dropdown-menu`}
                                      aria-labelledby="dropdownMenuButton"
                                    >
                                      <a
                                        className="dropdown-item"
                                        target="_blank"
                                        href={
                                          detail.membership_cta_option_1_link
                                            ? detail
                                                .membership_cta_option_1_link
                                                .url
                                            : "#"
                                        }
                                        style={{
                                          color: ColorToHex(
                                            detail.body
                                              ? filterArrayByType(
                                                  detail.body,
                                                  "color_picker"
                                                )[0].primary.color_name
                                              : "Tangerine"
                                          ),
                                        }}
                                      >
                                        {detail.membership_cta_option_1}
                                      </a>
                                      <a
                                        className="dropdown-item"
                                        target="_blank"
                                        href={
                                          detail.membership_cta_option_2_link
                                            ? detail
                                                .membership_cta_option_2_link
                                                .url
                                            : "#"
                                        }
                                        style={{
                                          color: ColorToHex(
                                            detail.body
                                              ? filterArrayByType(
                                                  detail.body,
                                                  "color_picker"
                                                )[0].primary.color_name
                                              : "Tangerine"
                                          ),
                                        }}
                                      >
                                        {detail.membership_cta_option_2}
                                      </a>{" "}
                                      <a
                                        className="dropdown-item"
                                        target="_blank"
                                        href={
                                          detail.membership_cta_option_3_link
                                            ? detail
                                                .membership_cta_option_3_link
                                                .url
                                            : "#"
                                        }
                                        style={{
                                          color: ColorToHex(
                                            detail.body
                                              ? filterArrayByType(
                                                  detail.body,
                                                  "color_picker"
                                                )[0].primary.color_name
                                              : "Tangerine"
                                          ),
                                        }}
                                      >
                                        {detail.membership_cta_option_3}
                                      </a>
                                    </div>
                                  </div>{" "}
                                  {/* End joinBox */}
                                  {/* If Gift Button */}
                                  <div className={styles.giftBox}>
                                    <button
                                      className={`btn dropdown-toggle ${styles.giftDropdownToggle}`}
                                      style={{
                                        color: ColorToHex(
                                          detail.body
                                            ? filterArrayByType(
                                                detail.body,
                                                "color_picker"
                                              )[0].primary.color_name
                                            : "Tangerine"
                                        ),
                                      }}
                                      isInverse
                                      type="button"
                                      id="dropdownMenuButton"
                                      data-toggle="dropdown"
                                      aria-haspopup="true"
                                      aria-expanded="false"
                                    >
                                      {detail.gift_cta_text || "Join Today!"}
                                    </button>
                                    <div
                                      className={`${styles.giftDropdownMenu} dropdown-menu`}
                                      aria-labelledby="dropdownMenuButton"
                                    >
                                      {detail.gift_dropdown_button &&
                                        detail.gift_dropdown_options[0]
                                          .gift_dropdown_option_link &&
                                        detail.gift_dropdown_options.map(
                                          option => (
                                            <a
                                              className="dropdown-item"
                                              target="_blank"
                                              href={
                                                option.gift_dropdown_option_link
                                                  ? renderLinkUrl(
                                                      option.gift_dropdown_option_link
                                                    )
                                                  : "#"
                                              }
                                              style={{
                                                color: ColorToHex(
                                                  detail.body
                                                    ? filterArrayByType(
                                                        detail.body,
                                                        "color_picker"
                                                      )[0].primary.color_name
                                                    : "Tangerine"
                                                ),
                                              }}
                                            >
                                              {option.gift_dropdown_option_text}
                                            </a>
                                          )
                                        )}
                                    </div>
                                  </div>{" "}
                                  {/* End giftBox */}
                                  {(detail.gift_cta_link ||
                                    (detail.gift_dropdown_button &&
                                      detail.gift_dropdown_options[0]
                                        .gift_dropdown_option_link)) && (
                                    <label
                                      className={styles.joinLabel}
                                      tabIndex={0}
                                      onKeyPress={() =>
                                        checkBoxOnEnter(
                                          `giftToggle${detail._meta.id}`
                                        )
                                      }
                                      htmlFor={`giftToggle${detail._meta.id}`}
                                    >
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="16"
                                        height="16"
                                        fill="currentColor"
                                        viewBox="0 0 16 16"
                                      >
                                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                        <path d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.235.235 0 0 1 .02-.022z" />
                                      </svg>
                                      <span style={{ marginLeft: "5px" }}>
                                        purchase as a gift
                                      </span>
                                    </label>
                                  )}
                                  {(detail.gift_cta_link ||
                                    (detail.gift_dropdown_button &&
                                      detail.gift_dropdown_options[0]
                                        .gift_dropdown_option_link)) && (
                                    <label
                                      className={styles.giftLabel}
                                      tabIndex={0}
                                      onKeyPress={() =>
                                        checkBoxOnEnter(
                                          `giftToggle${detail._meta.id}`
                                        )
                                      }
                                      htmlFor={`giftToggle${detail._meta.id}`}
                                    >
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="16"
                                        height="16"
                                        fill="currentColor"
                                        viewBox="0 0 16 16"
                                      >
                                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                      </svg>
                                      <span style={{ marginLeft: "5px" }}>
                                        purchase as a gift
                                      </span>
                                    </label>
                                  )}
                                </div>
                              </div>
                            )}
                          </div>
                          {d.cta_detail && <p>{d.cta_detail}</p>}

                          <p className={styles.ctaDetail}>
                            {detail.secondary_discount_text}
                          </p>
                          {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail" && (
                            <p
                              className={`${styles.ticketPricesMD} d-flex align-items-center`}
                            >
                              <sup className={styles.ticketPricesMDSup}>$</sup>
                              {getPrice(
                                jacksonClayItems,
                                detail.secondary_discount_plu,
                                0
                              ).replace("$", "")}
                            </p>
                          )}
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>

              {/* End of the header area and the start of the "tabley" area */}
              {d.fields[0] && (
                <div className={` row ${styles.comparisonRow}`}>
                  <div
                    className={`col-12 col-md-3 d-flex ${styles.comparisonRowTextContainer}`}
                  >
                    {d.fields[0].row_label}
                  </div>
                  {mDetail.map((comparisonRow, i) => {
                    return (
                      <div className={`col-4 col-md-3 ${styles.comparisonCol}`}>
                        <div
                          className="d-flex flex-grow-1"
                          style={{
                            backgroundColor: ColorToHex(
                              comparisonRow.body
                                ? filterArrayByType(
                                    comparisonRow.body,
                                    "color_picker"
                                  )[0].primary.color_name
                                : "Tangerine"
                            ),
                          }}
                        >
                          <div
                            className={styles.comparisonItemSM}
                            style={{
                              borderTopWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderRightWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderBottomWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderLeftWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderColor:
                                comparisonRow.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                comparisonRow.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail"
                              ? comparisonRow.adults
                              : renderCheckmark(
                                  comparisonRow.family___rides_membership
                                )}
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
              {d.fields[1] && (
                <div className={` row ${styles.comparisonRow}`}>
                  <div
                    className={`col-12 col-md-3 d-flex ${styles.comparisonRowTextContainer}`}
                  >
                    {d.fields[1].row_label}
                  </div>
                  {mDetail.map((comparisonRow, i) => {
                    return (
                      <div className={`col-4 col-md-3 ${styles.comparisonCol}`}>
                        <div
                          className="d-flex flex-grow-1"
                          style={{
                            backgroundColor: ColorToHex(
                              comparisonRow.body
                                ? filterArrayByType(
                                    comparisonRow.body,
                                    "color_picker"
                                  )[0].primary.color_name
                                : "Tangerine"
                            ),
                          }}
                        >
                          <div
                            className={styles.comparisonItemSM}
                            style={{
                              borderTopWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderRightWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderBottomWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderLeftWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderColor:
                                comparisonRow.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                comparisonRow.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail"
                              ? comparisonRow.guests
                              : comparisonRow.extra_tickets___ride_wristbands}
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
              {d.fields[2] && (
                <div className={` row ${styles.comparisonRow}`}>
                  <div
                    className={`col-12 col-md-3 d-flex ${styles.comparisonRowTextContainer}`}
                  >
                    {d.fields[2].row_label}
                  </div>
                  {mDetail.map((comparisonRow, i) => {
                    return (
                      <div className={`col-4 col-md-3 ${styles.comparisonCol}`}>
                        <div
                          className="d-flex flex-grow-1"
                          style={{
                            backgroundColor: ColorToHex(
                              comparisonRow.body
                                ? filterArrayByType(
                                    comparisonRow.body,
                                    "color_picker"
                                  )[0].primary.color_name
                                : "Tangerine"
                            ),
                          }}
                        >
                          <div
                            className={styles.comparisonItemSM}
                            style={{
                              borderTopWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderRightWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderBottomWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderLeftWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderColor:
                                comparisonRow.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                comparisonRow.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail"
                              ? comparisonRow.discounts + "%"
                              : comparisonRow.brew_at_the_zoo___wine_zoo_tickets}
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
              {/* 50% discount */}
              {d.fields[3] && (
                <div className={` row ${styles.comparisonRow}`}>
                  <div
                    className={`col-12 col-md-3 d-flex ${styles.comparisonRowTextContainer}`}
                  >
                    {d.fields[3].row_label}
                  </div>
                  {mDetail.map((comparisonRow, i) => {
                    return (
                      <div className={`col-4 col-md-3 ${styles.comparisonCol}`}>
                        <div
                          className="d-flex flex-grow-1"
                          style={{
                            backgroundColor: ColorToHex(
                              comparisonRow.body
                                ? filterArrayByType(
                                    comparisonRow.body,
                                    "color_picker"
                                  )[0].primary.color_name
                                : "Tangerine"
                            ),
                          }}
                        >
                          <div
                            className={styles.comparisonItemSM}
                            style={{
                              borderTopWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderRightWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderBottomWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderLeftWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderColor:
                                comparisonRow.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                comparisonRow.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail"
                              ? comparisonRow.discounted_general_admission_tickets
                              : renderCheckmark(
                                  comparisonRow.early_enrollment_for_summer_camps
                                )}
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
              {d.fields[4] && (
                <div className={` row ${styles.comparisonRow}`}>
                  <div
                    className={`col-12 col-md-3 d-flex ${styles.comparisonRowTextContainer}`}
                  >
                    {d.fields[4].row_label}
                  </div>
                  {mDetail.map((comparisonRow, i) => {
                    return (
                      <div className={`col-4 col-md-3 ${styles.comparisonCol}`}>
                        <div
                          className="d-flex flex-grow-1"
                          style={{
                            backgroundColor: ColorToHex(
                              comparisonRow.body
                                ? filterArrayByType(
                                    comparisonRow.body,
                                    "color_picker"
                                  )[0].primary.color_name
                                : "Tangerine"
                            ),
                          }}
                        >
                          <div
                            className={styles.comparisonItemSM}
                            style={{
                              borderTopWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderRightWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderBottomWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderLeftWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderColor:
                                comparisonRow.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                comparisonRow.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail"
                              ? renderCheckmark(comparisonRow.unlimited_rides)
                              : renderCheckmark(
                                  comparisonRow.invitation_to_lion_s_pride_society_and_other_events
                                )}
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
              {d.fields[5] && (
                <div className={` row ${styles.comparisonRow}`}>
                  <div
                    className={`col-12 col-md-3 d-flex ${styles.comparisonRowTextContainer}`}
                  >
                    {d.fields[5].row_label}
                  </div>
                  {mDetail.map((comparisonRow, i) => {
                    return (
                      <div className={`col-4 col-md-3 ${styles.comparisonCol}`}>
                        <div
                          className="d-flex flex-grow-1"
                          style={{
                            backgroundColor: ColorToHex(
                              comparisonRow.body
                                ? filterArrayByType(
                                    comparisonRow.body,
                                    "color_picker"
                                  )[0].primary.color_name
                                : "Tangerine"
                            ),
                          }}
                        >
                          <div
                            className={styles.comparisonItemSM}
                            style={{
                              borderTopWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderRightWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderBottomWidth:
                                comparisonRow.membership_best_value === true
                                  ? 0
                                  : "",
                              borderLeftWidth:
                                comparisonRow.membership_best_value === true
                                  ? 2
                                  : "",
                              borderColor:
                                comparisonRow.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                comparisonRow.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail"
                              ? comparisonRow.extra_tickets_and_ride_wristbands
                              : "$" + comparisonRow.facility_rental_discount}
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
              {d.fields[6] &&
                mDetail[0].__typename === "PRISMIC_Membership_detail" && (
                  <div className={` row ${styles.comparisonRow}`}>
                    <div
                      className={`col-12 col-md-3 d-flex ${styles.comparisonRowTextContainer}`}
                    >
                      {d.fields[6].row_label === "Special Experience" && (
                        <span
                          tabIndex={0}
                          className={styles.spanLinkGreen}
                          onClick={() => showDynamicModal("specialExperience")}
                        >
                          {d.fields[6].row_label}
                        </span>
                      )}
                      {d.fields[6].row_label !== "Special Experience" &&
                        d.fields[6].row_label}
                    </div>
                    {mDetail.map((comparisonRow, i) => {
                      return (
                        <div
                          className={`col-4 col-md-3 ${styles.comparisonCol}`}
                        >
                          <div
                            className="d-flex flex-grow-1"
                            style={{
                              backgroundColor: ColorToHex(
                                comparisonRow.body
                                  ? filterArrayByType(
                                      comparisonRow.body,
                                      "color_picker"
                                    )[0].primary.color_name
                                  : "Tangerine"
                              ),
                            }}
                          >
                            <div
                              className={styles.comparisonItemSM}
                              style={{
                                borderTopWidth:
                                  comparisonRow.membership_best_value === true
                                    ? 0
                                    : "",
                                borderRightWidth:
                                  comparisonRow.membership_best_value === true
                                    ? 2
                                    : "",
                                borderBottomWidth:
                                  comparisonRow.membership_best_value === true
                                    ? 2
                                    : "",
                                borderLeftWidth:
                                  comparisonRow.membership_best_value === true
                                    ? 2
                                    : "",
                                borderColor:
                                  comparisonRow.membership_best_value === true
                                    ? "red"
                                    : "",
                                borderStyle:
                                  comparisonRow.membership_best_value === true
                                    ? "solid"
                                    : "",
                              }}
                            >
                              {mDetail[0].__typename ===
                              "PRISMIC_Membership_detail"
                                ? renderCheckmark(comparisonRow.free_tShirt)
                                : renderCheckmark(
                                    comparisonRow.vip_golf_cart_tour_for_4
                                  )}
                            </div>
                          </div>
                        </div>
                      )
                    })}
                  </div>
                )}
              {mDetail[0].__typename === "PRISMIC_Elite_membership_detail" && (
                <div className={` row ${styles.comparisonRow}`}>
                  <div
                    className={`col-12 col-md-3 d-flex ${styles.comparisonRowTextContainer}`}
                  >
                    {d.fields[7].row_label}
                  </div>
                  {mDetail.map((comparisonRow, i) => {
                    return (
                      <div className={`col-4 col-md-3 ${styles.comparisonCol}`}>
                        <div
                          className="d-flex flex-grow-1"
                          style={{
                            backgroundColor: ColorToHex(
                              comparisonRow.body
                                ? filterArrayByType(
                                    comparisonRow.body,
                                    "color_picker"
                                  )[0].primary.color_name
                                : "Tangerine"
                            ),
                          }}
                        >
                          <div className={styles.comparisonItemSM}>
                            {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail"
                              ? renderCheckmark(comparisonRow.free_tShirt)
                              : renderCheckmark(
                                  comparisonRow.vip_safari_and_tours_before_zoo_opens
                                )}
                          </div>
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
            </div>
            {/*  = = = = = = = = = = = = = = = == = = = = = = = = = = = == = = = = = = = == = = = = = = = = == = = = == = 
                 *********************************************END DESKTOP VIEW **********************************************
                 = = = = = = =  = ==  = == = = == = = = = =  = = = == = =  = == = = = == = ==  = = =  = == =  = = =  = = =  =*/}
            <div className="col-12 col-md-3 mb-3 d-lg-none d-xl-none">
              <div>
                <h2 className={styles.ticketComparisonSectionTitle}>
                  {renderAsText(d.primary.membership_types)}
                </h2>
                <p className={styles.ticketComparisonSectionDescription}>
                  {renderAsText(d.primary.membership_type_description)}
                </p>
                <a
                  className={styles.ticketComparisonSectionLink}
                  target="_blank"
                  href={
                    d.primary.link._linkType === "Link.web" ||
                    d.primary.link._linkType === "Link.file"
                      ? d.primary.link.url
                      : "#"
                  }
                >
                  {renderAsText(d.primary.link_text)}
                </a>
              </div>
            </div>
            {mDetail.map(detail => {
              return (
                <div
                  className={`container d-lg-none d-xl-none ${styles.membershipDetailContainerMobile}`}
                >
                  <div className={` row m-0`}>
                    <div
                      className={`col-12 p-0 ${styles.membershipDetailHeaderContainer}`}
                    >
                      <div
                        className={styles.wholePageEditButton}
                        data-wio-id={detail._meta.id}
                      ></div>
                      <div
                        className={styles.membershipDetailHeader}
                        style={{
                          backgroundColor: ColorToHex(
                            detail.body
                              ? filterArrayByType(
                                  detail.body,
                                  "color_picker"
                                )[0].primary.color_name
                              : "Tangerine"
                          ),
                          borderTopWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderRightWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderBottomWidth:
                            detail.membership_best_value === true ? 0 : "",
                          borderLeftWidth:
                            detail.membership_best_value === true ? 2 : "",
                          borderColor:
                            detail.membership_best_value === true ? "red" : "",
                          borderStyle:
                            detail.membership_best_value === true
                              ? "solid"
                              : "",
                        }}
                      >
                        <div
                          className={styles.membershipBestValueHeaderContainer}
                          style={{
                            display:
                              detail.membership_best_value === true
                                ? "block"
                                : "none",
                          }}
                        >
                          {renderAsText(detail.membership_best_value_text)}
                        </div>
                        <div className={styles.membershipDetailHeaderHeading}>
                          {renderAsText(detail.title)}
                        </div>
                        <div className={styles.membershipDetailHeaderBody}>
                          {detail.above_price_text && (
                            <p className={styles.ticketPricing}>
                              {detail.above_price_text}
                            </p>
                          )}
                          {detail.plu && (
                            <p className={styles.ticketPrices}>
                              {mDetail[0].__typename ===
                              "PRISMIC_Membership_detail"
                                ? getPrice(onlinePriceItems, detail.plu, 0)
                                : getPrice(onlinePriceItems, detail.plu, 0)}
                            </p>
                          )}
                          {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail" && (
                            <p className={styles.ticketPricesXS}>
                              <strike>
                                {getPrice(items, detail.full_price_plu, 0)}
                              </strike>
                            </p>
                          )}
                          {/* <div className={ styles.ctaButton }>
                                    <Button color={ (detail.body) ? (filterArrayByType( detail.body, "color_picker")[0].primary.color_name) : "Tangerine" } isInverse text={ detail.cta_text || "Join Today!"} url={ (mDetail[0].__typename === "PRISMIC_Membership_detail") ? detail.cta_link.url : detail.membership_cta.url } />
                                  </div> */}

                          {/* Mobile Payment Plan Options */}
                          {detail.payment_plan_info_text &&
                            renderAsText(detail.payment_plan_info_text) !=
                              "" && (
                              <span
                                tabIndex={0}
                                className={styles.spanLinkWhite}
                                style={{ marginBottom: "10px" }}
                                onClick={() =>
                                  showDynamicModal(detail._meta.id)
                                }
                              >
                                or pay with monthly installments
                              </span>
                            )}

                          {/* Mobile Save the modal info for later creation */}
                          <span style={{ display: "none" }}>
                            {detail.payment_plan_info_text &&
                              renderAsText(detail.payment_plan_info_text) !=
                                "" &&
                              neededModals.push({
                                id: detail._meta.id,
                                heading:
                                  renderAsText(detail.title) + " Payment Plan",
                                text: renderAsHtml(
                                  detail.payment_plan_info_text
                                ),
                              })}
                          </span>

                          {detail.membership_dropdown == null && (
                            <Button
                              color={
                                detail.body
                                  ? filterArrayByType(
                                      detail.body,
                                      "color_picker"
                                    )[0].primary.color_name
                                  : "Tangerine"
                              }
                              isInverse
                              text={detail.cta_text || "Join Today!"}
                              url={
                                mDetail[0].__typename ===
                                "PRISMIC_Membership_detail"
                                  ? detail.cta_link.url
                                  : detail.membership_cta.url
                              }
                              size="large"
                            />
                          )}
                          {detail.membership_dropdown != null && (
                            <div className={`${styles.giftDropdown}`}>
                              <div className={styles.joinGiftContainer}>
                                <input
                                  id={`giftToggleMobile${detail._meta.id}`}
                                  className={styles.giftToggle}
                                  type="checkbox"
                                />
                                {/* First Button for if normal (not gift) */}
                                <div className={styles.joinBox}>
                                  <button
                                    className={`btn dropdown-toggle ${styles.giftDropdownToggle}`}
                                    style={{
                                      color: ColorToHex(
                                        detail.body
                                          ? filterArrayByType(
                                              detail.body,
                                              "color_picker"
                                            )[0].primary.color_name
                                          : "Tangerine"
                                      ),
                                    }}
                                    isInverse
                                    type="button"
                                    id="dropdownMenuButton"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    {detail.cta_text}
                                  </button>
                                  <div
                                    className={`${styles.giftDropdownMenu} dropdown-menu`}
                                    aria-labelledby="dropdownMenuButton"
                                  >
                                    <a
                                      className="dropdown-item"
                                      target="_blank"
                                      href={
                                        detail.membership_cta_option_1_link
                                          ? detail.membership_cta_option_1_link
                                              .url
                                          : "#"
                                      }
                                      style={{
                                        color: ColorToHex(
                                          detail.body
                                            ? filterArrayByType(
                                                detail.body,
                                                "color_picker"
                                              )[0].primary.color_name
                                            : "Tangerine"
                                        ),
                                      }}
                                    >
                                      {detail.membership_cta_option_1}
                                    </a>
                                    <a
                                      className="dropdown-item"
                                      target="_blank"
                                      href={
                                        detail.membership_cta_option_2_link
                                          ? detail.membership_cta_option_2_link
                                              .url
                                          : "#"
                                      }
                                      style={{
                                        color: ColorToHex(
                                          detail.body
                                            ? filterArrayByType(
                                                detail.body,
                                                "color_picker"
                                              )[0].primary.color_name
                                            : "Tangerine"
                                        ),
                                      }}
                                    >
                                      {detail.membership_cta_option_2}
                                    </a>{" "}
                                    <a
                                      className="dropdown-item"
                                      target="_blank"
                                      href={
                                        detail.membership_cta_option_3_link
                                          ? detail.membership_cta_option_3_link
                                              .url
                                          : "#"
                                      }
                                      style={{
                                        color: ColorToHex(
                                          detail.body
                                            ? filterArrayByType(
                                                detail.body,
                                                "color_picker"
                                              )[0].primary.color_name
                                            : "Tangerine"
                                        ),
                                      }}
                                    >
                                      {detail.membership_cta_option_3}
                                    </a>
                                  </div>
                                </div>{" "}
                                {/* End joinBox */}
                                {/* If Gift Button */}
                                <div className={styles.giftBox}>
                                  <button
                                    className={`btn dropdown-toggle ${styles.giftDropdownToggle}`}
                                    style={{
                                      color: ColorToHex(
                                        detail.body
                                          ? filterArrayByType(
                                              detail.body,
                                              "color_picker"
                                            )[0].primary.color_name
                                          : "Tangerine"
                                      ),
                                    }}
                                    isInverse
                                    type="button"
                                    id="dropdownMenuButton"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                    {detail.gift_cta_text || "Join Today!"}
                                  </button>
                                  <div
                                    className={`${styles.giftDropdownMenu} dropdown-menu`}
                                    aria-labelledby="dropdownMenuButton"
                                  >
                                    {detail.gift_dropdown_button &&
                                      detail.gift_dropdown_options[0]
                                        .gift_dropdown_option_link &&
                                      detail.gift_dropdown_options.map(
                                        option => (
                                          <a
                                            className="dropdown-item"
                                            target="_blank"
                                            href={
                                              option.gift_dropdown_option_link
                                                ? renderLinkUrl(
                                                    option.gift_dropdown_option_link
                                                  )
                                                : "#"
                                            }
                                            style={{
                                              color: ColorToHex(
                                                detail.body
                                                  ? filterArrayByType(
                                                      detail.body,
                                                      "color_picker"
                                                    )[0].primary.color_name
                                                  : "Tangerine"
                                              ),
                                            }}
                                          >
                                            {option.gift_dropdown_option_text}
                                          </a>
                                        )
                                      )}
                                  </div>
                                </div>{" "}
                                {/* End giftBox */}
                                {(detail.gift_cta_link ||
                                  (detail.gift_dropdown_button &&
                                    detail.gift_dropdown_options[0]
                                      .gift_dropdown_option_link)) && (
                                  <label
                                    className={styles.joinLabel}
                                    tabIndex={0}
                                    onKeyPress={() =>
                                      checkBoxOnEnter(
                                        `giftToggleMobile${detail._meta.id}`
                                      )
                                    }
                                    htmlFor={`giftToggleMobile${detail._meta.id}`}
                                  >
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="16"
                                      height="16"
                                      fill="currentColor"
                                      viewBox="0 0 16 16"
                                    >
                                      <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                      <path d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.235.235 0 0 1 .02-.022z" />
                                    </svg>
                                    <span style={{ marginLeft: "5px" }}>
                                      purchase as a gift
                                    </span>
                                  </label>
                                )}
                                {(detail.gift_cta_link ||
                                  (detail.gift_dropdown_button &&
                                    detail.gift_dropdown_options[0]
                                      .gift_dropdown_option_link)) && (
                                  <label
                                    className={styles.giftLabel}
                                    tabIndex={0}
                                    onKeyPress={() =>
                                      checkBoxOnEnter(
                                        `giftToggleMobile${detail._meta.id}`
                                      )
                                    }
                                    htmlFor={`giftToggleMobile${detail._meta.id}`}
                                  >
                                    <svg
                                      xmlns="http://www.w3.org/2000/svg"
                                      width="16"
                                      height="16"
                                      fill="currentColor"
                                      viewBox="0 0 16 16"
                                    >
                                      <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z" />
                                    </svg>
                                    <span style={{ marginLeft: "5px" }}>
                                      purchase as a gift
                                    </span>
                                  </label>
                                )}
                              </div>
                            </div>
                          )}
                          <p className={styles.ctaDetail}>
                            {detail.secondary_discount_text}
                          </p>
                          {mDetail[0].__typename ===
                            "PRISMIC_Membership_detail" && (
                            <p
                              className={`${styles.ticketPricesMD} d-flex align-items-center`}
                            >
                              <sup className={styles.ticketPricesMDSup}>$</sup>
                              {getPrice(
                                items,
                                detail.secondary_discount_plu,
                                0
                              ).replace("$", "")}
                            </p>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                  {d.fields[0] && (
                    <div
                      className={`m-0 row ${styles.comparisonRow}`}
                      style={{
                        backgroundColor: ColorToHex(
                          detail.body
                            ? filterArrayByType(detail.body, "color_picker")[0]
                                .primary.color_name
                            : "Tangerine"
                        ),
                      }}
                    >
                      <div className={`col-12 ${styles.comparisonCol} p-0`}>
                        <div className="d-flex flex-grow-1">
                          <div
                            className={` ${styles.comparisonItemSM} justify-content-between`}
                            style={{
                              borderTopWidth:
                                detail.membership_best_value === true ? 0 : "",
                              borderRightWidth:
                                detail.membership_best_value === true ? 2 : "",
                              borderBottomWidth:
                                detail.membership_best_value === true ? 0 : "",
                              borderLeftWidth:
                                detail.membership_best_value === true ? 2 : "",
                              borderColor:
                                detail.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                detail.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            <p
                              className={`m-0 ${styles.mobileComparisonLabel}`}
                            >
                              {d.fields[0].row_label}
                            </p>
                            <p className="m-0">
                              {mDetail[0].__typename ===
                              "PRISMIC_Membership_detail"
                                ? detail.adults
                                : renderCheckmark(
                                    detail.family___rides_membership
                                  )}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {d.fields[1] && (
                    <div
                      className={`m-0 row ${styles.comparisonRow}`}
                      style={{
                        backgroundColor: ColorToHex(
                          detail.body
                            ? filterArrayByType(detail.body, "color_picker")[0]
                                .primary.color_name
                            : "Tangerine"
                        ),
                      }}
                    >
                      <div className={`col-12 ${styles.comparisonCol} p-0`}>
                        <div className="d-flex flex-grow-1">
                          <div
                            className={` ${styles.comparisonItemSM} justify-content-between`}
                            style={{
                              borderTopWidth:
                                detail.membership_best_value === true ? 0 : "",
                              borderRightWidth:
                                detail.membership_best_value === true ? 2 : "",
                              borderBottomWidth:
                                detail.membership_best_value === true ? 0 : "",
                              borderLeftWidth:
                                detail.membership_best_value === true ? 2 : "",
                              borderColor:
                                detail.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                detail.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            <p
                              className={`m-0 ${styles.mobileComparisonLabel}`}
                            >
                              {d.fields[1].row_label}
                            </p>
                            <p className="m-0">
                              {mDetail[0].__typename ===
                              "PRISMIC_Membership_detail"
                                ? detail.guests
                                : detail.extra_tickets___ride_wristbands}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {d.fields[2] && (
                    <div
                      className={`m-0 row ${styles.comparisonRow}`}
                      style={{
                        backgroundColor: ColorToHex(
                          detail.body
                            ? filterArrayByType(detail.body, "color_picker")[0]
                                .primary.color_name
                            : "Tangerine"
                        ),
                      }}
                    >
                      <div className={`col-12 ${styles.comparisonCol} p-0`}>
                        <div className="d-flex flex-grow-1">
                          <div
                            className={` ${styles.comparisonItemSM} justify-content-between`}
                            style={{
                              borderTopWidth:
                                detail.membership_best_value === true ? 0 : "",
                              borderRightWidth:
                                detail.membership_best_value === true ? 2 : "",
                              borderBottomWidth:
                                detail.membership_best_value === true ? 0 : "",
                              borderLeftWidth:
                                detail.membership_best_value === true ? 2 : "",
                              borderColor:
                                detail.membership_best_value === true
                                  ? "red"
                                  : "",
                              borderStyle:
                                detail.membership_best_value === true
                                  ? "solid"
                                  : "",
                            }}
                          >
                            <p
                              className={`m-0 ${styles.mobileComparisonLabel}`}
                            >
                              {d.fields[2].row_label}
                            </p>
                            <p className="m-0">
                              {mDetail[0].__typename ===
                              "PRISMIC_Membership_detail"
                                ? detail.discounts + "%"
                                : detail.brew_at_the_zoo___wine_zoo_tickets}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {d.fields[3] &&
                    ((mDetail[0].__typename === "PRISMIC_Membership_detail" &&
                      detail.discounted_general_admission_tickets) ||
                      (mDetail[0].__typename !== "PRISMIC_Membership_detail" &&
                        detail.early_enrollment_for_summer_camps ===
                          "Yes")) && (
                      <div
                        className={`m-0 row ${styles.comparisonRow}`}
                        style={{
                          backgroundColor: ColorToHex(
                            detail.body
                              ? filterArrayByType(
                                  detail.body,
                                  "color_picker"
                                )[0].primary.color_name
                              : "Tangerine"
                          ),
                        }}
                      >
                        <div className={`col-12 ${styles.comparisonCol} p-0`}>
                          <div className="d-flex flex-grow-1">
                            <div
                              className={` ${styles.comparisonItemSM} justify-content-between`}
                              style={{
                                borderTopWidth:
                                  detail.membership_best_value === true
                                    ? 0
                                    : "",
                                borderRightWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderBottomWidth:
                                  detail.membership_best_value === true
                                    ? 0
                                    : "",
                                borderLeftWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderColor:
                                  detail.membership_best_value === true
                                    ? "red"
                                    : "",
                                borderStyle:
                                  detail.membership_best_value === true
                                    ? "solid"
                                    : "",
                              }}
                            >
                              <p
                                className={`m-0 ${styles.mobileComparisonLabel}`}
                              >
                                {d.fields[3].row_label}
                              </p>
                              <p className="m-0">
                                {mDetail[0].__typename ===
                                "PRISMIC_Membership_detail"
                                  ? detail.discounted_general_admission_tickets
                                  : renderCheckmark(
                                      detail.early_enrollment_for_summer_camps
                                    )}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  {d.fields[4] &&
                    ((mDetail[0].__typename === "PRISMIC_Membership_detail" &&
                      detail.unlimited_rides === "Yes") ||
                      (mDetail[0].__typename !== "PRISMIC_Membership_detail" &&
                        detail.invitation_to_lion_s_pride_society_and_other_events ===
                          "Yes")) && (
                      <div
                        className={`m-0 row ${styles.comparisonRow}`}
                        style={{
                          backgroundColor: ColorToHex(
                            detail.body
                              ? filterArrayByType(
                                  detail.body,
                                  "color_picker"
                                )[0].primary.color_name
                              : "Tangerine"
                          ),
                        }}
                      >
                        <div className={`col-12 ${styles.comparisonCol} p-0`}>
                          <div className="d-flex flex-grow-1">
                            <div
                              className={` ${styles.comparisonItemSM} justify-content-between`}
                              style={{
                                borderTopWidth:
                                  detail.membership_best_value === true
                                    ? 0
                                    : "",
                                borderRightWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderBottomWidth:
                                  detail.membership_best_value === true
                                    ? 0
                                    : "",
                                borderLeftWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderColor:
                                  detail.membership_best_value === true
                                    ? "red"
                                    : "",
                                borderStyle:
                                  detail.membership_best_value === true
                                    ? "solid"
                                    : "",
                              }}
                            >
                              <p
                                className={`m-0 ${styles.mobileComparisonLabel}`}
                              >
                                {d.fields[4].row_label}
                              </p>
                              <p className="m-0">
                                {mDetail[0].__typename ===
                                "PRISMIC_Membership_detail"
                                  ? renderCheckmark(detail.unlimited_rides)
                                  : renderCheckmark(
                                      detail.invitation_to_lion_s_pride_society_and_other_events
                                    )}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  {d.fields[5] &&
                    ((mDetail[0].__typename === "PRISMIC_Membership_detail" &&
                      detail.extra_tickets_and_ride_wristbands) ||
                      mDetail[0].__typename !==
                        "PRISMIC_Membership_detail") && (
                      <div
                        className={`m-0 row ${styles.comparisonRow}`}
                        style={{
                          backgroundColor: ColorToHex(
                            detail.body
                              ? filterArrayByType(
                                  detail.body,
                                  "color_picker"
                                )[0].primary.color_name
                              : "Tangerine"
                          ),
                        }}
                      >
                        <div className={`col-12 ${styles.comparisonCol} p-0`}>
                          <div className="d-flex flex-grow-1">
                            <div
                              className={` ${styles.comparisonItemSM} justify-content-between`}
                              style={{
                                borderTopWidth:
                                  detail.membership_best_value === true
                                    ? 0
                                    : "",
                                borderRightWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderBottomWidth:
                                  detail.membership_best_value === true
                                    ? 0
                                    : "",
                                borderLeftWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderColor:
                                  detail.membership_best_value === true
                                    ? "red"
                                    : "",
                                borderStyle:
                                  detail.membership_best_value === true
                                    ? "solid"
                                    : "",
                              }}
                            >
                              <p
                                className={`m-0 ${styles.mobileComparisonLabel}`}
                              >
                                {d.fields[5].row_label}
                              </p>
                              <p className="m-0">
                                {mDetail[0].__typename ===
                                "PRISMIC_Membership_detail"
                                  ? detail.extra_tickets_and_ride_wristbands
                                  : "$" + detail.facility_rental_discount}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  {d.fields[6] &&
                    ((d.fields[6].row_label === "Special Experience" &&
                      detail.free_tShirt === "Yes") ||
                      (mDetail[0].__typename !== "PRISMIC_Membership_detail" &&
                        detail.vip_safari_and_tours_before_zoo_opens ===
                          "Yes")) && (
                      <div
                        className={`m-0 row ${styles.comparisonRow}`}
                        style={{
                          backgroundColor: ColorToHex(
                            detail.body
                              ? filterArrayByType(
                                  detail.body,
                                  "color_picker"
                                )[0].primary.color_name
                              : "Tangerine"
                          ),
                        }}
                      >
                        <div className={`col-12 ${styles.comparisonCol} p-0`}>
                          <div className="d-flex flex-grow-1">
                            <div
                              className={` ${styles.comparisonItemSM} justify-content-between`}
                              style={{
                                borderTopWidth:
                                  detail.membership_best_value === true
                                    ? 0
                                    : "",
                                borderRightWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderBottomWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderLeftWidth:
                                  detail.membership_best_value === true
                                    ? 2
                                    : "",
                                borderColor:
                                  detail.membership_best_value === true
                                    ? "red"
                                    : "",
                                borderStyle:
                                  detail.membership_best_value === true
                                    ? "solid"
                                    : "",
                              }}
                            >
                              <p
                                className={`m-0 ${styles.mobileComparisonLabel}`}
                              >
                                {d.fields[6].row_label ===
                                  "Special Experience" && (
                                  <span
                                    tabIndex={0}
                                    className={styles.spanLink}
                                    onClick={() =>
                                      showDynamicModal("specialExperience")
                                    }
                                  >
                                    {d.fields[6].row_label}
                                  </span>
                                )}
                                {d.fields[6].row_label !==
                                  "Special Experience" && d.fields[6].row_label}
                              </p>
                              <p className="m-0">
                                {mDetail[0].__typename ===
                                "PRISMIC_Membership_detail"
                                  ? renderCheckmark(detail.free_tShirt)
                                  : renderCheckmark(
                                      detail.vip_safari_and_tours_before_zoo_opens
                                    )}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                </div> // end container
              ) //end return
            }) // end map
            }{" "}
            {/* end jsx */}
          </section>
        )
      })}
      <Modal
        key="specialExperience"
        show={openModals["specialExperience"]}
        onHide={() => hideDynamicModal("specialExperience")}
        size="lg"
        dialogClassName={styles.modal}
      >
        <Modal.Header closeButton className={styles.modalHeader}></Modal.Header>
        <Modal.Body className={styles.modalBody}>
          <h2 className={styles.modalTitle}>Special Experience</h2>
          {renderAsHtml(special_experience)}
          <div className={styles.modalButton}>
            <Button
              text="Got it!"
              size="large"
              color="Orchid"
              onClick={() => hideDynamicModal("specialExperience")}
            />
          </div>
        </Modal.Body>
      </Modal>
      {/* Create any modals requested over the course of the render */}
      {neededModals.map((modal, i) => {
        return (
          <Modal
            key={modal.id}
            show={openModals[modal.id]}
            onHide={() => {
              hideDynamicModal(modal.id)
            }}
            size="lg"
            dialogClassName={styles.modal}
          >
            <Modal.Header
              closeButton
              className={styles.modalHeader}
            ></Modal.Header>
            <Modal.Body className={styles.modalBody}>
              <h2 className={styles.modalTitle}>{modal.heading}</h2>
              {modal.text}
              <div className={styles.modalButton}>
                <Button
                  text="Got it!"
                  size="large"
                  color="Orchid"
                  onClick={() => hideDynamicModal(modal.id)}
                />
              </div>
            </Modal.Body>
          </Modal>
        )
      })}
    </Layout>
  )
}

export default MembershipsLanding

export const query = graphql`
  {
    prismic {
      allMembership_landing_pages {
        edges {
          node {
            title
            hero
            description
            callout_heading
            callout_description
            special_experience
            meta_title
            meta_description
            _meta {
              id
            }
            body {
              ... on PRISMIC_Membership_landing_pageBodyMembership_comparison_section {
                type
                fields {
                  row_label
                }
                primary {
                  link_text
                  m_detail_1 {
                    ... on PRISMIC_Elite_membership_detail {
                      _meta {
                        id
                      }
                      title
                      plu
                      vip_golf_cart_tour_for_4
                      vip_safari_and_tours_before_zoo_opens
                      invitation_to_lion_s_pride_society_and_other_events
                      family___rides_membership
                      facility_rental_discount
                      extra_tickets___ride_wristbands
                      early_enrollment_for_summer_camps
                      brew_at_the_zoo___wine_zoo_tickets
                      body {
                        ... on PRISMIC_Elite_membership_detailBodyColor_picker {
                          type
                          primary {
                            color_name
                          }
                        }
                      }
                      membership_cta {
                        ... on PRISMIC__ExternalLink {
                          _linkType
                          url
                        }
                      }
                    }
                    ... on PRISMIC_Membership_detail {
                      _meta {
                        id
                      }
                      title
                      above_price_text
                      adults
                      body {
                        ... on PRISMIC_Membership_detailBodyColor_picker {
                          type
                          primary {
                            color_name
                          }
                        }
                      }
                      cta_text
                      membership_dropdown
                      cta_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_1
                      membership_cta_option_1_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_2
                      membership_cta_option_2_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_3
                      membership_cta_option_3_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_best_value
                      membership_best_value_text
                      discounts
                      unlimited_rides
                      extra_tickets_and_ride_wristbands
                      free_tShirt
                      full_price_plu
                      guests
                      plu
                      secondary_discount_full_price_plu
                      secondary_discount_plu
                      secondary_discount_text
                      sky_safari___boat_passes
                      train__tram__carousel
                      discounted_general_admission_tickets
                      payment_plan_info_text
                      gift_dropdown_options {
                        gift_dropdown_option_link {
                          ... on PRISMIC__ExternalLink {
                            target
                            _linkType
                            url
                          }
                        }
                        gift_dropdown_option_text
                      }
                      gift_dropdown_button
                      gift_cta_text
                      gift_cta_link {
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                    }
                    ... on PRISMIC_Image {
                      _linkType
                    }
                  }
                  m_detail_2 {
                    ... on PRISMIC_Elite_membership_detail {
                      _meta {
                        id
                      }
                      title
                      plu
                      vip_golf_cart_tour_for_4
                      vip_safari_and_tours_before_zoo_opens
                      invitation_to_lion_s_pride_society_and_other_events
                      family___rides_membership
                      facility_rental_discount
                      extra_tickets___ride_wristbands
                      early_enrollment_for_summer_camps
                      brew_at_the_zoo___wine_zoo_tickets
                      body {
                        ... on PRISMIC_Elite_membership_detailBodyColor_picker {
                          type
                          primary {
                            color_name
                          }
                        }
                      }
                      membership_cta {
                        ... on PRISMIC__ExternalLink {
                          _linkType
                          url
                        }
                      }
                    }
                    ... on PRISMIC_Membership_detail {
                      _meta {
                        id
                      }
                      title
                      above_price_text
                      adults
                      discounted_general_admission_tickets
                      body {
                        ... on PRISMIC_Membership_detailBodyColor_picker {
                          type
                          primary {
                            color_name
                          }
                        }
                      }
                      cta_text
                      membership_dropdown
                      cta_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_1
                      membership_cta_option_1_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_2
                      membership_cta_option_2_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_3
                      membership_cta_option_3_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_best_value
                      membership_best_value_text
                      discounts
                      unlimited_rides
                      extra_tickets_and_ride_wristbands
                      free_tShirt
                      full_price_plu
                      guests
                      plu
                      secondary_discount_full_price_plu
                      secondary_discount_plu
                      secondary_discount_text
                      sky_safari___boat_passes
                      train__tram__carousel
                      payment_plan_info_text
                      gift_dropdown_options {
                        gift_dropdown_option_text
                        gift_dropdown_option_link {
                          ... on PRISMIC__ExternalLink {
                            target
                            _linkType
                            url
                          }
                        }
                      }
                      gift_dropdown_button
                      gift_cta_text
                      gift_cta_link {
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                    }
                  }
                  m_detail_3 {
                    ... on PRISMIC_Elite_membership_detail {
                      _meta {
                        id
                      }
                      title
                      plu
                      vip_golf_cart_tour_for_4
                      vip_safari_and_tours_before_zoo_opens
                      invitation_to_lion_s_pride_society_and_other_events
                      family___rides_membership
                      facility_rental_discount
                      extra_tickets___ride_wristbands
                      early_enrollment_for_summer_camps
                      brew_at_the_zoo___wine_zoo_tickets
                      body {
                        ... on PRISMIC_Elite_membership_detailBodyColor_picker {
                          type
                          primary {
                            color_name
                          }
                        }
                      }
                      membership_cta {
                        ... on PRISMIC__ExternalLink {
                          _linkType
                          url
                        }
                      }
                    }
                    ... on PRISMIC_Membership_detail {
                      _meta {
                        id
                      }
                      title
                      above_price_text
                      discounted_general_admission_tickets
                      adults
                      body {
                        ... on PRISMIC_Membership_detailBodyColor_picker {
                          type
                          primary {
                            color_name
                          }
                        }
                      }
                      cta_text
                      membership_dropdown
                      cta_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_1
                      membership_cta_option_1_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_2
                      membership_cta_option_2_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_cta_option_3
                      membership_cta_option_3_link {
                        _linkType
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      membership_best_value
                      membership_best_value_text
                      discounts
                      unlimited_rides
                      extra_tickets_and_ride_wristbands
                      free_tShirt
                      full_price_plu
                      guests
                      plu
                      secondary_discount_full_price_plu
                      secondary_discount_plu
                      secondary_discount_text
                      sky_safari___boat_passes
                      train__tram__carousel
                      payment_plan_info_text
                      gift_dropdown_button
                      gift_cta_text
                      gift_cta_link {
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                      }
                      gift_dropdown_options {
                        gift_dropdown_option_text
                        gift_dropdown_option_link {
                          ... on PRISMIC__ExternalLink {
                            target
                            _linkType
                            url
                          }
                        }
                      }
                    }
                  }
                  membership_type_description
                  membership_types
                  link {
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC_Membership_landing_page {
                      hero
                      title
                      body {
                        ... on PRISMIC_Membership_landing_pageBodyColor_picker {
                          type
                          label
                          primary {
                            color_name
                          }
                        }
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_Membership_landing_pageBodyGift_callout {
                type
                label
                primary {
                  gift_callout_title
                  gift_callout_label
                  gift_callout_image
                  gift_callout_description
                  gift_callout_button_text
                  gift_callout_color_name
                  gift_callout_button_link {
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                  }
                  gift_callout_button_option_2_link {
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                  }
                  gift_callout_button_option_2_text
                  gift_callout_button_option_3_text
                  gift_callout_button_option_3_link {
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                  }
                  gift_callout_dropdown_text
                }
              }
            }
            membership_renewal_link {
              ... on PRISMIC_Renewal_page {
                _meta {
                  uid
                  id
                  type
                }
              }
            }
            membership_renewal_link_text
          }
        }
      }
      allSitewide_alerts {
        edges {
          node {
            alerts {
              sitewide_alert_learn_more_link {
                ... on PRISMIC__ExternalLink {
                  target
                  _linkType
                  url
                }
              }
              sitewide_alert_learn_more_text
            }
          }
        }
      }
    }
  }
`
