import React from "react"
// import Layout from "../../../components/layout/layout"
// import SEO from "../../../components/seo"
import Button from "../../../components/button/button-link"
import Link from "gatsby-link"
import styles from "./polar.module.css"
import a from "./snow.module.scss"
import Bear from "./polarbear"

const Polar404 = props => (
  <>
    <div className={`${styles.gradient}`}></div>
    <div className="row m-0">
      <div className="container">
        <div className={`${styles.contentWrapper} mb-4`}>
          <div className="col-lg-5 p-0">
            <h1 className={styles.title}>
              Sorry to leave you
              <br />
              out in the cold,
            </h1>
            <h2 className={styles.subhead}>
              but the page you're looking for doesn't exist.
            </h2>
            <div className={styles.message}>
              <p className="mt-4">
                Slide on back to our home page and try again, or{" "}
                <Link to="/about/contact-us/">
                  <strong>contact us</strong>
                </Link>{" "}
                for help
              </p>
            </div>
            <div className={`${styles.ctaBtn} ${styles.moveIcon} mt-4`}>
              <Button
                icon="arrowleft"
                text="Back to Home"
                url="/"
                size="large"
                isInverse={false}
                color="Tangerine"
              />
            </div>
          </div>
          <div className="col-lg-7 p-0">
            <Bear className={styles.bear} />
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
            <div className={a.snowflake}></div>
          </div>
        </div>
      </div>
    </div>
  </>
)

export default Polar404
