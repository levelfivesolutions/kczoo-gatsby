import React from "react"

const OtterLeft = props => (
  <>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="308.927"
      height="343.483"
      viewBox="0 0 308.927 343.483"
      className={props.className}
    >
      <defs>
        <clipPath id="clipPath">
          <path
            id="Path_11654"
            data-name="Path 11654"
            d="M80.278,129.505l1.312,3.006,3.22.623-2.453,2.176.4,3.255-2.828-1.66-2.972,1.388.706-3.2-2.239-2.4,3.264-.319Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-2">
          <path
            id="Path_11655"
            data-name="Path 11655"
            d="M89.662,0,88.074,2.869l-3.264.32,2.239,2.4-.706,3.2L89.314,7.4l2.829,1.66-.4-3.254,2.453-2.177-3.22-.623Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-3">
          <path
            id="Path_11656"
            data-name="Path 11656"
            d="M17.037,140.342l1.312,3.006,3.22.623-2.454,2.177.4,3.255-2.829-1.661-2.971,1.388.706-3.2-2.239-2.4,3.264-.318Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-4">
          <path
            id="Path_11657"
            data-name="Path 11657"
            d="M204.64,73.377l-3.264.318,2.238,2.4-.7,3.2,2.97-1.388,2.829,1.661-.4-3.255,2.453-2.177-3.22-.623-1.312-3.006Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-5">
          <path
            id="Path_11658"
            data-name="Path 11658"
            d="M60.4,169.351l-5.856.571,4.016,4.3L57.3,179.968l5.33-2.49,5.074,2.979-.721-5.839,4.4-3.9L65.605,169.6,63.251,164.2Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-6">
          <path
            id="Path_11659"
            data-name="Path 11659"
            d="M136.85,105.793l2.353,5.393,5.777,1.118-4.4,3.9.721,5.84-5.074-2.979-5.33,2.49,1.265-5.746-4.016-4.3,5.856-.572Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-7">
          <path
            id="Path_11660"
            data-name="Path 11660"
            d="M5.855,62.13,0,62.7,4.016,67,2.75,72.747l5.331-2.491,5.073,2.98-.721-5.84,4.4-3.905-5.778-1.118L8.7,56.981Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-8">
          <path
            id="Path_11661"
            data-name="Path 11661"
            d="M167.791,48.854l2.354,5.392,5.776,1.119-4.4,3.9.722,5.839-5.074-2.979-5.331,2.491,1.266-5.746-4.016-4.3L164.943,54Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-9">
          <path
            id="Path_11662"
            data-name="Path 11662"
            d="M57.554,34.6l2.354,5.392,5.776,1.118-4.4,3.9L62,50.852l-5.073-2.979L51.6,50.364l1.266-5.746-4.016-4.3,5.856-.572Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-10">
          <path
            id="Path_11663"
            data-name="Path 11663"
            d="M93.3,52.929l13.205.43,7.841-10.634,3.671,12.692,12.537,4.171L119.614,67l-.093,13.212-10.43-8.11L96.5,76.1l4.49-12.426Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-11">
          <path
            id="Path_11664"
            data-name="Path 11664"
            d="M29.9,80.725l11.205,11.4,15.74-2.77-7.376,14.178,7.5,14.114L41.208,115.01,30.1,126.5,27.735,110.7l-14.362-7.011,14.3-7.135Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-12">
          <path
            id="Path_11665"
            data-name="Path 11665"
            d="M108.482,137.47l8.479,8.624,11.91-2.1-5.581,10.729,5.674,10.68-11.928-1.993-8.4,8.7-1.791-11.961-10.868-5.3,10.822-5.4Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-13">
          <path
            id="Path_11666"
            data-name="Path 11666"
            d="M176.82,104.991l11.2,11.4,15.74-2.77-7.375,14.178,7.5,14.114-15.764-2.634-11.106,11.493-2.366-15.806-14.362-7.011,14.3-7.135Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-14">
          <path
            id="Path_11667"
            data-name="Path 11667"
            d="M159.57,168.427l-5.855.572,4.015,4.3-1.265,5.746,5.331-2.491,5.073,2.98-.722-5.84,4.4-3.9-5.777-1.118-2.353-5.392Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-15">
          <rect
            id="Rectangle_1413"
            data-name="Rectangle 1413"
            width="203.521"
            height="150.858"
            fill="none"
          />
        </clipPath>
        <clipPath id="clipPath-16">
          <rect
            id="Rectangle_1412"
            data-name="Rectangle 1412"
            width="203.52"
            height="150.858"
            fill="none"
          />
        </clipPath>
        <clipPath id="clipPath-17">
          <rect
            id="Rectangle_1409"
            data-name="Rectangle 1409"
            width="2.244"
            height="1.608"
            fill="none"
          />
        </clipPath>
        <clipPath id="clipPath-18">
          <rect
            id="Rectangle_1410"
            data-name="Rectangle 1410"
            width="2.21"
            height="1.41"
            fill="none"
          />
        </clipPath>
        <clipPath id="clipPath-19">
          <rect
            id="Rectangle_1411"
            data-name="Rectangle 1411"
            width="6.104"
            height="5.509"
            fill="none"
          />
        </clipPath>
        <radialGradient
          id="radial-gradient"
          cx="0.409"
          cy="0.224"
          r="0.412"
          gradientTransform="matrix(0.963, -0.269, 0.269, 0.963, -0.045, 0.118)"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stop-color="#fff" stop-opacity="0.502" />
          <stop offset="1" stop-color="#a7d6ff" stop-opacity="0.502" />
        </radialGradient>
      </defs>
      <g id="Group_2409" data-name="Group 2409" transform="translate(98.167)">
        <g id="Group_2382" data-name="Group 2382">
          <g id="Group_2381" data-name="Group 2381" clipPath="url(#clipPath)">
            <rect
              id="Rectangle_1419"
              data-name="Rectangle 1419"
              width="10.527"
              height="10.25"
              transform="matrix(0.991, -0.136, 0.136, 0.991, 74.208, 129.672)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2384" data-name="Group 2384">
          <g id="Group_2383" data-name="Group 2383" clipPath="url(#clipPath-2)">
            <rect
              id="Rectangle_1420"
              data-name="Rectangle 1420"
              width="10.527"
              height="10.25"
              transform="translate(83.592 0.167) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2386" data-name="Group 2386">
          <g id="Group_2385" data-name="Group 2385" clipPath="url(#clipPath-3)">
            <rect
              id="Rectangle_1421"
              data-name="Rectangle 1421"
              width="10.527"
              height="10.251"
              transform="matrix(0.991, -0.136, 0.136, 0.991, 10.967, 140.509)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2388" data-name="Group 2388">
          <g id="Group_2387" data-name="Group 2387" clipPath="url(#clipPath-4)">
            <rect
              id="Rectangle_1422"
              data-name="Rectangle 1422"
              width="10.527"
              height="10.251"
              transform="translate(200.158 70.674) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2390" data-name="Group 2390">
          <g id="Group_2389" data-name="Group 2389" clipPath="url(#clipPath-5)">
            <rect
              id="Rectangle_1423"
              data-name="Rectangle 1423"
              width="18.885"
              height="18.388"
              transform="translate(52.361 164.502) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2392" data-name="Group 2392">
          <g id="Group_2391" data-name="Group 2391" clipPath="url(#clipPath-6)">
            <rect
              id="Rectangle_1424"
              data-name="Rectangle 1424"
              width="18.885"
              height="18.389"
              transform="translate(125.959 106.092) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2394" data-name="Group 2394">
          <g id="Group_2393" data-name="Group 2393" clipPath="url(#clipPath-7)">
            <rect
              id="Rectangle_1425"
              data-name="Rectangle 1425"
              width="18.885"
              height="18.389"
              transform="matrix(0.991, -0.136, 0.136, 0.991, -2.186, 57.28)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2396" data-name="Group 2396">
          <g id="Group_2395" data-name="Group 2395" clipPath="url(#clipPath-8)">
            <rect
              id="Rectangle_1426"
              data-name="Rectangle 1426"
              width="18.884"
              height="18.388"
              transform="translate(156.901 49.153) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2398" data-name="Group 2398">
          <g id="Group_2397" data-name="Group 2397" clipPath="url(#clipPath-9)">
            <rect
              id="Rectangle_1427"
              data-name="Rectangle 1427"
              width="18.884"
              height="18.388"
              transform="translate(46.664 34.897) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2400" data-name="Group 2400">
          <g
            id="Group_2399"
            data-name="Group 2399"
            clipPath="url(#clipPath-10)"
          >
            <rect
              id="Rectangle_1428"
              data-name="Rectangle 1428"
              width="50.964"
              height="50.876"
              transform="translate(77.241 71.142) rotate(-60.534)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2402" data-name="Group 2402">
          <g
            id="Group_2401"
            data-name="Group 2401"
            clipPath="url(#clipPath-11)"
          >
            <rect
              id="Rectangle_1429"
              data-name="Rectangle 1429"
              width="55.03"
              height="56.481"
              transform="translate(0.631 84.6) rotate(-16.914)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2404" data-name="Group 2404">
          <g
            id="Group_2403"
            data-name="Group 2403"
            clipPath="url(#clipPath-12)"
          >
            <rect
              id="Rectangle_1430"
              data-name="Rectangle 1430"
              width="41.642"
              height="42.741"
              transform="translate(86.33 140.402) rotate(-16.914)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2406" data-name="Group 2406">
          <g
            id="Group_2405"
            data-name="Group 2405"
            clipPath="url(#clipPath-13)"
          >
            <rect
              id="Rectangle_1431"
              data-name="Rectangle 1431"
              width="55.03"
              height="56.481"
              transform="translate(147.548 108.866) rotate(-16.914)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2408" data-name="Group 2408">
          <g
            id="Group_2407"
            data-name="Group 2407"
            clipPath="url(#clipPath-14)"
          >
            <rect
              id="Rectangle_1432"
              data-name="Rectangle 1432"
              width="18.884"
              height="18.389"
              transform="translate(151.529 163.578) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
      </g>
      <g
        id="Group_2367"
        data-name="Group 2367"
        transform="matrix(0.616, -0.788, 0.788, 0.616, 0, 250.605)"
      >
        <g id="Group_2366" data-name="Group 2366" clipPath="url(#clipPath-15)">
          <g id="Group_2365" data-name="Group 2365">
            <g
              id="Group_2364"
              data-name="Group 2364"
              clipPath="url(#clipPath-16)"
            >
              <path
                id="Path_11577"
                data-name="Path 11577"
                d="M510.577,317.059s43.945,8.45,65.59-4.6,11.687-40.1,3.793-47.552-16.234-8.036-15.678-9.744,24.128,3.015,32.245,25.059,1.779,41.179-22.238,52.071-66.936,8.779-76.054,2.219,1.577-17.9,12.342-17.457"
                transform="translate(-396.356 -204.148)"
                fill="#684f3e"
              />
              <path
                id="Path_11578"
                data-name="Path 11578"
                d="M89.917,50.542A24.736,24.736,0,0,0,75.24,54.49c-6.894,4.67-7.333,15.122-20.158,13.6-7.116,11.7-7.639,14.125-10.456,23.02s2.521,18.087,17.346,20.607,36.515,8.154,37.308,20.9-11.512,31.133-50.651,25.351S-.147,122.907,0,104.784.743,64.273,12.01,47.521,29.948,28.546,29.948,28.546s-3.521-8.875,15.289-13.11,22.512-2.887,22.512-2.887a6.371,6.371,0,0,1,.157,6.816c10.224-1.79,31.453,4.626,38.363,9.774s-16.352,21.4-16.352,21.4"
                transform="translate(0 -9.889)"
                fill="#8e6549"
              />
              <path
                id="Path_11579"
                data-name="Path 11579"
                d="M107.96,78.249S96.73,74.721,78.94,80.169,49.252,96.611,43.915,118.4s16.679,40.264,25.129,44.378,16.011,17.012.222,21.793-45.793-14.224-49.813-49.693c-4.766-42.05,22.984-65.9,50.112-71.85S121.552,70.1,121.552,70.1Z"
                transform="translate(-15.152 -49.243)"
                fill="#ddd7d3"
              />
              <path
                id="Path_11580"
                data-name="Path 11580"
                d="M441.463,82.121s-2.493.706-5.384,4.515a18.49,18.49,0,0,0-3.484,7.155l8.718,1,5.64-10.758Z"
                transform="translate(-346.532 -65.783)"
                fill="#ccc6c2"
              />
              <path
                id="Path_11581"
                data-name="Path 11581"
                d="M659.917,73.284s7.979-2.753,11.134,2.327.02,6.976-1.834,8.246a8.773,8.773,0,0,0-3.344,3.555c-1.276-3.054-5.956-14.128-5.956-14.128"
                transform="translate(-528.63 -58.108)"
                fill="#4f3323"
              />
              <path
                id="Path_11582"
                data-name="Path 11582"
                d="M558.948,93.154l13.51,13.125a66.24,66.24,0,0,0,7.835-10.4c3.962-6.418,5.515-13.834,5.127-16.8s-3.326-9.28-12.443-9.762c-10.141,2.406-15.337,19.861-14.028,23.843"
                transform="translate(-447.589 -55.522)"
                fill="#8e6549"
              />
              <path
                id="Path_11583"
                data-name="Path 11583"
                d="M486,81.54l-7.531-7.317a16.239,16.239,0,0,1,7.76-4.743c4.86-1.252,14.27-6.072,22.439-3.389-5.209,3.287-9.238,7.386-11.649,17.591C494.681,85.752,486,81.54,486,81.54"
                transform="translate(-383.279 -52.301)"
                fill="#ddd7d3"
              />
              <path
                id="Path_11584"
                data-name="Path 11584"
                d="M522.879,116.168a5.074,5.074,0,0,1,6.029-2.731c4.752,1.287,3.074,8.573,3.074,8.573l-7.311,1.772-2.773-2.626s.082-3.57.981-4.988"
                transform="translate(-418.069 -90.695)"
                fill="#f7f1ed"
              />
              <path
                id="Path_11585"
                data-name="Path 11585"
                d="M610.44,105.639s3,1.883,2.629,4.208a2.8,2.8,0,0,1-4.122,2.248,16.756,16.756,0,0,0-2.771-1.323c-.368-.073-.932-.131-1.109-.561s.223-.894.1-1.535a2.869,2.869,0,0,1,.988-3.251c1.977-1.49,4.289.213,4.289.213"
                transform="translate(-484.654 -83.962)"
                fill="#af8569"
              />
              <path
                id="Path_11586"
                data-name="Path 11586"
                d="M612.021,109.622s3.593,1.94,1.687,4.274-4.643-.221-5.192-.187-.893-.144-.771-.749-.884-5.413,4.277-3.338"
                transform="translate(-486.817 -87.4)"
                fill="#282623"
              />
              <path
                id="Path_11587"
                data-name="Path 11587"
                d="M646.488,137.754a26.825,26.825,0,0,1-1.574,2.657c-.9,1.33-.852,2.234.371,2.53s2.152-2.056,2.213-3.275-.543-2.777-1.01-1.912"
                transform="translate(-516.115 -110.148)"
                fill="#af8569"
              />
              <path
                id="Path_11588"
                data-name="Path 11588"
                d="M538.235,160.83l-5.343-5.191a4.849,4.849,0,0,1,1.378-.754c.821-.287,2.219.269,3.3-.257s1.959-1.352,3.376-.389-.811,4.638-2.709,6.592"
                transform="translate(-426.876 -123.198)"
                fill="#282623"
              />
              <path
                id="Path_11589"
                data-name="Path 11589"
                d="M685.11,87.169a6.315,6.315,0,0,0-.387-1.794,12.848,12.848,0,0,0-1.485-2.885c-1.1-1.463-1.819-2.6-.306-3a4.832,4.832,0,0,1,5.5,3.36c.97,3.03-.09,3.763-1.525,4.96-1.2,1-1.708.434-1.792-.645"
                transform="translate(-546.375 -63.598)"
                fill="#82523a"
              />
              <path
                id="Path_11590"
                data-name="Path 11590"
                d="M447.336,292.543s-2.521,8.056,2.647,11.062,6.974-.182,8.19-2.071a8.774,8.774,0,0,1,3.457-3.446c-3.09-1.187-14.294-5.545-14.294-5.545"
                transform="translate(-357.848 -234.343)"
                fill="#4f3323"
              />
              <path
                id="Path_11591"
                data-name="Path 11591"
                d="M465.415,188.529l13.51,13.125a66.24,66.24,0,0,1-10.174,8.132c-6.3,4.146-13.669,5.912-16.647,5.61s-9.372-3.057-10.117-12.156c2.113-10.206,19.41-15.9,23.428-14.711"
                transform="translate(-354.056 -150.896)"
                fill="#8e6549"
              />
              <path
                id="Path_11592"
                data-name="Path 11592"
                d="M452.9,117.507l-7.531-7.317a16.239,16.239,0,0,0-4.517,7.894c-1.112,4.894-5.657,14.44-2.74,22.527,3.136-5.3,7.117-9.447,17.247-12.152,2-2.4-2.458-10.953-2.458-10.953"
                transform="translate(-350.185 -88.268)"
                fill="#ddd7d3"
              />
              <path
                id="Path_11593"
                data-name="Path 11593"
                d="M484.8,154.221a4.915,4.915,0,0,0-2.608,5.971c1.464,4.6,8.867,2.736,8.867,2.736l1.586-7.193-2.773-2.626s-3.651.191-5.073,1.112"
                transform="translate(-386.048 -122.649)"
                fill="#f7f1ed"
              />
              <path
                id="Path_11594"
                data-name="Path 11594"
                d="M478.066,241.646s1.969,2.946,4.282,2.507a2.8,2.8,0,0,0,2.128-4.185,16.767,16.767,0,0,1-1.4-2.732c-.084-.366-.158-.928-.592-1.092s-.887.249-1.531.14a2.869,2.869,0,0,0-3.221,1.082c-1.432,2.019.337,4.281.337,4.281"
                transform="translate(-382.239 -189.133)"
                fill="#af8569"
              />
              <path
                id="Path_11595"
                data-name="Path 11595"
                d="M482,243.228s2.043,3.535,4.321,1.563-.355-4.634-.337-5.185-.17-.889-.771-.749-5.436-.727-3.213,4.371"
                transform="translate(-385.64 -191.312)"
                fill="#282623"
              />
              <path
                id="Path_11596"
                data-name="Path 11596"
                d="M511.159,276.829a26.808,26.808,0,0,0,2.61-1.65c1.3-.943,2.208-.916,2.54.3s-1.993,2.211-3.21,2.307-2.792-.463-1.94-.955"
                transform="translate(-409.272 -219.901)"
                fill="#af8569"
              />
              <path
                id="Path_11597"
                data-name="Path 11597"
                d="M531.6,168.257l-5.343-5.191a4.847,4.847,0,0,0-.714,1.4c-.263.829.333,2.21-.162,3.3s-1.295,2-.292,3.385,4.612-.945,6.511-2.9"
                transform="translate(-420.238 -130.625)"
                fill="#282623"
              />
              <path
                id="Path_11598"
                data-name="Path 11598"
                d="M461.613,317.3a6.314,6.314,0,0,1-1.8-.335,12.834,12.834,0,0,1-2.926-1.4c-1.494-1.055-2.647-1.744-3-.219a4.832,4.832,0,0,0,3.517,5.4c3.057.883,3.759-.2,4.914-1.667.963-1.224.385-1.72-.7-1.773"
                transform="translate(-363.52 -251.9)"
                fill="#82523a"
              />
              <g
                id="Group_2357"
                data-name="Group 2357"
                transform="translate(97.315 51.927)"
                opacity="0.36"
              >
                <g id="Group_2356" data-name="Group 2356">
                  <g
                    id="Group_2355"
                    data-name="Group 2355"
                    clipPath="url(#clipPath-17)"
                  >
                    <path
                      id="Path_11599"
                      data-name="Path 11599"
                      d="M490.543,262.508s-1.51-.873-1.381-1.342,1.791.242,2.113.761-.013.891-.732.581"
                      transform="translate(-489.154 -261.014)"
                      fill="#fff"
                    />
                  </g>
                </g>
              </g>
              <g
                id="Group_2360"
                data-name="Group 2360"
                transform="translate(123.18 25.59)"
                opacity="0.36"
              >
                <g id="Group_2359" data-name="Group 2359">
                  <g
                    id="Group_2358"
                    data-name="Group 2358"
                    clipPath="url(#clipPath-18)"
                  >
                    <path
                      id="Path_11600"
                      data-name="Path 11600"
                      d="M620.272,129.866s-1.228-.618-1.1-1.087,1.791.241,2.113.761-.3.636-1.014.326"
                      transform="translate(-619.164 -128.626)"
                      fill="#fff"
                    />
                  </g>
                </g>
              </g>
              <g
                id="Group_2363"
                data-name="Group 2363"
                transform="translate(105.516 34.08)"
                opacity="0.36"
              >
                <g id="Group_2362" data-name="Group 2362">
                  <g
                    id="Group_2361"
                    data-name="Group 2361"
                    clipPath="url(#clipPath-19)"
                  >
                    <path
                      id="Path_11601"
                      data-name="Path 11601"
                      d="M535.89,172.335a20.947,20.947,0,0,1-3.487,3.589c-1.905,1.482-2.139.712-1.985.474s1.232-.9,2.71-2.018a21.954,21.954,0,0,0,3.078-2.849c.5-.571.275.009-.316.8"
                      transform="translate(-530.379 -171.302)"
                      fill="#fff"
                    />
                  </g>
                </g>
              </g>
              <path
                id="Path_11602"
                data-name="Path 11602"
                d="M487.446,108.331a9.225,9.225,0,0,0-5.474,3,8.133,8.133,0,0,0-2.571,5.279c-.155.186-.758-3.5,1.747-6.08s6.7-2.692,6.3-2.2"
                transform="translate(-383.886 -86.595)"
                fill="#ccc6c2"
              />
              <path
                id="Path_11603"
                data-name="Path 11603"
                d="M498.972,123.985s-3.417-2.426-5.8.157-.288,5.9-.288,5.9a10.7,10.7,0,0,1,5.014-1.177,15.509,15.509,0,0,1,1.076-4.882"
                transform="translate(-394.143 -98.491)"
                fill="#893729"
              />
              <path
                id="Path_11604"
                data-name="Path 11604"
                d="M523.2,135.14l-2.46-2.281a17.451,17.451,0,0,1,.32-2.453,9.951,9.951,0,0,1,.91-2.259,13.2,13.2,0,0,0-.769,2.447,4.953,4.953,0,0,0,.031,2.243,27.762,27.762,0,0,0,2.151,2.278c.119.11-.183.025-.183.025"
                transform="translate(-417.142 -102.653)"
                fill="#282623"
              />
              <path
                id="Path_11605"
                data-name="Path 11605"
                d="M503.633,154.116l-2.46-2.281a19.6,19.6,0,0,0-2.541.393,10.826,10.826,0,0,0-2.318.953,14.5,14.5,0,0,1,2.519-.824,5.573,5.573,0,0,1,2.334-.049,28.172,28.172,0,0,1,2.446,1.984c.119.11.02-.177.02-.177"
                transform="translate(-397.575 -121.628)"
                fill="#282623"
              />
              <path
                id="Path_11606"
                data-name="Path 11606"
                d="M519.281,132.642s-1.079-.505-1.186-.333.84,1.4.84,1.4Z"
                transform="translate(-415.016 -105.958)"
                fill="#fff"
              />
              <path
                id="Path_11607"
                data-name="Path 11607"
                d="M502.526,149.625s-1.326-1.141-1.457-1,.364,1.347.364,1.347Z"
                transform="translate(-401.366 -119.05)"
                fill="#fff"
              />
              <path
                id="Path_11608"
                data-name="Path 11608"
                d="M496.624,124.384a4.9,4.9,0,0,1-.2,1.515c-.227.619-.851.451-.991.523s.273.692-.239,1.12-1.619.719-1.836.521,0-1.488.951-2.591,2.221-1.4,2.315-1.088"
                transform="translate(-395.145 -99.546)"
                fill="#af4537"
              />
              <path
                id="Path_11609"
                data-name="Path 11609"
                d="M722.719,531.392s-1.188,2.654,6.221,3.528c5.436.641,15.375-.1,17.77-1.165s.111-1.457-1.643-1.238a73.319,73.319,0,0,1-7.465.461c.167-.146,7.492-.777,8.579-.971s1.81-.9-.613-1.408-7.632-.17-8.133-.218a26.578,26.578,0,0,1,5.236-.34c2.423.049,3.259.1,3.287-.267s-2.006-.922-3.732-1.262-5.153-.024-5.013-.219a21.49,21.49,0,0,1,5.236,0c1.42.219,3.593.68,2.34-.315s-4.818-1.335-8.8-1.214-13.193.324-13.267,4.629"
                transform="translate(-578.881 -421.948)"
                fill="#3a3835"
              />
              <path
                id="Path_11610"
                data-name="Path 11610"
                d="M741,584.164s-2.217-1.882,4.091-5.866c4.628-2.923,13.913-6.543,16.535-6.613s.729,1.266-.948,1.826a73.307,73.307,0,0,0-6.934,2.8c.214.059,7.1-2.531,8.159-2.824s2.021.03.054,1.535-6.812,3.445-7.243,3.7a26.569,26.569,0,0,0,4.871-1.952c2.165-1.089,2.9-1.493,3.08-1.177s-1.411,1.7-2.823,2.749-4.638,2.244-4.429,2.359a21.472,21.472,0,0,0,4.724-2.258c1.187-.81,2.949-2.163,2.247-.724s-3.772,3.283-7.417,4.891-12.042,5.4-13.966,1.546"
                transform="translate(-593.279 -457.95)"
                fill="#3a3835"
              />
              <path
                id="Path_11611"
                data-name="Path 11611"
                d="M232.509,2.66s1.713,2.254-6.164,4.451c-5.78,1.612-16.653,2.777-19.417,2.228S206.573,8,208.507,7.877s8.356-.857,8.152-.962-8.233.673-9.44.694-2.1-.5.437-1.427,8.231-1.582,8.766-1.721a33.739,33.739,0,0,0-5.721.66c-2.615.5-3.511.7-3.6.364s2.022-1.234,3.836-1.873,5.572-.984,5.39-1.139a27.346,27.346,0,0,0-5.667.977c-1.5.469-3.779,1.3-2.583.142s5-2.144,9.33-2.774,14.329-2.159,15.1,1.843"
                transform="translate(-164.76 0)"
                fill="#3a3835"
              />
              <path
                id="Path_11612"
                data-name="Path 11612"
                d="M277.993,500.361s6.662,1.37,18.06-1.077,23.184-4.559,32.858-2.78,14.788,6.894,22.238,7.45,12.009,3.892,5.115,5.226-18.124,2.335-23.572,4.781-21.237,3.892-22.905,9.34,13.454,5.782,21.571,4.448,22.46-6.338,25.907-7.783,7.561,3.669.889,6.338-18.458,7.45-26.685,12.565-36.62,13.217-70.458,6.849c30.059-9.481,16.984-45.356,16.984-45.356"
                transform="translate(-209.083 -397.198)"
                fill="#8e6549"
              />
              <path
                id="Path_11613"
                data-name="Path 11613"
                d="M270.935,84.316s4.156,6.005,4.823,15.915-14.344,11.133-14.344,11.133-.959-13.565,2.3-19.9,7.22-7.146,7.22-7.146"
                transform="translate(-209.302 -67.542)"
                fill="#8e6549"
              />
              <path
                id="Path_11614"
                data-name="Path 11614"
                d="M204.27,77.6s.253-3.62-7.719-2.4c-5.849.9-15.841,4.876-17.923,6.926s.42,1.819,2.152,1.009a66.734,66.734,0,0,1,7.539-2.875c-.119.23-7.452,3.27-8.5,3.844s-1.54,1.661,1.15,1.536,7.943-2.153,8.479-2.249a24.246,24.246,0,0,1-5.283,2.037c-2.52.69-3.4.889-3.3,1.344s2.41.51,4.318.392,5.33-1.564,5.257-1.283-3.86,1.448-5.407,1.62-3.96.279-2.3,1.111,5.466.145,9.534-1.236,13.5-4.478,12-9.777"
                transform="translate(-142.46 -60.036)"
                fill="#3a3835"
              />
              <path
                id="Path_11615"
                data-name="Path 11615"
                d="M261.336,130.639a58.378,58.378,0,0,0-1.38,12.088c-.074,6.82-.993,15.374,2.9,20.122l-.181.3s-2.117-.888-2.86-6.966a99.834,99.834,0,0,1,0-18.643c.406-3.336,1.145-6.751,1.145-6.751Z"
                transform="translate(-207.77 -104.649)"
                fill="#4f3323"
              />
              <path
                id="Path_11616"
                data-name="Path 11616"
                d="M333.189,151.225s1.393,5.11,2.227,8.279a28.637,28.637,0,0,1,.889,6,18.9,18.9,0,0,0-.723-7.142c-1.167-4.644-1.823-7.293-1.823-7.293Z"
                transform="translate(-266.903 -121.019)"
                fill="#4f3323"
              />
            </g>
          </g>
        </g>
      </g>
      <ellipse
        id="Ellipse_134"
        data-name="Ellipse 134"
        cx="47.495"
        cy="36.443"
        rx="47.495"
        ry="36.443"
        transform="matrix(0.259, 0.966, -0.966, 0.259, 125.582, 130.721)"
        fill="url(#radial-gradient)"
      />
    </svg>
  </>
)
export default OtterLeft
