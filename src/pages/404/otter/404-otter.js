import React from "react"
// import Layout from "../../../components/layout/layout"
// import SEO from "../../../components/seo"
import Button from "../../../components/button/button-link"
import Link from "gatsby-link"
import styles from "./otter.module.css"
import OtterLeft from "./otterLeft"
import RocketLeft from "./rocketLeft"
import OtterMain from "./otterMain"
import SpaceTop from "./spaceTop"
import PlanetTop from "./planetTop"
import OtterRight from "./otterRight"
import Stars from "./stars"

const Otter404 = props => (
  <div className="row m-0">
    <div className="container">
      <div className={`${styles.contentWrapper}`}>
        <div className="col-lg-10 p-0 d-flex flex-column align-items-center justify-content-center p-0">
          <h1 className={styles.title}>OH NO!</h1>
          <h2 className={styles.subhead}>You're lost in otter space!</h2>
          <div className={`${styles.moveIcon} mt-4`}>
            <Button
              icon="arrowleft"
              text="Back to Home"
              url="/"
              size="large"
              isInverse={false}
              color="Tangerine"
            />
          </div>
          <div className={styles.message}>
            <p className="mt-4">
              The page you're looking for doesn't exist in our space-time
              continuum.
            </p>
            <p>
              Jet on back to our home page and try again, or{" "}
              <Link to="/about/contact-us/">
                <strong>contact us</strong>
              </Link>{" "}
              for help
            </p>
          </div>
        </div>
      </div>
      <OtterLeft className={styles.otterLeft} />
      <RocketLeft className={styles.rocketLeft} />
      <div className={styles.rightWrapper}>
        <OtterMain className={styles.otterMain} />
        <PlanetTop className={styles.planetTop} />
        <SpaceTop className={styles.spaceTop} />
        <OtterRight className={styles.otterRight} />
        <Stars className={styles.starsRight} />
        <Stars className={styles.starsLeft} />
      </div>
    </div>
  </div>
)

export default Otter404
