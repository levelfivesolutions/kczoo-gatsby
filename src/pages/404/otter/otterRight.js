import React from "react"

const OtterRight = props => (
  <>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="3616.979"
      height="393.456"
      viewBox="0 0 3616.979 393.456"
      className={props.className}
    >
      <defs>
        <clipPath id="clip-path">
          <rect
            id="Rectangle_1418"
            data-name="Rectangle 1418"
            width="181.093"
            height="222.528"
            fill="none"
          />
        </clipPath>
        <clipPath id="clip-path-2">
          <rect
            id="Rectangle_1417"
            data-name="Rectangle 1417"
            width="181.092"
            height="222.528"
            fill="none"
          />
        </clipPath>
        <clipPath id="clip-path-3">
          <rect
            id="Rectangle_1414"
            data-name="Rectangle 1414"
            width="1.026"
            height="2.26"
            fill="none"
          />
        </clipPath>
        <clipPath id="clip-path-4">
          <rect
            id="Rectangle_1415"
            data-name="Rectangle 1415"
            width="1.029"
            height="2.133"
            fill="none"
          />
        </clipPath>
        <clipPath id="clip-path-5">
          <rect
            id="Rectangle_1416"
            data-name="Rectangle 1416"
            width="7.291"
            height="1.13"
            fill="none"
          />
        </clipPath>
        <radialGradient
          id="radial-gradient"
          cx="0.409"
          cy="0.224"
          r="0.412"
          gradientTransform="matrix(0.963, -0.269, 0.269, 0.963, -0.045, 0.118)"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stop-color="#fff" stop-opacity="0.502" />
          <stop offset="1" stop-color="#a7d6ff" stop-opacity="0.502" />
        </radialGradient>
        <clipPath id="clip-path-6">
          <path
            id="Path_11654"
            data-name="Path 11654"
            d="M80.278,129.505l1.312,3.006,3.22.623-2.453,2.176.4,3.255-2.828-1.66-2.972,1.388.706-3.2-2.239-2.4,3.264-.319Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-7">
          <path
            id="Path_11655"
            data-name="Path 11655"
            d="M89.662,0,88.074,2.869l-3.264.32,2.239,2.4-.706,3.2L89.314,7.4l2.829,1.66-.4-3.254,2.453-2.177-3.22-.623Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-8">
          <path
            id="Path_11656"
            data-name="Path 11656"
            d="M17.037,140.342l1.312,3.006,3.22.623-2.454,2.177.4,3.255-2.829-1.661-2.971,1.388.706-3.2-2.239-2.4,3.264-.318Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-9">
          <path
            id="Path_11657"
            data-name="Path 11657"
            d="M204.64,73.377l-3.264.318,2.238,2.4-.7,3.2,2.97-1.388,2.829,1.661-.4-3.255,2.453-2.177-3.22-.623-1.312-3.006Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-10">
          <path
            id="Path_11658"
            data-name="Path 11658"
            d="M60.4,169.351l-5.856.571,4.016,4.3L57.3,179.968l5.33-2.49,5.074,2.979-.721-5.839,4.4-3.9L65.605,169.6,63.251,164.2Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-11">
          <path
            id="Path_11659"
            data-name="Path 11659"
            d="M136.85,105.793l2.353,5.393,5.777,1.118-4.4,3.9.721,5.84-5.074-2.979-5.33,2.49,1.265-5.746-4.016-4.3,5.856-.572Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-12">
          <path
            id="Path_11660"
            data-name="Path 11660"
            d="M5.855,62.13,0,62.7,4.016,67,2.75,72.747l5.331-2.491,5.073,2.98-.721-5.84,4.4-3.905-5.778-1.118L8.7,56.981Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-13">
          <path
            id="Path_11661"
            data-name="Path 11661"
            d="M167.791,48.854l2.354,5.392,5.776,1.119-4.4,3.9.722,5.839-5.074-2.979-5.331,2.491,1.266-5.746-4.016-4.3L164.943,54Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-14">
          <path
            id="Path_11662"
            data-name="Path 11662"
            d="M57.554,34.6l2.354,5.392,5.776,1.118-4.4,3.9L62,50.852l-5.073-2.979L51.6,50.364l1.266-5.746-4.016-4.3,5.856-.572Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-15">
          <path
            id="Path_11663"
            data-name="Path 11663"
            d="M93.3,52.929l13.205.43,7.841-10.634,3.671,12.692,12.537,4.171L119.614,67l-.093,13.212-10.43-8.11L96.5,76.1l4.49-12.426Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-16">
          <path
            id="Path_11664"
            data-name="Path 11664"
            d="M29.9,80.725l11.205,11.4,15.74-2.77-7.376,14.178,7.5,14.114L41.208,115.01,30.1,126.5,27.735,110.7l-14.362-7.011,14.3-7.135Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-17">
          <path
            id="Path_11665"
            data-name="Path 11665"
            d="M108.482,137.47l8.479,8.624,11.91-2.1-5.581,10.729,5.674,10.68-11.928-1.993-8.4,8.7-1.791-11.961-10.868-5.3,10.822-5.4Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-18">
          <path
            id="Path_11666"
            data-name="Path 11666"
            d="M176.82,104.991l11.2,11.4,15.74-2.77-7.375,14.178,7.5,14.114-15.764-2.634-11.106,11.493-2.366-15.806-14.362-7.011,14.3-7.135Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clip-path-19">
          <path
            id="Path_11667"
            data-name="Path 11667"
            d="M159.57,168.427l-5.855.572,4.015,4.3-1.265,5.746,5.331-2.491,5.073,2.98-.722-5.84,4.4-3.9-5.777-1.118-2.353-5.392Z"
            fill="#fdc843"
          />
        </clipPath>
      </defs>
      <g
        id="Group_2380"
        data-name="Group 2380"
        transform="translate(3437.237) rotate(7)"
      >
        <g id="Group_2379" data-name="Group 2379" clip-path="url(#clip-path)">
          <g
            id="Group_2378"
            data-name="Group 2378"
            transform="translate(0 0.001)"
          >
            <g
              id="Group_2377"
              data-name="Group 2377"
              clip-path="url(#clip-path-2)"
            >
              <path
                id="Path_11617"
                data-name="Path 11617"
                d="M4.834,39.487s10.944-3.944,18-11.019,11.188-16.8,12.91-19.536,4.961-4.587,5.705-5.815-.491-3.69-2.387-3-5.053.943-6.95,2.545-4.932,7.013-9.088,8.057-13.494.324-18.055-4.3-8.361,21.869-.138,33.061"
                transform="translate(109.643 62.749)"
                fill="#684f3e"
              />
              <path
                id="Path_11618"
                data-name="Path 11618"
                d="M15.663.53s-1.3-2.382-6.206,2.955C5.857,7.4.7,15.6.123,18.067s1.023.883,1.868-.613,3.929-6.321,3.942-6.11-3.723,6.3-4.2,7.263-.369,1.912,1.4.288S7.644,13,7.968,12.631a27.146,27.146,0,0,1-2.753,4.3c-1.428,1.875-1.944,2.5-1.688,2.725s1.84-1.07,3.085-2.24S9.589,13.381,9.654,13.6a21.955,21.955,0,0,1-3.007,4.109c-.979,1-2.571,2.449-1.108,2.009S9.3,16.662,11.5,13.47,18.834,2.94,15.663.53"
                transform="translate(0 197.524)"
                fill="#3a3835"
              />
              <path
                id="Path_11619"
                data-name="Path 11619"
                d="M47.9,3.535s3.376,11.85-8.137,20.639S18.489,31.959,12.1,35.013,5.352,40.2,2.164,40.579s-2.891-4.3,1.132-8.294S18.153,25.133,20.822,21.5A47,47,0,0,0,25.4,13.37L48,0Z"
                transform="translate(10.139 164.091)"
                fill="#684f3e"
              />
              <path
                id="Path_11620"
                data-name="Path 11620"
                d="M17.66.666s-1.019-2.514-6.5,2.23C7.136,6.377,1.081,13.941.226,16.322s.916.994,1.925-.4S6.774,10.092,6.763,10.3s-4.415,5.84-5,6.738-.584,1.857,1.361.446,5.151-5.345,5.515-5.673a27.15,27.15,0,0,1-3.224,3.954c-1.632,1.7-2.216,2.267-1.987,2.516s1.95-.854,3.32-1.874,3.416-3.666,3.456-3.445a21.97,21.97,0,0,1-3.455,3.741c-1.086.878-2.833,2.14-1.329,1.869s4.086-2.6,6.632-5.526S20.536,3.42,17.66.666"
                transform="translate(26.112 203.916)"
                fill="#3a3835"
              />
              <path
                id="Path_11621"
                data-name="Path 11621"
                d="M.19,11.565s-.074,2.712,6.85.564c5.081-1.576,13.661-6.078,15.4-7.917S22,2.935,20.515,3.8s-6.574,3.49-6.479,3.3,6.387-3.58,7.282-4.168,1.28-1.467-1.068-.954S13.389,4.816,12.924,4.97a27.152,27.152,0,0,1,4.539-2.33c2.177-.9,2.939-1.188,2.829-1.508s-2.129,0-3.793.386-4.6,1.989-4.547,1.77a21.966,21.966,0,0,1,4.665-2.042C17.963.877,20.069.421,18.584.066S13.8.814,10.292,2.47-1.341,7.889.19,11.565"
                transform="translate(158.011 62.401)"
                fill="#3a3835"
              />
              <path
                id="Path_11622"
                data-name="Path 11622"
                d="M2.674,91.13s19.976-11.1,28.4-29.958S28.2,22.522,45.051,4.266c11.375-12.323,34.3,4.882,37.113,15.848s-3.611,22.2-3.611,22.2-8.426,6.821-11.368,21.131S55.046,95.7,45.535,102.309s-27.6,15.024-39.222,17.3S2.674,91.13,2.674,91.13"
                transform="translate(51.402 71.41)"
                fill="#8e6549"
              />
              <path
                id="Path_11623"
                data-name="Path 11623"
                d="M5.66,12.969S-1.138,9.537.166,4.3s4.5-4.4,6.488-4a7.914,7.914,0,0,0,4.4-.071C9.883,2.98,5.66,12.969,5.66,12.969"
                transform="translate(11.488 165.033)"
                fill="#4f3323"
              />
              <path
                id="Path_11624"
                data-name="Path 11624"
                d="M27.666,16.992V0A59.766,59.766,0,0,0,16.009,1.471C9.365,2.942,3.591,6.6,1.915,8.716S-2,16.7,3.421,22.9c7.932,5.049,22.492-2.561,24.245-5.911"
                transform="translate(16.538 159.788)"
                fill="#8e6549"
              />
              <path
                id="Path_11625"
                data-name="Path 11625"
                d="M24.245,8.476v9.472a14.649,14.649,0,0,1-7.947-2.04C12.433,13.551,3.4,10.492,0,3.521,5.4,4.825,10.586,4.855,18.7,0c2.81.211,5.541,8.476,5.541,8.476"
                transform="translate(19.959 179.17)"
                fill="#ddd7d3"
              />
              <path
                id="Path_11626"
                data-name="Path 11626"
                d="M5.993,11.485S2.363,13.126.46,8.927C-1.647,4.28,4.171,0,4.171,0l5.7,4.28V8.426S7.48,11.093,5.993,11.485"
                transform="translate(34.333 179.22)"
                fill="#f7f1ed"
              />
              <path
                id="Path_11627"
                data-name="Path 11627"
                d="M.126,4.917S-.542,1.791,1.2.571A2.524,2.524,0,0,1,5.242,1.824a15.109,15.109,0,0,0,.886,2.625c.184.284.5.685.334,1.07s-.719.418-1.053.9a2.587,2.587,0,0,1-2.725,1.4C.478,7.483.126,4.917.126,4.917"
                transform="translate(24.686 172.558)"
                fill="#af8569"
              />
              <path
                id="Path_11628"
                data-name="Path 11628"
                d="M.182,3.79S-.821.246,1.887.012,4.662,3.154,5.03,3.489s.468.669,0,.97S2.083,8.433.182,3.79"
                transform="translate(25.349 173.719)"
                fill="#282623"
              />
              <path
                id="Path_11629"
                data-name="Path 11629"
                d="M.407,2.917a24.175,24.175,0,0,1,2.708-.652C4.545,2.014,5.1,1.412,4.52.435s-2.683-.1-3.511.627-1.455,2.1-.6,1.856"
                transform="translate(25.358 167.863)"
                fill="#af8569"
              />
              <path
                id="Path_11630"
                data-name="Path 11630"
                d="M5.982,0V6.72A4.368,4.368,0,0,1,4.627,6.3c-.7-.351-1.22-1.6-2.24-1.973S.281,3.912.013,2.391,3.524,0,5.982,0"
                transform="translate(38.223 176.78)"
                fill="#282623"
              />
              <path
                id="Path_11631"
                data-name="Path 11631"
                d="M3.879.2V4.347A18.543,18.543,0,0,1,2.056,6.22,8.168,8.168,0,0,1,0,7.407,11.674,11.674,0,0,0,2.094,5.931,6.006,6.006,0,0,0,3.561,3.938c.125-.6.188-3.674.188-3.874s.13.138.13.138"
                transform="translate(40.326 183.299)"
                fill="#282623"
              />
              <path
                id="Path_11632"
                data-name="Path 11632"
                d="M5.425,1.663a5.692,5.692,0,0,0-.917,1.378,11.57,11.57,0,0,0-.933,2.774C3.319,7.445,3.04,8.624,1.829,7.9A4.359,4.359,0,0,1,.549,2.229C1.9-.3,3.04-.078,4.716.1c1.4.146,1.355.832.709,1.565"
                transform="translate(12.472 166.244)"
                fill="#82523a"
              />
              <path
                id="Path_11633"
                data-name="Path 11633"
                d="M5.4,12.969s6.8-3.433,5.493-8.667S6.389-.1,4.4.306A7.914,7.914,0,0,1,0,.235C1.174,2.98,5.4,12.969,5.4,12.969"
                transform="translate(65.863 165.033)"
                fill="#4f3323"
              />
              <path
                id="Path_11634"
                data-name="Path 11634"
                d="M0,16.992V0A59.766,59.766,0,0,1,11.657,1.471C18.3,2.942,24.075,6.6,25.751,8.716S29.665,16.7,24.245,22.9C16.314,27.951,1.754,20.342,0,16.992"
                transform="translate(44.204 159.788)"
                fill="#8e6549"
              />
              <path
                id="Path_11635"
                data-name="Path 11635"
                d="M0,8.476v9.472a14.649,14.649,0,0,0,7.947-2.04c3.865-2.357,12.9-5.416,16.3-12.387C18.844,4.825,13.66,4.855,5.541,0,2.732.211,0,8.476,0,8.476"
                transform="translate(44.204 179.17)"
                fill="#ddd7d3"
              />
              <path
                id="Path_11636"
                data-name="Path 11636"
                d="M3.878,11.485s3.63,1.642,5.533-2.558C11.518,4.28,5.7,0,5.7,0L0,4.28V8.426s2.392,2.668,3.878,3.059"
                transform="translate(44.204 179.22)"
                fill="#f7f1ed"
              />
              <path
                id="Path_11637"
                data-name="Path 11637"
                d="M6.383,4.917S7.052,1.791,5.313.571A2.524,2.524,0,0,0,1.268,1.824,15.11,15.11,0,0,1,.381,4.449c-.184.284-.5.685-.334,1.07s.719.418,1.053.9a2.588,2.588,0,0,0,2.725,1.4c2.207-.343,2.558-2.909,2.558-2.909"
                transform="translate(57.214 172.558)"
                fill="#af8569"
              />
              <path
                id="Path_11638"
                data-name="Path 11638"
                d="M5.163,3.79S6.166.246,3.457.012.682,3.154.315,3.489s-.468.669,0,.97S3.261,8.433,5.163,3.79"
                transform="translate(57.715 173.719)"
                fill="#282623"
              />
              <path
                id="Path_11639"
                data-name="Path 11639"
                d="M4.338,2.917A24.175,24.175,0,0,0,1.63,2.265C.2,2.014-.351,1.412.226.435s2.683-.1,3.511.627,1.456,2.1.6,1.856"
                transform="translate(58.306 167.863)"
                fill="#af8569"
              />
              <path
                id="Path_11640"
                data-name="Path 11640"
                d="M0,0V6.72A4.368,4.368,0,0,0,1.354,6.3c.7-.351,1.22-1.6,2.24-1.973S5.7,3.912,5.968,2.391,2.457,0,0,0"
                transform="translate(44.204 176.78)"
                fill="#282623"
              />
              <path
                id="Path_11641"
                data-name="Path 11641"
                d="M0,.2V4.347A18.521,18.521,0,0,0,1.822,6.22,8.168,8.168,0,0,0,3.878,7.407,11.674,11.674,0,0,1,1.785,5.931,6,6,0,0,1,.318,3.938C.192,3.336.13.264.13.064S0,.2,0,.2"
                transform="translate(44.204 183.299)"
                fill="#282623"
              />
              <path
                id="Path_11642"
                data-name="Path 11642"
                d="M.432,1.663A5.7,5.7,0,0,1,1.35,3.041a11.569,11.569,0,0,1,.933,2.774c.256,1.63.536,2.808,1.746,2.081a4.359,4.359,0,0,0,1.28-5.667C3.959-.3,2.818-.078,1.142.1-.255.244-.213.93.432,1.663"
                transform="translate(70.079 166.244)"
                fill="#82523a"
              />
              <g
                id="Group_2370"
                data-name="Group 2370"
                transform="translate(61.521 174.54)"
                opacity="0.36"
              >
                <g id="Group_2369" data-name="Group 2369">
                  <g
                    id="Group_2368"
                    data-name="Group 2368"
                    clip-path="url(#clip-path-3)"
                  >
                    <path
                      id="Path_11643"
                      data-name="Path 11643"
                      d="M.855.5s.384,1.526,0,1.737S-.114.929.019.395.6-.157.855.5"
                      transform="translate(0 0)"
                      fill="#fff"
                    />
                  </g>
                </g>
              </g>
              <g
                id="Group_2373"
                data-name="Group 2373"
                transform="translate(28.22 174.486)"
                opacity="0.36"
              >
                <g id="Group_2372" data-name="Group 2372">
                  <g
                    id="Group_2371"
                    data-name="Group 2371"
                    clip-path="url(#clip-path-4)"
                  >
                    <path
                      id="Path_11644"
                      data-name="Path 11644"
                      d="M.868.719S1.24,1.9.855,2.113-.114.8.02.268s.6-.209.848.452"
                      transform="translate(0 0)"
                      fill="#fff"
                    />
                  </g>
                </g>
              </g>
              <g
                id="Group_2376"
                data-name="Group 2376"
                transform="translate(41.749 178.44)"
                opacity="0.36"
              >
                <g id="Group_2375" data-name="Group 2375">
                  <g
                    id="Group_2374"
                    data-name="Group 2374"
                    clip-path="url(#clip-path-5)"
                  >
                    <path
                      id="Path_11645"
                      data-name="Path 11645"
                      d="M1.034.138a18.9,18.9,0,0,1,4.514,0c2.157.3,1.806.936,1.555.986S5.749.891,4.094.64A19.794,19.794,0,0,0,.316.439C-.37.473.148.255,1.034.138"
                      transform="translate(0 0)"
                      fill="#fff"
                    />
                  </g>
                </g>
              </g>
              <path
                id="Path_11646"
                data-name="Path 11646"
                d="M.076,0A8.321,8.321,0,0,0,5.459,1.655,7.337,7.337,0,0,0,10.491,0C10.708-.017,8.7,2.692,5.459,2.692S-.493.05.076,0"
                transform="translate(38.746 191.819)"
                fill="#ccc6c2"
              />
              <path
                id="Path_11647"
                data-name="Path 11647"
                d="M1.438,64.277s17.52-10.648,24.474-24.289S29.389,11.368,40.623,0A32.783,32.783,0,0,0,26.882,22.134c-1.2,7.121-7.423,19.86-11.836,25.878S.5,61.945.5,61.945l-.5,3.5Z"
                transform="translate(89.331 113.725)"
                fill="#ddd7d3"
              />
              <path
                id="Path_11648"
                data-name="Path 11648"
                d="M51.592,3.5S53.6,15.657,41.16,23.08,19.14,28.4,12.446,30.7s-7.3,4.387-10.505,4.4-2.383-4.6,2.068-8.111,15.573-5.416,18.638-8.726a47,47,0,0,0,5.476-7.552L52.093,0Z"
                transform="translate(37.739 175.67)"
                fill="#8e6549"
              />
              <path
                id="Path_11649"
                data-name="Path 11649"
                d="M7.27,43.168S17.7,38.019,23.92,30.2,33.148,12.241,34.553,9.332s4.413-5.115,5.015-6.419S38.665-.7,36.86.2s-4.915,1.5-6.62,3.31-4.113,7.523-8.125,9.027S8.743,14.38,3.691,10.3-2.159,32.97,7.27,43.168"
                transform="translate(122.684 70.557)"
                fill="#8e6549"
              />
              <path
                id="Path_11650"
                data-name="Path 11650"
                d="M.1,9.528s-.379,2.687,6.743,1.33c5.226-1,14.257-4.5,16.191-6.135S22.738,3.4,21.168,4.1,14.244,6.83,14.359,6.653s6.749-2.839,7.7-3.323S23.5,2.016,21.11,2.262s-7.137,2.043-7.616,2.144a27.14,27.14,0,0,1,4.772-1.8c2.264-.653,3.054-.85,2.98-1.18s-2.115-.239-3.812-.043S12.64,2.837,12.716,2.626a21.967,21.967,0,0,1,4.865-1.5C18.96.906,21.1.689,19.668.17s-4.842.2-8.509,1.456S-1.009,5.7.1,9.528"
                transform="translate(146.914 56.375)"
                fill="#3a3835"
              />
              <path
                id="Path_11651"
                data-name="Path 11651"
                d="M0,78.3S8.578,67.317,30.779,59.293,71.3,45.384,76.518,34.417,82.536,10.478,80.931,3.39,84.275.047,84.408,17.3s-.533,30.76-16.248,40.79S39.338,72,31.581,82.029,0,78.3,0,78.3"
                transform="translate(94.227 0)"
                fill="#8e6549"
              />
              <path
                id="Path_11652"
                data-name="Path 11652"
                d="M1.438,11.749A21.556,21.556,0,0,0,0,0C2.34,3.544,2.307,7.983,2.056,11.247l-.618.5"
                transform="translate(87.893 167.421)"
                fill="#4f3323"
              />
              <path
                id="Path_11653"
                data-name="Path 11653"
                d="M7.306,0,0,6.72,7.909.709Z"
                transform="translate(119.456 80.859)"
                fill="#4f3323"
              />
            </g>
          </g>
        </g>
      </g>
      <ellipse
        id="Ellipse_133"
        data-name="Ellipse 133"
        cx="35.747"
        cy="27.428"
        rx="35.747"
        ry="27.428"
        transform="translate(3426.887 146.96) rotate(7)"
        fill="url(#radial-gradient)"
      />
      <g
        id="Group_2534"
        data-name="Group 2534"
        transform="translate(0 212.999)"
      >
        <g id="Group_2382" data-name="Group 2382">
          <g
            id="Group_2381"
            data-name="Group 2381"
            clip-path="url(#clip-path-6)"
          >
            <rect
              id="Rectangle_1419"
              data-name="Rectangle 1419"
              width="10.527"
              height="10.25"
              transform="matrix(0.991, -0.136, 0.136, 0.991, 74.208, 129.672)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2384" data-name="Group 2384">
          <g
            id="Group_2383"
            data-name="Group 2383"
            clip-path="url(#clip-path-7)"
          >
            <rect
              id="Rectangle_1420"
              data-name="Rectangle 1420"
              width="10.527"
              height="10.25"
              transform="translate(83.592 0.167) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2386" data-name="Group 2386">
          <g
            id="Group_2385"
            data-name="Group 2385"
            clip-path="url(#clip-path-8)"
          >
            <rect
              id="Rectangle_1421"
              data-name="Rectangle 1421"
              width="10.527"
              height="10.251"
              transform="matrix(0.991, -0.136, 0.136, 0.991, 10.967, 140.509)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2388" data-name="Group 2388">
          <g
            id="Group_2387"
            data-name="Group 2387"
            clip-path="url(#clip-path-9)"
          >
            <rect
              id="Rectangle_1422"
              data-name="Rectangle 1422"
              width="10.527"
              height="10.251"
              transform="translate(200.158 70.674) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2390" data-name="Group 2390">
          <g
            id="Group_2389"
            data-name="Group 2389"
            clip-path="url(#clip-path-10)"
          >
            <rect
              id="Rectangle_1423"
              data-name="Rectangle 1423"
              width="18.885"
              height="18.388"
              transform="translate(52.361 164.502) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2392" data-name="Group 2392">
          <g
            id="Group_2391"
            data-name="Group 2391"
            clip-path="url(#clip-path-11)"
          >
            <rect
              id="Rectangle_1424"
              data-name="Rectangle 1424"
              width="18.885"
              height="18.389"
              transform="translate(125.959 106.092) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2394" data-name="Group 2394">
          <g
            id="Group_2393"
            data-name="Group 2393"
            clip-path="url(#clip-path-12)"
          >
            <rect
              id="Rectangle_1425"
              data-name="Rectangle 1425"
              width="18.885"
              height="18.389"
              transform="matrix(0.991, -0.136, 0.136, 0.991, -2.186, 57.28)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2396" data-name="Group 2396">
          <g
            id="Group_2395"
            data-name="Group 2395"
            clip-path="url(#clip-path-13)"
          >
            <rect
              id="Rectangle_1426"
              data-name="Rectangle 1426"
              width="18.884"
              height="18.388"
              transform="translate(156.901 49.153) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2398" data-name="Group 2398">
          <g
            id="Group_2397"
            data-name="Group 2397"
            clip-path="url(#clip-path-14)"
          >
            <rect
              id="Rectangle_1427"
              data-name="Rectangle 1427"
              width="18.884"
              height="18.388"
              transform="translate(46.664 34.897) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2400" data-name="Group 2400">
          <g
            id="Group_2399"
            data-name="Group 2399"
            clip-path="url(#clip-path-15)"
          >
            <rect
              id="Rectangle_1428"
              data-name="Rectangle 1428"
              width="50.964"
              height="50.876"
              transform="translate(77.241 71.142) rotate(-60.534)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2402" data-name="Group 2402">
          <g
            id="Group_2401"
            data-name="Group 2401"
            clip-path="url(#clip-path-16)"
          >
            <rect
              id="Rectangle_1429"
              data-name="Rectangle 1429"
              width="55.03"
              height="56.481"
              transform="translate(0.631 84.6) rotate(-16.914)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2404" data-name="Group 2404">
          <g
            id="Group_2403"
            data-name="Group 2403"
            clip-path="url(#clip-path-17)"
          >
            <rect
              id="Rectangle_1430"
              data-name="Rectangle 1430"
              width="41.642"
              height="42.741"
              transform="translate(86.33 140.402) rotate(-16.914)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2406" data-name="Group 2406">
          <g
            id="Group_2405"
            data-name="Group 2405"
            clip-path="url(#clip-path-18)"
          >
            <rect
              id="Rectangle_1431"
              data-name="Rectangle 1431"
              width="55.03"
              height="56.481"
              transform="translate(147.548 108.866) rotate(-16.914)"
              fill="#fdc843"
            />
          </g>
        </g>
        <g id="Group_2408" data-name="Group 2408">
          <g
            id="Group_2407"
            data-name="Group 2407"
            clip-path="url(#clip-path-19)"
          >
            <rect
              id="Rectangle_1432"
              data-name="Rectangle 1432"
              width="18.884"
              height="18.389"
              transform="translate(151.529 163.578) rotate(-7.8)"
              fill="#fdc843"
            />
          </g>
        </g>
      </g>
    </svg>
  </>
)
export default OtterRight
