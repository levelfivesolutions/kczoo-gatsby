import React from "react"

const StarsRight = props => (
  <>
    <svg
      id="Group_2534"
      data-name="Group 2534"
      xmlns="http://www.w3.org/2000/svg"
      width="210.76"
      height="180.457"
      viewBox="0 0 210.76 180.457"
      className={props.className}
    >
      <defs>
        <clipPath id="clipPath">
          <path
            id="Path_11654"
            data-name="Path 11654"
            d="M80.278,129.505l1.312,3.006,3.22.623-2.453,2.176.4,3.255-2.828-1.66-2.972,1.388.706-3.2-2.239-2.4,3.264-.319Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-2">
          <path
            id="Path_11655"
            data-name="Path 11655"
            d="M89.662,0,88.074,2.869l-3.264.32,2.239,2.4-.706,3.2L89.314,7.4l2.829,1.66-.4-3.254,2.453-2.177-3.22-.623Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-3">
          <path
            id="Path_11656"
            data-name="Path 11656"
            d="M17.037,140.342l1.312,3.006,3.22.623-2.454,2.177.4,3.255-2.829-1.661-2.971,1.388.706-3.2-2.239-2.4,3.264-.318Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-4">
          <path
            id="Path_11657"
            data-name="Path 11657"
            d="M204.64,73.377l-3.264.318,2.238,2.4-.7,3.2,2.97-1.388,2.829,1.661-.4-3.255,2.453-2.177-3.22-.623-1.312-3.006Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-5">
          <path
            id="Path_11658"
            data-name="Path 11658"
            d="M60.4,169.351l-5.856.571,4.016,4.3L57.3,179.968l5.33-2.49,5.074,2.979-.721-5.839,4.4-3.9L65.605,169.6,63.251,164.2Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-6">
          <path
            id="Path_11659"
            data-name="Path 11659"
            d="M136.85,105.793l2.353,5.393,5.777,1.118-4.4,3.9.721,5.84-5.074-2.979-5.33,2.49,1.265-5.746-4.016-4.3,5.856-.572Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-7">
          <path
            id="Path_11660"
            data-name="Path 11660"
            d="M5.855,62.13,0,62.7,4.016,67,2.75,72.747l5.331-2.491,5.073,2.98-.721-5.84,4.4-3.905-5.778-1.118L8.7,56.981Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-8">
          <path
            id="Path_11661"
            data-name="Path 11661"
            d="M167.791,48.854l2.354,5.392,5.776,1.119-4.4,3.9.722,5.839-5.074-2.979-5.331,2.491,1.266-5.746-4.016-4.3L164.943,54Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-9">
          <path
            id="Path_11662"
            data-name="Path 11662"
            d="M57.554,34.6l2.354,5.392,5.776,1.118-4.4,3.9L62,50.852l-5.073-2.979L51.6,50.364l1.266-5.746-4.016-4.3,5.856-.572Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-10">
          <path
            id="Path_11663"
            data-name="Path 11663"
            d="M93.3,52.929l13.205.43,7.841-10.634,3.671,12.692,12.537,4.171L119.614,67l-.093,13.212-10.43-8.11L96.5,76.1l4.49-12.426Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-11">
          <path
            id="Path_11664"
            data-name="Path 11664"
            d="M29.9,80.725l11.205,11.4,15.74-2.77-7.376,14.178,7.5,14.114L41.208,115.01,30.1,126.5,27.735,110.7l-14.362-7.011,14.3-7.135Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-12">
          <path
            id="Path_11665"
            data-name="Path 11665"
            d="M108.482,137.47l8.479,8.624,11.91-2.1-5.581,10.729,5.674,10.68-11.928-1.993-8.4,8.7-1.791-11.961-10.868-5.3,10.822-5.4Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-13">
          <path
            id="Path_11666"
            data-name="Path 11666"
            d="M176.82,104.991l11.2,11.4,15.74-2.77-7.375,14.178,7.5,14.114-15.764-2.634-11.106,11.493-2.366-15.806-14.362-7.011,14.3-7.135Z"
            fill="#fdc843"
          />
        </clipPath>
        <clipPath id="clipPath-14">
          <path
            id="Path_11667"
            data-name="Path 11667"
            d="M159.57,168.427l-5.855.572,4.015,4.3-1.265,5.746,5.331-2.491,5.073,2.98-.722-5.84,4.4-3.9-5.777-1.118-2.353-5.392Z"
            fill="#fdc843"
          />
        </clipPath>
      </defs>
      <g id="Group_2382" data-name="Group 2382">
        <g id="Group_2381" data-name="Group 2381" clipPath="url(#clipPath)">
          <rect
            id="Rectangle_1419"
            data-name="Rectangle 1419"
            width="10.527"
            height="10.25"
            transform="matrix(0.991, -0.136, 0.136, 0.991, 74.208, 129.672)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2384" data-name="Group 2384">
        <g id="Group_2383" data-name="Group 2383" clipPath="url(#clipPath-2)">
          <rect
            id="Rectangle_1420"
            data-name="Rectangle 1420"
            width="10.527"
            height="10.25"
            transform="translate(83.592 0.167) rotate(-7.8)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2386" data-name="Group 2386">
        <g id="Group_2385" data-name="Group 2385" clipPath="url(#clipPath-3)">
          <rect
            id="Rectangle_1421"
            data-name="Rectangle 1421"
            width="10.527"
            height="10.251"
            transform="matrix(0.991, -0.136, 0.136, 0.991, 10.967, 140.509)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2388" data-name="Group 2388">
        <g id="Group_2387" data-name="Group 2387" clipPath="url(#clipPath-4)">
          <rect
            id="Rectangle_1422"
            data-name="Rectangle 1422"
            width="10.527"
            height="10.251"
            transform="translate(200.158 70.674) rotate(-7.8)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2390" data-name="Group 2390">
        <g id="Group_2389" data-name="Group 2389" clipPath="url(#clipPath-5)">
          <rect
            id="Rectangle_1423"
            data-name="Rectangle 1423"
            width="18.885"
            height="18.388"
            transform="translate(52.361 164.502) rotate(-7.8)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2392" data-name="Group 2392">
        <g id="Group_2391" data-name="Group 2391" clipPath="url(#clipPath-6)">
          <rect
            id="Rectangle_1424"
            data-name="Rectangle 1424"
            width="18.885"
            height="18.389"
            transform="translate(125.959 106.092) rotate(-7.8)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2394" data-name="Group 2394">
        <g id="Group_2393" data-name="Group 2393" clipPath="url(#clipPath-7)">
          <rect
            id="Rectangle_1425"
            data-name="Rectangle 1425"
            width="18.885"
            height="18.389"
            transform="matrix(0.991, -0.136, 0.136, 0.991, -2.186, 57.28)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2396" data-name="Group 2396">
        <g id="Group_2395" data-name="Group 2395" clipPath="url(#clipPath-8)">
          <rect
            id="Rectangle_1426"
            data-name="Rectangle 1426"
            width="18.884"
            height="18.388"
            transform="translate(156.901 49.153) rotate(-7.8)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2398" data-name="Group 2398">
        <g id="Group_2397" data-name="Group 2397" clipPath="url(#clipPath-9)">
          <rect
            id="Rectangle_1427"
            data-name="Rectangle 1427"
            width="18.884"
            height="18.388"
            transform="translate(46.664 34.897) rotate(-7.8)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2400" data-name="Group 2400">
        <g id="Group_2399" data-name="Group 2399" clipPath="url(#clipPath-10)">
          <rect
            id="Rectangle_1428"
            data-name="Rectangle 1428"
            width="50.964"
            height="50.876"
            transform="translate(77.241 71.142) rotate(-60.534)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2402" data-name="Group 2402">
        <g id="Group_2401" data-name="Group 2401" clipPath="url(#clipPath-11)">
          <rect
            id="Rectangle_1429"
            data-name="Rectangle 1429"
            width="55.03"
            height="56.481"
            transform="translate(0.631 84.6) rotate(-16.914)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2404" data-name="Group 2404">
        <g id="Group_2403" data-name="Group 2403" clipPath="url(#clipPath-12)">
          <rect
            id="Rectangle_1430"
            data-name="Rectangle 1430"
            width="41.642"
            height="42.741"
            transform="translate(86.33 140.402) rotate(-16.914)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2406" data-name="Group 2406">
        <g id="Group_2405" data-name="Group 2405" clipPath="url(#clipPath-13)">
          <rect
            id="Rectangle_1431"
            data-name="Rectangle 1431"
            width="55.03"
            height="56.481"
            transform="translate(147.548 108.866) rotate(-16.914)"
            fill="#fdc843"
          />
        </g>
      </g>
      <g id="Group_2408" data-name="Group 2408">
        <g id="Group_2407" data-name="Group 2407" clipPath="url(#clipPath-14)">
          <rect
            id="Rectangle_1432"
            data-name="Rectangle 1432"
            width="18.884"
            height="18.389"
            transform="translate(151.529 163.578) rotate(-7.8)"
            fill="#fdc843"
          />
        </g>
      </g>
    </svg>
  </>
)
export default StarsRight
