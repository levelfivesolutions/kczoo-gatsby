import React from "react"
// import Layout from "../../../components/layout/layout"
// import SEO from "../../../components/seo"
import Button from "../../../components/button/button-link"
import Link from "gatsby-link"
import styles from "./jungle.module.css"
import a from "./shake.module.scss"
import LeftJungle from "./leftJungle"
import RightJungle from "./rightJungle"
import BottomJungle from "./bottomJungle"
import SketchDivider from "./divider"
import Fern from "./fern"
import FernLeft from "./fern2"

const Jungle404 = props => (
  <div className={`${styles.background}`}>
    <div className={a.flyBuzz}>
      <div className={a.fly}>
        <div className={styles.fly}></div>
      </div>
    </div>
    <div className="row m-0">
      <div className="container">
        <LeftJungle className={styles.leftJungle} />
        <div className={styles.rightWrapper}>
          <RightJungle className={styles.rightJungle} />
        </div>
        <div className={`${styles.contentWrapper}`}>
          <div
            className={`col-lg-12 p-0 d-flex flex-column align-items-center justify-content-center ${styles.textWrapper}`}
          >
            <h1 className={styles.title}>Oh no!</h1>
            <h2 className={styles.subhead}>You've wandered off the path.</h2>
            <SketchDivider className={styles.sketchDivider} />
            <div className={`${styles.moveIcon} mt-4`}>
              <Button
                icon="arrowleft"
                text="Back to Home"
                url="/"
                size="large"
                isInverse={false}
                color="Zoo Green"
              />
            </div>
            <div className={styles.message}>
              <p className="mt-4">The page you're looking for doesn't exist.</p>
              <p>
                Hike on back to our home page and try again, or{" "}
                <Link to="/about/contact-us/">
                  <strong>contact us</strong>
                </Link>{" "}
                for help
              </p>
            </div>
          </div>
          <div className={styles.bottomWrapper}>
            <BottomJungle className={styles.bottomJungle} />
            <BottomJungle className={styles.bottomJungle2} />
            <BottomJungle className={styles.bottomJungle3} />
            <BottomJungle className={styles.bottomJungle4} />
            <Fern className={`${styles.fern} ${a.fern}`} />
            <FernLeft className={`${styles.fern2} ${a.fern2}`} />
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Jungle404
