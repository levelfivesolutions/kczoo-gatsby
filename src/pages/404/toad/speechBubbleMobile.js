import React from "react"

const SpeechBubbleMobile = props => (
  <>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="407.979"
      height="248.882"
      viewBox="0 0 407.979 248.882"
      className={props.className}
    >
      <defs>
        <filter
          id="Union_2"
          x="0"
          y="0"
          width="407.979"
          height="248.882"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="3" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="3" result="blur" />
          <feFlood floodOpacity="0.161" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#Union_2)">
        <path
          id="Union_2-2"
          data-name="Union 2"
          d="M-6297.365,1368.239c-12.341,1.155-25.1,1.761-38.164,1.761-107.69,0-194.99-41.19-194.99-92s87.3-92,194.99-92,194.988,41.188,194.988,92c0,38.976-51.366,72.291-123.9,85.7-2.388,15.284-7.158,46.133-7.759,52.428-.05.517-.2.759-.442.759C-6275.446,1416.882-6290.325,1384.109-6297.365,1368.239Z"
          transform="translate(6539.52 -1180)"
          fill="#fff"
        />
      </g>
      <style>
        {`.titleMobile {
        font-family: "Lifehack Sans";
        font-weight: "Bold";
        font-size: 48px;
        fill: #296f2e;
        z-index: 2;
        }

        .subheadMobile {
        font-family: "Lifehack Sans";
        font-weight: "Regular";
        font-size: 36px;
        fill: #3b3b3b;
        z-index: 2;
        }`}
      </style>
      <text className="titleMobile" x="210" y="100" textAnchor="middle">
        We're toad-ally sorry,
      </text>
      <text className="subheadMobile" x="210" y="130" textAnchor="middle">
        but that page doesn't exist!
      </text>
    </svg>
  </>
)
export default SpeechBubbleMobile
