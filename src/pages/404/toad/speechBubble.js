import React from "react"

const SpeechBubble = props => (
  <>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="767.284"
      height="272"
      viewBox="0 0 767.284 272"
      className={props.className}
    >
      <defs>
        <filter
          id="Union_1"
          x="0"
          y="0"
          width="767.284"
          height="272"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="3" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="3" result="blur" />
          <feFlood floodOpacity="0.161" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g transform="matrix(1, 0, 0, 1, 0, 0)" filter="url(#Union_1)">
        <path
          id="Union_1-2"
          data-name="Union 1"
          d="M-6446.942,1348c-34.112,5.8-138.763,23.574-150,25.441-10.918,1.813,82.94-40.832,118.672-56.955-6.055-9.767-9.267-19.979-9.267-30.489,0-70.141,143.045-127,319.5-127s319.5,56.86,319.5,127-143.047,127-319.5,127C-6287.846,1413-6392.254,1386.785-6446.942,1348Z"
          transform="translate(6606.82 -1153)"
          fill="#fff"
        />
      </g>
      <style>
        {`.title {
        font-family: "Lifehack Sans";
        font-weight: "Bold";
        line-height: 59px;
        font-size: 72px;
        fill: #296f2e;
        z-index: 2;
        }

        .subhead {
        font-family: "Lifehack Sans";
        font-weight: "Regular";
        line-height: 59px;
        font-size: 48px;
        fill: #3b3b3b;
        z-index: 2;
        }`}
      </style>
      <text className="title" x="440" y="140" textAnchor="middle">
        We're toad-ally sorry,
      </text>
      <text className="subhead" x="440" y="190" textAnchor="middle">
        but that page doesn't exist!
      </text>
    </svg>
  </>
)
export default SpeechBubble
