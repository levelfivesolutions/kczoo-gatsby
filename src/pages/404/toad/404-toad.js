import React from "react"
// import Layout from "../../../components/layout/layout"
// import SEO from "../../../components/seo"
import Button from "../../../components/button/button-link"
import Link from "gatsby-link"
import styles from "./toad.module.css"
import a from "./shake.module.scss"
import Toad from "./toad"
import SpeechBubble from "./speechBubble"
import SpeechBubbleMobile from "./speechBubbleMobile"

const Toad404 = props => (
  <div className={`${styles.background}`}>
    <div className="row m-0">
      <div className="container">
        <div className={`${styles.contentWrapper}`}>
          <div className="col-lg-5 p-0">
            <Toad className={styles.toad} />
            <div className={styles.textWrapperMobile}>
              <div className={`${styles.moveIcon} mt-2`}>
                <Button
                  icon="arrowleft"
                  text="Back to Home"
                  url="/"
                  size="large"
                  isInverse={false}
                  color="Zoo Green"
                />
              </div>
              <div className={styles.message}>
                <p className="mt-4">
                  Hop on back to our home page and try again, or{" "}
                  <Link to="/about/contact-us/">
                    <strong>contact us</strong>
                  </Link>{" "}
                  for help
                </p>
              </div>
            </div>
          </div>
          <div className="col-lg-7 p-0" align="center">
            <SpeechBubble
              className={`${styles.speechBubble} ${a.speechBubble}`}
            />
            <SpeechBubbleMobile
              className={`${styles.speechBubbleMobile} ${a.speechBubbleMobile}`}
            />
            <div className={styles.textWrapper}>
              <div className={`${styles.moveIcon} mt-2`}>
                <Button
                  icon="arrowleft"
                  text="Back to Home"
                  url="/"
                  size="large"
                  isInverse={false}
                  color="Zoo Green"
                />
              </div>
              <div className={styles.message}>
                <p className="mt-4">
                  Hop on back to our home page and try again, or{" "}
                  <Link to="/about/contact-us/">
                    <strong>contact us</strong>
                  </Link>{" "}
                  for help
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default Toad404
