import React from "react"
import { graphql } from "gatsby"
//import { Link } from "gatsby";
import { Modal } from "react-bootstrap"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import Hero from "../components/hero-landing/hero-landing"
import styles from "./day-camps-landing.module.css"
import SectionBlock from "../components/section-block"
import CampExperience from "../components/camp-experience/camp-experience"
import CampSeasonal from "../components/camp-seasonal/camp-seasonal"
import {
  renderAsText,
  renderAsHtml,
  filterArrayByType,
  getPrice,
} from "../utility"
import { getItems } from "../galaxy"

class RidesLanding extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [],
      show: false,
      title: "",
      bodyCopy: "",
    }

    this.handleShow = this.handleShow.bind(this)
    this.handleClose = this.handleClose.bind(this)
  }

  componentDidMount() {
    getItems(res => {
      this.setState({ items: res })
    })
  }

  handleShow({ title, information_field }) {
    this.setState({ show: true, title: title, bodyCopy: information_field })
  }

  handleClose() {
    this.setState({ show: false })
  }

  render() {
    const doc = this.props.data.prismic.allLanding_page_hero_blocks.edges
      .slice(0, 1)
      .pop()
    if (!doc) return null
    console.log("doc", doc)
    const {
      wide_header_size,
      title,
      summary,
      feature,
      button_label,
      cta,
      body,
      meta_title,
      meta_description,
      _meta,
    } = doc.node
    const { items } = this.state

    let features = filterArrayByType(body, "plu_feature_block")
    console.log("features", JSON.stringify(features))
    let columns = filterArrayByType(body, "column").pop().fields
    console.log("columns", JSON.stringify(columns))

    return (
      <Layout title={title} section="visit" {...this.props}>
        <SEO
          title={meta_title}
          description={meta_description}
          canonical={this.props.location.origin + this.props.location.pathname}
        />
        <Hero image={wide_header_size} pageId={_meta.id} />
        <TitleBar
          title={title}
          description={summary}
          calloutDescription={feature}
          link={cta}
          linkText={button_label}
        />
        {features.map((d, i) => {
          let prices = d.fields.map(field => ({
            price: getPrice(items, field.plu, 0),
            description: field.plu_description,
          }))
          return (
            <SectionBlock key={i}>
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <CampExperience
                      color={d.primary.color}
                      image={d.primary.feature_image}
                      title={d.primary.feature_title}
                      richDescription={d.primary.feature_text}
                      prices={prices}
                      btnText={d.primary.cta_label}
                      onClick={ev => this.handleShow(d.primary.cta_link)}
                    />
                  </div>
                </div>
              </div>
            </SectionBlock>
          )
        })}
        <SectionBlock>
          <div className="container">
            <div className="row">
              <div className="col-md-10 offset-md-1">
                <div className="row v-20-s">
                  {columns.map((d, i) => {
                    return (
                      <div className="col-md-4" key={i}>
                        <CampSeasonal
                          color={d.color}
                          image={d.column_image}
                          title={d.column_title}
                          description={d.column_text}
                          price={d.plu ? getPrice(items, d.plu, 0) : ""}
                          priceQualifier={d.plu_description}
                          price2={d.plu_2 ? getPrice(items, d.plu_2, 0) : ""}
                          priceQualifier2={d.plu_2_description}
                        />
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </div>
        </SectionBlock>
        {/*
				<section className={styles.section}>
					<div className="container">
						<div className="row v-20-s" >
							<CallOutBlock body={body} />
						</div>
					</div>
				</section>
				*/}
        <Modal
          show={this.state.show}
          onHide={this.handleClose}
          size="lg"
          dialogClassName={styles.modal}
        >
          <Modal.Header closeButton className={styles.modalHeader}>
            <Modal.Title
              className={styles.modalTitle}
              style={{ fontSize: 42, lineHeight: "48px" }}
            >
              {renderAsText(this.state.title)}
            </Modal.Title>
          </Modal.Header>

          <Modal.Body style={{ padding: "15px 30px" }}>
            {renderAsHtml(this.state.bodyCopy)}
          </Modal.Body>
        </Modal>
      </Layout>
    )
  }
}

export default RidesLanding

export const query = graphql`
  {
    prismic {
      allLanding_page_hero_blocks(uid: "rides-and-transportation") {
        edges {
          node {
            _meta {
              id
            }
            meta_title
            meta_description
            wide_header_size
            title
            summary
            feature
            button_label
            cta {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
            }
            body {
              ... on PRISMIC_Landing_page_hero_blockBodyPlu_feature_block {
                type
                label
                primary {
                  cta_label
                  cta_link {
                    ... on PRISMIC_Camp_information_overlay {
                      title
                      information_field
                    }
                  }
                  feature_image
                  feature_text
                  feature_title
                  color
                }
                fields {
                  plu_description
                  plu
                }
              }
              ... on PRISMIC_Landing_page_hero_blockBodyColumn {
                type
                label
                fields {
                  column_image
                  column_text
                  column_title
                  plu
                  plu_description
                  plu_2
                  plu_2_description
                  color
                }
              }
            }
          }
        }
      }
    }
  }
`
