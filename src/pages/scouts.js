import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
import { filterArrayByType, getPrice, renderLinkUrl } from "../utility"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import Hero from "../components/hero-landing/hero-landing"
import SectionBlock from "../components/section-block"
import CampExperience from "../components/camp-experience/camp-experience"
import { getItems } from "../galaxy"
import CampSeasonal from "../components/camp-seasonal/camp-seasonal"
import CallOutBlock from "../components/call-out/call-out-block"
import styles from "./homeschool.module.css"
import { linkResolver } from "gatsby-source-prismic-graphql"

const ScoutsLanding = props => {
  const { data, location } = props
  const doc = data.prismic.allScouts_landing_pages.edges.slice(0, 1).pop()
  const [items, setItems] = useState(0)

  useEffect(() => {
    getItems(res => {
      setItems(res)
    })
  }, [])

  console.log("DOC", doc)
  if (!doc) return null
  const {
    wide_header_size,
    title,
    subhead_description,
    body,
    meta_title = "",
    meta_description,
  } = doc.node

  var activeScouts = filterArrayByType(body, "camp_feature_callout")[0].fields

  return (
    <Layout title={title} section="camps" {...props}>
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero image={wide_header_size} pageId={doc.node._meta.id} />
      <TitleBar title={title} description={subhead_description} />
      {activeScouts.map((d, i) => {
        return (
          <SectionBlock>
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <CampExperience
                    color={d.color_name}
                    image={d.callout_image}
                    title={d.callout_title}
                    richDescription={d.description}
                    ageRange={d.age_range}
                    startDate={d.date_start}
                    endDate={d.date_end}
                    timeRange={d.time_range}
                    prices={[
                      {
                        price: getPrice(items, d.reg_plu_price, 0),
                        description: d.reg_plu_price_description,
                      },
                    ]}
                    priceDesc={d.fotz_plu_price_description}
                    btnText={d.cta_label}
                    url={linkResolver(d.cta)}
                  />
                </div>
              </div>
            </div>
          </SectionBlock>
        )
      })}
      <SectionBlock>
        <div className="container">
          <div className="row v-20-s">
            <CallOutBlock body={body} />
          </div>
        </div>
      </SectionBlock>
    </Layout>
  )
}

export default ScoutsLanding

export const query = graphql`
  {
    prismic {
      allScouts_landing_pages {
        edges {
          node {
            body {
              ... on PRISMIC_Scouts_landing_pageBodyCamp_feature_callout {
                type
                label
                fields {
                  cta {
                    ... on PRISMIC_Scout_detail {
                      title
                      dates
                      _meta {
                        uid
                        id
                        type
                        tags
                      }
                      _linkType
                    }
                    _linkType
                    ... on PRISMIC_Special_event {
                      hero_title
                      hero_title_color
                      _linkType
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                    }
                  }
                  age_range
                  callout_image
                  callout_title
                  color_name
                  cta_label
                  description
                  fotz_plu_price
                  fotz_plu_price_description
                  reg_plu_price
                  reg_plu_price_description
                  time_range
                }
              }
              ... on PRISMIC_Scouts_landing_pageBodyDetailed_callout {
                type
                label
                fields {
                  body_text
                  button_label
                  image1
                  title1
                  color
                  cta1 {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                      name
                    }
                    ... on PRISMIC__ImageLink {
                      _linkType
                      width
                      url
                      name
                      height
                    }
                  }
                }
              }
            }
            wide_header_size
            title
            meta_title
            meta_description
            subhead_description
            _meta {
              id
              type
              uid
              tags
            }
          }
        }
      }
    }
  }
`
