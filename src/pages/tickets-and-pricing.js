import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import Hero from "../components/hero-landing/hero-landing"
import styles from "./tickets-and-pricing.module.css"
import { RichText } from "prismic-reactjs"
import Button from "../components/button/button-link.js"
import {
  ColorToHex,
  filterArrayByType,
  getPrice,
  renderAsText,
  renderAsHtml,
} from "../utility"
import CTA from "../components/cta/cta.js"
import Text from "../components/text"
import TextBlocks from "../components/text-blocks"
import CallOutBlock from "../components/call-out/call-out-block"
import Icon from "../components/icon/icon"
import { Modal } from "react-bootstrap"
import { getItems, getJacksonClayItems } from "../galaxy"

class TicketsAndPricingLanding extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      show: true,
    }

    this.handleClose = this.handleClose.bind(this)
    this.handleCloseScroll = this.handleCloseScroll.bind(this)
    this.handleScrollDetails = this.handleScrollDetails.bind(this)
    this.handleScrollDetails = this.handleScrollDetails.bind(this)
  }

  componentDidMount() {
    getItems(res => {
      this.setState({ items: res })
    })
    getJacksonClayItems(res => {
      this.setState({ jacksonClayItems: res })
    })
  }

  handleClose() {
    this.setState({ show: false })
  }

  handleCloseScroll(ev) {
    ev.preventDefault()
    this.setState({ show: false })
    setTimeout(function() {
      let top = document.getElementById("details").offsetTop
      console.log("top", top)
      window.scrollTo({ top: top - 100, behavior: "smooth" })
    }, 300)
  }

  handleScrollDetails(ev) {
    ev.preventDefault()
    let top = document.getElementById("details").offsetTop
    console.log("top", top)
    window.scrollTo({ top: top - 100, behavior: "smooth" })
  }

  handleScrollTicketing(ev) {
    ev.preventDefault()
    document.getElementById("ticketing").scrollIntoView({ behavior: "smooth" })
  }

  render() {
    const doc = this.props.data.prismic.allTickets_and_pricing_landing_pages.edges
      .slice(0, 1)
      .pop()
    console.log("TICKETS AND PRICING doc -->", doc)
    if (!doc) return null
    const {
      wide_image,
      title,
      subheading,
      body,
      meta_title,
      meta_description,
      featured_text_title,
      featured_text,
    } = doc.node

    let ticketOptionBlocks = body
      .filter(d => {
        return d.type === "ticket_option_block"
      })
      .map(d => d.fields)[0]
      .slice(0, 3)
    let interstitial = filterArrayByType(body, "interstitial")
    let interstitial_title, interstitial_text, anchor_text
    if (interstitial.length > 0) {
      ;({
        interstitial_title,
        interstitial_text,
        anchor_text,
      } = interstitial[0].primary)
    }

    const cta = filterArrayByType(body, "cta")[0].fields[0];

    return (
      <Layout title={title} section="visit" {...this.props}>
        <SEO
          title={meta_title}
          description={meta_description}
          canonical={this.props.location.origin + this.props.location.pathname}
        />
        <Hero image={wide_image} pageId={doc.node._meta.id} />
        <TitleBar
          title={RichText.asText(title)}
          description={RichText.asText(subheading)}
        />
        {/* <div className={`container ${styles.topAnchor}`}>
          <a href="#details" onClick={this.handleScrollDetails}>
            Get the details
          </a>
          .
        </div> */}
        <main>
          <section
            id="ticketing"
            className={`${styles.section} ${styles.ticketOptionBlockSection}`}
          >
            <div className="container">
              <div className="row d-flex justify-content-center">
                <div className={`col-lg-10 ${styles.cta}`}>
                  {filterArrayByType(body, "cta")[0] && (
                    <CTA
                      color="Pumpkin"
                      icon={cta.icon}
                      title={renderAsText(cta.cta_title).replace(/\$([0-9]{1,2})\.([0-9]{2})/g,"<br/>\$<span>$1<sup style='top:-.3em;'>$2</sup></span>")}
                      description={cta.cta_text.replace(/Plan ahead and save\./g,"<strong>Plan ahead and save.</strong><br/>")}
                    />
                  )}
                </div>
              </div>
            </div>
            <div className="container text-center">
              <h2 id="details" className={styles.featuredTitle}>
                {renderAsText(featured_text_title)}
              </h2>
              <div className="row">
                <div className="col-lg-10 offset-lg-1">
                  <Text>{featured_text}</Text>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row d-flex justify-content-center">
                {ticketOptionBlocks.map((d, i) => {
                  console.log("d ->", d)
                  return (
                    <div
                      className={`col-lg-4 d-flex ${styles.ticketOptionBlockContainer}`}
                      key={i}
                    >
                      <div
                        className={styles.ticketOptionBlock}
                        style={{ backgroundColor: ColorToHex(d.color_picker) }}
                      >
                        <div className={styles.ticketOptionBlockHeader}>
                          {RichText.asText(d.title1)}
                        </div>
                        <div className={styles.ticketOptionBlockBody}>
                          <p className={styles.ticketPricing}>
                            {d.ticket_pricing}
                          </p>
                          {!/^[A-Za-z].*/.test(d.plu_price) && (
                            <p
                              className={styles.ticketPrices}
                              dangerouslySetInnerHTML={{
                                __html: renderAsText(d.title1)
                                  .toLowerCase()
                                  .includes("jackson")
                                  ? getPrice(
                                      this.state.jacksonClayItems,
                                      d.plu_price,
                                      2
                                    )
                                      .replace(
                                        /\$/g,
                                        "<sup style='font-size: 28px'>$</sup>"
                                      )
                                      .replace(
                                        /\.([0-9]{2})/g,
                                        "<sup style='font-size: 28px'>$1</sup>"
                                      )
                                  : getPrice(this.state.items, d.plu_price, 2)
                                      .replace(
                                        /\$/g,
                                        "<sup style='font-size: 28px'>$</sup>"
                                      )
                                      .replace(
                                        /\.([0-9]{2})/g,
                                        "<sup style='font-size: 28px'>$1</sup>"
                                      ),
                              }}
                            ></p>
                          )}
                          {/^[A-Za-z].*/.test(d.plu_price) &&
                            d.plu_price.toLowerCase() !==
                              "free with postcard" && (
                              <p
                                className={styles.ticketPrices}
                                style={{ fontSize: "30px" }}
                                dangerouslySetInnerHTML={{
                                  __html: renderAsText(d.title1)
                                    .toLowerCase()
                                    .includes("jackson")
                                    ? getPrice(
                                        this.state.jacksonClayItems,
                                        d.plu_price,
                                        2
                                      )
                                        .replace(
                                          /\$/g,
                                          "<sup style='font-size: 28px'>$</sup>"
                                        )
                                        .replace(
                                          /\.([0-9]{2})/g,
                                          "<sup style='font-size: 28px'>$1</sup>"
                                        )
                                    : getPrice(this.state.items, d.plu_price, 2)
                                        .replace(
                                          /\$/g,
                                          "<sup style='font-size: 28px'>$</sup>"
                                        )
                                        .replace(
                                          /\.([0-9]{2})/g,
                                          "<sup style='font-size: 28px'>$1</sup>"
                                        ),
                                }}
                              ></p>
                            )}
                          {/^[A-Za-z].*/.test(d.plu_price) &&
                            d.plu_price.toLowerCase() ===
                              "free with postcard" && (
                              <div
                                style={{ maxWidth: 270 }}
                                className="d-flex row m-auto justify-content-center align-items-center"
                              >
                                <div className="col-auto">
                                  <Icon icon="postcard" color="white" />
                                </div>
                                <div
                                  className="col text-left"
                                  style={{ fontSize: "30px", paddingLeft: 0 }}
                                >
                                  FREE with Postcard!
                                </div>
                              </div>
                            )}
                          <p>{d.age_callout}</p>
                        </div>
                        <div className={styles.ticketOptionBlockFooter}>
                          {/^[A-Za-z].*/.test(d.plu_price) &&
                            d.plu_price.toLowerCase() ===
                              "free with postcard" && (
                              <p
                                style={{
                                  fontWeight: "bold",
                                  fontSize: "18px",
                                  marginBottom: 5,
                                }}
                              >
                                No purchase required
                              </p>
                            )}
                          {d.cta && (
                            <Button
                              color={d.color_picker}
                              isInverse
                              text={d.cta_label}
                              url={d.cta.url}
                              size="large"
                            />
                          )}
                          {d.cta === null && d.cta_option_1_link && (
                            <div className="dropdown">
                              <button
                                className={`btn dropdown-toggle ${styles.dropdownToggle}`}
                                style={{ color: ColorToHex(d.color_picker) }}
                                type="button"
                                id="dropdownMenuButton"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                              >
                                {d.cta_label}
                              </button>
                              <div
                                className={`${styles.dropdownMenu} dropdown-menu`}
                                aria-labelledby="dropdownMenuButton"
                              >
                                <a
                                  className="dropdown-item"
                                  href={
                                    d.cta_option_1_link
                                      ? d.cta_option_1_link.url
                                      : ""
                                  }
                                  style={{ color: ColorToHex(d.color_picker) }}
                                >
                                  {d.cta_option_1}
                                </a>
                                <a
                                  className="dropdown-item"
                                  href={
                                    d.cta_option_2_link
                                      ? d.cta_option_2_link.url
                                      : ""
                                  }
                                  style={{ color: ColorToHex(d.color_picker) }}
                                >
                                  {d.cta_option_2}
                                </a>
                              </div>
                            </div>
                          )}
                          {d.plu_price.toLowerCase() ===
                            "free with postcard" && (
                            <p
                              className={styles.ctaDetail}
                              style={{ marginTop: 0 }}
                            >
                              {d.cta_detail || (
                                <span style={{ opacity: 0 }} aria-hidden="true">
                                  .
                                </span>
                              )}
                            </p>
                          )}
                          {d.plu_price.toLowerCase() !==
                            "free with postcard" && (
                            <p className={styles.ctaDetail}>
                              {d.cta_detail || (
                                <span style={{ opacity: 0 }} aria-hidden="true">
                                  .
                                </span>
                              )}
                            </p>
                          )}
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          </section>

          {
            filterArrayByType(body, "body_text_paragraphs").length > 0 &&
            <section className={styles.section}>
            <div className="container">
              <div className="row">
                <div className="col-lg-10 offset-lg-1">
                  <TextBlocks body={body} />
                  <a
                    href="#details"
                    onClick={this.handleScrollTicketing}
                    className={styles.bottomAnchor}
                  >
                    Back to Ticket Options
                  </a>
                </div>
              </div>
            </div>
          </section>
          }
        </main>

        <section> 
          <CallOutBlock body={body} />
        </section>

        {interstitial_text && (
          <Modal
            show={this.state.show}
            onHide={this.handleClose}
            size="lg"
            dialogClassName={styles.modal}
          >
            <Modal.Header
              closeButton
              className={styles.modalHeader}
            ></Modal.Header>
            <Modal.Body className={styles.modalBody}>
              <h2 className={styles.modalTitle}>
                {renderAsText(interstitial_title)}
              </h2>
              {renderAsHtml(interstitial_text)}
              <div className={styles.modalButton}>
                <Button
                  text="Got it!"
                  size="large"
                  color="Orchid"
                  onClick={this.handleClose}
                />
              </div>
              <a href="" onClick={this.handleCloseScroll}>
                {renderAsHtml(anchor_text)}
              </a>
            </Modal.Body>
          </Modal>
        )}
      </Layout>
    )
  }
}
export default TicketsAndPricingLanding

export const query = graphql`
  {
    prismic {
      allTickets_and_pricing_landing_pages {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            wide_image
            title
            subheading
            featured_text_title
            featured_text
            anchor_text
            body {
              ... on PRISMIC_Tickets_and_pricing_landing_pageBodyTicket_option_block {
                type
                label
                fields {
                  age_callout
                  color_picker
                  cta_detail
                  cta_label
                  cta {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                  }
                  cta_option_1
                  cta_option_1_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                  }
                  cta_option_2
                  cta_option_2_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                  }
                  plu_price
                  ticket_pricing
                  title1
                }
              }
              ... on PRISMIC_Tickets_and_pricing_landing_pageBodyCta1 {
                type
                label
                fields {
                  cta_label
                  cta_text
                  cta_title
                  icon
                  color
                  cta_option_1
                  cta_option_1_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                  }
                  cta_option_2
                  cta_option_2_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                  }
                }
              }
              ... on PRISMIC_Tickets_and_pricing_landing_pageBodyBody_text_paragraphs {
                type
                label
                fields {
                  design_style
                  paragraph
                }
              }
              ... on PRISMIC_Tickets_and_pricing_landing_pageBodyInterstitial {
                type
                label
                primary {
                  anchor_text
                  interstitial_text
                  interstitial_title
                }
              }
              ... on PRISMIC_Tickets_and_pricing_landing_pageBodyDetailed_callout {
                type
                label
                fields {
                  title: title1
                  text: text1
                  color
                  cta_label
                  image: image1
                  link {
                    _linkType
                    ... on PRISMIC_Membership_landing_page {
                      _linkType
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                  }
                  second_cta_label
                  second_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`

/*
export const query = graphql`
{
  prismic {
    allTickets_and_pricing_landing_pages {
      edges {
        node {
          _meta {
            id
          }
          meta_title
          meta_description
          wide_image
          title
          subheading
          body {
            ... on PRISMIC_Tickets_and_pricing_landing_pageBodyTicket_option_block {
              type
              label
              fields {
                age_callout
                color_picker
                cta_detail
                cta_label
                plu_price
                ticket_pricing
                title1
              }
            }
             ... on PRISMIC_Tickets_and_pricing_landing_pageBodyAddOns {
              type
              fields {
                icon_selector
                addOn_title
                addOn_text
              }
              primary {
                subheading
                title1
              }
            }
            ... on PRISMIC_Tickets_and_pricing_landing_pageBodyCta1 {
              type
              label
              fields {
                cta_label
                cta_text
                cta_title
                icon
                color
              }
            }
          }
        }
      }
    }
  }
}
`
*/
