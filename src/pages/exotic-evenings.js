import React from "react";
import { graphql } from 'gatsby';
//import { Link } from "gatsby";
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import styles from './day-camps-landing.module.css';
import CampExperience from '../components/camp-experience/camp-experience';
import { filterArrayByType, renderAsText } from '../utility';
import { getItems } from '../galaxy';
import { getPrice } from '../utility';
import SectionBlock from "../components/section-block";
import AddOnBlock from "../components/add-on/add-on-block";

class ExoticEveningsLanding extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
			items: [],
    };
  }

  componentDidMount() {
		getItems( (res) => {
			this.setState( { items: res } )
		});
	} 
  
  render() {
		const doc = this.props.data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
    if (!doc) return null;
		console.log('doc', doc);
    const { wide_header_size, title, summary, cta, button_label, body, meta_title, meta_description } = doc.node;
		const { items } = this.state;
  
    var overnightCamps = filterArrayByType(body, "plu_feature_block");

    return (
      <Layout title={ title } section="rent" { ...this.props }>
        <SEO title={ meta_title } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
        <Hero image={ wide_header_size } pageId={ doc.node._meta.id } />
        <TitleBar 
        	title={ title }
        	description={ summary }
        	link={ cta } 
        	linkText={ button_label }
        />
        {
          overnightCamps.map((d, i) => {
						let prices = d.fields.map(field => ({ price: getPrice(items, field.plu, 0), description: field.plu_description }));
						
            return (
              <SectionBlock key={ i } className={styles.section}>
                <div className="container">
                  <div className="row">
                    <div className="col-md-12">
                      <CampExperience
                        color={ d.primary.color }
                        image={ d.primary.feature_image }
                        title={d.primary.feature_title}
                        richDescription={ d.primary.feature_text }
												prices={ prices }
                        btnText={ "Book Your Evening!" }
                        url={`/form/exotic-evening-request-form?eveningType=${ renderAsText(d.primary.feature_title).replace(/ /g,'-') }`}
                      />
                    </div>
                  </div>
                </div>
              </SectionBlock>

            );
          })
        }
        <SectionBlock >
          <div className="container">
            <div className="row">
              <div className="col-12">
                <AddOnBlock dark body={ body } />
              </div>
            </div>
          </div>
        </SectionBlock>

      </Layout>
    );
  }
}

export default ExoticEveningsLanding;

export const query = graphql`
{
  prismic {
    allLanding_page_hero_blocks(uid: "exotic-evenings") {
      edges {
        node {
          meta_title
          meta_description
          wide_header_size
          title
          summary
          feature
          button_label
          cta {
						_linkType
						... on PRISMIC__Document {
							_meta {
								id
								tags
								type
								uid
							}
						}
						... on PRISMIC__ExternalLink {
							_linkType
							url
						}
						... on PRISMIC__FileLink {
							_linkType
							url
						}
          }
          body_text
          tall_image
          _meta {
						id
						type
						uid
						tags
          }
          _linkType
          body {
            ... on PRISMIC_Landing_page_hero_blockBodyPlu_feature_block {
              type
              fields {
                plu
                plu_description
              }
              primary {
                color
                cta_label
                feature_image
                feature_text
                feature_title
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyAddOns {
              type
              fields {
                addOn_text: description
                addOn_title
                icon_selector
              }
              primary {
                subheading
                title1
              }
            }
          }
        }
      }
    }
  }
}
`
