import React, { useState, useEffect } from "react";
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import styles from './day-camps-landing.module.css';
import CampExperience from '../components/camp-experience/camp-experience';
import { getItems } from '../galaxy';
import { filterArrayByType, getPrice } from '../utility'

const KeeperForADay = (props) => {
	const { data, location } = props;
  const [items, setItems] = useState(0);

  useEffect(() => {
    getItems( (res) => {
    	setItems(res) 
    });
  }, []);
	
	const doc = data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
	console.log("DOC ", doc);
  if (!doc) return null;
	const { wide_header_size, title, summary, body, button_label, cta, meta_title="", meta_description } = doc.node;

	var features = filterArrayByType( body, "feature_block_with_plu_and_fotz")[0].fields;
	console.log("features", features);

	return (
		<Layout title={ title } section="camps" { ...props }>
			<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
			<Hero image={ wide_header_size } pageId={doc.node._meta.id}/>
			<TitleBar 
				title={ title } 
				description={ summary } 
				linkText={ button_label } 
				link={ cta } 
			/>
			{
				features.map((d, i) => {
					console.log('features', d);
					return (
						<section className={styles.section}>
							<div className="container">
								<div className="row">
									<div className="col-md-12">
										<CampExperience
											color={ d.color }
											image={ d.wide_image }
											title={ d.title }
											richDescription={ d.description }
											prices={[
												{
													price: getPrice(items, d.plu_price),
													description: d.plu_description
												}
											]}
											priceDesc={ d.additional_text }
											btnText={ d.cta_label }
											btnLink={ d.cta_link }
										/>
									</div>
								</div>
							</div>
						</section>
					);
				})
			}
		</Layout>
	);
}

export default KeeperForADay;

export const query = graphql`
{
    prismic {
			allLanding_page_hero_blocks(uid: "keeper-for-a-day") {
				edges {
					node {
						_meta {
							id
							uid
						}
						meta_description
						meta_title
						title
						wide_header_size
						summary
						feature
						button_label
						cta {
							_linkType
							... on PRISMIC__ExternalLink {
								_linkType
								url
							}
						}
						body {
							... on PRISMIC_Landing_page_hero_blockBodyFeature_block_with_plu_and_fotz {
								type
								label
								fields {
									wide_image
									title:title1
									description
									color
									additional_text
									plu_price
									plu_description
									cta_label
									cta_link {
										... on PRISMIC_Detail_with_plu {
											title
											_linkType
											_meta {
												id
												tags
												type
												uid
											}
										}
									}
								}
							}
						}
					}
				}
			}
    }
  }  
`
