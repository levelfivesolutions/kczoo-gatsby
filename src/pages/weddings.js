import React from "react";
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import styles from './corporate-meetings.module.css';
import { getItems } from '../galaxy'
import { filterArrayByType, renderAsText, getPrice } from '../utility'
import CorporateMeetingRoom from "../components/corporate-meeting/corporate-meeting-room";
import SectionBlock from "../components/section-block";
import CallOutBlock from "../components/call-out/call-out-block"

class WeddingsLanding extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
     	
    }
  }

  componentDidMount() {
		getItems( (res) => {
			this.setState( { items: res } )
		});
	}

  render() {
		const doc = this.props.data.prismic.allWeddings_landing_pages.edges.slice(0,1).pop();
    if (!doc) return null;
    const { button_label, cta_link, callout_text, meta_description, body, meta_title, subhead_description, title, wide_image } = doc.node;
  
    var rooms = [];
    rooms = this.props.data.prismic.allWedding_packages.edges.map(d => { return (d.node) });
    
    return (
      <Layout title={ title } { ...this.props }>
        <SEO title={ meta_title } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
        <Hero image={ wide_image } pageId={doc.node._meta.id} />
        <TitleBar 
        	title={ title } 
        	link={ cta_link }
        	linkText={ button_label } 
        	description={ subhead_description } 
        	calloutTitle={ callout_text } 
        />
        {
          rooms.map((d, i) => {
            console.log("GADSFADSFADSF ", d);
            return (
              <SectionBlock id={ d._meta.id } key={ i } className={styles.section}  >
                <div className="container" >
                  <div className="row">
                    <div className="col-md-12">
                      <CorporateMeetingRoom
                        color={ d.body && filterArrayByType( d.body , "color_picker")[0].primary.color_name }
                        image={ d.wide_image['Feature-Block'] }
                        title={ d.title }
                        description={d.body_text}
                        price={ getPrice( this.state.items, d.plu_price) }
                        quantifier="Rental Price"
                        btnText={ renderAsText(d.cta_label) }
                        link={ d.cta }
                      />
                    </div>
                  </div>
                </div>
              </SectionBlock>

            );
          })
        }
				<CallOutBlock body={body} />
      </Layout>
    );
  }
}

export default WeddingsLanding;

export const query = graphql`
{
  prismic {
    allWeddings_landing_pages {
      edges {
        node {
          _meta {
            id
          }
          body {
            ... on PRISMIC_Weddings_landing_pageBodyDetailed_callout {
              type
              fields {
                body_text
                button_label
                cta:cta1 {
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                }
                color
                image: image1
                title: title1
              }
            }
          }
          button_label
          cta_link {
            _linkType
            ... on PRISMIC__ExternalLink {
              _linkType
              url
            }
          }
          callout_text
          meta_description
          meta_title
          subhead_description
          summary
          tall_image
          thumbnail_image1
          title
          wide_image
        }
      }
    }
    allWedding_packages {
      edges {
        node {
          _meta {
            id
          }
          body {
            ... on PRISMIC_Wedding_packageBodyColor_picker {
              type
              primary {
                color_name
              }
            }
          }
          body_text
          cta_label
          cta {
						_linkType
						... on PRISMIC__Document {
							_meta {
								id
								tags
								type
								uid
							}
						}
						... on PRISMIC__ExternalLink {
							_linkType
							url
						}
          }
          image
          plu_price
          spot_image
          tall_image
          title
          wide_image
        }
      }
    }
  }
}
`
