import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/big-event/hero"
import SlicerR from "../../components/slice-r/slice-r"
import { renderImageUrl, renderAsText, ColorToHex } from "../../utility"
import AquariumNavigation from "../../components/aquarium-navigation/aquarium-navigation"
import Titlebar from "../../components/big-event/titlebar"
import SmartLink from "../../components/smart-link"
import Icon from "../../components/icon/icon"
import styles from "./sponsors.module.scss"

function Separator({ icon, name, color, className }) {
  const style = {
    backgroundColor: ColorToHex(color),
  }
  return (
    <div className={`${styles.separator} ${className}`}>
      <div className={styles.separatorCenter}>
        <div className={styles.iconContainer} style={style}>
          <Icon icon={icon} width="60" height="60" color="GREY" />
        </div>
        <h6 className={styles.separatorTitle}>{name}</h6>
      </div>
    </div>
  )
}

const AquariumSponsors = props => {
  const { data, location } = props
  console.log("data ->", data)

  const doc = data.prismic.allAquarium_sponsorss.edges.slice(0, 1).pop()
  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    page_title,
    introduction,
    body,
    subhead,
    presenting_sponsor,
    presenting_sponsor_link,
    sponsors_tier_1,
    sponsors_tier_2,
    sponsors_tier_3,
    sponsors_tier_4,
    sponsors_tier_5,
    sponsors_tier_6,
    sponsors_tier_7,
  } = doc.node

  const event = data.prismic.allAquarium_sponsorss.edges.slice(0, 1).pop().node
  console.log("event", event)

  const parentPage = {
    title: `Aquarium`,
    url: `/aquarium`,
  }
  return (
    <Layout
      title={meta_title || page_title}
      parent={parentPage}
      section="activities"
      {...props}
    >
      <SEO
        title={renderAsText(page_title)}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero
        wide_image={
          "https://images.prismic.io/kansascityzoo/18d80e69-ef66-4a36-bf5d-12557c513fd6_Aquarium-Pacific+Reef-Large+Windows.jpg?auto=compress,format"
        }
        pageId={doc.node._meta.id}
      />
      <AquariumNavigation {...event} id={_meta.id} location={location} />
      <Titlebar title={page_title} introduction={introduction} />
      <div className="offset-md-1 col-md-10 d-flex justify-content-center text-center">
        <em className={styles.subhead}>{subhead}</em>
      </div>
      <div className="container">
        {presenting_sponsor && (
          <section className={styles.section}>
            <Separator color="White" />
            <div className="row">
              <div className="col-md-4 offset-md-4 d-flex justify-content-center">
                <SmartLink link={presenting_sponsor_link}>
                  <img
                    src={renderImageUrl(presenting_sponsor)}
                    alt={presenting_sponsor?.alt || ""}
                    className="img-fluid"
                  />
                </SmartLink>
              </div>
            </div>
          </section>
        )}
        {sponsors_tier_1?.length > 0 && (
          <section className={styles.section}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <div className={`${styles.twoCol}`}>
                  {sponsors_tier_1.map(d => {
                    return (
                      <div className={`${styles.tierOne}`}>
                        <SmartLink link={d.link}>{d.name}</SmartLink>
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </section>
        )}
        {sponsors_tier_2?.length > 0 && (
          <section className={styles.section}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Separator color="White" icon="fish1" className="mb-2" />
                <div className={styles.columns}>
                  {sponsors_tier_2.map(d => {
                    return (
                      <div className={styles.linkWrapper}>
                        <SmartLink link={d.link}>{d.name}</SmartLink>
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </section>
        )}
        {sponsors_tier_3?.length > 0 && (
          <section className={styles.section}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Separator color="White" icon="fish2" className="mb-2" />
                <div className={styles.columns}>
                  {sponsors_tier_3.map(d => {
                    return (
                      <div className={styles.linkWrapper}>
                        <SmartLink link={d.link}>{d.name}</SmartLink>
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </section>
        )}
        {sponsors_tier_4?.length > 0 && (
          <section className={styles.section}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Separator color="White" icon="fish3" className="mb-2" />
                <div className={styles.columns}>
                  {sponsors_tier_4.map(d => {
                    return (
                      <div className={styles.linkWrapper}>
                        <SmartLink link={d.link}>{d.name}</SmartLink>
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </section>
        )}
        {sponsors_tier_5?.length > 0 && (
          <section className={styles.section}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Separator color="White" icon="fish4" className="mb-2" />
                <div className={styles.columns}>
                  {sponsors_tier_5.map(d => {
                    return (
                      <div className={styles.linkWrapper}>
                        <SmartLink link={d.link}>{d.name}</SmartLink>
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </section>
        )}
        {sponsors_tier_6?.length > 0 && (
          <section className={styles.section}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Separator color="White" icon="fish5" className="mb-2" />
                <div className={`${styles.columns} ${styles.two}`}>
                  {sponsors_tier_6.map(d => {
                    return (
                      <div className={styles.linkWrapper}>
                        <SmartLink link={d.link}>{d.name}</SmartLink>
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </section>
        )}
        {sponsors_tier_7?.length > 0 && (
          <section className={styles.section}>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Separator color="White" icon="fish6" className="mb-2" />
                <div className={styles.columns}>
                  {sponsors_tier_7.map(d => {
                    return (
                      <div className={styles.linkWrapper}>
                        <SmartLink link={d.link}>{d.name}</SmartLink>
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </section>
        )}
      </div>
      {body.map(slice => {
        return <SlicerR data={slice} />
      })}
    </Layout>
  )
}

export default AquariumSponsors

export const query = graphql`
  query AquariumSponsors {
    prismic {
      allAquarium_sponsorss {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            page_title
            introduction
            subhead
            presenting_sponsor
            presenting_sponsor_link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            presenting_sponsor_name
            sponsors_tier_1 {
              logo
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_2 {
              logo
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_3 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_4 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_5 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_6 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_7 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            body {
              ... on PRISMIC_Aquarium_sponsorsBodyReused_image_text_block {
                type
                label
                primary {
                  inner_block {
                    _linkType
                    ... on PRISMIC_Reusable_image_text_block {
                      image_text_image
                      image_text_title
                      image_text_text
                      image_text_color
                      image_text_link_text
                      image_text_link {
                        ... on PRISMIC_Aquarium_donations {
                          _linkType
                          _meta {
                            id
                            uid
                            type
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
