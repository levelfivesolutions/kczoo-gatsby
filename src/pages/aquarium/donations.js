import React, { useEffect } from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import TitleBar from "../../components/titlebar-landing/titlebar-landing.js"
import Hero from "../../components/hero-detail"
import Title from "../../components/title"

import SlicerR from "../../components/slice-r/slice-r"
import {
  ColorToHex,
  renderAsHtml,
  renderAsText,
  linkResolver,
} from "../../utility"
import AquariumNavigation from "../../components/aquarium-navigation/aquarium-navigation"
import styles from "./aquarium-donations.module.css"
import ticketingStyles from "../tickets-and-pricing.module.css"
import Button from "../../components/button/button-link"
import { RichText } from "prismic-reactjs"

const AquariumDonations = props => {
  const { data, location } = props
  const doc = data.prismic.allAquarium_donationss.edges.slice(0, 1).pop()

  // useEffect(() => {
  //   setTimeout(() => {
  //     document.getElementById(
  //       "thermometer-color"
  //     ).style.maxHeight = `calc(450px * (${aquarium_donation_currently_raised} / ${aquarium_donation_goal_amount}))`
  //   }, 2000)
  // }, [])
  console.log("DOC", doc)
  if (!doc) return null
  const {
    aquarium_donations_body_text,
    aquarium_donation_goal_amount,
    aquarium_donation_currently_raised,
    aquarium_donations_description,
    aquarium_donations_title,
    donation_types,
    meta_description,
    meta_title,
    spot_image,
    tall_image,
    title,
    wide_image,
    body,
  } = doc.node

  return (
    <Layout
      title={aquarium_donations_title}
      parent={{
        title: "Aquarium",
        url: "/aquarium",
      }}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero image={wide_image} pageId={doc.node._meta.id} />
      <AquariumNavigation location={location} />
      <section
        id="ticketing"
        className={`${ticketingStyles.section} ${ticketingStyles.ticketOptionBlockSection}`}
      >
        <div className="container">
          {aquarium_donations_title && (
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Title className={styles.title}>
                  {aquarium_donations_title}
                </Title>
              </div>
            </div>
          )}
          {aquarium_donations_description && (
            <div className="row">
              <div className="offset-md-1 col-md-10">
                {renderAsHtml(aquarium_donations_description)}
              </div>
            </div>
          )}
          {body.map(slice => {
            return <SlicerR data={slice} />
          })}
        </div>
      </section>
      {/* <section
        className={`${ticketingStyles.section} ${ticketingStyles.ticketOptionBlockSection}`}
        style={{ backgroundColor: "#F3F3F3", paddingTop: 1, paddingBottom:1 }}
      >
        <div
          className="container"
          style={{
            marginTop: "80px",
            marginBottom: "40px",
          }}
        >
          <div className="row">
            <div className="col-md-8">
              {renderAsHtml(aquarium_donations_body_text)}
            </div>
            <div className={`col-md-4 d-flex justify-content-end position-relative ${styles.thermometerContainer}`}>
              <div className={styles.thermometerImage}>
                <div className={styles.thermometerScale}></div>
                <div className={styles.thermometerGoal}>
                  &#36;{aquarium_donation_goal_amount}M Goal
                </div>
                <div className={styles.thermometerColor} id="thermometer-color">
                  <div className={styles.thermometerArrow}>
                    <div className={styles.amountRaised}>
                      <div style={{ fontSize: "34px", lineHeight: "36px" }}>
                      &#36;{aquarium_donation_currently_raised}M
                      </div>
                      <div style={{ fontSize: "20px" }}>Raised</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section> */}
      <section
        className={`${ticketingStyles.section} ${ticketingStyles.ticketOptionBlockSection}`}
        style={{ backgroundColor: "#F3F3F3" }}
      >
        <div className="container">
          <div className={`${styles.donationRow} row`}>
            {donation_types.map((d, i) => {
              return (
                <div
                  className={`col-lg-4 ${ticketingStyles.ticketOptionBlockContainer}`}
                  key={i}
                >
                  <div
                    className={ticketingStyles.ticketOptionBlock}
                    style={{
                      backgroundColor: ColorToHex(d.donation_type_color),
                    }}
                  >
                    <div className={ticketingStyles.ticketOptionBlockHeader}>
                      {renderAsHtml(d.donation_type_title)}
                    </div>
                    <div
                      className={ticketingStyles.ticketOptionBlockBody}
                      style={{ paddingTop: "20px" }}
                    >
                      <p
                        style={{
                          fontSize: "36px",
                          fontFamily: "LifeHack Sans",
                          lineHeight: "40px",
                        }}
                      >
                        {renderAsText(d.donation_type_text)}
                      </p>
                      <p
                        style={{
                          fontSize: "20px",
                          lineHeight: "22px",
                        }}
                      >
                        {renderAsHtml(d.donation_type_description)}
                      </p>
                    </div>
                    <div className={ticketingStyles.ticketOptionBlockFooter}>
                      {d.donation_type_link && (
                        <Button
                          color={d.donation_type_color}
                          isInverse
                          text={d.donation_type_link_text}
                          url={linkResolver(d.donation_type_link)}
                          size="large"
                        />
                      )}
                      <p
                        className={ticketingStyles.ctaDetail}
                        style={{ marginTop: "40px" }}
                      ></p>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default AquariumDonations

export const query = graphql`
  {
    prismic {
      allAquarium_donationss {
        edges {
          node {
            aquarium_donation_currently_raised
            _meta {
              uid
              id
            }
            aquarium_donation_goal_amount
            aquarium_donations_title
            aquarium_donations_description
            aquarium_donations_body_text
            body {
              ... on PRISMIC_Aquarium_donationsBodyReused_image_text_block {
                type
                label
                primary {
                  inner_block {
                    ... on PRISMIC_Reusable_image_text_block {
                      image_text_image
                      image_text_title
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                      image_text_background_image
                      image_text_color
                      image_text_text
                      image_text_link_text
                      image_text_link {
                        ... on PRISMIC__ImageLink {
                          _linkType
                          url
                        }
                        ... on PRISMIC__FileLink {
                          _linkType
                          url
                        }
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                        ... on PRISMIC_Form {
                          _meta {
                            uid
                            id
                            type
                            tags
                          }
                        }
                        ... on PRISMIC_Aquarium_donations {
                          _meta {
                            uid
                            type
                            tags
                            id
                          }
                        }
                        ... on PRISMIC_Detail {
                          _meta {
                            uid
                            type
                            tags
                            id
                          }
                        }
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_Aquarium_donationsBodyImage_text_block {
                type
                label
                primary {
                  image_text_color
                  image_text_image
                  image_text_text
                  image_text_title
                  image_text_link_text
                  image_text_link {
                    _linkType
                    ... on PRISMIC__ImageLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC_Detail {
                      _linkType
                      _meta {
                        uid
                        type
                        tags
                      }
                    }
                    ... on PRISMIC_Form {
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                    }
                  }
                }
              }
            }
            meta_description
            meta_title
            spot_image
            tall_image
            wide_image
            donation_types {
              donation_type_link {
                ... on PRISMIC_Form {
                  _meta {
                    uid
                    type
                    tags
                    id
                  }
                }
                ... on PRISMIC__ImageLink {
                  _linkType
                  url
                }
                ... on PRISMIC__FileLink {
                  _linkType
                  url
                }
                ... on PRISMIC__ExternalLink {
                  target
                  _linkType
                  url
                }
                ... on PRISMIC_Detail {
                  _meta {
                    uid
                    type
                    tags
                    id
                  }
                }
              }
              donation_type_link_text
              donation_type_text
              donation_type_description
              donation_type_title
              donation_type_color
            }
          }
        }
      }
    }
  }
`
