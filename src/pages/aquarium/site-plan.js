import React, { useEffect, useState } from "react"
import Layout from "../../components/layout/layout"
import SectionBlock from "../../components/section-block"
import AquariumMap from "../../components/aquarium-map/aquarium-map.jsx"
import { filterArrayByType } from "../../utility"
import TitleBar from "../../components/titlebar-landing/titlebar-landing.js"
import { Modal } from "react-bootstrap"
import styles from "./site-plan.module.css"
import Button from "../../components/button/button-link"
import { renderAsHtml, renderAsText } from "../../utility"
import Title from "../../components/title"
import SEO from "../../components/seo"
import Hero from "../../components/hero-detail"
import AquariumNavigation from "../../components/aquarium-navigation/aquarium-navigation"
import SliceR from "../../components/slice-r/slice-r"
import AquariumSubAreaModal from "../../components/aquarium-sub-area-modal/aquarium-sub-area-modal"
import Icon from "../../components/icon/icon"


export default props => {
  const { data, location } = props
  console.log(">>>", data)

  const doc = data.prismic.allAquarium_site_plans.edges.slice(0, 1).pop()
  console.log("doc -->", doc)
  if (!doc) return null

  const [openModals, setOpenModals] = useState({})
  const [constructionModalOpen, setConstructionModalOpen] = useState(false);

  /**
   * @param id - the id of the modal to show
   */
  function showDynamicModal(id) {
    let newModal = { ...openModals }
    newModal[id] = true
    setOpenModals({ ...newModal })
  }

  /**
   * @param id - the id of the modal to hide
   */
  function hideDynamicModal(id) {
    let newModal = { ...openModals }
    newModal[id] = false
    setOpenModals({ ...newModal })
  }

  const areas = filterArrayByType(doc.node.body, "aquarium_area")

  useEffect(() => {
    areas.forEach(area => {
      if (area.primary.area_id) {
        let element = document.getElementById(area.primary.area_id)
        if (typeof element != "undefined") {
          element.addEventListener("click", () => {
            console.log("test id")
            showDynamicModal(area.primary.area_id)
          })
        }

        element = document.getElementsByClassName(area.primary.area_id)[0]
        if (typeof element != "undefined") {
          console.log("test")
          element.addEventListener("click", () => {
            showDynamicModal(area.primary.area_id)
          })
        }
      }
    })
  }, [])

  let { title, summary, meta_title, meta_description, construction_cam_thumbnail, construction_cam_youtube_video_url } = doc.node

  console.log({ areas })

  var vidRegex = /v=([0-9a-zA-Z].*)/g;
  var vidUrlMatch = vidRegex.exec(construction_cam_youtube_video_url.url);
  return (
    <Layout
      title={title}
      parent={{
        title: "Aquarium",
        url: "/aquarium",
      }}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero
        image={doc.node.wide_image}
        tallImage={doc.node.tall_image}
        pageId={doc.node._meta.id}
      />
      <AquariumNavigation location={location} />
      <SectionBlock paddingBottom={false} color="White">
        <div className="container">
          {title && (
            <div className="row">
              <div className="offset-md-2 text-center txt-black col-md-8">
                <Title className={styles.title}>{title}</Title>
              </div>
            </div>
          )}
          {summary && (
            <div className="row">
              <div className="offset-md-2 text-center col-md-8">
                {renderAsHtml(summary)}
              </div>
            </div>
          )}
          <div className="row">
            <div className="col-md-12 p-0">
              {/* <div className={styles.constructionCamWindow}>

              </div> */}
              <AquariumMap
                area_1_thumb={areas[0].primary.area_thumbnail_image}
                thumbUrl={construction_cam_thumbnail?.url || ""}
                setConstructionModalOpen={setConstructionModalOpen}
              />
              <div className={styles.mapKeyContainer} >
                <div className={styles.mapKeyHead}>Zones</div>
                <div className="d-flex justify-content-center">
                  <div className={styles.mapKeyBody}>
                    <p className="Exhibit_Zone_1 justify-content-between">
                      <div className="d-flex">
                        <span>1</span> WARM COASTLINE
                      </div>{" "}
                      <Icon icon="modal" color="white" />{" "}
                    </p>
                    <p className="Exhibit_Zone_2 justify-content-between">
                      <div className="d-flex">
                        <span>2</span> WARM SHALLOWS
                      </div>
                      <Icon icon="modal" color="white" />
                    </p>
                    <p className="Exhibit_Zone_3 justify-content-between">
                      <div className="d-flex">
                        <span>3</span> WARM REEF
                      </div>
                      <Icon icon="modal" color="white" />
                    </p>
                    <p className="Exhibit_Zone_4 justify-content-between">
                      <div className="d-flex">
                        <span>4</span> OPEN OCEAN
                      </div>
                      <Icon icon="modal" color="white" />
                    </p>
                    <p className="Exhibit_Zone_5 justify-content-between">
                      <div className="d-flex">
                        <span>5</span> COLD SHALLOWS
                      </div>
                      <Icon icon="modal" color="white" />
                    </p>
                    <p className="Exhibit_Zone_6 justify-content-between">
                      <div className="d-flex">
                        <span>6</span> COLD COASTLINE
                      </div>
                      <Icon icon="modal" color="white" />
                    </p>
                  </div>
                </div>
              </div>
              <svg style={{ marginBottom: 20}} onClick={() => setConstructionModalOpen(true)} className="constructionCam d-block d-md-none" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" width="100%" viewBox="0 0 388 282">
        <defs>
          <pattern id="camPattern" width="1" height="1" viewBox="0 27.914 399.878 186.086">
            <image preserveAspectRatio="xMidYMid slice" width="100%" height="100%" href={construction_cam_thumbnail?.url || ""}/>
          </pattern>
          <filter id="Rectangle_1062_2" x="0" y="0" width="388" height="282" filterUnits="userSpaceOnUse">
            <feOffset dy="3" input="SourceAlpha"/>
            <feGaussianBlur stdDeviation="3" result="blur"/>
            <feFlood flood-opacity="0.161"/>
            <feComposite operator="in" in2="blur"/>
            <feComposite in="SourceGraphic"/>
          </filter>
          <clipPath id="clip-path_2">
            <rect id="Rectangle_1058" data-name="Rectangle 1058" width="128" height="55" transform="translate(-0.4 -0.115)" fill="#fff" stroke="#707070" stroke-width="1"/>
          </clipPath>
          <clipPath id="clip-path-2_2">
            <rect id="Rectangle_1057" data-name="Rectangle 1057" width="115.556" height="58.957" fill="#fff" stroke="#707070" stroke-width="1"/>
          </clipPath>
        </defs>
        <g id="Group_1822" data-name="Group 1822" transform="translate(-62 -1078.439)">
          <g transform="matrix(1, 0, 0, 1, 62, 1078.44)" filter="url(#Rectangle_1062_2)">
            <g id="Rectangle_1062-2" data-name="Rectangle 1062" transform="translate(9 6)" stroke="#fff" stroke-width="10" fill="url(#camPattern)">
              <rect width="370" height="264" rx="5" stroke="none"/>
              <rect x="5" y="5" width="360" height="254" fill="none"/>
            </g>
          </g>
          <g id="Group_1821" data-name="Group 1821" transform="translate(82.401 1095.554)">
            <g id="Group_1819" data-name="Group 1819" transform="translate(0 0)">
              <rect id="Rectangle_1056" data-name="Rectangle 1056" width="349" height="55" transform="translate(-1.4 -0.115)" fill="#fdc843"/>
            </g>
            <text id="SEE_OUR_PROGRESS_" data-name="SEE OUR PROGRESS!" transform="translate(140.337 37.054)" fill="#222" font-size="27" font-family="DINCondensedBold, DINOT" font-weight="700"><tspan x="0" y="0">SEE OUR PROGRESS!</tspan></text>
            <g id="Mask_Group_47" data-name="Mask Group 47" transform="translate(-1 0)" clip-path="url(#clip-path_2)">
              <g id="Group_1820" data-name="Group 1820" transform="translate(-0.289 -0.448)" clip-path="url(#clip-path-2_2)">
                <line id="Line_242" data-name="Line 242" x1="74.52" y2="76.041" transform="translate(-15.879 -7.406)" fill="none" stroke="#222" stroke-width="15"/>
                <line id="Line_243" data-name="Line 243" x1="74.52" y2="76.041" transform="translate(-55.1 -7.406)" fill="none" stroke="#222" stroke-width="15"/>
                <path id="Path_10104" data-name="Path 10104" d="M64.505-7-10.762,66.651" transform="translate(34.104 -0.406)" fill="none" stroke="#222" stroke-width="15"/>
                <path id="Path_10105" data-name="Path 10105" d="M64.505-7-10.762,66.651" transform="translate(73.325 -0.406)" fill="none" stroke="#222" stroke-width="15"/>
                <path id="Path_10106" data-name="Path 10106" d="M64.505-7-10.762,66.651" transform="translate(112.546 -0.406)" fill="none" stroke="#222" stroke-width="15"/>
              </g>
            </g>
          </g>
          <path id="Path_10107" data-name="Path 10107" d="M0,0H347.778V51.111H0Z" transform="translate(82.111 1287.328)" fill="#222" opacity="0.5"/>
          <g id="Group_1826" data-name="Group 1826" transform="translate(193.681 1301.217)">
            <text id="View_Larger" data-name="View Larger" transform="translate(0 18)" fill="#fff" font-size="17" font-family="DINRoundOT-Bold, DIN Round OT" font-weight="700"><tspan x="0" y="0">View Larger</tspan></text>
            <path id="expand" d="M8.073,9.807a.341.341,0,0,1,0,.5l-3.6,3.6,1.56,1.56a.694.694,0,0,1-.488,1.181H.694a.667.667,0,0,1-.488-.206A.666.666,0,0,1,0,15.951V11.1a.666.666,0,0,1,.206-.488A.666.666,0,0,1,.694,10.4a.666.666,0,0,1,.488.206l1.56,1.56,3.6-3.6a.341.341,0,0,1,.5,0ZM16.645.694V5.548a.69.69,0,0,1-1.181.488L13.9,4.475l-3.6,3.6a.341.341,0,0,1-.5,0L8.572,6.838a.341.341,0,0,1,0-.5l3.6-3.6-1.56-1.56A.666.666,0,0,1,10.4.694a.666.666,0,0,1,.206-.488A.666.666,0,0,1,11.1,0h4.855a.666.666,0,0,1,.488.206A.667.667,0,0,1,16.645.694Zm0,14.564" transform="translate(107.993 6.519)" fill="#fff"/>
          </g>
        </g>
      </svg>
            </div>
          </div>
        </div>
      </SectionBlock>
      {doc.node.body.map(slice => {
        return <SliceR key={Math.random * 999999} data={slice} />
      })}
      {areas &&
        areas.map(area => (
          <AquariumSubAreaModal
            area={area}
            hideDynamicModal={hideDynamicModal}
            openModals={openModals}
          />
        ))}
          <Modal
      key="constructionCamModal"
      show={constructionModalOpen}
      onHide={() => setConstructionModalOpen(false)}
      size="xl"
    >
      <Modal.Body>
      <div
        className="container-fluid d-flex justify-content-center"
        style={{
          backgroundImage: `url(/images/ConstructionStripes@2x.png)`,
          backgroundRepeat: "repeat-y",
          backgroundSize: "116px 160px",
          paddingLeft: 130,
          backgroundColor: "#FDC843",
          minHeight: 60,
          paddingTop: "10px",
          paddingBottom: "10px",
        }}
      >
        <div className="row d-flex align-items-center flex-grow-1 justify-content-between" style={{padding: '0 15px'}}>
          <div></div>
          <div className="col-md-auto d-none d-md-flex">
            <p
              style={{
                lineHeight: "30px",
                whiteSpace: "nowrap",
                fontFamily: "DINCondensedBold",
                fontSize: "30px",
                marginBottom: 0,
                paddingTop: 8,
              }}
            >
              SEE OUR PROGRESS!
            </p>
          </div>
          <div role="button" onClick={() => setConstructionModalOpen(false)} style={{ cursor: 'pointer'}}>
            <Icon icon="close" width="30"  />
          </div>
        </div>
      </div>
      {vidUrlMatch[1] &&
        <iframe width="100%" height="622" src={`https://www.youtube.com/embed/${vidUrlMatch[1]}`} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      }</Modal.Body>
    </Modal>
    </Layout>
  )
}

export const query = graphql`
{
  prismic {
    allAquarium_site_plans {
      edges {
        node {
          body {
            ... on PRISMIC_Aquarium_site_planBodyAquarium_area {
              type
              label
              fields {
                sub_area_image
                sub_area_title
                sub_area_description
                sub_area_water_capacity
                sub_area_available_for_naming_rights
              }
              primary {
                area_id
                area_name
                area_thumbnail_image
              }
            }
            ... on PRISMIC_Aquarium_site_planBodyReused_image_text_block {
              type
              label
              primary {
                inner_block {
                  ... on PRISMIC_Reusable_image_text_block {
                    image_text_image
                    image_text_title
                    image_text_background_image
                    _linkType
                    _meta {
                      uid
                      id
                    }
                    image_text_text
                    image_text_link_text
                    image_text_link {
                      ... on PRISMIC__ImageLink {
                        _linkType
                        url
                      }
                      ... on PRISMIC__FileLink {
                        _linkType
                        url
                      }
                      ... on PRISMIC__ExternalLink {
                        target
                        _linkType
                        url
                      }
                      ... on PRISMIC_Detail {
                        _linkType
                        _meta {
                          id
                          uid
                          tags
                          type
                        }
                      }
                      ... on PRISMIC_Aquarium_donations {
                        _linkType
                        _meta {
                          id
                          uid
                          tags
                          type
                        }
                      }
                      ... on PRISMIC_Form {
                        _linkType
                        _meta {
                          uid
                          id
                          tags
                          type
                        }
                      }
                      _linkType
                    }
                    image_text_color
                  }
                }
              }
            }
          }
          above_map_text
          _meta {
            uid
          }
          meta_description
          meta_title
          spot_image
          summary: summary1
          tall_image
          title
          wide_image
          construction_cam_thumbnail
          construction_cam_youtube_video_url {
            ... on PRISMIC__ExternalLink {
              target
              _linkType
              url
            }
          }
        }
      }
    }
  }
}
`
