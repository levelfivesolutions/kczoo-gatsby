import React, { useState, useEffect } from "react";
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import styles from './day-camps-landing.module.css';
import SectionBlock from '../components/section-block';
import CardLanding from '../components/card-landing';
import { renderAsText, renderAsHtml, filterArrayByType, getPrice } from '../utility';
import { getItems } from '../galaxy';

class AnimalEncounters extends React.Component {

  constructor(props) {console.log('props', props);
    super(props);
    this.state = {
			items: []
    };
  }

  componentDidMount() {
		getItems( (res) => {
			this.setState( { items: res } )
		});
	}

  render() {
		const { items } = this.state;
		const doc = this.props.data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
		console.log('ANIMAL ENCOUNTERS doc', this.state.doc);
    if (!doc) return null;
		const { wide_header_size, title, summary, feature, cta, button_label, body, meta_title, meta_description, _meta } = doc.node;
		
		let features = filterArrayByType(body, 'feature_block').pop().fields;
		console.log('features', features);

		return (
			<Layout title={ title } section="animals" { ...this.props }>
				<SEO title={ meta_title } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
				<Hero image={ wide_header_size }  pageId={ _meta.id }/>
				<TitleBar 
					title={ title } 
					description={ summary }
					calloutDescription={ feature }
					link={ cta }
					linkText={ button_label }
				/>				
				{
					features.map((d, i) => {console.log(d);
						//let prices = d.fields.map(field => ({ price: getPrice(items, field.plu, 0), description: field.plu_description }));
						return (
							<SectionBlock key={ i }>
								<div className="container">
									<div className="row">
										<div className="col-md-12">
											<CardLanding
												color={ d.color_name }
												image={ d.callout_image }
												title={ d.callout_title }
												description={ d.callout_description }
												prices={ [] }
												urlText={ d.cta_text }
												link={ d.cta }
											/>
										</div>
									</div>
								</div>
							</SectionBlock>

						);
					})
				}		
			</Layout>
		);
	}
}

export default AnimalEncounters;

export const query = graphql`
{
  prismic {
    allLanding_page_hero_blocks(uid: "animal-encounters") {
      edges {
        node {
          _meta {
            uid
            type
            tags
            id
          }
          meta_title
          meta_description
          wide_header_size
          title
          summary
          body_text
          feature
          button_label
          cta {
						_linkType
						... on PRISMIC__Document {
							_meta {
								id
								tags
								type
								uid
							}
						}
						... on PRISMIC__ExternalLink {
							_linkType
							url
						}
						... on PRISMIC__FileLink {
							_linkType
							url
						}
          }
          body {
            ... on PRISMIC_Landing_page_hero_blockBodyFeature_block {
              type
              label
              fields {
                callout_description
                callout_image
                callout_title
                color_name
                cta_text
                cta:cta1 {
                  ... on PRISMIC_Detail {
                    _meta {
                      uid
                      type
											tags
                    }
                    _linkType
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
`
