import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import Text from "../components/text"
import Button from "../components/button/button-link.js"
import Hero from "../components/hero-landing/hero-landing.js"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import AddOnBlock from "../components/add-on/add-on-block"
import styles from "./hours-location-parking.module.css"

class HoursLocationParking extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const { data } = this.props
    console.log("data", data)
    const doc = data.prismic.allHours_location_parking_landing_pages.edges
      .slice(0, 1)
      .pop()
    console.log("doc -->", doc)
    if (!doc) return null
    const {
      title,
      meta_title,
      meta_description,
      wide_image,
      address,
      hours,
      body,
    } = doc.node

    return (
      <Layout title={title} section="visit" {...this.props}>
        <SEO
          title={meta_title}
          canonical={this.props.location.origin + this.props.location.pathname}
          description={meta_description}
        />
        <Hero image={wide_image} pageId={doc.node._meta.id} />
        <TitleBar title={title} calloutDescription={hours} />
        <div className={styles.contentArea}>
          <div className="container">
            <div className="row">
              <div className="col-md-5">
                <iframe
                  title="Kansas City Zoo Map"
                  className={styles.map}
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d49605.91943092049!2d-94.56418332110735!3d39.00687327955395!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87c0e5ca1583a001%3A0xb44f49e90df6aefd!2sKansas%20City%20Zoo!5e0!3m2!1sen!2sus!4v1580402537629!5m2!1sen!2sus"
                  width="600"
                  height="450"
                ></iframe>
              </div>
              <div className="col-md-7">
                <h2 className={styles.contentTitle}>Zoo Location</h2>
                <Text>{address}</Text>
                <Button
                  icon="location"
                  color="Zoo Green"
                  text="Get Directions"
                  url="https://goo.gl/maps/txPgibjFcZ7XBVax7"
                />
              </div>
            </div>
          </div>
        </div>
        <section className={styles.section}>
          {body && <AddOnBlock body={body} dark={true} />}
        </section>
      </Layout>
    )
  }
}

export default HoursLocationParking

export const query = graphql`
  query HoursLocationParkingQuery {
    prismic {
      allHours_location_parking_landing_pages {
        edges {
          node {
            title
            body {
              ... on PRISMIC_Hours_location_parking_landing_pageBodyAddOns {
                fields {
                  text
                  icon_selector
                  addOn_title
                }
                primary {
                  subheading
                  title1
                }
                type
                label
              }
            }
            address
            hours
            wide_image
            _meta {
              id
              uid
            }
            meta_description
            meta_title
          }
        }
      }
    }
  }
`
