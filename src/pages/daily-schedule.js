import React from "react"
import { graphql } from "gatsby"
import { RichText } from "prismic-reactjs"
import Link from "gatsby-link"
import Layout from "../components/layout/layout"
import moment from "moment"
//import get from 'lodash/get';
//import Helmet from 'react-helmet';
//import Img from 'gatsby-image';
import groupBy from "lodash.groupby"
import sortBy from "lodash.sortby"
import sorteduniqby from "lodash.sorteduniqby"
import Button from "../components/button/button-link.js"
import Hero from "../components/hero-landing/hero-landing.js"
import TitleBar from "../components/titlebar-landing/titlebar-landing.js"
import TextBlocks from "../components/text-blocks"
import CallOutSimple from "../components/call-out-simple"
import styles from "./daily-schedule.module.css"
import { generateActivitiesByDate, linkResolver } from "../utility"
import "react-dates/lib/css/_datepicker.css"
import "react-dates/initialize"
import { SingleDatePicker } from "react-dates"
import SEO from "../components/seo"
import { renderAsText } from "../utility"

class MenuItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}

    this.handleClick = this.handleClick.bind(this)
  }
  handleClick(ev) {
    ev.preventDefault()
    this.props.onChange(this.props.value)
  }
  render() {
    return (
      <a className="dropdown-item" href="#" onClick={this.handleClick}>
        {this.props.value}
      </a>
    )
  }
}

class ScheduleLanding extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      focused: false,
      activityData: props.data.prismic.allActivitys.edges,
      totalCount: props.data.prismic.allActivitys.totalCount,
      filterDate: moment(),
      filterActivityName: "All Activities",
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleDatePickerOpen = this.handleDatePickerOpen.bind(this)
  }
  loadActivities(cursor) {
    console.log("DAILY SCHEDULE props", this.props)
    if (this.props.prismic) {
      this.props.prismic
        .load({
          variables: { cursor: cursor },
          query, // (optional)
          fragments: [], // (optional)
        })
        .then(res => {
          console.log("LOADED", res.data)
          this.setState({
            activityData: this.state.activityData.concat(
              res.data.allActivitys.edges
            ),
          })
          if (res.data.allActivitys.pageInfo.hasNextPage) {
            this.loadActivities(res.data.allActivitys.pageInfo.endCursor)
          }
        })
        .catch(function(err) {
          // This will fix your error since you are now handling the error thrown by your first catch block
          console.log(err.message)
        })
    }
  }
  componentDidMount() {
    let {
      hasNextPage,
      endCursor,
    } = this.props.data.prismic.allActivitys.pageInfo
    if (hasNextPage) {
      this.loadActivities(endCursor)
    }
  }
  handleChange(activity) {
    this.setState({ filterActivityName: activity })
  }
  handleDatePickerOpen(ev) {
    ev.stopPropagation()
    if (!this.state.focused) {
      this.setState({ focused: true })
    }
  }
  render() {
    const doc = this.props.data.prismic.allActivity_schedule_landing_pages.edges
      .slice(0, 1)
      .pop()
    console.log("DAILY SCHEDULE doc -->", doc)
    if (!doc) return null

    let {
      image,
      title,
      introduction_text,
      feature,
      cta,
      body,
      _meta,
    } = doc.node
    let { activityData, totalCount, filterDate } = this.state

    let timeRows = []
    let activityNames = []

    // RENDER ACTIVITIES AFTER ALL HAVE BEEN LOADED
    if (activityData.length === totalCount) {
      console.log("activityData -->", activityData)

      let activities = generateActivitiesByDate(activityData, filterDate)
      console.log("activities -->", activities)

      // BUILD A LIST OF ACTIVITY NAMES
      activityNames = sorteduniqby(activities, d =>
        RichText.asText(d.title)
      ).map(d => RichText.asText(d.title))

      // FILTER BY ACTIVITY
      if (this.state.filterActivityName !== "All Activities") {
        activities = activities.filter(
          d => RichText.asText(d.title) === this.state.filterActivityName
        )
      }
      console.log("Activities filtered", activities)

      // GROUP BY TIME
      let groups = groupBy(activities, d => d.date.format())
      console.log("groups", groups)

      timeRows = Object.keys(groups).map((key, i) => {
        let sorted = sortBy(groups[key], [
          function(d) {
            return RichText.asText(d.title)
          },
        ])
        let items = sorted.map((d, j) => {
          //   console.log(j + ". d", d)
          return (
            <div className={styles.activityDetail} key={j}>
              <div className="row">
                <div
                  className={styles.wholePageEditButton}
                  data-wio-id={d.id}
                ></div>
                <div className={`col-lg-3 ${styles.activityImage}`}>
                  <img
                    src={
                      d.wide_images
                        ? d.wide_images["Search-Schedule-Thumb"].url
                        : ""
                    }
                    alt=""
                  />
                </div>
                <div className={`col-lg-5 ${styles.activityBody}`}>
                  {d.link && (
                    <Link
                      to={linkResolver(d.link._meta)}
                      className={styles.activityLink}
                    >
                      {RichText.asText(d.title)}
                    </Link>
                  )}
                  {!d.link && (
                    <div className={styles.activityTitle}>
                      {RichText.asText(d.title)}
                    </div>
                  )}
                  <div className={styles.activityDesc}>{d.description}</div>
                </div>
                <div className={`col-lg-4 ${styles.activityLocation}`}>
                  <div className={styles.activityLocationDetails}>
                    <div className={styles.location}>{d.location}</div>
                    <div className={styles.duration}>
                      <span className={styles.iconClock}></span> {d.duration}{" "}
                      {d.duration_time_units}
                    </div>
                  </div>
                  <Button
                    text="Map"
                    icon="map"
                    url={`/zoo-map?location=activity-${d.uid}`}
                  />
                </div>
              </div>
            </div>
          )
        })

        return (
          <div className={styles.activityTimeRow} key={i}>
            <div className="container">
              <div className="row">
                <div className="col-lg-2">
                  <div className={styles.activityTime}>
                    {moment(key).format("h:mm A")}
                  </div>
                </div>
                <div className="col-lg-10">
                  <div className={styles.activities}>{items}</div>
                </div>
              </div>
            </div>
          </div>
        )
      })
    }

    return (
      <Layout title={title} section="activities" {...this.props}>
        <Hero image={image} pageId={_meta.id} />
        <TitleBar
          title={title}
          description={introduction_text}
          calloutDescription={feature}
          link={cta}
        />
        <SEO
          title={doc.node.meta_title || renderAsText(doc.node.title)}
          description={doc.node.meta_description}
          canonical={this.props.location.origin + this.props.location.pathname}
        />
        <div className="container">
          <div className="row">
            <div className="col-md-10 offset-md-1">
              <TextBlocks body={body} />
            </div>
          </div>
        </div>
        {timeRows.length > 0 && (
          <>
            <nav className={`navbar navbar-expand-lg ${styles.filterBar}`}>
              <div className="container">
                <div className={`filterDate ${styles.filterDate}`}>
                  <div className={styles.calendarWrapper}>
                    <div
                      className={styles.calendar}
                      onClick={this.handleDatePickerOpen}
                    >
                      <div className={styles.calendarDay}>
                        {moment(filterDate).format("D")}
                      </div>
                      <div className={styles.calendarMonth}>
                        {moment(filterDate).format("MMM")}
                      </div>
                    </div>
                  </div>
                  <SingleDatePicker
                    readOnly={true}
                    date={filterDate} // momentPropTypes.momentObj or null
                    onDateChange={date => this.setState({ filterDate: date })} // PropTypes.func.isRequired
                    focused={this.state.focused} // PropTypes.bool
                    onFocusChange={({ focused }) =>
                      this.setState({ focused: focused })
                    } // PropTypes.func.isRequired
                    displayFormat="ddd, MMM D, YYYY"
                    hideKeyboardShortcutsPanel={true}
                    id="filter-date" // PropTypes.string.isRequired,
                    numberOfMonths={1}
                  />
                  <span
                    className={styles.iconChevronDownWhite}
                    onClick={this.handleDatePickerOpen}
                  ></span>
                </div>
                <div className={styles.filterBarRight}>
                  <ul className="navbar-nav">
                    <li className="nav-item dropdown">
                      <a
                        className={`nav-link dropdown-toggle ${styles.filterActivityToggle}`}
                        href="#"
                        id="navbarDropdown"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        <span className={styles.filterActivityName}>
                          {this.state.filterActivityName}
                        </span>
                        <span
                          className={`${styles.iconChevronDownWhite} ml-3`}
                        ></span>
                      </a>
                      <div
                        className="dropdown-menu dropdown-menu-right"
                        aria-labelledby="navbarDropdown"
                      >
                        <MenuItem
                          key={99999}
                          value="All Activities"
                          onChange={this.handleChange}
                        />
                        {activityNames.map((d, i) => (
                          <MenuItem
                            value={d}
                            onChange={this.handleChange}
                            key={i}
                          />
                        ))}
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
            <div className={styles.activitiesContainer}>{timeRows}</div>
          </>
        )}
        <CallOutSimple body={body} />
      </Layout>
    )
  }
}

export default ScheduleLanding

export const query = graphql`
  query ScheduleLandingQuery($cursor: String) {
    prismic {
      allActivity_schedule_landing_pages {
        edges {
          node {
            _meta {
              id
              tags
              uid
            }
            meta_title
            meta_description
            title
            image: image1
            introduction_text
            feature
            cta {
              _linkType
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__FileLink {
                _linkType
                url
              }
            }
            body {
              ... on PRISMIC_Activity_schedule_landing_pageBodyBody_text_paragraphs {
                type
                label
                fields {
                  design_style
                  paragraph
                }
              }
              ... on PRISMIC_Activity_schedule_landing_pageBodySimple_callout {
                type
                label
                primary {
                  section_title
                  section_description
                }
                fields {
                  callout_title
                  callout_image
                  cta1 {
                    ... on PRISMIC_Landing_page_hero_block {
                      title
                      _meta {
                        id
                        type
                        uid
                        tags
                      }
                    }
                    ... on PRISMIC_Detail {
                      title
                      _meta {
                        id
                        type
                        uid
                        tags
                      }
                    }
                    ... on PRISMIC_Special_events_landing_page {
                      title
                      _meta {
                        id
                        type
                        uid
                        tags
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      allActivitys(after: $cursor) {
        edges {
          node {
            wide_images
            title
            description
            activity_parent_page {
              _linkType
              ... on PRISMIC_Detail {
                _linkType
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            _meta {
              id
              tags
              type
              uid
            }
            activity_times_are_active
            activity_times {
              duration
              duration_time_units
              frequency
              location
              start_am_pm
              start_hour
              start_minute
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
      }
    }
  }
`
