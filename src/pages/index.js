import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import Hero from "../components/hero-home"
import CTA from "../components/cta/cta"
import FeatureBlock from "../components/home-feature/home-feature-block.js"
import Events from "../components/home-events"
import CallOut from "../components/call-out/call-out"

import styles from "./index.module.css"

export const query = graphql`
  query HomeQuery($cursor: String) {
    prismic {
      allHome_pages {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            hero_title
            hero_image
            hero_cta_label
            hero_cta_link {
              _linkType
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
            }
            hero_secondary_cta_label
            hero_secondary_cta_link {
              _linkType
              ... on PRISMIC_Form {
                title
                _linkType
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            hero_description
            hero_callout_title
            hero_callout_icon
            hero_callout_color
            hero_callout_description
            hero_callout_cta_link {
              _linkType
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
            }
            hero_callout_cta_any_link {
              _linkType
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC_Form {
                title
                _linkType
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            home_features {
              feature_text
              internal_link {
                ... on PRISMIC_Birthday_parties_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall
                  title
                  wide_image: wide_header_size
                }
                ... on PRISMIC_Conservation_project {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Purrfect_picnics_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image: wide_header_size
                }
                ... on PRISMIC_Homeschool_detail {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Membership_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Weddings_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Camp_experience {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Special_experience {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Animals_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Tickets_and_pricing_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Picnic_request_form_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Corporate_meetings_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Activity_schedule_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Animal_cam_detail {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Landing_page_hero_block {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image: wide_header_size
                }
                ... on PRISMIC_Preschool_detail {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tallImage
                  title
                  wide_image
                }
                ... on PRISMIC_Campexperience {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Overnights_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Hours_location_parking_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                  wide_image
                }
                ... on PRISMIC_Detail {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Membership_detail {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Special_encounter {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Special_exhibit {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_How_you_can_help_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Special_event {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                  wide_image: spot_images
                  tall_image
                }
                ... on PRISMIC_Special_events_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                }
                ... on PRISMIC_Camp {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  camp_title
                  wide_image
                }
                ... on PRISMIC_Day_camps_landing_page {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  title
                  tall
                  wide_image: wide_header_size
                }
                ... on PRISMIC_Scout_detail {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Campovernight {
                  _linkType
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  tall_image
                  title
                  wide_image
                }
                ... on PRISMIC_Aquarium_home {
                  title
                  aquarium_summary
                  _linkType
                  _meta {
                    uid
                    type
                    tags
                    id
                  }
                  tall_image
                  wide_image
                }
              }
            }
            detailed_callout {
              internal_link {
                ... on PRISMIC_Landing_page_hero_block {
                  title
                  summary
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                  _linkType
                  tall_image
                }
                ... on PRISMIC_Detail {
                  title
                  summary1
                  _linkType
                  tall_image
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC_Aquarium_home {
                  title
                  summary: aquarium_summary
                  _linkType
                  _meta {
                    id
                    type
                    tags
                    uid
                  }
                  tall_image
                  wide_image
                }
              }
            }
          }
        }
      }
      allSpecial_events(after: $cursor) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            title
            spot_images
            activity_time {
              start_time
              number_of_days
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
      }
    }
  }
`

class HomePage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      events: props.data.prismic.allSpecial_events.edges,
    }
  }
  loadEvents(cursor) {
    this.props.prismic
      .load({
        variables: { cursor: cursor },
        query, // (optional)
        fragments: [], // (optional)
      })
      .then(res => {
        console.log("LOADED", res.data)
        this.setState({
          events: this.state.events.concat(res.data.allSpecial_events.edges),
        })
        if (res.data.allSpecial_events.pageInfo.hasNextPage) {
          this.loadEvents(res.data.allSpecial_events.pageInfo.endCursor)
        }
      })
      .catch(function(err) {
        // This will fix your error since you are now handling the error thrown by your first catch block
        console.log(err.message)
      })
  }
  componentDidMount() {
    let {
      hasNextPage,
      endCursor,
    } = this.props.data.prismic.allSpecial_events.pageInfo
    if (hasNextPage) {
      this.loadEvents(endCursor)
    }
  }
  render() {
    const { data, location } = this.props
    const doc = data.prismic.allHome_pages.edges.slice(0, 1).pop()
    console.log("doc", this.state.events, doc)
    if (!doc) return null
    const {
      _meta,
      meta_title,
      meta_description,
      hero_title,
      hero_image,
      hero_description,
      hero_cta_label,
      hero_cta_link,
      hero_secondary_cta_label,
      hero_secondary_cta_link,
      hero_callout_title,
      hero_callout_icon,
      hero_callout_color,
      hero_callout_description,
      hero_callout_cta_link,
      hero_callout_cta_any_link,
      home_features,
      detailed_callout,
    } = doc.node
    const { events } = this.state
    const featureColors = [
      "Pumpkin",
      "Canary",
      "Terra Cotta",
      "Tangerine",
      "Zoo Green",
    ]
    let featureItems = home_features.slice(0, 6).map((d, i) => {
      console.log("Featured item linkage", d.internal_link)
      return {
        title: d.internal_link.title || d.internal_link.camp_title,
        description: d.feature_text,
        color: featureColors[i],
        wide_image: d.internal_link.wide_image,
        tall_image: d.internal_link.tall_image || d.internal_link.wide_image,
        link: d.internal_link,
      }
    })

    console.log(
      " ********************************************************* detailed callouts",
      featureItems
    )
    const calloutColors = ["Orchid", "Pumpkin"]

    return (
      <Layout section="" location={location} {...this.props}>
        <SEO
          title={meta_title}
          description={meta_description}
          canonical={location.origin + location.pathname}
        />
        <Hero
          pageId={_meta.id}
          title={hero_title}
          subtitle={hero_description}
          image={hero_image}
          btnText={hero_cta_label}
          link={hero_cta_link}
          btn2Text={hero_secondary_cta_label}
          link2={hero_secondary_cta_link}
        />
        <div className={styles.bodyTop}>
          <div className="container">
            <div className="row">
              <div className="offset-xl-1 col-xl-10">
                <div className={styles.ctaWrapper}>
                  <CTA
                    title={hero_callout_title}
                    description={hero_callout_description}
                    icon={hero_callout_icon}
                    color={hero_callout_color || "Orchid"}
                    link={hero_callout_cta_any_link}
                  />
                </div>
              </div>
            </div>
          </div>
          <section>
            <div className="container">
              <FeatureBlock items={featureItems} />
            </div>
          </section>
        </div>
        <Events events={events} />
        <section
          className={`${styles.section} ${styles.bodyBottom}`} /* style={ bodyBottomStyle }*/
        >
          <div className="container">
            <div className="row">
              <div className="col-lg-10 offset-lg-1">
                <div className="row">
                  {detailed_callout.slice(0, 3).map((d, i) => {
                    return (
                      <div className="col-md-6" key={i}>
                        <CallOut
                          image={
                            d.internal_link.tall_image &&
                            d.internal_link.tall_image.url
                          }
                          title={d.internal_link.title}
                          description={
                            d.internal_link.summary || d.internal_link.summary1
                          }
                          link={d.internal_link}
                          color={calloutColors[i]}
                        />
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    )
  }
}
export default HomePage
