import React from "react";
import { graphql } from 'gatsby';
//import { Link } from "gatsby";
import { filterArrayByType } from '../utility.js';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import SectionBlock from '../components/section-block';
import CardLanding from '../components/card-landing';

const ExhibitLanding = (props) => {
	let { data, location } = props;
	console.log('>>>', data);

	const doc = data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
	console.log('doc -->', doc);
	if (!doc) return null;
	
  const { wide_header_size, title, feature, summary, meta_title, meta_description, body, _meta } = doc.node;
	const featuredItems = filterArrayByType(body, "feature_block").pop().fields;
	
	return (
		<Layout title={ title } section="animals" { ...props }>
			<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
      <Hero image={ wide_header_size } pageId={ _meta.id } />
      <TitleBar 
      	title={ title } 
      	description={ summary }
      	calloutDescription={ feature } 
      />
      {
				featuredItems.map((d, i) => {
					return (
						<SectionBlock key={ i }>
							<div className="container">
								<div className="row">
									<div className="col-md-12">
										<CardLanding
											color={ d.color_name }
											image={ d.callout_image }
											season=''
											year=''
											minAge=''
											maxAge=''
											title={ d.callout_title }
											description={ d.callout_description }
											urlText={ d.cta_text }
											link={ d.cta }
										/>
									</div>
								</div>
							</div>
						</SectionBlock>
					);
				})
			}
		</Layout>
	);
}

export default ExhibitLanding;

export const query = graphql`
{
  prismic {
    allLanding_page_hero_blocks(uid: "featured-exhibits") {
      edges {
        node {
          _meta {
            id
            tags
            type
            uid
          }
          meta_title
          meta_description
          wide_header_size
          title
          summary
          feature
          button_label
          cta {
            ... on PRISMIC__ExternalLink {
              _linkType
              url
            }
          }
          body {
            ... on PRISMIC_Landing_page_hero_blockBodyFeature_block {
              type
              label
              fields {
                callout_description
                callout_image
                callout_title
                color_name
                cta_text
                cta:cta1 {
                  ... on PRISMIC_Special_exhibit {
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                }
              }
            }
					}
        }
      }
    }
	}
}
`

