import React, { useEffect, useRef, useState } from "react"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import Jungle from "./404/jungle/404-jungle"
import Toad from "./404/toad/404-toad"
// import Polar from "./404/polar/404-polar"
// import Otter from "./404/otter/404-otter"

const NotFoundPage = props => {
  const pagesArr = [<Toad />, <Jungle />]
  const [page, setPage] = useState(null)

  function switchPage(num) {
    switch (num) {
      case 0:
        return <Toad />
      case 1:
        return <Jungle />
      // case 2:
      //   return <Polar />
      // case 3:
      //   return <Otter />
      default:
        return <Toad />
    }
  }

  useEffect(() => {
    setPage(switchPage(Math.floor(Math.random() * pagesArr.length)))
  }, [])

  return (
    <Layout title="404: Not found" section="" {...props}>
      <SEO title="404: Not found" />
      {page && page}
    </Layout>
  )
}

export default NotFoundPage
