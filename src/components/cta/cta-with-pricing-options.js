import React from 'react';
import Button from '../button/button-link.js';
import { ColorToHex, renderAsText, renderAsHtml, filterArrayByType } from '../../utility.js';
import Icon from '../icon/icon';

import styles from './cta.module.css';

/*
	... on PRISMIC_Tickets_and_pricing_landing_pageBodyCta1 {
		type
		label
		fields {
			cta_label
			cta_text
			cta_title
			icon
			color
			cta_option_1
			cta_option_1_link {
				_linkType
				... on PRISMIC__ExternalLink {
					_linkType
					url
				}
			}
			cta_option_2
			cta_option_2_link {
				_linkType
				... on PRISMIC__ExternalLink {
					_linkType
					url
				}
			}
		}
	}
*/

export default ({ body }) => {
	let cta = filterArrayByType(body, 'cta');
	console.log('cta', cta);
	
	if (cta.length === 0) {
		return null;
	}
	
	const { color, cta_label, cta_text, cta_title, icon, cta_option_1, cta_option_1_link, cta_option_2, cta_option_2_link } = cta[0].fields[0];
	
	let style = {
		backgroundColor: ColorToHex(color)
	};
	
	let dropdownStyle = {
		color: ColorToHex(color)
	};
	
  return (
		<div className={ styles.cta } style={ style }>
			{ 
				icon && 
				<div className={ styles.ctaImage }>
					<Icon icon={ icon } width="72" color="white" />
				</div>
			}
			<div className={ styles.ctaDetails }>
				<h3 className={ styles.ctaTitle }>{ renderAsText(cta_title) }</h3>
				<div className={ styles.ctaDescription }>{ renderAsHtml(cta_text) }</div>
				<div className="dropdown">
					<button className={`btn dropdown-toggle ${ styles.dropdownToggle }`} style={ dropdownStyle } type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						{ cta_label }
					</button>
					<div className={`${ styles.dropdownMenu } dropdown-menu`} aria-labelledby="dropdownMenuButton">
						<a className="dropdown-item" href={ cta_option_1_link ? cta_option_1_link.url : '' }  style={ dropdownStyle }>{ cta_option_1 }</a>
						<a className="dropdown-item" href={ cta_option_2_link ? cta_option_2_link.url : '' }  style={ dropdownStyle }>{ cta_option_2 }</a>
					</div>
				</div>
			</div>
  	</div>
	);
}
