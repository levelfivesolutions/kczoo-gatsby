import React from "react"
import Button from "../button/button-link.js"
import { ColorToHex, renderAsText, renderAsHtml } from "../../utility.js"
import Icon from "../icon/icon"

import styles from "./cta.module.css"

export default ({ color, icon, title, description, link, data, linkText }) => {
  if (data) {
    title = data.fields[0]?.cta_title
    description = data.fields[0]?.cta_text
    icon = data.fields[0]?.icon
    color = data.fields[0]?.color || data.fields[0]?.cta_banner_color
    link = data.fields[0]?.cta_link
    linkText = data.fields[0]?.cta_label
  }

  console.log("COLOR", color)

  let style = {
    backgroundColor: ColorToHex(color),
  }

  return (
    <div className={styles.cta} style={style}>
      {icon && (
        <div className={styles.ctaImage}>
          <Icon icon={icon} width="72" color="white" />
        </div>
      )}
      <div className={styles.ctaDetails}>
        <h3
          className={styles.ctaTitle}
          dangerouslySetInnerHTML={{ __html: renderAsText(title) }}
        ></h3>
        <div
          className={styles.ctaDescription}
          dangerouslySetInnerHTML={{ __html: renderAsHtml(description) }}
        ></div>
        {link && (
          <Button
            text={linkText || "Learn More"}
            link={link}
            color={color}
            isInverse={true}
          />
        )}
      </div>
    </div>
  )
}
