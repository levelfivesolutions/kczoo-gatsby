import React from "react"
import Button from "../button/button-link.js"
import { ColorToHex, renderAsText, renderAsHtml } from "../../utility.js"
import Icon from "../icon/icon"

import styles from "./cta.module.css"

export default ({ ctaColor, ctaTextColor, ctaImage, ctaImageAlt, ctaTitle, ctaDescription, ctaButtonLabel, ctaButtonSize, ctaLink }) => {
  let style = {
    backgroundColor: ColorToHex(ctaColor)
  }
  let textStyle = {
    color: ColorToHex(ctaTextColor)
  }

  return (
    <div className={styles.ctaDetailed}>
      <div className="row no-gutters">
        <div className="col-3">
          <img src={ctaImage} alt={ctaImageAlt} style={{width:"100%"}} />
        </div>
        <div className="col-9">
          <div className={styles.ctaBodyWrapper} style={style}>
            <h3 style={textStyle}>{ctaTitle}</h3>
            <p style={textStyle}>{ctaDescription}</p>
            {ctaLink && (
              <Button
                text={ctaButtonLabel}
                link={ctaLink}
                color={ctaColor}
                isInverse={true}
                size={"flex"}
              />
            )}
          </div>
        </div>
      </div>
    </div>


    // <div className={styles.cta} style={style}>
    //   {icon && (
    //     <div className={styles.ctaImage}>
    //       <Icon icon={icon} width="72" color="white" />
    //     </div>
    //   )}
    //   <div className={styles.ctaDetails}>
    //     <h3 className={styles.ctaTitle}>{renderAsText(title)}</h3>
    //     <div className={styles.ctaDescription}>{renderAsHtml(description)}</div>
    //     {link && (
    //       <Button
    //         text="Learn More"
    //         link={link}
    //         color={color}
    //         isInverse={true}
    //       />
    //     )}
    //   </div>
    // </div>
  )
}
