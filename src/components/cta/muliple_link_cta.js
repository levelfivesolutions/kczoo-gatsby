import React from "react"
import Button from "../button/button-link.js"
import { ColorToHex, renderAsText, renderAsHtml } from "../../utility.js"
import Icon from "../icon/icon"
import { linkResolver } from "../../utility.js"

import styles from "./cta.module.css"
import SmartLink from "../smart-link/index.js"

export default ({ data }) => {
  let style = {
    backgroundColor: "#F7F7F7",
  }

  console.log("Multiple Link CTA Data", data)

  if (!data) return null

  let description = data.primary[0]?.multiple_link_cta_description
  let color = data.primary[0]?.multiple_link_cta_color
  let links = data?.fields

  return (
    <div className={styles.cta} style={style}>
      <div className={styles.ctaDetails}>
        {description && (
          <div className={styles.ctaDescription}>
            {renderAsText(description)}
          </div>
        )}
        {links &&
          links.map(link => {
            return (
              <SmartLink link={linkResolver(link.multiple_link_cta_link)}>
                {link.multiple_link_cta_text}
              </SmartLink>
            )
          })}
      </div>
    </div>
  )
}
