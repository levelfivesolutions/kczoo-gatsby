import React from 'react';
import Button from '../button/button-link.js';
import { ColorToHex, renderAsText, renderAsHtml } from '../../utility.js';
import Icon from '../icon/icon'

import styles from './banner.module.css'

export default ({ color, icon, title, description, link }) => {
	let style = {
		backgroundColor: ColorToHex(color)
	};
	
  return (
		<div className={ styles.cta } style={ style }>
			{ 
				icon && 
				<div className={ styles.ctaImage }>
					<Icon icon={ icon } width="72" color="white" />
				</div>
			}
			<div className={ styles.ctaDetails }>
				<h3 className={ styles.ctaTitle }>{ renderAsText(title) }</h3>
				<div className={ styles.ctaDescription }>{ renderAsHtml(description) }</div>
				{ 
					link && 
					<Button text="Learn More" link={ link } color={ color } isInverse={ true } />
				}
			</div>
  	</div>
	);
}
