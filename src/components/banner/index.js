import React from 'react';
import Banner from './banner.js';
import SectionBlock from '../section-block';
import { filterArrayByType } from '../../utility.js';

export default ({ title, description, body }) => {
	let data = filterArrayByType(body, "banner");
	console.log("BANNER", data);
	
	if (data.length === 0) {
		return null;
	}
	
	let { title1, text, icon, banner_color } = data[0].fields[0];
	
  return (
		<SectionBlock>
			<div className="container">
				<div className="row">
					<div className={`col-md-10 offset-md-1`}>
						<Banner  
							color={ banner_color }
							icon={ icon }
							title={ title1 } 
							description={ text } 
						/>
					</div>
				</div>
			</div>
		</SectionBlock>
	);
}
