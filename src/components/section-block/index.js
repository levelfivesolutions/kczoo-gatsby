import React from "react"
import { ColorToHex, getTextColor, renderAsText } from "../../utility.js"
import styles from "./index.module.css"
import { renderImageUrl } from '../../utility'

export default ({ shading = 'odd', classList, title, description, background, children, id, color, paddingBottom = true, paddingTop = true }) => {

  let passedStyles = (color && !background)
    ? { backgroundColor: ColorToHex(color), color: getTextColor(color) }
    : (background)
      ? { backgroundImage: `url(${renderImageUrl(background)})`, backgroundSize: "cover", backgroundPosition: "center" }
      : {}

  if (!paddingBottom) {
    passedStyles = Object.assign(passedStyles, { paddingBottom: 0 })
  }
  if (!paddingTop) {
    passedStyles = Object.assign(passedStyles, { paddingTop: 0 })
  }

  let classes = `section ${styles.section} `;
  if (shading === 'odd') {
    classes += styles.sectionOdd;
  }
  if (shading === 'even') {
    classes += styles.sectionEven;
  }

  return (
    <section
      className={`${classes} ${classList}`}
      style={passedStyles}
      data-wio-id={id}
    >
      {(title || description) && (
        <div className={`${styles.sectionHeader} container`}>
          <div className="row">
            <div className="col-md-10 offset-md-1">
              {title && (
                <h2 className={styles.sectionTitle}>{renderAsText(title)}</h2>
              )}
              {description && (
                <div className={styles.sectionDesc}>{description}</div>
              )}
            </div>
          </div>
        </div>
      )}
      {children}
    </section>
  )
}
