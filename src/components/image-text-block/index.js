import React from "react"
import Tile from "../tile/tile.js"
import SectionBlock from "../section-block"
import { linkResolver, renderAsHtml, renderAsText } from "../../utility.js"
import styles from "./image-text-block.module.css"
import ButtonLink from "../button/button-link.js"

export default ({ data, innerBlock }) => {
  if (!data) {
    return null
  }

  let primary
  if (innerBlock) {
    primary = data
  } else {
    primary = data.primary
  }

  console.log("TRYING IMAGE TEXT BLOCK", data)
  let {
    image_text_title = "",
    image_text_color = "",
    image_text_link = "",
    image_text_link_text = "",
    image_text_text = "",
    image_text_background_image = "",
    image_text_image = null,
  } = primary

  return (
    <SectionBlock
      background={image_text_background_image}
      color={image_text_color || "White"}
    >
      <div className={`${styles.container} container`}>
        <div className="row">
          <div className="col-md-4 d-flex align-items-center justify-content-center">
            <img
              src={image_text_image && image_text_image.url}
              alt={image_text_image && image_text_image.alt}
            />
          </div>
          <div className="col-md-8 d-flex flex-column justify-content-center">
            <div>
              <h2 className={styles.title}>{renderAsText(image_text_title)}</h2>
              {renderAsHtml(image_text_text)}
              {image_text_link && (
                <div style={{ float: "left" }}>
                  <ButtonLink
                    color={image_text_color}
                    link={image_text_link}
                    isInverse
                    text={image_text_link_text}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </SectionBlock>
  )
}
