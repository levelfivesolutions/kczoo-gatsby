import React, { useState } from "react"
import { ColorToHex, linkResolver } from "../../utility"
import styles from "./aquarium-navigation.module.css"
import { StaticQuery, graphql } from "gatsby"
import { Link } from "gatsby"
import { Modal } from "react-bootstrap"

const AquariumNavigation = props => {
  const { location } = props

  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  return (
    <StaticQuery
      query={AquariumNavQuery}
      render={data => {
        const doc = data.prismic.allAquarium_homes.edges[0].node.navigation
        console.log("AQUARIUM NAV ITEM: ", doc)

        return (
          <>
            <nav
              className={`navbar navbar-expand-lg ${styles.filterBar}`}
              style={{ backgroundColor: ColorToHex("Zoo Blue") }}
            >
              <div className="container d-flex justify-content-center">
                <ul className={`navbar-nav ${styles.navbarNav}`}>
                  {doc.map(page => {
                    return (
                      <li
                        key={page.aquarium_page_name}
                        className={`${styles.navItem} nav-item`}
                      >
                        <Link
                          className={
                            location.pathname.replace(/\//g, "") ===
                            linkResolver(page.aquarium_page._meta).replace(
                              /\//g,
                              ""
                            )
                              ? `nav-link ${styles.navLink} ${styles.navLinkActive}`
                              : `nav-link ${styles.navLink}`
                          }
                          to={linkResolver(page.aquarium_page._meta)}
                        >
                          {page.aquarium_page_name}
                        </Link>
                      </li>
                    )
                  })}
                </ul>
                <div
                  onClick={handleShow}
                  className={`d-flex d-lg-none ${styles.expandBtn}`}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="47"
                    height="47"
                    viewBox="0 -3 47 47"
                  >
                    <defs>
                      <filter
                        id="Ellipse_138"
                        x="0"
                        y="0"
                        width="47"
                        height="47"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="3" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="3" result="blur" />
                        <feFlood floodOpacity="0.161" />
                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      transform="matrix(1, 0, 0, 1, 0, 0)"
                      filter="url(#Ellipse_138)"
                    >
                      <circle
                        id="Ellipse_138-2"
                        data-name="Ellipse 138"
                        cx="14.5"
                        cy="14.5"
                        r="14.5"
                        transform="translate(9 6)"
                        fill="#fff"
                      />
                    </g>
                    <path
                      id="Path_11582"
                      data-name="Path 11582"
                      d="M12639.027,2986l7.564,7.564,7.563-7.564"
                      transform="translate(-12623.527 -2967.495)"
                      fill="none"
                      stroke="#0082b5"
                      strokeLinecap="round"
                      strokeWidth="3"
                    />
                  </svg>
                </div>
              </div>
            </nav>
            <Modal
              show={show}
              onHide={handleClose}
              dialogClassName={styles.modalDialogMobile}
            >
              <Modal.Header
                closeButton
                className={styles.modalHeaderMobile}
              />
              <Modal.Body className={styles.modalBodyMobile}>
                <div className={`mt-4 ${styles.goText}`}>GO TO SECTION</div>
                <div
                  className={
                    "d-flex flex-row my-4 px-5 align-items-center justify-content-around"
                  }
                >
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                </div>
                <nav className="navbar navbar-expand-lg p-0">
                  <div className="container d-flex justify-content-center mb-4">
                    <ul className={`navbar-nav flex-grow-1 ${styles.navbarNavMobile}`}>
                      {doc.map(page => {
                        return (
                          <li
                            key={page.aquarium_page_name}
                            className={`${styles.navItem} nav-item`}
                          >
                            <Link
                              className={
                                location.pathname.replace(/\//g, "") ===
                                linkResolver(page.aquarium_page._meta).replace(
                                  /\//g,
                                  ""
                                )
                                  ? `nav-link ${styles.navLinkMobile} ${styles.navLinkActiveMobile}`
                                  : `nav-link ${styles.navLinkMobile}`
                              }
                              to={linkResolver(page.aquarium_page._meta)}
                            >
                              {page.aquarium_page_name}
                            </Link>
                          </li>
                        )
                      })}
                    </ul>
                  </div>
                </nav>
              </Modal.Body>
            </Modal>
          </>
        )
      }}
    />
  )
}

export default AquariumNavigation

const AquariumNavQuery = graphql`
 {
  prismic {
    allAquarium_homes {
      edges {
        node {
          _meta {
            id
          }
          navigation {
            aquarium_page_name
            aquarium_page {
              ... on PRISMIC_Detail {
                _meta {
                  uid
                  id
                  type
                  tags
                }
                _linkType
              }
              ... on PRISMIC_Aquarium_home {
                _linkType
                _meta {
                  uid
                  id
                  type
                  tags
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC_Aquarium_donations {
                _meta {
                  uid
                  type
                  tags
                }
                _linkType
              }
              ... on PRISMIC_Aquarium_site_plan {
                _linkType
                _meta {
                  uid
                  type
                  tags
                  id
                }
              }
              ... on PRISMIC_Aquarium_sponsors {
                _linkType
                _meta {
                  uid
                  id
                  type
                  tags
                }
              }
            }
          }
          wide_image
        }
      }
    }
  }
}
`
