import React, { useState, useEffect } from "react"
import { renderAsHtml } from "../../utility"
import Icon from "../icon/icon"
import Button from "../button/button-link"
import { Modal } from "react-bootstrap"
import styles from "./wish-list-card.module.css"

const WishListCard = ({ cards }) => {
  const [openModals, setOpenModals] = useState({})
  const [popupOpen, setPopupOpen] = useState(false)

  useEffect(() => {
    setPopupOpen(true)
  }, [])

  /**
   * @param id - the id of the modal to show
   */
  function showDynamicModal(id) {
    let newModal = { ...openModals }
    newModal[id] = true
    setOpenModals({ ...newModal })
  }

  /**
   * @param id - the id of the modal to hide
   */
  function hideDynamicModal(id) {
    let newModal = { ...openModals }
    newModal[id] = false
    setOpenModals({ ...newModal })
  }

  return cards?.map((card, i) => {
    return (
      <div className={`${styles.cardBody}`} key={i}>
        <div className="d-flex">
          <div>
            <span className={styles.cardTitle}>
              {card.wish_list_card_title}
            </span>
            <span className={styles.cardDescription}>
              {renderAsHtml(card.wish_list_card_descriptio)}
            </span>
            <Button
              text="View List"
              size="small"
              color="Tangerine"
              onClick={() => {
                showDynamicModal(`wishlist${i}`)
              }}
              onKeyPress={() => {
                showDynamicModal(`wishlist${i}`)
              }}
            />
          </div>
          <Icon
            icon={`map-${card.wish_list_card_icon}`}
            width={140}
            height={140}
            color="ORCHID"
            className="ml-4"
          />
          <Modal
            show={openModals[`wishlist${i}`]}
            onHide={() => {
              hideDynamicModal(`wishlist${i}`)
            }}
            dialogClassName={styles.modalDialog}
            size="lg"
            centered
            scrollable
          >
            <Modal.Header closeButton className={styles.modalHeader} />
            <Modal.Body className={styles.modalBody}>
              <div className="row">
                <div className="col-md-3 m-0 p-0">
                  <span className={styles.cardTitle}>
                    {card.wish_list_card_title}
                  </span>
                  <div className={styles.cardDescription}>
                    {renderAsHtml(card.wish_list_card_descriptio)}
                  </div>
                </div>
                <div className={`${styles.listContent} col-md-9 m-0 p-0`}>
                  {renderAsHtml(card.wish_list_card_modal_body_text)}
                </div>
              </div>
            </Modal.Body>
          </Modal>
        </div>
      </div>
    )
  })
}

export default WishListCard
