import React, { useEffect } from "react"
import "./aquarium-map.css"

const MAX_TRANSLATE = 6

function updatePositions(height, width, elements) {
  const maxVHTranslation = -height * MAX_TRANSLATE
  const maxVWTranslation = -width * MAX_TRANSLATE
  const length = elements.length
  for (let i = 0; i < elements.length; i++) {
    const translateVH = (i / length) * maxVHTranslation
    const translateVW = (i / length) * maxVWTranslation

    elements[
      i
    ].style.transform = `translate3d(${translateVW}vh, ${translateVH}vh, 0)`
  }
}

const AquariumMap = ({data, thumbUrl, setConstructionModalOpen}) => {
  return (
    <>
    

    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      xlink="http://www.w3.org/1999/xlink"
      x="0px"
      y="0px"
      viewBox="0 0 4873.3 3987.2"
      style={{ enableBackground: "new 0 0 4873.3 3987.2;" }}
      space="preserve"
    >
      <g id="Layer_1_2_">
        <g id="Layer_2_1_">
          <image
            style={{ overflow: "visible", enableNackground: "new;" }}
            width="2400"
            height="1922"
            href="/images/aquarium-map/10@2x.jpg"
            transform="matrix(1.8075 0 0 1.8075 264.73 250.3323)"
          ></image>
        </g>
        <svg onClick={() => setConstructionModalOpen(true)} className="constructionCam d-none d-md-block" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" width="35%" height="1670" viewBox="0 0 388 282">
        <defs>
          <pattern id="pattern" width="1" height="1" viewBox="0 27.914 399.878 186.086">
            <image preserveAspectRatio="xMidYMid slice" width="100%" height="100%" href={thumbUrl || ""}/>
          </pattern>
          <filter id="Rectangle_1062" x="0" y="0" width="388" height="282" filterUnits="userSpaceOnUse">
            <feOffset dy="3" input="SourceAlpha"/>
            <feGaussianBlur stdDeviation="3" result="blur"/>
            <feFlood flood-opacity="0.161"/>
            <feComposite operator="in" in2="blur"/>
            <feComposite in="SourceGraphic"/>
          </filter>
          <clipPath id="clip-path">
            <rect id="Rectangle_1058" data-name="Rectangle 1058" width="128" height="55" transform="translate(-0.4 -0.115)" fill="#fff" stroke="#707070" stroke-width="1"/>
          </clipPath>
          <clipPath id="clip-path-2">
            <rect id="Rectangle_1057" data-name="Rectangle 1057" width="115.556" height="58.957" fill="#fff" stroke="#707070" stroke-width="1"/>
          </clipPath>
        </defs>
        <g id="Group_1822" data-name="Group 1822" transform="translate(-62 -1078.439)">
          <g transform="matrix(1, 0, 0, 1, 62, 1078.44)" filter="url(#Rectangle_1062)">
            <g id="Rectangle_1062-2" data-name="Rectangle 1062" transform="translate(9 6)" stroke="#fff" stroke-width="10" fill="url(#pattern)">
              <rect width="370" height="264" rx="5" stroke="none"/>
              <rect x="5" y="5" width="360" height="254" fill="none"/>
            </g>
          </g>
          <g id="Group_1821" data-name="Group 1821" transform="translate(82.401 1095.554)">
            <g id="Group_1819" data-name="Group 1819" transform="translate(0 0)">
              <rect id="Rectangle_1056" data-name="Rectangle 1056" width="349" height="55" transform="translate(-1.4 -0.115)" fill="#fdc843"/>
            </g>
            <text id="SEE_OUR_PROGRESS_" data-name="SEE OUR PROGRESS!" transform="translate(140.337 37.054)" fill="#222" font-size="27" font-family="DINCondensedBold, DINOT" font-weight="700"><tspan x="0" y="0">SEE OUR PROGRESS!</tspan></text>
            <g id="Mask_Group_47" data-name="Mask Group 47" transform="translate(-1 0)" clip-path="url(#clip-path)">
              <g id="Group_1820" data-name="Group 1820" transform="translate(-0.289 -0.448)" clip-path="url(#clip-path-2)">
                <line id="Line_242" data-name="Line 242" x1="74.52" y2="76.041" transform="translate(-15.879 -7.406)" fill="none" stroke="#222" stroke-width="15"/>
                <line id="Line_243" data-name="Line 243" x1="74.52" y2="76.041" transform="translate(-55.1 -7.406)" fill="none" stroke="#222" stroke-width="15"/>
                <path id="Path_10104" data-name="Path 10104" d="M64.505-7-10.762,66.651" transform="translate(34.104 -0.406)" fill="none" stroke="#222" stroke-width="15"/>
                <path id="Path_10105" data-name="Path 10105" d="M64.505-7-10.762,66.651" transform="translate(73.325 -0.406)" fill="none" stroke="#222" stroke-width="15"/>
                <path id="Path_10106" data-name="Path 10106" d="M64.505-7-10.762,66.651" transform="translate(112.546 -0.406)" fill="none" stroke="#222" stroke-width="15"/>
              </g>
            </g>
          </g>
          <path id="Path_10107" data-name="Path 10107" d="M0,0H347.778V51.111H0Z" transform="translate(82.111 1287.328)" fill="#222" opacity="0.5"/>
          <g id="Group_1826" data-name="Group 1826" transform="translate(193.681 1301.217)">
            <text id="View_Larger" data-name="View Larger" transform="translate(0 18)" fill="#fff" font-size="17" font-family="DINRoundOT-Bold, DIN Round OT" font-weight="700"><tspan x="0" y="0">View Larger</tspan></text>
            <path id="expand" d="M8.073,9.807a.341.341,0,0,1,0,.5l-3.6,3.6,1.56,1.56a.694.694,0,0,1-.488,1.181H.694a.667.667,0,0,1-.488-.206A.666.666,0,0,1,0,15.951V11.1a.666.666,0,0,1,.206-.488A.666.666,0,0,1,.694,10.4a.666.666,0,0,1,.488.206l1.56,1.56,3.6-3.6a.341.341,0,0,1,.5,0ZM16.645.694V5.548a.69.69,0,0,1-1.181.488L13.9,4.475l-3.6,3.6a.341.341,0,0,1-.5,0L8.572,6.838a.341.341,0,0,1,0-.5l3.6-3.6-1.56-1.56A.666.666,0,0,1,10.4.694a.666.666,0,0,1,.206-.488A.666.666,0,0,1,11.1,0h4.855a.666.666,0,0,1,.488.206A.667.667,0,0,1,16.645.694Zm0,14.564" transform="translate(107.993 6.519)" fill="#fff"/>
          </g>
        </g>
      </svg>
        <g id="Layer_1_1_">
          <g id="Exhibit_Zone_1">
            <g id="Zone_1_Button" transform="translate(1.5 178.394)">
              <circle
                id="Ellipse_110_5_"
                className="st0"
                cx="2912.1"
                cy="1225"
                r="54.5"
              />
              <text
                transform="matrix(1 0 0 1 2892.1787 1249.3903)"
                className="st1 st2 st3"
              >
                1
              </text>
            </g>
            <g
              id="Zone_1_Mobile_Button"
              className="mobileButton"
              transform="translate(1.5 178.394)"
            >
              <circle
                id="Ellipse_110_11_"
                className="st0"
                cx="2929.8"
                cy="1198"
                r="107.3"
              />
              <text
                transform="matrix(1 0 0 1 2890.5938 1245.9454)"
                className="st1 st2 st4"
              >
                1
              </text>
            </g>
            <g id="Zone_1_Info" transform="translate(-240.998 -1195.393)">
              <polygon
                id="Transitional_Hitbox_4_"
                className="st5"
                points="2582.4,2366.8 2862.5,2640.2 3780.6,2393 3780.6,2366.8 				"
              />
              <path
                id="Union_1_4_"
                className="aquast6"
                d="M3111.1,2366.8h-528.7v-965h1198.3v965h-568.5l-50.5,81.1L3111.1,2366.8z"
              />
              <rect
                x="3010.4"
                y="1554.2"
                className="st5"
                width="648.8"
                height="182.5"
              />
              <text
                transform="matrix(1 0 0 1 3010.4451 1604.1576)"
                className="st7 st2 st8"
              >
                ZONE1:{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 3010.4451 1673.8575)"
                className="st7 st2 st8"
              >
                WARM COASTLINE
              </text>
              <rect
                x="2701.8"
                y="1789"
                className="st5"
                width="972.5"
                height="373.1"
              />
              <text
                transform="matrix(1 0 0 1 2701.8054 1832.6539)"
                className="st9 st10 st11"
              >
                Tides cycle and waves crash, shaping{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 2701.8054 1893.6539)"
                className="st9 st10 st11"
              >
                warm coastal habitats. Lakes, rivers, and{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 2701.8054 1954.6539)"
                className="st9 st10 st11"
              >
                streams bring freshwater to warm, salty{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 2701.8054 2015.6539)"
                className="st9 st10 st11"
              >
                coastlines. Marine life adapts at rocky{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 2701.8054 2076.6538)"
                className="st9 st10 st11"
              >
                shores, mangroves, and sandy beaches.
              </text>
              <g id="Zone_1_Thumb">
                <defs>
                  <path
                    id="Area_Image_4_"
                    d="M2720,1500.7h196c10,0,18.2,8.1,18.2,18.2v199.7c0,10-8.1,18.2-18.2,18.2h-196
							c-10,0-18.2-8.1-18.2-18.2v-199.7C2701.8,1508.8,2709.9,1500.7,2720,1500.7z"
                  />
                </defs>
                <clipPath id="Area_Image_6_">
                  <use href="#Area_Image_4_" style={{ overflow: "visible" }} />
                </clipPath>
                <g id="oljDgz_1_" className="st12">
                  <image
                    style={{ overflow: "visible" }}
                    width="130"
                    height="130"
                    id="oljDgz"
                    href="/images/aquarium-map/Zone1.jpg"
                    transform="matrix(1.8312 0 0 1.8312 2700.7717 1498.6581)"
                  ></image>
                </g>
              </g>
              <g id="More_Info_8_" transform="translate(-107 82.394)">
                <path
                  id="Path_9776_4_"
                  className="st0"
                  d="M3143.3,2079.7h272.3c34.1,0,61.7,27.6,61.7,61.7c0,34.1-27.6,61.7-61.7,61.7h-272.3
						c-34.1,0-61.7-27.6-61.7-61.7C3081.5,2107.3,3109.2,2079.7,3143.3,2079.7z"
                />
                <text
                  transform="matrix(1 0 0 1 3154.1575 2163.2336)"
                  className="st1 st2 st8"
                >
                  More Info
                </text>
              </g>
            </g>
            <polygon
              id="Zone_1_Target"
              points="3010.8,1014.2 3512.2,1069.8 3603.8,1392.8 3264.5,1446.2 3072.8,1467.2 3081.4,1542.3 
				2924.3,1602 2736.4,1494.7 2805.3,1489.6 2726.8,1461.6 2583.9,1415.2 2815.9,1204.3 			"
            />
          </g>
          <g id="Exhibit_Zone_6">
            <g id="Zone_6_Info" transform="translate(-240.998 -1195.393)">
              <polygon
                id="Transitional_Hitbox_3_"
                className="st5"
                points="2564.8,3271.1 2526.3,3547.2 3809.5,3271.1 				"
              />
              <path
                id="Union_1_3_"
                className="aquast6"
                d="M3114,3271.1h-549.2V2263.4h1244.7v1007.7H3219l-52.5,84.7L3114,3271.1z"
              />
              <rect
                x="3009.4"
                y="2416.5"
                className="st5"
                width="674"
                height="183.3"
              />
              <text
                transform="matrix(1.0343 0 0 1 3009.4375 2466.6663)"
                className="st7 st2 st13"
              >
                ZONE 6:{" "}
              </text>
              <text
                transform="matrix(1.0343 0 0 1 3009.4375 2536.6663)"
                className="st7 st2 st13"
              >
                COLD COASTLINE
              </text>
              <rect
                x="2688.8"
                y="2652.3"
                className="st5"
                width="1010.2"
                height="436"
              />
              <text
                transform="matrix(1.0343 0 0 1 2688.8281 2696.1594)"
                className="st9 st10 st14"
              >
                The rocky coastal habitat can at times be{" "}
              </text>
              <text
                transform="matrix(1.0343 0 0 1 2688.8281 2757.4592)"
                className="st9 st10 st14"
              >
                violent, battering, and fiercely cold.{" "}
              </text>
              <text
                transform="matrix(1.0343 0 0 1 2688.8281 2818.6594)"
                className="st9 st10 st14"
              >
                Animals adapt to dynamic extremes at the{" "}
              </text>
              <text
                transform="matrix(1.0343 0 0 1 2688.8281 2879.9592)"
                className="st9 st10 st14"
              >
                rocky shore. Tide pools are a quiet calm{" "}
              </text>
              <text
                transform="matrix(1.0343 0 0 1 2688.8281 2941.259)"
                className="st9 st10 st14"
              >
                where animals grip tightly to the rocks{" "}
              </text>
              <text
                transform="matrix(1.0343 0 0 1 2688.8281 3002.4592)"
                className="st9 st10 st14"
              >
                when the seas change and bear down.
              </text>
              <g id="Zone_6_Thumb">
                <defs>
                  <path
                    id="Area_Image_3_"
                    d="M2707.4,2362.7h204.3c10.2,0,18.5,8.3,18.5,18.5v199.9c0,10.2-8.3,18.5-18.5,18.5h-204.3
							c-10.2,0-18.5-8.3-18.5-18.5v-199.9C2688.8,2371,2697.1,2362.7,2707.4,2362.7z"
                  />
                </defs>
                <clipPath id="Area_Image_7_">
                  <use href="#Area_Image_3_" style={{ overflow: "visible" }} />
                </clipPath>
                <g id="J0YaiV_1_" className="st15">
                  <image
                    style={{ overflow: "visible" }}
                    width="130"
                    height="130"
                    id="J0YaiV"
                    href="/images/aquarium-map/Zone6.jpg"
                    transform="matrix(1.8672 0 0 1.8672 2688.8 2362.7148)"
                  ></image>
                </g>
              </g>
              <g id="More_Info_6_" transform="translate(-107 82.394)">
                <path
                  id="Path_9776_3_"
                  className="st0"
                  d="M3143.3,2986.4h282.9c35.4,0,64.1,27.8,64.1,62s-28.7,62-64.1,62h-282.9
						c-35.4,0-64.1-27.8-64.1-62C3079.1,3014.2,3107.8,2986.4,3143.3,2986.4z"
                />
                <text
                  transform="matrix(1.0343 0 0 1 3154.5723 3070.2939)"
                  className="st1 st2 st13"
                >
                  More Info
                </text>
              </g>
            </g>
            <g id="Zone_6_Button" transform="translate(1.5 178.394)">
              <ellipse
                id="Ellipse_110_4_"
                className="st0"
                cx="2928.7"
                cy="2118.7"
                rx="56.6"
                ry="54.7"
              />
              <text
                transform="matrix(1.0343 0 0 1 2908.0029 2143.1704)"
                className="st1 st2 st16"
              >
                6
              </text>
            </g>
            <g
              id="Zone_6_Mobile_Button"
              className="mobileButton"
              transform="translate(1.5 178.394)"
            >
              <ellipse
                id="Ellipse_110_10_"
                className="st0"
                cx="2896.9"
                cy="2091"
                rx="107.6"
                ry="104"
              />
              <text
                transform="matrix(1.0343 0 0 1 2857.554 2137.5354)"
                className="st1 st2 st17"
              >
                6
              </text>
            </g>
            <path
              id="Zone_6_Target"
              d="M2295.3,2572.7l39.9,22.1l226.7,22.1l429-237l302.2-158.5l198.1-186.1l-156.8-53.7l-75.5-2.7
				l-183.4-85.5l-22.2-6.9l-159.2,157.1l21.3,8.3l-56.9,51L2174,2286.1l202.5,122.6l11.4,13.8l-11.4,5.5l-116.9,99.8"
            />
          </g>
          <g id="Exhibit_Zone_2">
            <g id="Zone_2_Info" transform="translate(-240.998 -1195.393)">
              <path
                id="Union_1_1_"
                className="aquast6"
                d="M2328.8,2384.7h-528.7v-951.5h1198.3v951.5h-568.5l-50.5,80L2328.8,2384.7z"
              />
              <polygon
                id="Transitional_Hitbox_1_"
                className="st5"
                points="1800.1,2384.7 1680.6,2829.1 3056.2,2430.9 2998.3,2384.7 				"
              />
              <rect
                x="2228.1"
                y="1585.6"
                className="st5"
                width="648.8"
                height="182.5"
              />
              <text
                transform="matrix(1 0 0 1 2228.1289 1635.5907)"
                className="st7 st2 st8"
              >
                ZONE 2:{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 2228.1289 1705.2906)"
                className="st7 st2 st8"
              >
                WARM SHALLOWS
              </text>
              <rect
                x="1919.5"
                y="1820.4"
                className="st5"
                width="972.5"
                height="373.1"
              />
              <text
                transform="matrix(1 0 0 1 1919.4894 1864.087)"
                className="st9 st10 st11"
              >
                Tides create ever-changing habitats{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1919.4894 1925.087)"
                className="st9 st10 st11"
              >
                between land and sea. Warm shallow{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1919.4894 1986.087)"
                className="st9 st10 st11"
              >
                lagoons, sandy beaches, marshes, and{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1919.4894 2047.087)"
                className="st9 st10 st11"
              >
                mud flats provide nutrient-rich waters{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1919.4894 2108.0869)"
                className="st9 st10 st11"
              >
                where food is plentiful.
              </text>
              <g id="Zone_2_Thumb">
                <defs>
                  <path
                    id="Area_Image_1_"
                    d="M1937.6,1532.1h196.1c10,0,18.2,8.1,18.2,18.2V1750c0,10-8.1,18.2-18.2,18.2h-196.1
							c-10,0-18.2-8.1-18.2-18.2v-199.7C1919.5,1540.2,1927.6,1532.1,1937.6,1532.1z"
                  />
                </defs>
                <clipPath id="Area_Image_8_">
                  <use href="#Area_Image_1_" style={{ overflow: "visible" }} />
                </clipPath>
                <g id="_x36_wYInf_1_" className="st18">
                  <image
                    style={{ overflow: "visible" }}
                    width="130"
                    height="130"
                    id="_x36_wYInf"
                    href="/images/aquarium-map/Zone2.jpg"
                    transform="matrix(1.8162 0 0 1.8162 1919.4 1532.1149)"
                  ></image>
                </g>
              </g>
              <g id="More_Info_2_" transform="translate(-107 82.394)">
                <path
                  id="Path_9776_1_"
                  className="st0"
                  d="M2352,2091h272.3c34.1,0,61.7,27.6,61.7,61.7c0,34.1-27.6,61.7-61.7,61.7H2352
						c-34.1,0-61.7-27.6-61.7-61.7C2290.3,2118.7,2317.9,2091,2352,2091z"
                />
                <text
                  transform="matrix(1 0 0 1 2362.894 2174.5486)"
                  className="st1 st2 st8"
                >
                  More Info
                </text>
              </g>
            </g>
            <g id="Zone_2_Button" transform="translate(1.5 178.394)">
              <circle
                id="Ellipse_110_2_"
                className="st0"
                cx="2138.7"
                cy="1228.9"
                r="54.5"
              />
              <text
                id="_x32_"
                transform="matrix(1 0 0 1 2118.6982 1253.2905)"
                className="st1 st2 st3"
              >
                2
              </text>
            </g>
            <g
              id="Zone_2_Mobile_Button"
              className="mobileButton"
              transform="translate(1.5 178.394)"
            >
              <circle
                id="Ellipse_110_9_"
                className="st0"
                cx="2138.7"
                cy="1214.4"
                r="107"
              />
              <text
                id="_x32__1_"
                transform="matrix(1 0 0 1 2099.4141 1262.2727)"
                className="st1 st2 st19"
              >
                2
              </text>
            </g>
            <polygon
              id="Zone_2_Target"
              points="2267.6,1889.5 1767.2,1713.9 1748.7,1732.4 1437.8,1627.4 2327.3,1038.5 2815.3,1213.5 
				2716.5,1295.9 2420,1584.2 2500.3,1697.4 			"
            />
          </g>
          <g id="Exhibit_Zone_3">
            <g id="Zone_3_Info" transform="translate(-240.998 -1195.393)">
              <polygon
                id="Transitional_Hitbox_2_"
                className="st5"
                points="1141.5,3082.4 1272,3411.9 2259.8,3173.8 2339.7,3082.4 				"
              />
              <path
                id="Union_1_2_"
                className="aquast6"
                d="M1670.2,3082.4h-528.7v-939.9h1198.3v939.9h-568.5l-50.5,79L1670.2,3082.4z"
              />
              <rect
                x="1569.6"
                y="2294.9"
                className="st5"
                width="648.8"
                height="182.5"
              />
              <text
                transform="matrix(1 0 0 1 1569.5618 2344.8489)"
                className="st7 st2 st8"
              >
                ZONE 3:{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1569.5618 2414.5488)"
                className="st7 st2 st8"
              >
                WARM REEF
              </text>
              <rect
                x="1260.9"
                y="2529.7"
                className="st5"
                width="972.5"
                height="373.1"
              />
              <text
                transform="matrix(1 0 0 1 1260.9221 2573.3452)"
                className="st9 st10 st11"
              >
                Abundant plants and animals create a{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1260.9221 2634.3452)"
                className="st9 st10 st11"
              >
                vibrant underwater community. With{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1260.9221 2695.3452)"
                className="st9 st10 st11"
              >
                plentiful sun and clear water, reefs are{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1260.9221 2756.3452)"
                className="st9 st10 st11"
              >
                home to vast populations of creatures{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1260.9221 2817.3452)"
                className="st9 st10 st11"
              >
                including fish, corals, and turtles.
              </text>
              <g id="Zone_3_Thumb">
                <defs>
                  <path
                    id="Area_Image_2_"
                    d="M1279.1,2241.3h196.1c10,0,18.2,8.1,18.2,18.2v199.7c0,10-8.1,18.2-18.2,18.2h-196.1
							c-10,0-18.2-8.1-18.2-18.2v-199.7C1260.9,2249.5,1269.1,2241.3,1279.1,2241.3z"
                  />
                </defs>
                <clipPath id="Area_Image_9_">
                  <use href="#Area_Image_2_" style={{ overflow: "visible" }} />
                </clipPath>
                <g id="_x35_Je4vt_1_" className="st20">
                  <image
                    style={{ overflow: "visible" }}
                    width="130"
                    height="130"
                    id="_x35_Je4vt"
                    href="/images/aquarium-map/Zone3.jpg"
                    transform="matrix(1.8162 0 0 1.8162 1260.9 2241.3149)"
                  ></image>
                </g>
              </g>
              <g id="More_Info_4_" transform="translate(-107 82.394)">
                <path
                  id="Path_9776_2_"
                  className="st0"
                  d="M1702.4,2800.3h272.3c34.1,0,61.7,27.6,61.7,61.7s-27.6,61.7-61.7,61.7h-272.3
						c-34.1,0-61.7-27.6-61.7-61.7S1668.3,2800.3,1702.4,2800.3z"
                />
                <text
                  transform="matrix(1 0 0 1 1713.2742 2883.8069)"
                  className="st1 st2 st8"
                >
                  More Info
                </text>
              </g>
            </g>
            <g id="Zone_3_Button" transform="translate(1.5 178.394)">
              <circle
                id="Ellipse_110_3_"
                className="st0"
                cx="1474.1"
                cy="1933.7"
                r="54.5"
              />
              <text
                transform="matrix(1 0 0 1 1454.1604 1958.1125)"
                className="st1 st2 st3"
              >
                3
              </text>
            </g>
            <g
              id="Zone_3_Mobile_Button"
              className="mobileButton"
              transform="translate(1.5 178.394)"
            >
              <circle
                id="Ellipse_110_8_"
                className="st0"
                cx="1528.6"
                cy="1900.5"
                r="107.6"
              />
              <text
                transform="matrix(1 0 0 1 1489.245 1948.6108)"
                className="st1 st2 st21"
              >
                3
              </text>
            </g>
            <polygon
              id="Zone_3_Target"
              points="1011.5,2181.8 1569.9,1861.8 1906.8,1861.8 2029.9,1937.9 2071.3,2062.1 2071.3,2156.1 
				1897.8,2268 1879.9,2268 1860.9,2255.7 1681.8,2254.6 1541.4,2337.4 1541.4,2471.7 1163.7,2471.7 1155.8,2482.9 1011.5,2399 			
				"
            />
          </g>
          <g id="Exhibit_Zone_5">
            <polygon
              id="Zone_5_Target"
              points="1696.4,2661.6 1566.6,2486.1 2008.6,2231.8 2118.3,2295.6 2173.1,2283.3 2249.2,2333.7 
				2373.5,2406.4 2258.2,2516.1 2088.1,2493.7 2094.8,2480.3 1755.7,2560.9 1848.6,2686.2 1745.6,2725.4 			"
            />
            <g id="Zone_5_Info" transform="translate(-240.998 -1195.393)">
              <path
                id="Union_1_5_"
                className="aquast6"
                d="M2228.9,3386.1h-528.7V2379.6h1198.3V3386H2330l-50.5,84.6L2228.9,3386.1z"
              />
              <polygon
                id="Transitional_Hitbox_5_"
                className="st5"
                points="1700.2,3384.3 2022.1,3582.7 2488.1,3554.7 2898.4,3384.3 				"
              />
              <rect
                x="2134.8"
                y="2532"
                className="st5"
                width="648.8"
                height="182.5"
              />
              <text
                transform="matrix(1 0 0 1 2134.7827 2582.0027)"
                className="st7 st2 st8"
              >
                ZONE 5:{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 2134.7827 2651.7026)"
                className="st7 st2 st8"
              >
                COLD SHALLOWS
              </text>
              <rect
                x="1826.1"
                y="2766.8"
                className="st5"
                width="972.5"
                height="495.1"
              />
              <text
                transform="matrix(1 0 0 1 1826.1431 2810.499)"
                className="st9 st10 st11"
              >
                Shallow, sunny, and cold, food here is{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1826.1431 2871.499)"
                className="st9 st10 st11"
              >
                plentiful due to the nutrient-rich water.{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1826.1431 2932.499)"
                className="st9 st10 st11"
              >
                Craggy shelves, rocky bottoms, shoreline{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1826.1431 2993.499)"
                className="st9 st10 st11"
              >
                crevices, and forests of kelp provide{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1826.1431 3054.499)"
                className="st9 st10 st11"
              >
                habitat where marine life has adapted for{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1826.1431 3115.499)"
                className="st9 st10 st11"
              >
                hiding and hunting.
              </text>
              <g id="Zone_5_Thumb" className="st22">
                <defs>
                  <path
                    id="Area_Image_5_"
                    d="M1844.3,2478.5h196.1c10,0,18.2,8.1,18.2,18.2v199.7c0,10-8.1,18.2-18.2,18.2h-196.1
							c-10,0-18.2-8.1-18.2-18.2v-199.7C1826.1,2486.6,1834.3,2478.5,1844.3,2478.5z"
                  />
                </defs>
                <clipPath id="Area_Image_10_" className="st23">
                  <use href="#Area_Image_5_" style={{ overflow: "visible" }} />
                </clipPath>
                <g id="A1utbM_1_" className="st24">
                  <image
                    style={{ overflow: "visible" }}
                    width="130"
                    height="130"
                    id="A1utbM"
                    href="/images/aquarium-map/Zone5.jpg"
                    transform="matrix(1.8222 0 0 1.8222 1825.7074 2477.7297)"
                  ></image>
                </g>
              </g>
              <g id="More_Info_10_" transform="translate(-107 82.394)">
                <path
                  id="Path_9776_5_"
                  className="st0"
                  d="M2239.5,3105.5h272.3c34.1,0,61.7,27.6,61.7,61.7s-27.6,61.7-61.7,61.7h-272.3
						c-34.1,0-61.7-27.6-61.7-61.7S2205.4,3105.5,2239.5,3105.5z"
                />
                <text
                  transform="matrix(1 0 0 1 2250.3899 3189.0515)"
                  className="st1 st2 st8"
                >
                  More Info
                </text>
              </g>
            </g>
            <g
              id="Zone_5_Mobile_Button"
              className="mobileButton"
              transform="translate(1.5 178.394)"
            >
              <circle
                id="Ellipse_110_7_"
                className="st0"
                cx="2072.6"
                cy="2160"
                r="107.7"
              />
              <text
                transform="matrix(1 0 0 1 2033.1233 2208.1465)"
                className="st1 st2 st25"
              >
                5
              </text>
            </g>
            <g id="Zone_5_Button" transform="translate(1.5 178.394)">
              <circle
                id="Ellipse_110_1_"
                className="st0"
                cx="2037.7"
                cy="2214.5"
                r="54.5"
              />
              <text
                transform="matrix(1 0 0 1 2017.7253 2238.8611)"
                className="st1 st2 st3"
              >
                5
              </text>
            </g>
          </g>
          <g id="Exhibit_Zone_4">
            <g id="Zone_4_Info" transform="translate(-240.998 -1195.393)">
              <path
                id="Union_1"
                className="aquast6"
                d="M1614.4,3678h-528.7V2610H2284v1068h-568.5l-50.5,89.7L1614.4,3678z"
              />
              <polygon
                id="Transitional_Hitbox"
                className="st5"
                points="1085.7,3677.6 1754.5,3984 2284,3677.6 				"
              />
              <rect
                x="1513.8"
                y="2762.4"
                className="st5"
                width="648.8"
                height="182.5"
              />
              <text
                transform="matrix(1 0 0 1 1513.8375 2812.355)"
                className="st7 st2 st8"
              >
                ZONE 4:{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1513.8375 2882.0549)"
                className="st7 st2 st8"
              >
                OPEN OCEAN
              </text>
              <rect
                x="1205.2"
                y="2997.2"
                className="st5"
                width="972.5"
                height="434.1"
              />
              <text
                transform="matrix(1 0 0 1 1205.1981 3040.8513)"
                className="st9 st10 st11"
              >
                Away from the coastlines, colder water is{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1205.1981 3101.8513)"
                className="st9 st10 st11"
              >
                continually fed by currents. The unusual{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1205.1981 3162.8513)"
                className="st9 st10 st11"
              >
                creatures found here adapted to life with{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1205.1981 3223.8513)"
                className="st9 st10 st11"
              >
                little light and warmth, and extreme pres
              </text>
              <text
                transform="matrix(1 0 0 1 2106.498 3223.8513)"
                className="st9 st10 st11"
              >
                -
              </text>
              <text
                transform="matrix(1 0 0 1 1205.1981 3284.8513)"
                className="st9 st10 st11"
              >
                sures. The open water is the domain of jel
              </text>
              <text
                transform="matrix(1 0 0 1 2148.998 3284.8513)"
                className="st9 st10 st11"
              >
                -
              </text>
              <text
                transform="matrix(1 0 0 1 1205.1981 3345.8513)"
                className="st9 st10 st11"
              >
                lyfish, while crabs and isopods crawl on{" "}
              </text>
              <text
                transform="matrix(1 0 0 1 1205.1981 3406.8513)"
                className="st9 st10 st11"
              >
                the ocean floor.
              </text>
              <g>
                <defs>
                  <path
                    id="Area_Image"
                    d="M1223.3,2708.9h196.1c10,0,18.2,8.1,18.2,18.2v199.7c0,10-8.1,18.2-18.2,18.2h-196.1
							c-10,0-18.2-8.1-18.2-18.2v-199.7C1205.2,2717,1213.3,2708.9,1223.3,2708.9z"
                  />
                </defs>
                <clipPath id="Area_Image_11_">
                  <use href="#Area_Image" style={{ overflow: "visible" }} />
                </clipPath>
                <g id="ceWv39_1_" className="st26">
                  <image
                    style={{ overflow: "visible" }}
                    width="130"
                    height="130"
                    id="ceWv39"
                    href="/images/aquarium-map/Zone4.jpg"
                    transform="matrix(1.8106 0 0 1.8106 1205.1444 2708.8882)"
                  ></image>
                </g>
              </g>
              <g id="More_Info_1_" transform="translate(-107 82.394)">
                <path
                  id="Path_9776"
                  className="st0"
                  d="M1646.6,3391.3h272.3c34.1,0,61.7,27.6,61.7,61.7c0,34.1-27.6,61.7-61.7,61.7h-272.3
						c-34.1,0-61.7-27.6-61.7-61.7C1584.9,3418.9,1612.5,3391.3,1646.6,3391.3z"
                />
                <text
                  transform="matrix(1 0 0 1 1657.5499 3474.769)"
                  className="st1 st2 st8"
                >
                  More Info
                </text>
              </g>
            </g>
            <g id="Zone_4_Button" transform="translate(1.5 178.394)">
              <circle
                id="Ellipse_110"
                className="st0"
                cx="1431.5"
                cy="2518.9"
                r="54.5"
              />
              <text
                transform="matrix(1 0 0 1 1411.5388 2543.2288)"
                className="st1 st2 st3"
              >
                4
              </text>
            </g>
            <g
              id="Zone_4_Mobile_Button"
              className="mobileButton"
              transform="translate(1.5 178.394)"
            >
              <circle
                id="Ellipse_110_6_"
                className="st0"
                cx="1493.3"
                cy="2484.3"
                r="107.5"
              />
              <text
                transform="matrix(1 0 0 1 1453.8197 2532.2446)"
                className="st1 st2 st27"
              >
                4
              </text>
            </g>
            <path
              id="Zone_4_Target"
              d="M1158.8,2489.1c6.6-0.8,9.8-0.4,23.5,0.5c38.4,2.3,40.1,0.5,69.4,2.2c43,2.5,64.5,3.8,77.2,11.2
				c9.7,5.7,28.5,19.6,40.3,54.8c1.6-0.2,29.4-3.9,44.5,15.5c5.8,7.4,7.5,15.4,8.1,20.3c18.8-2.7,46.8-3.6,77.2,6.7
				c21.2,7.2,36.3,17.4,43.6,22.4c16.6,11.4,27.4,22.5,39.2,34.7c12.9,13.4,23.3,26.1,31.3,36.9c20.1,18.3,40.3,36.6,60.4,54.8
				c-43.6,16.8-87.3,33.6-130.9,50.4c-5.9,2.6-16,6.3-29.1,6.7c-12.3,0.4-22.1-2.3-28-4.5c-53-13.8-105.9-27.6-158.9-41.4
				c-25.4-15.3-50.7-30.6-76.1-45.9c-7.6-7.6-15.3-15.6-23-23.8c-27.2-29.4-50.3-58.8-69.9-86.9c-22.6,6-41.6-1.6-45.9-13.4
				c-0.7-1.8-2.6-8.2,2.2-17.9c-19.4-9.3-38.8-18.7-58.2-28c20.9-11.9,37.2-22.1,48.1-29.1
				C1127.4,2500.3,1139.1,2491.6,1158.8,2489.1z"
            />
          </g>
        </g>
      </g>
      <g id="Layer_2_2_">
        <g id="Group_1728" transform="translate(-796 -1561)">
          <g id="Group_1715" transform="translate(-151 89)">
            <path
              id="Rectangle_1008"
              className="st0"
              d="M3398.9,4413.7h1360c10,0,18.1,8.1,18.1,18.1V4925c0,10-8.1,18.1-18.1,18.1h-1360
				c-10,0-18.1-8.1-18.1-18.1v-493.2C3380.7,4421.8,3388.8,4413.7,3398.9,4413.7z"
            />
            <g id="Group_1714">
              <g id="Group_1713">
                <g id="Group_1710" transform="translate(-11425 -7598)">
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14878.2656 12450.5566)"
                    className="st1 st2 st28"
                  >
                    3
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14906.5029 12450.5566)"
                    className="st1 st2 st29"
                  >
                    {" "}
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14946.8076 12450.5566)"
                    className="st1 st2 st28"
                  >
                    WARM REEF
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14878.2656 12359.8877)"
                    className="st1 st2 st28"
                  >
                    2
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14906.5029 12359.8877)"
                    className="st1 st2 st29"
                  >
                    {" "}
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14946.8076 12359.8877)"
                    className="st1 st2 st28"
                  >
                    WARM SHALLOWS
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14878.2656 12269.2168)"
                    className="st1 st2 st28"
                  >
                    1
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14906.5029 12269.2168)"
                    className="st1 st2 st29"
                  >
                    {" "}
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 14946.8076 12269.2168)"
                    className="st1 st2 st28"
                  >
                    WARM COASTLINE
                  </text>
                </g>
                <g id="Group_1712" transform="translate(-11223 -7592)">
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15408.8711 12444.5566)"
                    className="st1 st2 st28"
                  >
                    6
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15437.1094 12444.5566)"
                    className="st1 st2 st29"
                  >
                    {" "}
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15477.4141 12444.5566)"
                    className="st1 st2 st28"
                  >
                    COLD COASTLINE
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15408.8711 12353.8877)"
                    className="st1 st2 st28"
                  >
                    5
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15437.1094 12353.8877)"
                    className="st1 st2 st29"
                  >
                    {" "}
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15477.4141 12353.8877)"
                    className="st1 st2 st28"
                  >
                    COLD SHALLOWS
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15408.8711 12263.2168)"
                    className="st1 st2 st28"
                  >
                    4
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15437.1094 12263.2168)"
                    className="st1 st2 st29"
                  >
                    {" "}
                  </text>
                  <text
                    style={{ fontFamily: "DINOTBold" }}
                    transform="matrix(1 0 0 1 15477.4141 12263.2168)"
                    className="st1 st2 st28"
                  >
                    OPEN OCEAN
                  </text>
                </g>
              </g>
            </g>
          </g>
          <path
            id="Rectangle_987"
            className="aqst30"
            d="M3247.9,4502.7h1360c10,0,18.1,8.1,18.1,18.1v90.7l0,0H3229.7l0,0v-90.7
			C3229.7,4510.8,3237.8,4502.7,3247.9,4502.7z"
          />
          <text
            transform="matrix(1 0 0 1 3845.6592 4582.5059)"
            className="st1 st2 st31 st32"
            style={{ fontFamily: "Lifehack Sans" }}
          >
            ZONES
          </text>
        </g>
      </g>
    </svg>
    </>
  )
}

export default AquariumMap
