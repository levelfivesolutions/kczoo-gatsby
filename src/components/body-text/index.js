import React from 'react'
import styles from './index.module.css';

export default ({ children, noBackground=false }) => {
	let backgroundStyle = {
		backgroundImage: noBackground ? "none" : 'url(/images/backgrounds/background-leaf.svg)'
	};
	
	return (
		<div className={ styles.bodyText } style={ backgroundStyle }>
			{ children }
		</div>
	);
}
