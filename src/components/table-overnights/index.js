import React from 'react';
import { filterArrayByType } from '../../utility.js';
import moment from 'moment';
import Button from '../button/button-link.js';
import styles from './index.module.css';

export default ({ body }) => {
	console.log("body >>>", body);
	let tableData = filterArrayByType(body, 'date_table__5_column');
	console.log('date_table__5_column', tableData);
	
	if (tableData.length === 0) {
		return null;
	}
	
	let dataItems = tableData[0].fields;
		
	/*
		DATA ITEMS

		{
			"type": "date_table__5_column",
			"label": null,
			"fields": [
				{
					"column": "6+",
					"column_a": "Slumber Down Under",
					"column_b": "Register",
					"end_date_time": "2020-04-26T13:30:00+0000",
					"start_date_time": "2020-04-25T23:00:00+0000"
				},
				{
					"column": "6+",
					"column_a": "Slumber Down Under",
					"column_b": "Register",
					"end_date_time": "2020-05-30T13:30:00+0000",
					"start_date_time": "2020-05-29T23:00:00+0000"
				}
			]
		}		
		
	*/
	let tableRows = [];
	if (dataItems.length === 0) {
		tableRows.push(<tr><td colspan="4"><em>None scheduled</em></td></tr>);
	} else {
		tableRows = dataItems.map((d, i) => {
			return (
				<tr key={ i }>
					<td>
						<div className={ styles.calendarInline }>
							<div className={ `${ styles.calendar} mr-3` }>
								<div className={ styles.calendarDay }>{ moment(d.start_date_time).format('D') }</div>
								<div className={ styles.calendarMonth }>{ moment(d.start_date_time).format('MMM') }</div>
							</div>
							-
							<div className={ `${ styles.calendar } ml-3` }>
								<div className={ styles.calendarDay }>{ moment(d.end_date_time).format('D') }</div>
								<div className={ styles.calendarMonth }>{ moment(d.end_date_time).format('MMM') }</div>
							</div>
						</div>
					</td>
					<td>{ moment(d.start_date_time).format("h:mm a") } - { moment(d.end_date_time).format("h:mm a") }</td>
					<td>{ d.column }</td>
					<td>{ d.column_a }</td>
					<td className="text-right">
						{ d.form_link && <Button text={ d.column_b } url={ d.form_link } /> }
					</td>
				</tr>
			);
		})
	}

	return (
		<div className={ styles.tableWrapper }>
			<div className="container">
				<div className="table-responsive">
					<table className={`table table-striped ${ styles.table }`}>
						<thead>
							<tr className="d-none d-sm-table-row">
								<th>DATE</th>
								<th>TIME</th>
								<th>AGE</th>
								<th>DESCRIPTION</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{ tableRows }
						</tbody>
					</table>
				</div>
			</div>
		</div>
	)
}
