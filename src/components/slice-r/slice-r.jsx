import React from "react"
import CallOutBlock from "../call-out/call-out-slice"
import DonationBanner from "../donation-banner"
import ImageTextBlock from "../image-text-block"
import VideoTextBlock from "../video-text-block"
import Columns from "../columns"
import SimpleColumns from "../columns/simple"
import CTA from "../cta/cta"
import MultipleLinkCta from "../jazzoo/cta/muliple_link_cta"
import InfoCallout from "../info-callout"
import Carousel from "../carousel/carousel"
import DetailPageContent from "../detail-page-content/detail-page-content"
import { isEmptyObject } from "jquery"

const SliceR = ({ data, classList = "", ...args}) => {
  if (!data) return null
  console.log("SLICE DATA", data)
  if (typeof data.type === "undefined") return null

  // if (!data.fields) return null
  // if (data.fields.length === 0) return null

  switch (
    data.type.replace(/\d+$/, "") // regex gets rid of trailing numbers
  ) {
    case "cta":
      return <CTA data={data} {...args} />
      break
    case "multiple_link_cta":
      return <MultipleLinkCta data={data} {...args} />
      break
    case "detailed_callout":
      return <CallOutBlock item={data} {...args} />
      break
    case "carousel":
      return <Carousel data={data} {...args} />
      break
    case "image_text_block":
      return <ImageTextBlock data={data} {...args} />
      break
    case "video_text_block":
      return <VideoTextBlock data={data} {...args} />
      break
    case "column":
      return <Columns classList={classList} sliceData={data} {...args} />
      break
    case "simple_columns":
      return <SimpleColumns classList={classList} data={data} {...args} />
      break
    case "reused_image_text_block":
      return (
        <ImageTextBlock innerBlock data={data.primary.inner_block} {...args} />
      )
      break
    case "detail_page_content":
      return <DetailPageContent classList={classList} data={data} {...args} />
      break
    case "info_callout":
      return <InfoCallout data={data} {...args} />
    default:
      return null
      break
  }
}

export default SliceR
