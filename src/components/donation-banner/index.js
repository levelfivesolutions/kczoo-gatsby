import React from 'react';
import Tile from '../tile/tile.js';
import SectionBlock from '../section-block';
import { renderAsHtml, renderAsText } from '../../utility.js';
import styles from './donation-banner.module.css';
import ButtonLink from '../button/button-link.js';

export default ({ data, innerBlock }) => {

	console.log("Donation Banner", data, innerBlock);
	
	if (!data) {
		return null;
	}

	let primary;
	if (innerBlock){
		primary = data;
	} else {
		primary = data.primary;
	}


	const {
		donation_banner_title="",
		donation_banner_color = "",
		donation_banner_link="",
		donation_banner_link_text="",
		donation_banner_text="",
		donation_banner_image=null,
	} = primary;
	
  return (
		<SectionBlock color={ donation_banner_color }>
			<div className={`${styles.container} container`}>
				<div className="row">
					<div className="col-md-4">
						<img 
							src={ donation_banner_image && donation_banner_image.url } 
							alt={ donation_banner_image && donation_banner_image.alt }
						/>
					</div>
					<div className="col-md-8">
						<h2 className={styles.title}>{ renderAsText(donation_banner_title) }</h2>
						{
							renderAsHtml(donation_banner_text)
						}
						<ButtonLink color={donation_banner_color} isInverse text={donation_banner_link_text} />
					</div>
				</div>
			</div>
		</SectionBlock>
	);
}
