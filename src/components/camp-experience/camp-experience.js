import React from 'react';
import Pricing from '../pricing/pricing.js';
import Box from '../box/box.js';
import moment from 'moment';
import Icon from '../icon/icon.js';
import { RichText } from 'prismic-reactjs';
import { ColorToHex, renderAsText, renderAsHtml } from '../../utility.js';
import styles from './camp-experience.module.css';
import { priceFromPLU } from '../../galaxy';
import { renderImageUrl } from '../../utility.js';

class CampExperience extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		let { color, title, richDescription, btnText, description, dates, ageRange, minAge, timeRange, maxAge, season, startDate, year, endDate, image, price, priceQualifier, priceDesc, prices, url, btnLink, onClick } = this.props;

		var hexColor = ColorToHex(color);
		
		var list = null;
		var listItems = [];
		if (dates) {
			listItems.push(
				<React.Fragment>
					<span className={ styles.campIconsIcon }><Icon icon="calendar" height="22" width="22" /></span> { dates }
				</React.Fragment>);
		}
		if (timeRange) {
			listItems.push(
				<React.Fragment>
					<span className={ styles.campIconsIcon }><Icon icon="clock" height="22" width="22" /></span> { renderAsText(timeRange) }
				</React.Fragment>);
		}
		if (endDate) {
			listItems.push(
				<React.Fragment>
					<span className={ styles.campIconsIcon }><Icon icon="calendar" height="22" width="22" /></span> {moment(startDate).format('MMM D')} - {moment(endDate).format('MMM D')}
				</React.Fragment>);
		}
		if (season) {
			listItems.push(
				<React.Fragment>
					<span className={ styles.campIconsIcon }><Icon icon="calendar" height="22" width="22" /></span> { season } { year }
				</React.Fragment>);
		}
		if (minAge) {
			listItems.push(
				<React.Fragment>
					<span className={ styles.campIconsIcon }><Icon icon="group of friends" height="22" width="24" /></span> Age Groups: {minAge} - {maxAge}
				</React.Fragment>
			);
		}
		if (ageRange) {
			listItems.push(
				<React.Fragment>
					<span className={ styles.campIconsIcon }><Icon icon="group of friends" height="22" width="24" /></span> Age Groups: { ageRange }
				</React.Fragment>
			);
		}
		
		if (listItems.length > 0) {
			list = 
				<ul className={ styles.campIcons }>
					{ listItems.map(el => <li>{ el }</li>) }
				</ul>;
		}
		
		return (
			<div className={styles.camp}>
				<div className="row">
					<div className="col-lg-5"><div className={styles.campImageWrapper}><img src={ renderImageUrl(image) } className={styles.campImage} alt="" /></div></div>
					<div className="col-lg-7">
						<div className={styles.campBody}>
							<h2 className={styles.campTitle} style={{ backgroundColor: hexColor }}>{renderAsText(title)}</h2>
							<div className={styles.campDesc}>
								{ list }
								{ richDescription && renderAsHtml(richDescription) }
								{ description && renderAsText(description) }
							</div>
							<div>
								<Box>
									<Pricing
										price={ price }
										prices={ prices }
										quantifier={ priceQualifier }
										description={ priceDesc }
										size="medium"
										btnText={ btnText }
										btnColor={ color }
										btnUrl={ url }
										btnLink={ btnLink }
										onClick={ onClick }
									/>
								</Box>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default CampExperience;
