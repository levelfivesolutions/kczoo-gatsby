import React, { useState, useEffect } from "react"
import styles from "./aquarium-sub-area-modal.module.css"
import { Modal } from "react-bootstrap"
import Button from "../button/button-link"
import {
  renderAsHtml,
  renderAsText,
  filterArrayByType,
  renderImageUrl,
  responsiveImageUrl,
} from "../../utility"
import Icon from "../icon/icon"
import ButtonLink from "../button/button-link"

const AquariumSubAreaModal = props => {
  const { area, hideDynamicModal, openModals } = props
  const subAreas = typeof area.fields != "undefined" ? area.fields : null
  const [subAreaIndex, setSubAreaIndex] = useState(subAreas[0] ? 0 : null)
  const [subAreaImage, setSubAreaImage] = useState(subAreas[0].sub_area_image)

  let touchstartX = 0
  let touchstartY = 0
  let touchendX = 0
  let touchendY = 0

  function handleGesture() {
    // console.log({ touchendX, touchstartX }, touchendX - touchstartX )
    const BUFFER = 80
    var swiped = "swiped: "
    if (touchendX - touchstartX > BUFFER) {
      prevSubArea()
    }
    if (touchstartX - touchendX > BUFFER) {
      nextSubArea()
    }
  }

  function debounce(fn, ms) {
    let timer
    return _ => {
      clearTimeout(timer)
      timer = setTimeout(_ => {
        timer = null
        fn.apply(this, arguments)
      }, ms)
    }
  }

  useEffect(() => {
    const debouncedHandleResize = debounce(function handleResize() {
      setSubAreaImage(responsiveImageUrl(subAreas[subAreaIndex].sub_area_image))
    }, 100)

    window.addEventListener("resize", debouncedHandleResize)

    return _ => {
      window.removeEventListener("resize", debouncedHandleResize)
    }
  })

  const transitionDuration = 250

  const transitionOut = () => {
    const textCol = document.getElementById(`subAreaTextCol${primary.area_id}`)
    if (textCol) {
      textCol.style.opacity = 0
    }
  }

  const transitionIn = () => {
    const textCol = document.getElementById(`subAreaTextCol${primary.area_id}`)
    if (textCol) {
      textCol.style.opacity = 1
    }
  }

  const nextSubArea = () => {
    transitionOut()
    setTimeout(() => {
      if (subAreaIndex < subAreas.length - 1) {
        setSubAreaImage(
          responsiveImageUrl(subAreas[subAreaIndex + 1].sub_area_image)
        )
        setSubAreaIndex(subAreaIndex + 1)
      } else {
        setSubAreaImage(responsiveImageUrl(subAreas[0].sub_area_image))
        setSubAreaIndex(0)
      }
      transitionIn()
    }, transitionDuration)
  }
  const prevSubArea = () => {
    transitionOut()
    setTimeout(() => {
      if (subAreaIndex > 0) {
        setSubAreaImage(
          responsiveImageUrl(subAreas[subAreaIndex - 1].sub_area_image)
        )
        setSubAreaIndex(subAreaIndex - 1)
      } else {
        setSubAreaImage(
          responsiveImageUrl(subAreas[subAreas.length - 1].sub_area_image)
        )
        setSubAreaIndex(subAreas.length - 1)
      }
      transitionIn()
    }, transitionDuration)
  }

  const { primary } = area
  return (
    <Modal
      key={primary.area_id}
      show={openModals[primary.area_id]}
      onHide={() => hideDynamicModal(primary.area_id)}
      size="xl"
      dialogClassName={styles.modal}
    >
      <Modal.Body className={styles.modalBody}>
        <div
          className={`container-fluid no-gutters ${styles.modalContainer}`}
          id={`sitePlanModal${primary.area_id}`}
          onTouchStart={event => {
            touchstartX = event.changedTouches[0].screenX
            touchstartY = event.changedTouches[0].screenY
          }}
          onTouchEnd={event => {
            touchendX = event.changedTouches[0].screenX
            touchendY = event.changedTouches[0].screenY
            handleGesture()
          }}
        >
          <div className="row no-gutters">
            <div className="col-lg-6 position-relative">
              <div
                className={styles.subAreaImage}
                id={`subAreaImage${primary.area_id}`}
                on
                style={{
                  backgroundImage: `url(${responsiveImageUrl(subAreaImage)})`,
                }}
              >
                <div className={styles.areaLabel}>{primary.area_name}</div>
              </div>
            </div>
            <div
              className={`col-lg-6 d-flex flex-column subAreaTextColHeight justify-content-between`}
              id={`subAreaTextCol${primary.area_id}`}
            >
              <div className={`d-flex flex-column ${styles.subAreaTextCol}`}>
                <div className={styles.subAreaHeader}>
                  <h4 className={styles.modalTitle}>
                    {subAreas[subAreaIndex].sub_area_title}
                  </h4>
                  <div className={styles.subAreaHeaderWaterClose}>
                    {subAreas[subAreaIndex].sub_area_water_capacity && (
                      <div
                        style={{
                          marginRight: "40px",
                          fontSize: "14px",
                          fontFamily: "DINOTBold",
                        }}
                        className="d-flex flex-column justify-content-between align-items-center"
                      >
                        <div>
                          {subAreas[subAreaIndex].sub_area_water_capacity}
                        </div>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="77.674"
                          height="13"
                          viewBox="0 0 77.674 13"
                        >
                          <g
                            id="Group_1704"
                            data-name="Group 1704"
                            transform="translate(-1101.5 -217.5)"
                          >
                            <g
                              id="Group_1684"
                              data-name="Group 1684"
                              transform="translate(1317.466 30.466)"
                            >
                              <path
                                id="Path_9760"
                                data-name="Path 9760"
                                style={{
                                  fill: "none",
                                  stroke: "#000",
                                  strokeLinecap: "round",
                                  strokeMiterlimit: 10,
                                }}
                                className={"cls-1"}
                                d="M-215.466,195.534c6.386,0,6.386,4,12.773,4s6.387-4,12.775-4,6.39,4,12.78,4,6.388-4,12.776-4,6.392,4,12.785,4,6.392-4,12.785-4"
                              />
                            </g>
                            <g
                              id="Group_1686"
                              data-name="Group 1686"
                              transform="translate(1317.466 22.466)"
                            >
                              <path
                                id="Path_9760-2"
                                data-name="Path 9760"
                                style={{
                                  fill: "none",
                                  stroke: "#000",
                                  strokeLinecap: "round",
                                  strokeMiterlimit: 10,
                                }}
                                className="cls-1"
                                d="M-215.466,195.534c6.386,0,6.386,4,12.773,4s6.387-4,12.775-4,6.39,4,12.78,4,6.388-4,12.776-4,6.392,4,12.785,4,6.392-4,12.785-4"
                              />
                            </g>
                            <g
                              id="Group_1689"
                              data-name="Group 1689"
                              transform="translate(0 8)"
                            >
                              <g
                                id="Group_1685"
                                data-name="Group 1685"
                                transform="translate(1317.466 18.466)"
                              >
                                <path
                                  id="Path_9760-3"
                                  data-name="Path 9760"
                                  style={{
                                    fill: "none",
                                    stroke: "#000",
                                    strokeLinecap: "round",
                                    strokeMiterlimit: 10,
                                  }}
                                  className="cls-1"
                                  d="M-215.466,195.534c6.386,0,6.386,4,12.773,4s6.387-4,12.775-4,6.39,4,12.78,4,6.388-4,12.776-4,6.392,4,12.785,4,6.392-4,12.785-4"
                                />
                              </g>
                            </g>
                          </g>
                        </svg>
                      </div>
                    )}
                    <div onClick={() => hideDynamicModal(primary.area_id)}>
                      <Icon icon="close" width="30" />
                    </div>
                  </div>
                </div>
                <div className={styles.subAreaDescription}>
                  {renderAsHtml(subAreas[subAreaIndex].sub_area_description)}
                </div>
              </div>
              <div>
                {subAreas[subAreaIndex]
                  .sub_area_available_for_naming_rights && (
                  <div className="d-flex align-items-center justify-content-center">
                    <div className={styles.namingRightsContainer}>
                      <div>
                        <h5 className="mb-0 mt-1">
                          THIS AREA IS AVAILABLE FOR NAMING RIGHTS!
                        </h5>
                      </div>
                      <ButtonLink
                        link="/form/aquarium-sponsorship"
                        isInverse
                        text="Contact Us"
                      />
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className={styles.subAreaNavigation}>
          <div className={styles.subAreaPrevious} onClick={() => prevSubArea()}>
            <Icon icon="previous" width="15" color="white" />
          </div>
          <div className="d-flex">
            {subAreas.map((sub, i) => (
              <div
                className={`${styles.subAreaDot} ${
                  sub.sub_area_title === subAreas[subAreaIndex].sub_area_title
                    ? styles.subAreaDotActive
                    : ""
                }`}
                onClick={() => {
                  setSubAreaImage(
                    responsiveImageUrl(subAreas[i].sub_area_image)
                  )
                  setSubAreaIndex(i)
                }}
              ></div>
            ))}
          </div>
          <div className={styles.subAreaNext} onClick={() => nextSubArea()}>
            <Icon icon="next" width="15" color="white" />
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}

export default AquariumSubAreaModal
