import React from "react"
import Button from "../button/button-link.js"
import styles from "./titlebar-landing.module.css"
import { renderAsText, renderAsHtml, renderLinkUrl } from "../../utility"

class TitleBarWithCallout extends React.Component {
  render() {
    let {
      title,
      description,
      calloutTitle,
      calloutDescription,
      url,
      link,
      linkText,
      linkTarget,
    } = this.props
    let backgroundStyle = {
      backgroundImage:
        "linear-gradient(rgba(255,255,255,0.001), rgba(255, 255, 255, 1.0)), url(/images/bg-palm.jpg)",
    }

    return (
      <div className="container">
        <div className={styles.titleBarTop}></div>
        <div className={styles.titleBar} style={backgroundStyle}>
          <div className="row">
            <div className="col-xl col-lg">
              <h1 className={`h1 ${styles.titleBarTitle}`}>
                {title && renderAsText(title)}
              </h1>
              <div className={styles.titleBarDesc}>
                {description && renderAsText(description)}
              </div>
            </div>
            <div className="col-xl-4 col-lg-5">
              <div className={styles.callOut}>
                <div className={styles.callOutInner}>
                  {calloutTitle && (
                    <p className={styles.calloutTitle}>
                      {renderAsText(calloutTitle)}
                    </p>
                  )}
                  {renderAsText(calloutDescription) !== "" &&
                    renderAsHtml(calloutDescription)}
                  {linkText && (
                    <div className={styles.callOutButtonContainer}>
                      <Button
                        text={linkText}
                        link={link}
                        url={url}
                        color="Zoo Green"
                        size="small"
                      />
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

class TitleBarNoCallout extends React.Component {
  render() {
    let { title, description } = this.props
    let backgroundStyle = {
      backgroundImage:
        "linear-gradient(rgba(255,255,255,0.001), rgba(255, 255, 255, 1.0)), url(/images/bg-palm.jpg)",
    }

    return (
      <div className="container">
        <div className={styles.titleBarTop}></div>
        <div className={styles.titleBarNoCallout} style={backgroundStyle}>
          <h1 className={`h1 ${styles.titleBarTitleNoCallout}`}>
            {title && renderAsText(title)}
          </h1>
          {description && (
            <div
              className={`${styles.titleBarDesc} ${styles.titleBarNoCalloutDesc}`}
            >
              {renderAsText(description)}
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default ({
  title,
  description,
  calloutTitle,
  calloutDescription,
  link,
  linkText,
}) => {
  let titleBar

  if (calloutTitle || calloutDescription || link || linkText) {
    titleBar = (
      <TitleBarWithCallout
        title={title}
        description={description}
        calloutTitle={calloutTitle}
        link={link}
        linkText={linkText}
        calloutDescription={calloutDescription}
      />
    )
  } else {
    titleBar = (
      <TitleBarNoCallout
        title={title}
        description={description}
        link={link}
        linkText={linkText}
      />
    )
  }

  return titleBar
}
