import React from "react"
import { renderAsText, renderAsHtml, ColorToHex, RichText } from "../../utility"
import Button from "../button/button-link"
import styles from "./renewal-card.module.css"

const RenewalCards = ({ cards }) => {
  console.log("yeeeee", cards)
  return cards?.map((card, i) => {
    return (
      <div className={`${styles.cardContainer} col-lg-4 d-flex`} key={i}>
        <div
          className={styles.cardStyles}
          style={{ backgroundColor: ColorToHex(card.color) }}
        >
          <div className={styles.cardHeader}>
            {renderAsText(card.renewal_option_title)}
          </div>
          <div className={styles.cardBody}>
            <p className={styles.cardDescription}>
              {renderAsHtml(card.renewal_option_description)}
            </p>
          </div>
          <div className={styles.cardFooter}>
            {card.renewal_button_text && (
              <Button
                color={card.color}
                isInverse
                text={card.renewal_button_text}
                url={card.renewal_option_link.url}
                size="large"
              />
            )}
            <p>{card.renewal_option_message}</p>
          </div>
        </div>
      </div>
    )
  })
}

export default RenewalCards
