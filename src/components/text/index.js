import React from 'react';
import { renderAsHtml } from '../../utility';
import styles from './index.module.css';

export default ({ children }) => {
	return (
		<div className={ `${ styles.bodyText } body-text text-block` }>
			{ renderAsHtml(children) }
		</div>
	);
}
