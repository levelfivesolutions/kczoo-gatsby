import React from 'react';
import Tile from '../tile/tile.js';
import SectionBlock from '../section-block';
import { filterArrayByType } from '../../utility.js';

export default ({ title, description, body }) => {
	let data = filterArrayByType(body, "simple_callout");
	console.log('simple_callout', data);
	
	if (data.length === 0) {
		return null;
	}
	
	let { section_title, section_description } = data[0].primary;
	let callouts = data[0].fields;
	
  return (
		<SectionBlock title={ section_title || title } description={ section_description || description }>
			<div className="container">
				<div className="row">
					{
						callouts.map((d, i) => {
							console.log('--d', d);
							return (
								<div className="col-md-4" key={ i }>
									<Tile 
										title={ d.callout_title } 
										desc="" 
										link={ d.cta1._meta }
										image={ d.callout_image } 
										color="Orchid" 
									/>
								</div>
							);
						})
					}
				</div>
			</div>
		</SectionBlock>
	);
}
