import React from 'react';
import { renderAsHtml, renderAsText } from '../../utility.js';
import styles from './add-on.module.css';
import Icon from '../icon/icon'

export default ({ title, copy, icon, dark}) => {
	return (
		<div className={`col-12 col-md-6 col-lg-4 ${ styles.addOnContainer }`} >
			<div className={(dark) ? styles.addOnDark : styles.addOnLight }>
				<div className={`row ${styles.addOnHeadRow}`}>
					<div className="col">
						<h3 className={ styles.addOnHeading }>
							{ renderAsText(title)}
						</h3>
					</div>
					<div className={`col-auto" ${styles.addOnIconCol}`} >
						<div className={ styles.addOnIcon }>
							<Icon icon={ icon } height="40" width="40" color="grey" />
						</div>
					</div>
				</div>
				<div className={`row ${styles.addOnCopyRow}`}>
					<div>
						{ renderAsHtml(copy) }
					</div>
				</div>
			</div>
		</div>
	);
}
