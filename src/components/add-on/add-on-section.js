import React from 'react';
import { renderAsText } from '../../utility.js';
import AddOn from './add-on';
import SectionBlock from '../section-block';
import { filterArrayByType } from '../../utility'

import styles from './add-on-block.module.css';

export default ({ body, dark }) => {
	console.log("BODY from block >>>", body);
	let addOns = filterArrayByType(body, "add-ons");
	console.log('addOns', addOns);
	
	let items = addOns.map( d => d.fields );

	if (items.length === 0) {
		return null;
	}
	
	let { title, title1, subheading } = addOns.pop().primary;
	
	return (
		<SectionBlock>
			<div className="container">
				<div className={ styles.sectionHeading }>
					{ title && <h2 className={ styles.sectionTitle }>{ renderAsText(title) }</h2> }
					{ title1 && <h2 className={ styles.sectionTitle }>{ renderAsText(title1) }</h2> }
					{ subheading && <div className={ styles.sectionSubheading }>{ subheading }</div> }
				</div>
				<div className={`row ${ styles.addOnRow }`}>
					{
						items.pop().map((addOn, i) => {
							return (
								<AddOn 
									icon={ addOn.icon_selector }
									key={ i }
									dark={ dark } 
									title={ addOn.addOn_title } 
									copy={ addOn.addOn_text || addOn.text || addOn.description } 
								/>
							);
						})					
					}
				</div>
			</div>
		</SectionBlock>
	);
}
