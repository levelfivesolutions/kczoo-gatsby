import React from "react"
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

import styles from "./carousel.module.css"
import { renderAsText, renderAsHtml, ColorToHex } from "../../utility"
import SectionBlock from '../section-block';

function NextArrow(props) {
  const { className, style, onClick } = props
  return (
    <button
      type="button"
      className={styles.carouselNext}
      style={{ ...style }}
      onClick={onClick}
    >
      Next
    </button>
  )
}

function PrevArrow(props) {
  const { className, style, onClick } = props
  return (
    <button
      type="button"
      className={styles.carouselPrev}
      style={{ ...style }}
      onClick={onClick}
    >
      Previous
    </button>
  )
}

export default function SimpleSlider({ data }) {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  }
  return (
    <SectionBlock>
      <div className="container">
        <div className={styles.carouselContainer}>
          <h2 style={{ textAlign: "center", fontSize: "36px", marginBottom: "60px"}}>{renderAsText(data.primary.carousel_heading_text)}</h2>
          {console.log("Data from carousel", data)}
          <Slider {...settings}>
            {data.fields.map(d => (
              <div className="container">
                <div className="row">
                  {d.carousel_slide_image.url && (
                    <div className="col-md-6" >
                      <img
                      style={{ borderLeft: `20px solid ${ColorToHex(d.carousel_slide_accent_color)}`}}
                        src={d.carousel_slide_image.url}
                        alt={d.carousel_slide_image.alt}
                      />
                    </div>
                  )}
                  <div className="col-md-6">
                    {renderAsHtml(d.carousel_slide_text)}
                  </div>
                </div>
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </SectionBlock>
  )
}
// carousel_slide_accent_color
// carousel_slide_image
// carousel_slide_text

// primary {
// carousel_heading_text
