import React, { useState } from "react"
import Tile from "../tile/tile.js"
import $ from "jquery"
import SectionBlock from "../section-block"
import { linkResolver, renderAsHtml, renderAsText } from "../../utility.js"
import styles from "./image-text-block.module.css"
import ButtonLink from "../button/button-link.js"
import Loading from "../loading"

export default ({ data, innerBlock }) => {
  const [constructionCam, setConstructionCam] = useState(null)

  if (!data) {
    return null
  }

  let primary
  if (innerBlock) {
    primary = data
  } else {
    primary = data.primary
  }

  if (typeof window !== "undefined") {
    $.ajax({
      url: "https://kc-zoo-proxies.azurewebsites.net/",
      type: "GET",
      success: function(res) {
        console.log(res)
        var imgSrc = res
          .replace("</body>", "")
          .replace("</html>", "")
          .replace('<img id="imgMain" name="pic" src="', "")
          .replace('">', "")
        imgSrc = "https://www.p-tn.net/pwc/kczoo/" + imgSrc
        setConstructionCam(imgSrc)
      },
    })
  }

  console.log("TRYING IMAGE TEXT BLOCK", data)
  let {
    background_color = "",
    background_image = "",
    video_text_text = "",
    video_source_url = "",
  } = primary

  return (
    <>
      <div
        className="container-fluid d-flex justify-content-center"
        style={{
          backgroundImage: `url(/images/ConstructionStripes@2x.png)`,
          backgroundRepeat: "repeat-y",
          backgroundSize: "116px 160px",
          paddingLeft: 130,
          backgroundColor: "#FDC843",
          minHeight: 60,
          paddingTop: "10px",
          paddingBottom: "10px",
          marginTop: "30px",
        }}
      >
        <div className="row d-flex align-items-center">
          <div className="col-md-auto">
            <p
              style={{
                lineHeight: "30px",
                whiteSpace: "nowrap",
                fontFamily: "DINCondensedBold",
                fontSize: "30px",
                marginBottom: 0,
                paddingTop: 8,
              }}
            >
              CONSTRUCTION CAM
            </p>
          </div>
          <div className="col-md-auto">{renderAsText(video_text_text)}</div>
        </div>
      </div>
      <SectionBlock
        background={background_image}
        color={background_color || "White"}
      >
        <div className={`${styles.container} container`}>
          <div className="row">
            <div className="col-md-6 d-flex flex-column justify-content-center"></div>
            {constructionCam && (
              <div className="col-md-6 d-flex align-items-center justify-content-center">
                <div
                  style={{
                    background: `url(${constructionCam}) center`,
                    width: "100%",
                    backgroundSize: "cover",
                    margin: "auto",
                    height: 323,
                    boxShadow: "4px 4px 7px rgba(0,0,0,.4)",
                    borderTop: "10px solid #FDC843",
                  }}
                ></div>
              </div>
            )}
            {!constructionCam && (
              <div
                className="col-md-6 d-flex align-items-center justify-content-center"
                style={{
                  height: 323,
                  width: "100%",
                  padding: 80,
                  backgroundColor: "#555",
                  boxShadow: "4px 4px 7px rgba(0,0,0,.4)",
                  borderTop: "10px solid #FDC843",
                }}
              >
                <Loading width={100} />
              </div>
            )}
          </div>
        </div>
      </SectionBlock>
    </>
  )
}
