import React from "react"
import SectionBlock from "../section-block"
import Button from "../button/button-link.js"
import {
  ColorToHex,
  renderImageUrl,
  renderAsText,
  renderAsHtml,
  filterArrayByType,
  getAttribute,
} from "../../utility.js"
import styles from "./call-out-block.module.css"

export default ({ body, item }) => {
  let block

  if (body) {
    block = filterArrayByType(body, "detailed_callout")
  }

  if (item) {
    block = item
  }

  console.log("BLOCKES", block)

  let style, styleImage

  let dataItems = block.fields.slice(0, 3)
  return (
    <SectionBlock>
      <div className="container">
      <div className="row v-20-s">

        {dataItems.map((block, i) => {
          console.log("block", block)

          style = {
            backgroundColor: ColorToHex(block.color),
          }

          let link = block.link || block.cta
          let image = block.image || block.image1

          // PULL IMAGE FROM LINKED DETAIL PAGE
          if (link) {
            if (
              link.hasOwnProperty("wide_image") ||
              link.hasOwnProperty("tall_image")
            ) {
              if (
                dataItems.length >
                2 /*|| window.matchMedia('(max-width: 500px)').matches*/
              ) {
                image = link.wide_image["Home-Feature-Small"]
              } else {
                image = link.tall_image["Section-Callout-Tall"]
              }
            }
          }
          styleImage = {
            backgroundImage: `url(${renderImageUrl(image)})`,
          }

          return (
              <div
                className={`col-lg-${12 / dataItems.length} ${
                  styles.boxContainer
                }`}
                key={i}
              >
                <div
                  className={
                    dataItems.length > 2 ? styles.boxMobile : styles.box
                  }
                  style={style}
                >
                  <div
                    className={
                      dataItems.length > 2
                        ? styles.boxImageMobile
                        : styles.boxImage
                    }
                    style={styleImage}
                  ></div>
                  <div
                    className={
                      dataItems.length > 2
                        ? styles.boxContentMobile
                        : styles.boxContent
                    }
                  >
                    <div>
                      <h3 className={styles.boxTitle}>
                        {renderAsText(getAttribute(block, "title"))}
                      </h3>
                      <div className={styles.boxDescription}>
                        {renderAsHtml(
                          getAttribute(block, "text") || block.body_text
                        )}
                      </div>
                    </div>
                    <div>
                      {(block.cta_label || block.button_label) && (
                        <div>
                          <Button
                            text={block.cta_label || block.button_label}
                            link={link}
                            color={block.color}
                            isInverse={true}
                          />
                        </div>
                      )}
                      {block.second_cta_label && (
                        <div className="mt-2">
                          <Button
                            text={block.second_cta_label}
                            link={block.second_link}
                            color={block.color}
                            isInverse={true}
                          />
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
          )
        })}
        </div>
      </div>
    </SectionBlock>
  )
}
