import React from 'react';
import Button from '../button/button-link.js';
import { ColorToHex, renderImageUrl, renderAsText, renderAsHtml } from '../../utility.js';
import styles from './call-out.module.css';

export default ({ image, color, title, description, link }) => {
	const style = {
		backgroundColor: ColorToHex(color)
	};
	const styleImage = {
		backgroundImage: `url(${ renderImageUrl(image) })`
	};
	return (
		<div className={ styles.box } style={ style }>
			<div className={ styles.boxImage } style={ styleImage }></div>
			<div className={ styles.boxContent }>
				<h3 className={ styles.boxTitle }>{ renderAsText(title) }</h3>
				<div className={ styles.boxDescription }>{ renderAsHtml(description) }</div>
				<div><Button text="Learn More" link={ link } color={ color } isInverse={ true } /></div>
			</div>
		</div>
	);
}
