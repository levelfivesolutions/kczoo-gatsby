import React from "react"
import { renderAsText, ColorToHex } from "../../utility"
import styles from "./index.module.css"

export default ({ children, color, className }) => {
  let style = null
  if (color) {
    style = {
      color: ColorToHex(color),
    }
  }
  return (
    <h1 className={`${styles.title} ${className}`} style={style}>
      {renderAsText(children)}
    </h1>
  )
}
