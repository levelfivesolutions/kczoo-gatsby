import React from 'react';
import { Link } from "gatsby";
import { renderAsHtml, linkResolver } from '../../utility.js';
import styles from './index.module.css';

export default ({ data, className }) => {
	const hasLinks = data.fields.filter(d => d.title).length > 0;
	return (
		<div className={`${styles.callout} ${className}`}>
			<div className="row">
				<div className={hasLinks ? 'col-md-8' : 'col-md-12'}>
					{renderAsHtml(data.primary.info_description)}
				</div>
				{
					hasLinks &&
					<div className="col-md-4">
						{
							data.fields.map(d => <div><Link to={linkResolver(d.info_link)}>{d.info_link_title}</Link></div>)
						}
					</div>
				}
			</div>
		</div>
	);
}
