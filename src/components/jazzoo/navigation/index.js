import React, { useState } from "react"
import { ColorToHex, linkResolver } from "../../../utility"
import styles from "./index.module.css"
import { StaticQuery, graphql } from "gatsby"
import { Link, navigate } from "gatsby"
import ButtonLink from "../../button/button-link"
import { Modal } from "react-bootstrap"

const JazzooNavigation = props => {
  const { id, ticketLink, ticketLinkActive, ticketLinkText } = props

  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  const [showMenuBtn, setShowMenuBtn] = useState(false)
  const addMenuBtn = () => setShowMenuBtn(true)
  const hideMenuBtn = () => setShowMenuBtn(false)

  const isActive = []

  return (
    <StaticQuery
      query={JazzooNavQuery}
      render={data => {
        const doc = data.prismic.allJazzoo_homs.edges[0].node.navigation
        console.log("JAZZOO NAV ITEM: ", doc)

        return (
          <>
            <nav
              className={`navbar navbar-expand-lg ${styles.filterBar}`}
              style={{ backgroundColor: ColorToHex("Jazzoo 2022 Teal") }}
            >
              <div className="container-fluid d-flex justify-content-center">
                <ul className={`navbar-nav ${styles.navbarNav}`}>
                  {doc.map(page => {
                    let classes = `nav-link ${styles.navLink} `
                    if (page.page_link && id === page.page_link._meta.id) {
                      classes += styles.navLinkActive
                      isActive.push(page)
                    }
                    if (isActive.length === 0) {
                      addMenuBtn()
                    } else if (
                      doc.page_title === "Overview" ||
                      "Music, Restaurants, Drinks" ||
                      "Sponsors" ||
                      "Volunteers"
                    ) {
                      hideMenuBtn()
                    }

                    return (
                      <li
                        key={page.page_name}
                        className={`${styles.navItem} nav-item`}
                      >
                        <Link
                          className={classes}
                          to={
                            page.page_link
                              ? linkResolver(page.page_link._meta)
                              : null
                          }
                        >
                          {page.page_title}
                        </Link>
                      </li>
                    )
                  })}
                  {
                    <div className="d-none d-lg-block">
                      <li className={`d-flex ml-3 mt-3 align-items-center`}>
                        <ButtonLink
                          size="large"
                          color="Jazzoo 2022 Green"
                          url={
                            "https://store.kansascityzoo.org/webstore/shop/viewitems.aspx?cg=SE&c=SEJAZZOO"
                          }
                          isInverse
                          text={ticketLinkText || "Buy Tickets"}
                        />
                      </li>
                    </div>
                  }
                  <li
                    className={`d-lg-none nav-link ${
                      showMenuBtn === true ? "" : "d-none"
                    } ${styles.navLinkMobile}`}
                  >
                    Menu
                  </li>
                </ul>
                <div
                  onClick={handleShow}
                  className={`d-flex d-lg-none ${styles.expandBtn}`}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="47"
                    height="47"
                    viewBox="0 -3 47 47"
                  >
                    <defs>
                      <filter
                        id="Ellipse_138"
                        x="0"
                        y="0"
                        width="47"
                        height="47"
                        filterUnits="userSpaceOnUse"
                      >
                        <feOffset dy="3" input="SourceAlpha" />
                        <feGaussianBlur stdDeviation="3" result="blur" />
                        <feFlood floodOpacity="0.161" />
                        <feComposite operator="in" in2="blur" />
                        <feComposite in="SourceGraphic" />
                      </filter>
                    </defs>
                    <g
                      transform="matrix(1, 0, 0, 1, 0, 0)"
                      filter="url(#Ellipse_138)"
                    >
                      <circle
                        id="Ellipse_138-2"
                        dataName="Ellipse 138"
                        cx="14.5"
                        cy="14.5"
                        r="14.5"
                        transform="translate(9 6)"
                        fill="#fff"
                      />
                    </g>
                    <path
                      id="Path_11582"
                      dataName="Path 11582"
                      d="M12639.027,2986l7.564,7.564,7.563-7.564"
                      transform="translate(-12623.527 -2967.495)"
                      fill="none"
                      stroke="#107977"
                      strokeLinecap="round"
                      strokeWidth="3"
                    />
                  </svg>
                </div>
              </div>
            </nav>
            <Modal
              show={show}
              onHide={handleClose}
              dialogClassName={styles.modalDialogMobile}
            >
              <Modal.Header closeButton className={styles.modalHeaderMobile}>
                <img
                  className={styles.modalHero}
                  src="https://images.prismic.io/kansascityzoo/b5391247-523d-47b0-adc1-c6a236d836cd_jazzoo_2022_footer.png?auto=compress,format"
                ></img>
              </Modal.Header>
              <Modal.Body
                className={styles.modalBodyMobile}
                style={{
                  backgroundColor: ColorToHex("Jazzoo 2022 Teal"),
                }}
              >
                <div className={`mt-4 ${styles.goText}`}>GO TO SECTION</div>
                <div
                  className={
                    "d-flex flex-row my-4 px-5 align-items-center justify-content-around"
                  }
                >
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                  <div className={styles.dividerDot}></div>
                </div>
                <nav className="navbar navbar-expand-lg p-0">
                  <div className="container d-flex justify-content-center">
                    <ul
                      className={`navbar-nav d-flex flex-grow-1 ${styles.navbarNavMobile} `}
                    >
                      {doc.map(page => {
                        let classes = `nav-link ${styles.navLinkMobile} `
                        if (page.page_link && id === page.page_link._meta.id) {
                          classes += styles.navLinkActiveModal
                        }

                        return (
                          <li
                            key={page.page_name}
                            className={`${styles.navItem} nav-item`}
                          >
                            <Link
                              className={classes}
                              to={
                                page.page_link
                                  ? linkResolver(page.page_link._meta)
                                  : null
                              }
                            >
                              {page.page_title}
                            </Link>
                          </li>
                        )
                      })}
                      <li
                        className={`d-flex flex-column align-items-center mt-4 mb-4`}
                      >
                        <ButtonLink
                          size="large"
                          color="Jazzoo 2022 Green"
                          url={
                            "https://store.kansascityzoo.org/webstore/shop/viewitems.aspx?cg=SE&c=SEJAZZOO"
                          }
                          isInverse
                          text={ticketLinkText || "Buy Tickets"}
                        />
                      </li>
                    </ul>
                  </div>
                </nav>
              </Modal.Body>
            </Modal>
          </>
        )
      }}
    />
  )
}

export default JazzooNavigation

const JazzooNavQuery = graphql`
  {
    prismic {
      allJazzoo_homs {
        edges {
          node {
            title
            navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
              }
              page_title
            }
          }
        }
      }
    }
  }
`
