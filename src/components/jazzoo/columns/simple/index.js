import React from 'react'
import SectionBlock from '../../section-block'
import Button from '../../button/button-link'
import { renderAsHtml } from '../../../utility.js'
import styles from './index.module.css';

export default ({ title, description, body, data, ...args }) => {
    if (!data || data.length === 0) {
        return null;
    }

    return (
        <SectionBlock {...args}>
            <div className="container">
                <div className="row">
                    <div className="offset-md-1 col-md-10">
                        <div className="row v-20-s">
                            {
                                data.fields.map((d, i) => {
                                    return (
                                        <div className="col-md-4" key={i}>
                                            <div className={styles.wrapper}>
                                                <div className={styles.contentWrapper}>
                                                    {renderAsHtml(d.simple_columns_body_text)}
                                                </div>
                                                <div className={styles.buttonWrapper}>
                                                    <Button color={d.simple_columns_color} text={d.simple_columns_cta_label} link={d.simple_columns_cta} />
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </SectionBlock>
    );
}
