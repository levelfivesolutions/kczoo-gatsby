import React from "react"
import { StaticQuery, graphql } from "gatsby"
import styles from "./index.module.css"
import Fringe from "../fringe"
import { renderImageUrl } from "../../../utility.js"

export default function Hero({ pageId }) {
  return (
    <StaticQuery
      query={JazzooHeroQuery}
      render={data => {
        const doc = data.prismic.allJazzoo_homs.edges[0].node
        const spon = data.prismic.allJazzoo_sponsorss.edges[0].node
        console.log("****>>>>>>>", data)
        let imageUrl = renderImageUrl(doc.wide_image)
        let style = {
          backgroundImage: `url(${imageUrl})`,
        }

        return (
          <Fringe>
            <div className={styles.hero} style={style}>
              <div className={`${styles.heroText} col-lg-6 p-0`}></div>
              <div className={styles.divider}></div>
              <div className={`${styles.sponsorWrapper} col-lg-3 p-0`}>
                <span className={styles.sponsorText}>PRESENTED BY:</span>
                {spon.presenting_sponsor && (
                  <img
                    src={spon.presenting_sponsor?.url}
                    alt={spon.presenting_sponsor?.alt}
                    width="280px"
                    height="auto"
                    className={styles.sponsorImg}
                  />
                )}
              </div>
            </div>
          </Fringe>
        )
      }}
    />
  )
}

const JazzooHeroQuery = graphql`
  {
    prismic {
      allJazzoo_homs {
        edges {
          node {
            wide_image
          }
        }
      }
      allJazzoo_sponsorss {
        edges {
          node {
            presenting_sponsor
          }
        }
      }
    }
  }
`
