import React from "react"
import Button from "../../button/button-link.js"
import { ColorToHex, renderAsText, renderAsHtml } from "../../../utility.js"
import Icon from "../../icon/icon"
import { Link } from "gatsby"
import { linkResolver } from "../../../utility.js"

import styles from "./cta.module.css"
import SmartLink from "../../smart-link/index.js"

export default ({ data }) => {
  let style = {
    backgroundColor: "#FCEAC3",
    color: "#3B3B3B",
  }

  console.log("Multiple Link CTA Data", data)

  if (!data) return null

  let description = data.primary?.multiple_link_cta_description
  let color = data.primary?.multiple_link_cta_color
  let links = data?.fields

  return (
    <div className={styles.cta} style={style}>
      <div className={`${styles.ctaDetails}`}>
        {description && (
          <div className={`${styles.ctaDescription} d-flex flex-shrink-1`}>
            {renderAsHtml(description)}
            <div className="d-flex flex-grow-1 flex-column ml-1 justify-content-between align-items-center">
              <div aria-hidden="true" className={styles.dividerDot}></div>
              <div aria-hidden="true" className={styles.dividerDot}></div>
              <div aria-hidden="true" className={styles.dividerDot}></div>
            </div>
          </div>
        )}
        <div className="d-flex  flex-grow-1 flex-column justify-content-center align-items-start">
          {links &&
            links.map(link => {
              console.log(
                "TEST LINKT",
                link,
                linkResolver(link.multiple_link_cta_link)
              )
              return (
                <SmartLink
                  className={styles.multipleLinkLink}
                  link={link.multiple_link_cta_link}
                >
                  <span style={{ fontSize: "20px" }}>
                    {link.multiple_link_cta_text}
                  </span>
                </SmartLink>
              )
            })}
        </div>
      </div>
    </div>
  )
}
