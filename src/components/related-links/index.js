import React from "react"
import RelatedLinks from "./related-links.js"
import { filterArrayByType } from "../../utility"

import styles from "./related-links.module.css"

export default ({ body, isInverse }) => {
  let data = filterArrayByType(body, "related_links")

  if (data.length === 0) {
    return null
  }

  return <RelatedLinks links={data[0].fields} isInverse={isInverse} />
}
