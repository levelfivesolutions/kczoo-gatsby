import React from "react"
import { Link } from "gatsby"
import { linkResolver } from "../../utility.js"
import styles from "./related-links.module.css"

export default ({ links = [], isInverse = false }) => {
  return (
    <div
      className={isInverse ? `card ${styles.cardWhite}` : `card ${styles.card}`}
    >
      <div className={`card-header ${styles.header}`}>Related Links</div>
      <ul className={`list-group list-group-flush ${styles.list}`}>
        {links.map((d, i) => {
          console.log("LINK", d)
          if (d.exInLink === null) {
            return (
              <li key={i} className={`list-group-item ${styles.listItem}`}>
                <h6 className={styles.title}>{d.link_name}</h6>
                <div className={styles.description}>{d.description}</div>
              </li>
            )
          } else if (
            d.exInLink._linkType === "Link.web" ||
            d.exInLink._linkType === "Link.file"
          ) {
            return (
              <li key={i} className={`list-group-item ${styles.listItem}`}>
                <h6 className={styles.title}>
                  <a href={d.exInLink.url} target="_blank">
                    {d.link_name}
                  </a>
                </h6>
                <div className={styles.description}>{d.description}</div>
              </li>
            )
          } else {
            return (
              <li key={i} className={`list-group-item ${styles.listItem}`}>
                <h6 className={styles.title}>
                  <Link to={linkResolver(d.exInLink._meta)}>{d.link_name}</Link>
                </h6>
                <div className={styles.description}>{d.description}</div>
              </li>
            )
          }
        })}
      </ul>
    </div>
  )
}
