import React from 'react';
import Box from '../box/box.js';
import Pricing from '../pricing/pricing.js';
import { filterArrayByType, getPrice } from '../../utility.js';

export default ({ body, items }) => {
	let callout = filterArrayByType(body, 'banner_callout');
	console.log('banner_callout', callout);
	
	if (callout.length === 0) {
		return null;
	}
	
	/*
		... on PRISMIC_DetailBodyBanner_callout {
			type
			label
			fields {
				banner_text
				color
				cta_label
				cta_link {
					... on PRISMIC__ExternalLink {
						_linkType
						url
					}
				}
				plu_price
				plu_price_description
			}
		}
	*/
	
	const { plu_price, plu_price_description, banner_text, cta_label, cta_link, color } = callout[0].fields[0];
	
  return (
		<div className="mt-3">
			<Box color={ color }>
				<Pricing
					btnColor={ color }
					isInverse={ color ? true : false }
					price={ getPrice(items, plu_price, 0) }
					quantifier={ plu_price_description }
					description={ banner_text }
					btnText={ cta_label }
					btnLink={ cta_link }
				/>
			</Box>
		</div>
	);
}
