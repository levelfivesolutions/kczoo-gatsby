import React from 'react';
import Feature from './home-feature.js';

import styles from './home-feature-block.module.css';

function FeatureBlockTwoCards(items) {
	return (
		<div className="row">
			<div className="offset-lg-1 col-lg-5">
				<Feature 
					link={ items[0].link }
					wide_image={ items[0].wide_image } 
					tall_image={ items[0].tall_image } 
					title={ items[0].title } 
					color={ items[0].color } 
					description={ items[0].description } 
				/>
			</div>
			<div className="col-lg-5">
				<Feature 
					link={ items[1].link }
					wide_image={ items[1].wide_image } 
					tall_image={ items[1].tall_image } 
					title={ items[1].title } 
					color={ items[1].color } 
					description={ items[1].description} 
				/>
			</div>
		</div>
	);
}

function FeatureBlockThreeCards(items) {
	return (
		<div className="row">
			<div className="offset-lg-1 col-lg-4">
				<Feature 
					link={ items[0].link }
					wide_image={ items[0].wide_image } 
					tall_image={ items[0].tall_image } 
					title={ items[0].title } 
					color={ items[0].color } 
					description={ items[0].description } 
					tall={ true } 
				/>
			</div>
			<div className="col-lg-6">
				<div className="row">
					<div className="col-lg-12">
						<Feature 
							link={ items[1].link }
							wide_image={ items[1].wide_image } 
							tall_image={ items[1].tall_image } 
							title={ items[1].title } 
							color={ items[1].color } 
							description={ items[1].description} 
							wide={ true }
						/>
					</div>
				</div>
				<div className="row">
					<div className="col-lg-12">
						<Feature 
							link={ items[2].link }
							wide_image={ items[2].wide_image } 
							tall_image={ items[2].tall_image } 
							title={ items[2].title } 
							color={ items[2].color } 
							description={ items[2].description } 
							wide={ true }
						/>
					</div>
				</div>
			</div>
		</div>
	);
}

function FeatureBlockFourCards(items) {
	return (
		<div className="row">
			<div className="offset-lg-1 col-lg-4">
				<Feature 
					link={ items[0].link }
					wide_image={ items[0].wide_image } 
					tall_image={ items[0].tall_image } 
					title={ items[0].title } 
					color={ items[0].color } 
					description={ items[0].description } 
					tall={ true } 
				/>
			</div>
			<div className="col-lg-6">
				<div className="row">
					<div className="col-lg-12">
						<Feature 
							link={ items[1].link }
							wide_image={ items[1].wide_image } 
							tall_image={ items[1].tall_image } 
							title={ items[1].title } 
							color={ items[1].color } 
							description={ items[1].description } 
							wide={ true } 
						/>
					</div>
				</div>
				<div className="row">
					<div className="col-lg-6">
						<Feature 
							link={ items[2].link }
							wide_image={ items[2].wide_image } 
							tall_image={ items[2].tall_image } 
							title={ items[2].title } 
							color={ items[2].color } 
							description={ items[2].description } 
						/>
					</div>
					<div className="col-lg-6">
						<Feature 
							link={ items[3].link }
							wide_image={ items[3].wide_image } 
							tall_image={ items[3].tall_image } 
							title={ items[3].title } 
							color={ items[3].color } 
							description={ items[3].description } 
						/>
					</div>
				</div>
			</div>
		</div>
	);
}

function FeatureBlockFourCardsSmall(items) {
	return (
		<>
			<div className="row">
				<div className="col-md-6">
					<Feature 
						link={ items[0].link }
						wide_image={ items[0].wide_image } 
						tall_image={ items[0].tall_image } 
						title={ items[0].title } 
						color={ items[0].color } 
						description={ items[0].description } 
					/>
				</div>
				<div className="col-md-6">
					<Feature 
						link={ items[1].link }
						wide_image={ items[1].wide_image } 
						tall_image={ items[1].tall_image } 
						title={ items[1].title } 
						color={ items[1].color } 
						description={ items[1].description } 
					/>
				</div>
			</div>
			<div className="row">
				<div className="col-md-6">
					<Feature 
						link={ items[2].link }
						wide_image={ items[2].wide_image } 
						tall_image={ items[2].tall_image } 
						title={ items[2].title } 
						color={ items[2].color } 
						description={ items[2].description } 
					/>
				</div>
				<div className="col-md-6">
					<Feature 
						link={ items[3].link }
						wide_image={ items[3].wide_image } 
						tall_image={ items[3].tall_image } 
						title={ items[3].title } 
						color={ items[3].color } 
						description={ items[3].description } 
					/>
				</div>
			</div>
		</>
	);
}

function FeatureBlockFiveCards(items) {
	return (
		<div className="row">
			<div className="offset-lg-1 col-lg-4">
				<Feature 
					link={ items[0].link }
					wide_image={ items[0].wide_image } 
					tall_image={ items[0].tall_image } 
					title={ items[0].title } 
					color={ items[0].color } 
					description={ items[0].description } 
					tall={ true } 
				/>
			</div>
			<div className="col-lg-6">
				<div className="row">
					<div className="col-lg-6">
						<Feature 
							link={ items[1].link }
							wide_image={ items[1].wide_image } 
							tall_image={ items[1].tall_image } 
							title={ items[1].title } 
							color={ items[1].color } 
							description={ items[1].description } 
						/>
					</div>
					<div className="col-lg-6">
						<Feature 
							link={ items[2].link }
							wide_image={ items[2].wide_image } 
							tall_image={ items[2].tall_image } 
							title={ items[2].title } 
							color={ items[2].color } 
							description={ items[2].description } 
						/>
					</div>
				</div>
				<div className="row">
					<div className="col-lg-6">
						<Feature 
							link={ items[3].link }
							wide_image={ items[3].wide_image } 
							tall_image={ items[3].tall_image } 
							title={ items[3].title } 
							color={ items[3].color } 
							description={ items[3].description } 
						/>
					</div>
					<div className="col-lg-6">
						<Feature 
							link={ items[4].link }
							wide_image={ items[4].wide_image } 
							tall_image={ items[4].tall_image } 
							title={ items[4].title } 
							color={ items[4].color } 
							description={ items[4].description } 
						/>
					</div>
				</div>
			</div>
		</div>
	);
}

function FeatureBlockFiveCardsSmall(items) {
	return (
		<>
			<div className="row">
				<div className="col-md-12">
					<Feature 
						link={ items[0].link }
						wide_image={ items[0].wide_image } 
						tall_image={ items[0].tall_image } 
						title={ items[0].title } 
						color={ items[0].color } 
						description={ items[0].description } 
					/>
				</div>
			</div>
			<div className="row">
				<div className="col-md-6">
					<Feature 
						link={ items[1].link }
						wide_image={ items[1].wide_image } 
						tall_image={ items[1].tall_image } 
						title={ items[1].title } 
						color={ items[1].color } 
						description={ items[1].description } 
					/>
				</div>
				<div className="col-md-6">
					<Feature 
						link={ items[2].link }
						wide_image={ items[2].wide_image } 
						tall_image={ items[2].tall_image } 
						title={ items[2].title } 
						color={ items[2].color } 
						description={ items[2].description } 
					/>
				</div>
			</div>
			<div className="row">
				<div className="col-md-6">
					<Feature 
						link={ items[3].link }
						wide_image={ items[3].wide_image } 
						tall_image={ items[3].tall_image } 
						title={ items[3].title } 
						color={ items[3].color } 
						description={ items[3].description } 
					/>
				</div>
				<div className="col-md-6">
					<Feature 
						link={ items[4].link }
						wide_image={ items[4].wide_image } 
						tall_image={ items[4].tall_image } 
						title={ items[4].title } 
						color={ items[4].color } 
						description={ items[4].description } 
					/>
				</div>
			</div>
		</>
	);
}

class FeatureBlock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			//imageUrl: '',
			isLarge: false,
		};

    this.handleChangeScreenWidth = this.handleChangeScreenWidth.bind(this);
  }
	handleChangeScreenWidth(ev) {
		this.setState({ isLarge: ev.matches });
	}
	componentDidMount() {
		this.match = window.matchMedia(`(min-width: 1200px)`);
		this.handleChangeScreenWidth(this.match);
    this.match.addListener(this.handleChangeScreenWidth);
  }
  componentWillUnmount() {
    this.match.removeListener(this.handleChangeScreenWidth);
  }
	render() {
		let { items } = this.props;
		let { isLarge } = this.state;
		let featureCards;
		switch(items.length) {
			case 2:
				featureCards = FeatureBlockTwoCards(items);
				break;
			case 3:
				featureCards = FeatureBlockThreeCards(items);
				break;
			case 4:
				if (isLarge) {
					featureCards = FeatureBlockFourCards(items);	
				} else {
					featureCards = FeatureBlockFourCardsSmall(items);
				}
				break;
			default:
				if (isLarge) {
					featureCards = FeatureBlockFiveCards(items);	
				} else {
					featureCards = FeatureBlockFiveCardsSmall(items);
				}
		}

		return (
			<div className={ styles.feature }>
				{ featureCards }
			</div>
		);
	}
}

export default FeatureBlock;
