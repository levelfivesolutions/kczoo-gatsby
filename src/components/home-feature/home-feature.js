import React, { useState, useEffect } from "react"
import Link from "gatsby-link"
import {
  ColorToHex,
  renderAsText,
  renderAsHtml,
  renderImageUrl,
  linkResolver,
} from "../../utility.js"

import styles from "./home-feature.module.css"

export default ({
  wide_image,
  tall_image,
  title,
  description,
  link,
  color = "Zoo Green",
  tall,
  wide,
}) => {
  let style = {
    backgroundColor: ColorToHex(color),
  }

  function selectImage(tall, wide) {
    if (typeof window != "undefined") {
      const width = window.innerWidth

      console.log({ width, wide, tall, wide_image, tall_image })
      if (wide) {
        if (width >= 992 && width < 1200) {
          return typeof wide_image["Home-Feature-Small"] != "undefined"
            ? wide_image["Home-Feature-Small"] || wide_image?.url
            : ""
        } else {
          return typeof wide_image["Home-Feature-Wide"] != "undefined"
            ? wide_image["Home-Feature-Wide"] || wide_image?.url
            : ""
        }
      }
      if (tall) {
        if (tall_image) {
          return tall_image
        }
      }
    }
    if (tall) {
      if (tall_image) {
        return tall_image
      }
    } else {
      return typeof wide_image["Home-Feature-Wide"] !== "undefined"
        ? wide_image["Home-Feature-Wide"]
        : wide_image?.url
    }
  }

  const [image, setImage] = useState(selectImage(tall, wide))

  let classes = styles.feature

  useEffect(() => {
    if (typeof window != "undefined") {
      window.addEventListener("resize", () => {
        if (wide) {
          if (wide_image) {
            setImage(selectImage(tall, wide))
          } else {
            setImage(wide_image)
          }
        }

        if (tall) {
          if (tall_image) {
            setImage(tall_image)
          }
        }
      })
    }
    console.log({
      wide_image,
      tall_image,
      title,
      description,
      link,
      tall,
      wide,
      image,
    })
    let image = selectImage(tall, wide)
    setImage(selectImage(tall, wide))
  }, [])

  console.log({
    wide_image,
    tall_image,
    title,
    description,
    link,
    tall,
    wide,
    image,
  })
  if (tall) {
    classes += ` ${styles.featureTall}`
  } else if (wide) {
    classes += ` ${styles.featureWide}`
  }

  return (
    <Link to={linkResolver(link._meta)} className={styles.featureLink}>
      <div className={styles.featureContainer}>
        <div className={classes}>
          <div
            className={styles.featureImage}
            style={{ backgroundImage: `url(${renderImageUrl(image)})` }}
          ></div>
          <div className={styles.featureHeader} style={style}>
            <div className={styles.featureWrapper}>
              <h3 className={styles.featureTitle}>{renderAsText(title)}</h3>
              <div className={styles.featureDescription}>
                {renderAsHtml(description)}
              </div>
            </div>
            <span className={styles.featureArrow}>&rarr;</span>
          </div>
        </div>
      </div>
    </Link>
  )
}
