import React from 'react';
import Link from 'gatsby-link';
import { ColorToHex, renderAsText, renderAsHtml, renderImageUrl, linkResolver } from '../../utility.js';

import styles from './home-feature.module.css'

export default ({ wide_image, tall_image, title, description, link, color = 'Zoo Green', tall, wide }) => {
	let style = {
		backgroundColor: ColorToHex(color)
	};
	
	let classes = styles.feature;
	let wideImage = wide_image ? wide_image['wide-fat-short'] : '';
	let image = wideImage;
	let imgStyle = null;
	if (tall) {
		classes += ` ${styles.featureTall}`;
		image = tall_image;
	} else 
	
	if (wide) {
		classes += ` ${styles.featureWide}`;
	}
	
  return (
		<div className={ styles.featureContainer }>
			<div className={ classes }>
				<picture className={ styles.featurePicture }>
					<source media="(max-width: 768px)" srcSet={ renderImageUrl(wideImage) } />
					<source srcSet={ renderImageUrl(image) } />
					<img src={ renderImageUrl(image) } alt={ image.alt } className={ styles.featureImage } />
				</picture>
				<div className={styles.featureHeader} style={ style }>
					<div className={ styles.featureWrapper }>
						<h3 className={styles.featureTitle}>
							<Link to={ linkResolver(link) }>{ renderAsText(title) }</Link>
						</h3>
						<div className={ styles.featureDescription }>{ renderAsHtml(description) }</div>
					</div>
					<Link to={ linkResolver(link._meta) } className={styles.featureLink}>&rarr;</Link>
				</div>
			</div>
		</div>
	)
}
