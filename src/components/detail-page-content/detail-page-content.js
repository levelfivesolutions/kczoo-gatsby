import React from "react"
import Title from "../title"
import BodyText from "../body-text"
import Text from "../text"
import { RichText } from "prismic-reactjs"
import styles from "../../templates/detail.module.css"
import "./detail-page-content.css"
import {
  filterArrayByType,
  renderAsHtml,
  renderAsText,
  renderImageUrl,
} from "../../utility"
import TextBlocks from "../text-blocks"
import SectionBlock from "../section-block"

export default function DetailPageContent({ data, classList, ...args }) {

  if (!data || data?.fields?.length <= 0) return null
  const {
    detail_page_content_title,
    detail_page_content_body_text,
    detail_page_content_optional_image,
    detail_page_content_summary,
    detail_page_content_under_optional_image_caption,
  } = data.fields[0]

  const hasSidebar = detail_page_content_optional_image

  return (
    <SectionBlock classList={classList} {...args}>
      <BodyText {...args}>
        <div className="container">
          <div className="row">
            <div className="offset-md-1 col-md-10">
              <Title className={styles.title}>
                {renderAsText(detail_page_content_title)}
              </Title>
            </div>
          </div>
          {!hasSidebar && (
            <div className="row">
              <div className="offset-md-1 col-md-10">
                {detail_page_content_body_text &&
                  RichText.asText(detail_page_content_body_text).length > 0 && (
                    <Text>{detail_page_content_body_text}</Text>
                  )}
                <TextBlocks body={null} />
              </div>
            </div>
          )}
          {hasSidebar && (
            <div className="row">
              <div className="offset-md-1 col-md-7">
                {detail_page_content_body_text &&
                  renderAsHtml(detail_page_content_body_text)}
              </div>
              <div className="col-md-3">
                {detail_page_content_optional_image && (
                  <figure className={styles.activityFigure}>
                    <img
                      src={renderImageUrl(detail_page_content_optional_image)}
                      className={`${styles.activityImage} img-fluid`}
                      alt=""
                    />
                    {detail_page_content_under_optional_image_caption && (
                      <aside>
                        {renderAsHtml(
                          detail_page_content_under_optional_image_caption
                        )}
                      </aside>
                    )}
                  </figure>
                )}
              </div>
            </div>
          )}
        </div>
      </BodyText>
    </SectionBlock>
  )
}
