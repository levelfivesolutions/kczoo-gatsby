import React from 'react';
import styles from './hero-landing.module.css';
import { renderImageUrl } from '../../utility.js';

class Hero extends React.Component {
  render() {
		let { image, pageId } = this.props;
		let imageUrl = renderImageUrl(image);
		let style = {
			backgroundImage: `url(${ imageUrl })`
		};
		
		return (
			<div className={ styles.hero } style={ style }>
				<div className={ styles.wholePageEditButton } data-wio-id={ pageId }></div>
			</div>
		)
	}
}

export default Hero;
