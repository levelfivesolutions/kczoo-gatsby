import React from "react"
import { isObject, isString } from "lodash"
import { renderImageUrl, ColorToHex } from "../../utility.js"
import styles from "./index.module.css"

class Hero extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSmall: null,
      backgroundImage: "none",
      backgroundOpacity: 0,
    }

    this.handleChangeScreenWidth = this.handleChangeScreenWidth.bind(this)
  }
  handleChangeScreenWidth(ev) {
    this.setState({ isSmall: ev.matches })
    if (typeof window != "undefined" && !this.state.isSmall) {
      var isWindowSmall = window.innerWidth < 576 ? true : false
    }
    if ((this.state.isSmall || isWindowSmall) && this.props.tallImage) {
      this.setState({
        backgroundImage: `url(${renderImageUrl(this.props.tallImage)})`,
      })
    } else {
      this.setState({
        backgroundImage: `url(${renderImageUrl(this.props.image)})`,
      })
    }
  }
  componentDidMount() {
    this.match = window.matchMedia(`(max-width: 576px)`)
    this.handleChangeScreenWidth(this.match)
    this.match.addListener(this.handleChangeScreenWidth)
  }
  componentWillUnmount() {
    this.match.removeListener(this.handleChangeScreenWidth)
  }
  render() {
    const { image, tallImage, pageId, heroForegroundUrl, heroForegroundAlt, heroTitle, heroTitleColor, heroBorderColor } = this.props
    const { isSmall } = this.state
    let style = {}

    let heroForegroundStyle = {
      backgroundImage: `url(${heroForegroundUrl})`,
      backgroundSize: "contain",
      backgroundRepeat: "no-repeat",
      backgroundPosition: "center",
      width: "335px",
      height: "200px",
      marginBottom: "32px"
    }

    let titleStyle = {
      color: ColorToHex(heroTitleColor),
      borderTopColor: ColorToHex(heroBorderColor),
      borderBottomColor: ColorToHex(heroBorderColor),
    }

    function ImageOrTitle() {
      if(heroForegroundUrl) {
        console.log("Hero Foreground!")
        return(
          <div style={heroForegroundStyle}></div>
        )
      }
      else if(heroTitle) {
        console.log("Hero Title!")
        return(
          <div className={styles.heroTitle} style={titleStyle}>
            {heroTitle}
          </div>
        )
      }
    }

    return (
      <div
        className={styles.hero}
        style={{
          backgroundImage: this.state.backgroundImage,
        }}
      >
        <div className={styles.wholePageEditButton} data-wio-id={pageId}></div>
        {ImageOrTitle()}
      </div>
    )
  }
}

export default Hero
