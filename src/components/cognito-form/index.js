import React from "react"
import { ColorToHex } from '../../utility.js';
import "./index.css"

class CognitoForm extends React.Component {

  componentDidMount() {
    window.Cognito.mount(String(this.props.id), `#cognitoForm${this.props.id}`)
  }

  render() {
    const style = {
      color: ColorToHex('Zoo Blue')
    }

    return (
      <div
        className={`cognito cognitoForm`}
        id={`cognitoForm${this.props.id}`}
      >
      </div>
    )
  }
}

export default CognitoForm
