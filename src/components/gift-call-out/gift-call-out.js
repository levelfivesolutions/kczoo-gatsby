import React from 'react';
import Button from '../button/button-link.js';
import styles from './gift-call-out.module.css';
import { renderAsText, ColorToHex, filterArrayByType, renderAsHtml, renderLinkUrl } from '../../utility';

class GiftCallOut extends React.Component {
	render() {
		let { 
			giftCalloutLabel,
			giftCalloutTitle,
			giftCalloutDescription,
			giftCalloutImage,
			giftCalloutButtonText,
			giftCalloutButtonLink,
			giftCalloutColorPicker,
			giftCalloutBody,
			giftCalloutButtonOption2,
			giftCalloutButtonOption3,
			giftCalloutButtonOption2Link,
			giftCalloutButtonOption3Link,
			giftCalloutDropdownText,
			url,
			link,
			linkText,
			linkTarget } = this.props;
		let backgroundStyle = {
			backgroundColor: '#EEEEEE'
		};

		return (
			<div className="container">
				<div className={styles.giftTitleBar} style={backgroundStyle}>
					<div className="row">
						<div className="col-lg-4 col-md-12">
							<div className={styles.giftCalloutImageContainer}>
								<img src={giftCalloutImage} alt="" className={"img-fluid " + styles.giftCalloutImage} />
							</div>
						</div>
						<div className="col-lg-8 col-md-12">
							<div style={{ backgroundColor: ColorToHex((giftCalloutBody) ? (giftCalloutColorPicker) : "Tangerine") }} className={styles.giftTitleBarLabel}>{giftCalloutLabel && renderAsText(giftCalloutLabel)}</div>
							<h1 style={{ color: ColorToHex((giftCalloutBody) ? (giftCalloutColorPicker) : "Tangerine") }} className={`h1 ${styles.giftTitleBarTitle}`}>{giftCalloutTitle && renderAsText(giftCalloutTitle)}</h1>
							<div className={styles.giftTitleBarDesc}>{giftCalloutDescription && renderAsText(giftCalloutDescription)}</div>
							{
								(!giftCalloutButtonOption2 && !giftCalloutButtonOption3) &&  <a style={{ color: ColorToHex((giftCalloutBody) ? (giftCalloutColorPicker) : "Tangerine") }} className={styles.giftCalloutButton} href={giftCalloutButtonLink} size="large" >{giftCalloutButtonText}</a>
							}
							{
								(giftCalloutButtonOption2 || giftCalloutButtonOption3)  &&

								<div className={`${ styles.giftDropdown}`}>
									<button className={`btn dropdown-toggle ${ styles.giftDropdownToggle }`} style={{ color: ColorToHex((giftCalloutBody) ? (giftCalloutColorPicker) : "Tangerine")}} isInverse type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                		{ giftCalloutDropdownText }
									</button>
									
									<div className={`${ styles.giftDropdownMenu } dropdown-menu`} aria-labelledby="dropdownMenuButton">
										<a className="dropdown-item" 
										target="_blank"
										href={giftCalloutButtonLink ? giftCalloutButtonLink : '#'} 
										style={{ color: ColorToHex((giftCalloutBody) ? (giftCalloutColorPicker) : "Tangerine") }}>
											{giftCalloutButtonText}
										</a>
										{
											giftCalloutButtonOption2 &&
											<a className="dropdown-item" 
											target="_blank"
											href={giftCalloutButtonOption2Link ? giftCalloutButtonOption2Link : '#'} 
											style={{ color: ColorToHex((giftCalloutBody) ? (giftCalloutColorPicker) : "Tangerine") }}>
												{giftCalloutButtonOption2}
											</a>
										}
										{
											giftCalloutButtonOption3 &&
											<a className="dropdown-item" 
											target="_blank"
											href={giftCalloutButtonOption3Link ? giftCalloutButtonOption3Link : '#'} 
											style={{ color: ColorToHex((giftCalloutBody) ? (giftCalloutColorPicker) : "Tangerine") }}>
												{giftCalloutButtonOption3}
											</a>
										}	
									</div>
								</div>
							}
					</div>
				</div>
			</div>
			</div >
			
		)

	}
}

export default GiftCallOut;



