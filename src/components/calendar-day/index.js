import React from 'react';
import moment from 'moment';
import { ColorToHex } from '../../utility';
import styles from './index.module.css';

export default function ({ date, size, color }) {
    if (!date) {
        return null;
    }
	let style = null;
	if (color) {
		style = {
			backgroundColor: ColorToHex(color)
		};		
	}

    return (
        <div className={ size === 'extra-large' ? styles.calendarExtraLarge : size === 'large' ? styles.calendarLarge : styles.calendar }>
            <div className={ size === 'extra-large' ? styles.calendarDayExtraLarge : size === 'large' ? styles.calendarDayLarge : styles.calendarDay }>
                { moment(date).format('D') }
            </div>
            <div className={ size === "extra-large" ? styles.calendarMonthExtraLarge : size === 'large' ? styles.calendarMonthLarge : styles.calendarMonth } style={ style }>
                {  size === 'large' || size === "extra-large" ? moment(date).format('MMMM') : moment(date).format('MMM') }
            </div>
        </div>
    );
}
