import React, { useEffect, useState } from "react"
import moment from "moment"
import sortBy from "lodash.sortby"
import { linkResolver, renderImageUrl, renderAsText } from "../../utility.js"
import { Link } from "gatsby"
import { RichText } from "prismic-reactjs"
import Button from "../button/button-link.js"

import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

import Slider from "react-slick"

import styles from "./index.module.css"
import { number } from "prop-types"

function NextArrow(props) {
  const { className, style, onClick } = props
  return (
    <button
      type="button"
      className={styles.carouselNext}
      style={{ ...style }}
      onClick={onClick}
    >
      Next
    </button>
  )
}

function PrevArrow(props) {
  const { className, style, onClick } = props
  return (
    <button
      type="button"
      className={styles.carouselPrev}
      style={{ ...style }}
      onClick={onClick}
    >
      Previous
    </button>
  )
}

export default ({ events }) => {
  const [isRendered, setIsRendered] = useState(false)

  var settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    mobileFirst: true,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  }

  useEffect(() => {
    setIsRendered(true)
  }, [])

  // BUILD EVENT TIMES
  let eventTimes = []
  let today = moment()

  events.forEach(event => {
    if (event.node.activity_time) {
      event.node.activity_time.forEach(time => {
        if (time.start_time) {
          let endDate = moment(time.start_time).add(
            (time.number_of_days || 1) - 1,
            "days"
          )
          if (endDate.isSameOrAfter(today)) {
            eventTimes.push(
              Object.assign({}, time, {
                _meta: event.node._meta,
                title: event.node.title,
                image: event.node.spot_images,
              })
            )
          }
        }
      })
    }
  })
  console.log("eventTimes", eventTimes)

  // SORT BY TIME
  let eventsByTime = sortBy(eventTimes, [d => d.start_time])
  console.log("eventsByTime", eventsByTime)

  if (!isRendered) {
    return (
      <section className={styles.carouselContainer}>
        <div className={styles.carouselHeader}>
          <h2 className={styles.carouselTitle}>Upcoming Events</h2>
          <Button
            text="See All"
            url="/events"
            color="Zoo Green"
            isInverse={true}
          />
        </div>
        <div className={`text-center ${styles.carouselWrapper}`}>
          <span style={{ fontSize: "18px" }}>Loading...</span>
        </div>
      </section>
    )
  } else
    return (
      <section className={styles.carouselContainer}>
        <div className={styles.carouselHeader}>
          <h2 className={styles.carouselTitle}>Upcoming Events</h2>
          <Button
            text="See All"
            url="/events"
            color="Zoo Green"
            isInverse={true}
          />
        </div>
        <div className={styles.carouselWrapper}>
          <Slider {...settings}>
            {eventsByTime.slice(0, 7).map((d, i) => {
              console.log("EVENT", d)
              let dateString = ""
              if (d.start_time) {
                let startDate = moment(d.start_time)
                dateString = startDate.format("MMMM D")

                if (d.number_of_days && d.number_of_days > 0) {
                  let endDate = moment(d.start_time).add(
                    d.number_of_days - 1,
                    "days"
                  )
                  if (startDate.month() === endDate.month()) {
                    dateString += ` - ${endDate.format("D")}`
                  } else {
                    dateString += ` - ${endDate.format("MMMM D")}`
                  }
                }
              }

              return (
                <div className={styles.eventContainer} key={i}>
                  <div className={styles.eventImageContainer}>
                    <Link to={linkResolver(d._meta)}>
                      <img
                        className={styles.eventImage}
                        src={renderImageUrl(d.image)}
                        alt={d.alt}
                      />
                    </Link>
                  </div>
                  <h3 className={styles.eventTitle}>
                    <Link
                      to={linkResolver(d._meta)}
                      className={styles.eventTitleLink}
                    >
                      {renderAsText(d.title)}
                    </Link>
                  </h3>
                  <div className={styles.eventDate}>{dateString}</div>
                </div>
              )
            })}
          </Slider>
        </div>
      </section>
    )
}
