import React from 'react';
import { Link } from 'gatsby';
import { renderAsText, renderImageUrl, linkResolver } from '../../utility.js';
import Button from '../button/button-link.js';
import styles from './index.module.css';

export default function ({animal}) {console.log('animal', animal);

	function handleClickModalHide() {
		console.log("Trying to hide modal");
		//window.$('#animalModal').modal('hide');
	}



	return (
		<div id="animalModal" className="modal" tabIndex="-1" role="dialog">
			<div className={`${ styles.modalDialog } modal-dialog modal-lg`} role="document">
				<img src={ animal.tall_images ? renderImageUrl(animal.tall_images['Animals-Overlay'] || animal.tall_images) : '' } className={ styles.modalImage } alt="" />
				<div className={`modal-content ${ styles.modalContent }`}>
					<div className={`modal-header ${ styles.modalHeader }`}>
						<h5 className={`modal-title ${ styles.modalTitle }`}>{ animal.animal_name }</h5>
						<button type="button" className="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div className={`modal-body ${ styles.modalBody }`}>
						<p>{ animal.animal_description }</p>
						<div className={ styles.modalIconBar }>
							<Link to={ `/zoo-map${ (animal.map_id) ? '?location=' + renderAsText(animal.map_id) : '' }`} onClick={ev => this.handleClickModalHide() }><span className={ `${styles.iconMap} mr-2` }></span>Find on Map</Link>
							{ 
								animal.animal_cam && 
								<Link to={ linkResolver(animal.animal_cam._meta) } onClick={ev => this.handleClickModalHide() }>
									<span className={ `${styles.iconCam} mr-2` }></span>View Cam
								</Link>
							}
							{ 
								animal.donate_cta && 
								<Link to={ linkResolver(animal.donate_cta._meta) } onClick={ev => this.handleClickModalHide() }>
									<span className={ `${styles.iconHeart} mr-2` }></span>Donate Today!
								</Link>
							}
						</div>
						{
							animal.button_link &&
							<div className={ styles.cta }>
								<div className={ styles.ctaBody }>
									<h6 className={ styles.ctaTitle }>{ renderAsText(animal.button_link.title) }</h6>
									<div>{ animal.button_link.summary }</div>
								</div>
								<Button 
									text="Learn More" 
									isInverse={ true } 
									link={ animal.button_link }
									onClick2={ window.$('#animalModal').hasClass('show') ? console.log("inital open of modal") : console.log("close modal") }
								/>
							</div>
						}
					</div>
				</div>
			</div>
		</div>	
	);
}
