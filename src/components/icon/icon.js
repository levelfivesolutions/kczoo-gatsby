import React from "react"
import styles from "./icon.module.css"

const icons = {
  alert: {
    src: "/images/icons/alert.svg",
  },
  animalfeed: {
    src: "/images/icons/animalfeed.svg",
  },
  arrow: {
    src: "/images/icons/arrowright.svg",
  },
  arrowleft: {
    src: "/images/icons/arrowleft.svg",
  },
  award: {
    src: "/images/icons/award.svg",
  },
  badge: {
    src: "/images/icons/badge.svg",
  },
  cake: {
    src: "/images/icons/cake.svg",
  },
  calendar: {
    src: "/images/icons/calendar.svg",
  },
  caricature: {
    src: "/images/icons/caricaturist.svg",
  },
  carousel: {
    src: "/images/icons/carousel.svg",
  },
  check: {
    src: "/images/icons/check.svg",
  },
  clock: {
    src: "/images/icons/clock.svg",
  },
  close: {
    src: "/images/icons/close.svg",
  },
  expand: {
    src: "/images/icons/down.svg",
  },
  cupcake: {
    src: "/images/icons/cupcake.svg",
  },
  decorations: {
    src: "/images/icons/balloons.svg",
  },
  elephant: {
    src: "/images/icons/elephant.svg",
  },
  email: {
    src: "/images/icons/email.svg",
  },
  modal: {
    src: "/images/icons/icon-modal.svg",
  },
  collapse: {
    src: "/images/icons/up.svg",
  },
  facepaint: {
    src: "/images/icons/facepaint.svg",
  },
  fb: {
    src: "/images/icons/fb.svg",
  },
  fish1: {
    src: "/images/icons/fish1.svg",
  },
  fish2: {
    src: "/images/icons/fish2.svg",
  },
  fish3: {
    src: "/images/icons/fish3.svg",
  },
  fish4: {
    src: "/images/icons/fish4.svg",
  },
  fish5: {
    src: "/images/icons/fish5.svg",
  },
  fish6: {
    src: "/images/icons/fish6.svg",
  },
  food: {
    src: "/images/icons/food.svg",
  },
  "group of friends": {
    src: "/images/icons/group.svg",
  },
  hamburgermenu: {
    src: "/images/icons/hamburgermenu.svg",
  },
  hippo: {
    src: "/images/icons/hippo.svg",
  },
  home: {
    src: "/images/icons/home.svg",
  },
  instagram: {
    src: "/images/icons/instagram.svg",
  },
  lion: {
    src: "/images/icons/lion.svg",
  },
  location: {
    src: "/images/icons/location.svg",
  },
  map: {
    src: "/images/icons/map.svg",
  },
  media: {
    src: "/images/icons/media.svg",
  },
  minus: {
    src: "/images/icons/minus.svg",
  },
  monkey: {
    src: "/images/icons/monkey.svg",
  },
  newsletter: {
    src: "/images/icons/newsletter.svg",
  },
  next: {
    src: "/images/icons/next.svg",
  },
  penguin: {
    src: "/images/icons/penguin.svg",
  },
  pizza: {
    src: "/images/icons/pizza.svg",
  },
  plus: {
    src: "/images/icons/plus.svg",
  },
  plush: {
    src: "/images/icons/plush.svg",
  },
  polarbear: {
    src: "/images/icons/polarbear.svg",
  },
  previous: {
    src: "/images/icons/previous.svg",
  },
  register: {
    src: "/images/icons/register.svg",
  },
  gift: {
    src: "/images/gift.svg",
  },
  rides: {
    src: "/images/icons/train.svg",
  },
  "sea-lion": {
    src: "/images/icons/sealion.svg",
  },
  search: {
    src: "/images/icons/search.svg",
  },
  tickets: {
    src: "/images/icons/tickets.svg",
  },
  tiger: {
    src: "/images/icons/tiger.svg",
  },
  twitter: {
    src: "/images/icons/twitter.svg",
  },
  wristband: {
    src: "/images/icons/wristband.svg",
  },
  youtube: {
    src: "/images/icons/youtube.svg",
  },
  "zoo-bucks": {
    src: "/images/icons/zoobucks.svg",
  },
  baboon: {
    src: "/images/icon-baboon.svg",
  },
  giraffe: {
    src: "/images/icon-giraffe.svg",
  },
  flamingo: {
    src: "/images/icon-flamingo.svg",
  },
  postcard: {
    src: "/images/icon-postcard.svg",
  },
  "map-camel": {
    src: "/images/icons/map-camel.svg",
  },
  "map-chimpanzee": {
    src: "/images/icons/map-chimpanzee.svg",
  },
  "map-elephant": {
    src: "/images/icons/map-elephant.svg",
  },
  "map-giraffe": {
    src: "/images/icons/map-giraffe.svg",
  },
  "map-hippo": {
    src: "/images/icons/map-hippo.svg",
  },
  "map-orangutan": {
    src: "/images/icons/map-orangutan.svg",
  },
  "map-penguin": {
    src: "/images/icons/map-penguin.svg",
  },
  "map-tamarin": {
    src: "/images/icons/map-tamarin.svg",
  },
  "map-shows": {
    src: "/images/icons/map-shows.svg",
  },
  "map-veterinary": {
    src: "/images/icons/map-veterinary.svg",
  },
}

const colors = {
  "ZOO GREEN": {
    mods:
      "invert(47%) sepia(8%) saturate(3283%) hue-rotate(74deg) brightness(121%) contrast(90%)",
  },
  "ZOO BLUE": {
    mods:
      "invert(33%) sepia(90%) saturate(714%) hue-rotate(158deg) brightness(102%) contrast(105%)",
  },
  ORCHID: {
    mods:
      "invert(33%) sepia(53%) saturate(1187%) hue-rotate(296deg) brightness(86%) contrast(84%)",
  },
  TANGERINE: {
    mods:
      "invert(61%) sepia(63%) saturate(453%) hue-rotate(343deg) brightness(102%) contrast(97%)",
  },
  SEA: {
    mods:
      "invert(68%) sepia(7%) saturate(3182%) hue-rotate(107deg) brightness(90%) contrast(98%)",
  },
  "TERRA COTTA": {
    mods:
      "invert(31%) sepia(23%) saturate(4113%) hue-rotate(334deg) brightness(96%) contrast(84%)",
  },
  PUMPKIN: {
    mods:
      "invert(71%) sepia(54%) saturate(6568%) hue-rotate(334deg) brightness(102%) contrast(98%)",
  },
  CANARY: {
    mods:
      "invert(82%) sepia(71%) saturate(664%) hue-rotate(323deg) brightness(104%) contrast(101%)",
  },
  WHITE: {
    mods:
      "invert(100%) sepia(100%) saturate(0%) hue-rotate(288deg) brightness(102%) contrast(102%)",
  },
  GREY: {
    mods:
      "invert(45%) sepia(0%) saturate(0%) hue-rotate(252deg) brightness(95%) contrast(87%)",
  },
  "JAZZOO-RED": {
    mods:
      "invert(42%) sepia(86%) saturate(749%) hue-rotate(342deg) brightness(90%) contrast(96%)",
  },
  "JAZZOO-ORANGE": {
    mods:
      "invert(73%) sepia(54%) saturate(1166%) hue-rotate(324deg) brightness(105%) contrast(97%)",
  },
  "JAZZOO-GREEN": {
    mods:
      "invert(81%) sepia(5%) saturate(2614%) hue-rotate(78deg) brightness(95%) contrast(77%)",
  },
  "JAZZOO-LIGHT-GREEN": {
    mods:
      "invert(84%) sepia(50%) saturate(600%) hue-rotate(17deg) brightness(95%) contrast(82%)",
  },
  "JAZZOO-BLUE": {
    mods:
      "invert(91%) sepia(28%) saturate(3535%) hue-rotate(163deg) brightness(93%) contrast(95%)",
  },
  BLACK: {
    mods: "",
  },
}

export default ({
  icon = "alert",
  className,
  height,
  width,
  color = "BLACK",
  alt,
}) => {
  return (
    <img
      src={icons[icon] ? icons[icon].src : icons["alert"].src}
      className={className}
      height={height}
      width={width}
      alt={alt}
      style={{
        filter: `brightness(0) saturate(100%) ${colors[color.toUpperCase()]
          .mods || ""}`,
      }}
    />
  )
}
