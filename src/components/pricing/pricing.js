import React from 'react';
import Button from '../button/button-link.js';
import styles from './pricing.module.css';

export default ({ isInverse, price, quantifier, description, prices, icon, btnColor, btnText, btnUrl, btnLink, onClick }) => {
	let classes = styles.pricing;
	if (isInverse) {
		classes = `${ styles.pricing } ${ styles.pricingInverse }`;
	}
	return (
		<div className={ classes }>
			{ 
				price &&
				<div className={ styles.pricingPrice }>
					<div className={ styles.pricingQuantity }>{ price }</div>
					<div className={ styles.pricingQuantifier }>{ quantifier }</div>
				</div> 
			}
			{
				prices && 
				prices.map((d, i) => {
					return (
						<div className={ styles.pricingPrice } key={ i }>
							<div className={ styles.pricingQuantity }>{ d.price}</div>
							<div className={ styles.pricingQuantifier }>{ d.description }</div>
						</div> 
					);
				})
			}
			{ description && <div className={ styles.pricingDesc }>{ description }</div> }
			{ 
				btnText && 
				<div className={ styles.pricingButton }>
					<Button 
						isInverse={ isInverse } 
						color={ btnColor } 
						text={ btnText } 
						icon={ icon} 
						url={ btnUrl } 
						link={ btnLink } 
						size="medium"
						onClick={ onClick }
					/>
				</div> 
			}
		</div>
	);
}
