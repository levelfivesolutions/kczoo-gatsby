import React from 'react';
import { ColorToHex, renderAsText } from '../../utility';
import styles from './index.module.css'
import Icon from '../icon/icon';

class SiteWideAlert extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            window: null
        }
    }

    componentDidMount(){
        this.setState({ window: window })
    }
    
    render() {
        console.log("PROPS", this.props)
		let { alerts, id } = this.props;
		
		
        
		return (
			alerts.map( (alert, i) => {

				if(this.state.window){
					let dismiss = () => {
							document.getElementById(`sitewide-alert-${i}`).style.transitionDelay = "0";
							document.getElementById(`sitewide-alert-${i}`).style.transitionDuration = ".2s";
							document.getElementById(`sitewide-alert-${i}`).style.maxHeight = "0";
							document.getElementById(`sitewide-alert-${i}`).style.border = "0px solid #FFF";
							this.state.window.sessionStorage.setItem(renderAsText( alert.sitewide_alert_title), true)
					}
					if( !this.state.window.sessionStorage.getItem(renderAsText( alert.sitewide_alert_title))) {
						return(
							<div className={`${styles.alertContainer} container-fluid`} id={`sitewide-alert-${i}`} key={ i } style={{ animationDelay: `${.5 * (i + 1)}s`, zIndex: `${1005 - i}` }}>
								<div className={ styles.wholePageEditButton } data-wio-id={ id }></div>
									<div className={` ${styles.alertRow} row`} style={{ backgroundColor: ColorToHex(alert.sitewide_alert_color || "Zoo Blue")}}>
											<div className={` ${styles.alertCol} col-lg-auto`}>
													<p className={ styles.alertTitle }>{ renderAsText( alert.sitewide_alert_title) }</p>
											</div>
											<div className={` ${styles.alertCol} ${ styles.alertDescriptionCol} col`}>
													<div>{ renderAsText( alert.sitewide_alert_description) + ' ' }
														{
															alert.sitewide_alert_learn_more_link && 
															<a className={ styles.alertLearnMoreLink } href={ renderAsText(alert.sitewide_alert_learn_more_link.url) } >{ alert.sitewide_alert_learn_more_text }</a> }
													</div>
											</div>
											<div onClick={ dismiss } className={` ${styles.alertCol} ${ styles.alertCloseCol } justify-content-center col-12 col-lg-auto`}>
													<span className="mr-3">Dismiss</span><Icon icon="close" width="15" color="white"></Icon>
											</div>
									</div>
							</div>
						)
					}
				}
			})
		);
	}
}

export default SiteWideAlert;