import React from 'react';
import { Link } from "gatsby";
import { renderAsText } from '../../utility.js';
import styles from './index.module.css';

export default ({ title, bodyCopy, image, internalUrl, externalLink }) => {
	return (
		<div className={ styles.projectContainer }>
			<div className={ styles.projectImageContainer }>
			{
				externalLink &&
				<a href={ externalLink.url } target="_blank">
					<img className={ styles.projectImage } src={ image ? image.url : '' } alt={ image ? image.alt : '' } />
				</a>
			}
			{
				!externalLink &&
				<Link to={ internalUrl }>
					<img className={ styles.projectImage } src={ image ? image.url : '' } alt={ image ? image.alt : '' } />
				</Link>
			}
			</div>
			{
				externalLink &&
				<h3 className={ styles.projectTitle }>
					<a href={ externalLink.url } className={ styles.projectLink } target="_blank">{ renderAsText(title) }</a>
				</h3>
			}
			{
				!externalLink &&
				<h3 className={ styles.projectTitle }>
					<Link to={ internalUrl } className={ styles.projectLink }>{ renderAsText(title) }</Link>
				</h3>
			}
			<div className={ styles.projectBody }>{ renderAsText(bodyCopy) }</div>
		</div>
	);
}
