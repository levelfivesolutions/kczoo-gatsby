import React from "react"
import { Link } from "gatsby"
import { renderAsText, renderAsHtml, renderLinkUrl, chunk } from "../../utility"
import styles from "./index.module.css"

export default ({ title, parent, footer }) => {
  let footerLinks = chunk(footer.footer_links, 2)
  let socialLinks = chunk(footer.footer_social_links, 3)

  console.log("FOOTER ", footer)
  return (
    <>
      <div className={styles.kczooBreadcrumbs}>
        <div className="container">
          <>
            <a href="/">
              <img
                src="/images/icon-home.svg"
                className="align-self-center mr-2"
                alt="Home"
              />{" "}
              Home
            </a>
            {parent && (
              <>
                {" "}
                &gt; <a href={parent.url}>{parent.title}</a>
              </>
            )}
            {title && <> &gt; {renderAsText(title)}</>}
          </>
        </div>
      </div>
      <footer className={styles.kczooFooter}>
        <div className="container">
          <div className={`${styles.mainRow} row p-0 m-0`}>
            <div className={`${styles.footerLogo} col-lg-7 p-0`}>
              <div className={`${styles.moveLinks} d-flex m-0`}>
                <div className={`${styles.centerImg} col-lg-3 p-0`}>
                  <img
                    src="/images/kczoo-white.png"
                    width="130"
                    alt="Kansas City Zoo"
                  />
                </div>
                <div className={`${styles.footerNavWrapper} mb-4`}>
                  {footerLinks.map((chunk, i) => {
                    return (
                      <div
                        className={`${styles.navSpacing} mr-4 ml-3`}
                        key={Math.random(0, 9999999)}
                      >
                        {chunk.map((link, j) => {
                          return (
                            <React.Fragment key={Math.random(0, 99999)}>
                              <div className="mb-1">
                                <a href={renderLinkUrl(link.footer_link)}>
                                  {renderAsText(link.footer_link_text)}
                                </a>
                              </div>
                            </React.Fragment>
                          )
                        })}
                      </div>
                    )
                  })}
                </div>
              </div>
              <hr className={`${styles.divider} mb-5 mt-2`} />
              <div className="offset-lg-3">
                <div className={`${styles.associationsWrapper} my-2`}>
                  <div className="mb-3">
                    <img
                      src="/images/logo-zoological@2x.png"
                      width="53"
                      className="m-0"
                      alt=""
                    />
                  </div>
                  <div className="mb-3">
                    <img
                      src="/images/logo-aza@2x.png"
                      width="86"
                      className="m-0"
                      alt=""
                    />
                  </div>
                  <div className="mb-3">
                    <img
                      src="/images/logo-waz@2x.png"
                      width="104"
                      className="m-0"
                      alt=""
                    />
                  </div>
                  <div className="mb-3">
                    <img
                      src="/images/logo-kc-parks@2x.png"
                      width="100"
                      alt=""
                    />
                  </div>
                </div>
                <div className={styles.copyrightWrapper}>
                  <p className={styles.footerCopyright}>
                    &copy; {new Date().getFullYear()} Kansas City Zoo. All
                    rights reserved.
                  </p>
                </div>
                <hr className={`${styles.divider} mt-2 mb-4`} />
              </div>
            </div>
            <div className={`${styles.footerContact} col-lg-2 p-0`}>
              <div>{renderAsHtml(footer.footer_hours)}</div>
              <div>{renderAsHtml(footer.footer_location)}</div>
              <div>{renderAsHtml(footer.footer_contact)}</div>
            </div>
            <hr className={`${styles.divider} mt-3 mb-3`} />
            <div className="col-lg-2 p-0">
              <div className={`${styles.footerSocial} mt-4`}>
                {socialLinks.map((chunk, i) => {
                  return (
                    <div key={Math.random(0, 9999999)}>
                      {chunk.map((link, k) => {
                        return (
                          <React.Fragment key={Math.random(0, 99999)}>
                            <div className="media mb-2">
                              <a
                                href={renderLinkUrl(link.footer_social_link)}
                                target="_blank"
                              >
                                <img
                                  src={link.footer_social_link_icon.url}
                                  className="align-self-center mr-2"
                                  alt={link.footer_social_link_icon.alt}
                                />
                              </a>
                              <div className="media-body">
                                <a
                                  href={renderLinkUrl(link.footer_social_link)}
                                  target="_blank"
                                >
                                  {renderAsText(link.footer_social_link_text)}
                                </a>
                              </div>
                            </div>
                          </React.Fragment>
                        )
                      })}
                    </div>
                  )
                })}
              </div>
              <div className={styles.copyrightWrapperMobile}>
                <p className={styles.footerCopyright}>
                  &copy; {new Date().getFullYear()} Kansas City Zoo. All rights
                  reserved.
                </p>
              </div>
            </div>
          </div>
        </div>
        {/* Audacy GT script  */}
        <script
          src="https://js.adsrvr.org/up_loader.1.1.0.js"
          type="text/javascript"
        ></script>
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `
							ttd_dom_ready( function() {
									if (typeof TTDUniversalPixelApi === 'function') {
											var universalPixelApi = new TTDUniversalPixelApi();
											universalPixelApi.init("r81t8jx", ["dfk3a84"], "https://insight.adsrvr.org/track/up");
									}
							});
						`,
          }}
        />
      </footer>
    </>
  )
}
