import { StaticQuery, graphql } from "gatsby"
import { Link } from "gatsby"
import { sortBy } from "lodash"
import {
  renderAsText,
  generateActivitiesByDate,
  linkResolver,
} from "../../utility.js"
import moment from "moment"
import PropTypes from "prop-types"
import React from "react"
import Icon from "../icon/icon.js"
import Button from "../button/button-link.js"
import { withPreview } from "gatsby-source-prismic-graphql"

import styles from "./index.module.css"

const activityQuery = graphql`
  query ActivityTableQuery($cursor: String) {
    prismic {
      allActivitys(after: $cursor) {
        edges {
          node {
            activity_parent_page {
              _linkType
              ... on PRISMIC_Detail {
                _linkType
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            title
            activity_times_are_active
            activity_times {
              duration
              duration_time_units
              frequency
              location
              start_am_pm
              start_hour
              start_minute
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
          hasPreviousPage
          startCursor
        }
        totalCount
      }
    }
  }
`

const ActivityTable = ({ data }) => {
  //console.log('Activities -->', data);
  if (!data) return null

  let activityTimes = generateActivitiesByDate(data, moment())
  console.log("activityTimes", activityTimes)

  // REMOVE TIMES EARLIER IN THE DAY
  let now = moment()
  let activityTimesFiltered = activityTimes
    .filter(d => d.date.isSameOrAfter(now))
    .slice(0, 3)

  return (
    <>
      <h6 className={styles.dropdownMenuHeading}>
        Activities {moment().format("MMM D")}{" "}
        <Button
          text="See All"
          url="/daily-schedule"
          size="small"
          isInverse={true}
          color="Zoo Green"
        />
      </h6>
      <table className={`table table-sm ${styles.activitiesTable}`}>
        <tbody>
          {activityTimesFiltered.length === 0 && (
            <tr key={0}>
              <td colSpan="2">
                No more activities scheduled today. Check back tomorrow!
              </td>
            </tr>
          )}
          {activityTimesFiltered.length > 0 &&
            activityTimesFiltered.map((d, i) => (
              <tr key={i}>
                <td>{d.date.format("h:mm A")}</td>
                {d.link && (
                  <td>
                    <Link to={linkResolver(d.link._meta)}>
                      {renderAsText(d.title)}
                    </Link>
                  </td>
                )}
              </tr>
            ))}
        </tbody>
      </table>
    </>
  )
}

/*
const ActivityTableComponent = () => {
	return(
	<StaticQuery 
		query={activityQuery} 
		render={withPreview(data => ( <ActivityTable data={data} /> ) , activityQuery)}
		/>
	)
}
*/

/*
const Test = (props) => {
	console.log('****** TEST -->', props);
	let data = [];
	function load(cursor) {
		props.prismic.load({
			variables: { cursor: cursor },
			query: activityQuery
		})
		.then(res => { 
			console.log('****** LOADED', res.data); 
			if (res.data.allActivitys.pageInfo.hasNextPage) {
				load(res.data.allActivitys.pageInfo.endCursor);
			}

		});	
	}
	load();
	return null;
}

export default Test;
*/

class ActivityTableBootstrap extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      totalCount: -1,
    }
  }
  load(cursor) {
    //console.log('prismic???', this.props.prismic, this.props)
    if (this.props.prismic) {
      this.props.prismic
        .load({
          variables: { cursor: cursor },
          query: activityQuery, // (optional)
          fragments: [], // (optional)
        })
        .then(res => {
          //console.log('>>>>>> LOADED', res.data);
          this.setState({
            data: this.state.data.concat(res.data.allActivitys.edges),
            totalCount: res.data.allActivitys.totalCount,
          })
          if (res.data.allActivitys.pageInfo.hasNextPage) {
            this.load(res.data.allActivitys.pageInfo.endCursor)
          }
        })
    }
  }
  componentDidMount() {
    this.load(null)
  }
  render() {
    return <ActivityTable data={this.state.data} />
  }
}
export default ActivityTableBootstrap
