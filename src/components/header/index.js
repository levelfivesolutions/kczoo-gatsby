import React from "react"
import { StaticQuery, graphql } from "gatsby"
import { withPreview } from "gatsby-source-prismic-graphql"
import { Link, navigate } from "gatsby"
import { renderImageUrl } from "../../utility.js"
import PropTypes from "prop-types"
import Icon from "../icon/icon.js"
import Button from "../button/button-link.js"
import ActivityTable from "./activityTable.js"
import EventsPanel from "./eventsPanel.js"
import splashIcon from "../../images/splash-icon.png"

import styles from "./index.module.css"

const siteStructureQuery = graphql`
  query {
    prismic {
      allSite_structures {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            navAnimalsImage
            navCamps_and_toursImage
            navRent_the_zooImage
          }
        }
      }
    }
  }
`

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchDisplay: false,
      searchValue: "",
      open: false,
    }

    this.searchFormRef = React.createRef()
    this.searchInputRef = React.createRef()

    this.handleChange = this.handleChange.bind(this)
    this.handleKeyDown = this.handleKeyDown.bind(this)
    this.handleClickSearchShow = this.handleClickSearchShow.bind(this)
    this.handleClickSearchHide = this.handleClickSearchHide.bind(this)
  }
  componentDidMount() {
    window.$("#navbarSupportedContent").on("show.bs.collapse", () => {
      this.setState({ open: true })
    })
    window.$("#navbarSupportedContent").on("hidden.bs.collapse", () => {
      this.setState({ open: false })
    })
  }
  componentDidUpdate() {
    if (this.searchInputRef.current) {
      this.searchInputRef.current.focus()
    }
  }
  handleChange(ev) {
    this.setState({ searchValue: ev.target.value })
  }
  handleKeyDown(ev) {
    console.log("handleKeyDown", ev)
    if (ev.key === "Enter") {
      navigate("/search/", {
        state: { searchValue: this.state.searchValue },
      })
    }
  }
  handleClickSearchShow(ev) {
    ev.preventDefault()
    this.setState({ searchDisplay: true })
    document.body.addEventListener("click", this.handleClickSearchHide)
  }
  handleClickSearchHide(ev) {
    if (
      this.searchFormRef.current &&
      !this.searchFormRef.current.contains(ev.target)
    ) {
      this.setState({ searchDisplay: false })
      document.body.removeEventListener("click", this.handleClickSearchHide)
    }
  }
  render() {
    console.log("HEADER PROPS", this.props)
    let { siteTitle, section, data } = this.props
    const doc = data.prismic.allSite_structures.edges.slice(0, 1).pop()
    let animalsImageStyle,
      campsImageStyle,
      rentImageStyle = {}
    if (doc) {
      animalsImageStyle = {
        backgroundImage: `url( ${renderImageUrl(doc.node.navAnimalsImage)} )`,
      }
      campsImageStyle = {
        backgroundImage: `url( ${renderImageUrl(
          doc.node.navCamps_and_toursImage
        )} )`,
      }
      rentImageStyle = {
        backgroundImage: `url( ${renderImageUrl(
          doc.node.navRent_the_zooImage
        )} )`,
      }
    }
    let { searchDisplay } = this.state

    const isSectionActive = (currentSection, classes) => {
      if (section === currentSection) {
        return `${classes} active`
      }
      return classes
    }

    let navClasses = `${styles.kczooNavbar} kczoo-navbar navbar navbar-expand-xl`
    if (this.state.open) {
      navClasses += ` ${styles.kczooNavbarOpen}`
    }

    if (typeof window !== "undefined") {
      document.addEventListener(
        "click",
        function() {
          setTimeout(() => {
            let targetClass = document.getElementsByClassName("dropdown show")
            let targetText = document.getElementById("aquariumBanner")
            let targetImage = document.getElementById("bannerImage")
            let targetCaret = document.getElementById("bannerCaret")
            const bannerStylesDimText = {
              transitionDuration: "0.2s",
              color: "rgba(255, 255, 255, 0.5)",
            }
            const bannerStylesDimImg = {
              transitionDuration: "0.2s",
              opacity: "0.5",
            }
            const bannerStyles = {
              transitionDuration: "0.2s",
              color: "rgba(255, 255, 255, 1.0)",
              opacity: "1.0",
            }

            // So it does not freak out if the aquarium bar is not there
            if (targetText && targetImage && targetCaret) {
              if (targetClass.length > 0) {
                Object.assign(targetText.style, bannerStylesDimText)
                Object.assign(targetImage.style, bannerStylesDimImg)
                Object.assign(targetCaret.style, bannerStylesDimImg)
              }
              if (targetClass.length < 1) {
                Object.assign(targetText.style, bannerStyles)
                Object.assign(targetImage.style, bannerStyles)
                Object.assign(targetCaret.style, bannerStyles)
              }
            }
          })
        },
        100
      )
    }

    return (
      <>
        <nav className={navClasses}>
          <div>
            <Link
              className={`${styles.kczooBrand} navbar-brand`}
              style={{ position: "relative", zIndex: 101 }}
              to="/"
            >
              <img
                src="/images/logo-kczoo@2x.png"
                alt="Kansas City Zoo"
                width="144"
                className={styles.kczooLogo}
              />
            </Link>
            {/* <Link
                to="/aquarium"
                className={`${styles.kczooBrand} navbar-brand ${styles.aquariumCard}`}
              >
                New Aquarium in 2023!
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="6.413"
                  height="10.828"
                  viewBox="0 0 6.413 10.828"
                >
                  <path
                    id="Path_9757"
                    data-name="Path 9757"
                    d="M12687.83,8954.8l4,4-4,4"
                    transform="translate(-12686.416 -8953.387)"
                    fill="none"
                    stroke="#fff"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                  />
                </svg>
              </Link> */}
          </div>

          <button
            className={`${styles.navbarToggler} navbar-toggler collapsed`}
            type="button"
            id="myCollapsible"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className={`${styles.togglerIcon}`}></span>
          </button>

          <div
            className={`${styles.navbarCollapse} collapse navbar-collapse`}
            id="navbarSupportedContent"
          >
            <div
              className={`${styles.btnGroup} btn-group`}
              role="group"
              aria-label=""
            >
              <Link to="/tickets-and-pricing" className={`${styles.btn} btn`}>
                GET TICKETS
              </Link>
              <Link to="/daily-schedule" className={`${styles.btn} btn`}>
                DAILY SCHEDULE
              </Link>
              <Link to="/zoo-map" className={`${styles.btn} btn`}>
                VIEW MAP
              </Link>
            </div>

            <div className={`${styles.navbarCollapseSearch} form`}>
              <div
                className={`${styles.navbarCollapseSearchInputGroup} input-group`}
              >
                <input
                  type="text"
                  className={`${styles.searchInputCollapse} form-control`}
                  placeholder="Search"
                  ref={this.searchInputRef}
                  value={this.state.value}
                  onChange={this.handleChange}
                  onKeyDown={this.handleKeyDown}
                />
                <div className="input-group-append">
                  <span className={`${styles.searchIcon} input-group-text`}>
                    <Icon
                      icon="search"
                      width="21"
                      height="21"
                      color="ZOO GREEN"
                      alt="Search"
                    />
                  </span>
                </div>
              </div>
            </div>

            <ul className={`${styles.navPrimary} navbar-nav mr-auto`}>
              <li
                className={isSectionActive(
                  "visit",
                  `${styles.navPrimaryItem} nav-item dropdown`
                )}
              >
                <a
                  className={`${styles.navLink} ${styles.navLinkPrimary} dropdown-toggle`}
                  href="#"
                  id="menuVisit"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Visit
                </a>
                <div
                  className={`${styles.dropdownMenu} ${styles.dropdownMenuPrimary} dropdown-menu`}
                  aria-labelledby="menuVisit"
                >
                  <div className={styles.dropdownMenuBody}>
                    <div
                      className={`${styles.dropdownMenuAside} ${styles.dropdownMenuAsideVisit} ${styles.dropdownMenuAsideAlignBottom}`}
                    >
                      <div className={styles.dropdownMenuAsideInner}>
                        <div className="mb-4">
                          <Icon icon="map" color="WHITE" width="85" />
                        </div>
                        <Button text="Zoo Map" url="/zoo-map" size="small" />
                      </div>
                    </div>
                    <div className={styles.dropdownMenuContentTight}>
                      <div className="row no-gutters">
                        <div className="col-xl-6">
                          <Link
                            to="/tickets-and-pricing"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Tickets &amp; Pricing
                          </Link>
                          <Link
                            to="/hours-location-parking"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Hours, Location &amp; Parking
                          </Link>
                          <Link
                            to="/visit/zoo-policies"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Zoo Policies
                          </Link>
                        </div>
                        <div className="col-xl-6">
                          <Link
                            to="/visit/guest-services"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Guest Services
                          </Link>
                          <Link
                            to="/visit/dining"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Dining
                          </Link>
                          <Link
                            to="/rides-and-transportation"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Rides &amp; Transportation
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li
                className={isSectionActive(
                  "animals",
                  `${styles.navPrimaryItem} nav-item dropdown`
                )}
              >
                <a
                  className={`${styles.navLink} ${styles.navLinkPrimary} dropdown-toggle`}
                  href="#"
                  id="menuAnimals"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Animals
                </a>
                <div
                  className={`${styles.dropdownMenu} ${styles.dropdownMenuPrimary} dropdown-menu`}
                  aria-labelledby="menuAnimals"
                >
                  <div className={styles.dropdownMenuBody}>
                    <div
                      className={`${styles.dropdownMenuAside} ${styles.dropdownMenuAsideAnimals} ${styles.dropdownMenuAsideAlignBottom}`}
                      style={animalsImageStyle}
                    >
                      <Button
                        text="Animal Cams"
                        url="/animal-cams"
                        size="small"
                      />
                    </div>
                    <div className={styles.dropdownMenuContentTight}>
                      <div className="row no-gutters">
                        <div className="col-xl-6">
                          <Link
                            to="/animals"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Our Animals
                          </Link>
                          <Link
                            to="/featured-exhibits"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Featured Exhibits
                          </Link>
                        </div>
                        <div className="col-xl-6">
                          <Link
                            to="/animal-cams"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Animal Cameras
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li
                className={isSectionActive(
                  "activities",
                  `${styles.navPrimaryItem} nav-item dropdown`
                )}
              >
                <a
                  className={` ${styles.navLink} ${styles.navLinkPrimary} dropdown-toggle`}
                  href="#"
                  id="menuActivitiesEvents"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Activities &amp; Events
                </a>
                <div
                  className={`${styles.dropdownMenu} ${styles.dropdownMenuPrimary} dropdown-menu`}
                  aria-labelledby="menuActivitiesEvents"
                >
                  <div className={styles.dropdownMenuBody}>
                    <div
                      className={`${styles.dropdownMenuAside} ${styles.dropdownMenuAsideAlignBottom}`}
                    >
                      <div>
                        <EventsPanel {...this.props} />
                      </div>
                    </div>
                    <div className={styles.dropdownMenuContent}>
                      <ActivityTable {...this.props} />
                    </div>
                  </div>
                </div>
              </li>
              <li
                className={isSectionActive(
                  "camps",
                  `${styles.navPrimaryItem} nav-item dropdown`
                )}
              >
                <a
                  className={` ${styles.navLink} ${styles.navLinkPrimary} dropdown-toggle`}
                  href="#"
                  id="menuCampsTours"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Camps &amp; Tours
                </a>
                <div
                  className={`${styles.dropdownMenu} ${styles.dropdownMenuPrimary} dropdown-menu`}
                  aria-labelledby="menuCampsTours"
                >
                  <div className={styles.dropdownMenuBody}>
                    <div
                      className={`${styles.dropdownMenuAside} ${styles.dropdownMenuAsideCamps}`}
                      style={campsImageStyle}
                    ></div>
                    <div className={styles.dropdownMenuContentTight}>
                      <div className="row no-gutters">
                        <div className="col-xl-6">
                          <Link
                            to="/day-camps"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Day Camps
                          </Link>
                          <Link
                            to="/overnights"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Overnights
                          </Link>
                        </div>
                        <div className="col-xl-6">
                          {/* <Link to="/guided-tours" className={ `${ styles.dropdownItem } dropdown-item` }>Guided Tours</Link> */}
                          <Link
                            to="/scouts"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Scouts
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li
                className={isSectionActive(
                  "rent",
                  `${styles.navPrimaryItem} nav-item dropdown`
                )}
              >
                <a
                  className={` ${styles.navLink} ${styles.navLinkPrimary} dropdown-toggle`}
                  href="#"
                  id="menuRent"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Rent the Zoo
                </a>
                <div
                  className={`${styles.dropdownMenu} ${styles.dropdownMenuPrimary} dropdown-menu dropdown-menu-right`}
                  aria-labelledby="menuRent"
                >
                  <div className={styles.dropdownMenuBody}>
                    <div
                      className={`${styles.dropdownMenuAside} ${styles.dropdownMenuAsideRent}`}
                      style={rentImageStyle}
                    ></div>
                    <div className={styles.dropdownMenuContentTight}>
                      <div className="row">
                        <div className="col-xl-6">
                          <Link
                            to="/birthdays"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Birthdays
                          </Link>
                          <Link
                            to="/weddings"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Weddings
                          </Link>
                          <Link
                            to="/picnics"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Picnics
                          </Link>
                        </div>
                        <div className="col-xl-6">
                          <Link
                            to="/exotic-evenings"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Exotic Evenings
                          </Link>
                          <Link
                            to="/corporate-meetings"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Corporate Meetings
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li
                className={isSectionActive(
                  "donate",
                  `${styles.navPrimaryItem} nav-item dropdown`
                )}
              >
                <a
                  className={`${styles.navLink} ${styles.navLinkPrimary} dropdown-toggle`}
                  href="#"
                  id="menuSupportUs"
                  role="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Support Us
                </a>
                <div
                  className={`${styles.dropdownMenu} ${styles.dropdownMenuPrimary} dropdown-menu dropdown-menu-right`}
                  aria-labelledby="menuSupportUs"
                >
                  <div className={styles.dropdownMenuBody}>
                    <div
                      className={`${styles.dropdownMenuAside} ${styles.dropdownMenuAsideVisit} ${styles.dropdownMenuAsideAlignBottom}`}
                    >
                      <div className={styles.dropdownMenuAsideInner}>
                        <div className="mb-4">
                          <Icon icon="gift" color="WHITE" width="85" />
                        </div>
                        <Button text="Donate Now" url="/donate" size="small" />
                      </div>
                    </div>
                    <div className={styles.dropdownMenuContentTight}>
                      <div className="row no-gutters">
                        <div className="col-xl-6">
                          {/* Left Side */}
                          <Link
                            to="/form/donate-to-the-zoo-form"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            General Donations
                          </Link>
                          <Link
                            to="/support-us/adopt-a-wild-child"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Adopt a Wild Child
                          </Link>
                          <Link
                            to="/donate/giving-plaques"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Plaques
                          </Link>
                          <Link
                            to="/support-us/planned-giving-and-stock"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            <span style={{ whiteSpace: "nowrap" }}>
                              Planned Giving &amp; Stock
                            </span>
                          </Link>
                          <Link
                            to="/support-us/memorials-and-tributes"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            <span style={{ whiteSpace: "nowrap" }}>
                              Memorials &amp; Tributes
                            </span>
                          </Link>
                        </div>
                        <div className="col-xl-6">
                          {/* Right Side */}
                          <Link
                            to="/volunteers"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Volunteer
                          </Link>
                          <Link
                            to="/support-us/corporate-sponsors"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Corporate Sponsors
                          </Link>
                          <Link
                            to="/form/kansas-city-zoo-license-plate"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            License Plates
                          </Link>
                          <Link
                            to="/support-us/amazon-smile"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Amazon Smile
                          </Link>
                          <Link
                            to="jazzoo"
                            className={`${styles.dropdownItem} dropdown-item`}
                          >
                            Jazzoo
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
            <div className={`${styles.navbarRight}`}>
              {searchDisplay && (
                <div
                  className={`${styles.searchForm} form`}
                  ref={this.searchFormRef}
                >
                  <div className={`${styles.searchInputGroup} input-group`}>
                    <div className="input-group-prepend">
                      <span className={`${styles.searchIcon} input-group-text`}>
                        <Icon
                          icon="search"
                          width="21"
                          height="21"
                          color="ZOO GREEN"
                          alt="Search"
                        />
                      </span>
                    </div>
                    <input
                      type="text"
                      className={`${styles.searchInput} form-control`}
                      placeholder="Search"
                      ref={this.searchInputRef}
                      value={this.state.value}
                      onChange={this.handleChange}
                      onKeyDown={this.handleKeyDown}
                    />
                  </div>
                </div>
              )}
              {!searchDisplay && (
                <ul className={`${styles.navUtility} navbar-nav ml-auto`}>
                  <li className={`${styles.navUtilityItem} nav-item`}>
                    <Link
                      to="/memberships"
                      className={`${styles.navLink} ${styles.navLinkUtility} ${styles.navLinkButton}`}
                    >
                      Membership
                    </Link>
                  </li>
                  <li className={`${styles.navUtilityItem} nav-item`}>
                    <a
                      className={`${styles.navLink} ${styles.navLinkUtility}`}
                      onClick={this.handleClickSearchShow}
                      href="#"
                      title="Search"
                    >
                      <Icon
                        icon="search"
                        width="21"
                        height="21"
                        color="white"
                        alt="Search"
                      />
                    </a>
                  </li>
                </ul>
              )}
              <ul className={`${styles.navSecondary} navbar-nav`}>
                <li
                  className={isSectionActive(
                    "conservation",
                    `${styles.navSecondaryItem} nav-item dropdown`
                  )}
                >
                  <a
                    className={`${styles.navLink} ${styles.navLinkSecondary} dropdown-toggle`}
                    href="#"
                    id="menuConservation"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    CONSERVATION
                  </a>
                  <div
                    className={`${styles.dropdownMenu} dropdown-menu dropdown-menu-right`}
                    aria-labelledby="menuConservation"
                  >
                    <div className={styles.dropdownMenuContentSec}>
                      <Link
                        to="/conservation/how-you-can-help"
                        className={`${styles.dropdownItem} dropdown-item`}
                      >
                        How You Can Help
                      </Link>
                      <Link
                        to="/conservation-projects/field-research-projects"
                        className={`${styles.dropdownItem} ${styles.dropdownItemSec} dropdown-item`}
                      >
                        Field and Research Projects
                      </Link>
                      <Link
                        to="/conservation-projects/green-initiatives"
                        className={`${styles.dropdownItem} ${styles.dropdownItemSec} dropdown-item`}
                      >
                        Green Initiatives
                      </Link>
                      <Link
                        to="/conservation-projects/partnerships"
                        className={`${styles.dropdownItem} ${styles.dropdownItemSec} dropdown-item`}
                      >
                        Partnerships
                      </Link>
                    </div>
                  </div>
                </li>
                <li
                  className={isSectionActive(
                    "education",
                    `${styles.navSecondaryItem} nav-item dropdown`
                  )}
                >
                  <a
                    className={`${styles.navLink} ${styles.navLinkSecondary} dropdown-toggle`}
                    href="#"
                    id="menuEducation"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    EDUCATION
                  </a>
                  <div
                    className={`${styles.dropdownMenu} dropdown-menu dropdown-menu-right`}
                    aria-labelledby="menuEducation"
                  >
                    <div className={styles.dropdownMenuContentSec}>
                      <Link
                        to="/education/field-trips"
                        className={`${styles.dropdownItem} ${styles.dropdownItemSec} dropdown-item`}
                      >
                        Field Trips
                      </Link>
                      <Link
                        to="/zoomobile"
                        className={`${styles.dropdownItem} ${styles.dropdownItemSec} dropdown-item`}
                      >
                        Zoomobile
                      </Link>
                      <Link
                        to="/distance-learning"
                        className={`${styles.dropdownItem} ${styles.dropdownItemSec} dropdown-item`}
                      >
                        Distance Learning
                      </Link>
                      <Link
                        to="/zooed"
                        className={`${styles.dropdownItem} ${styles.dropdownItemSec} dropdown-item`}
                      >
                        ZooED
                      </Link>
                      <Link
                        to="/homeschool"
                        className={`${styles.dropdownItem} ${styles.dropdownItemSec} dropdown-item`}
                      >
                        Homeschool
                      </Link>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div className={styles.kczooNavbarFooter}>
              <div className="container">
                <div className="row">
                  <div className="col-10 offset-1">
                    <p className={styles.footerText}>
                      Friends of the Zoo members get discounts and free
                      admission!
                    </p>
                  </div>
                </div>
              </div>
              <Button text="Become a Member" url="/memberships" />
            </div>
          </div>
        </nav>
        {!this.props?.location?.href?.includes("zoo-map") && (
          <Link
            to="/aquarium"
            className={styles.aquariumBar}
            id="aquariumBanner"
          >
            <img
              id="bannerImage"
              src={splashIcon}
              alt="Splash Icon"
              width="19"
              height="24;"
            />
            <span>New Aquarium opening in 2023! </span>
            <span>Are you ready for the big splash? </span>
            <span>Explore Today </span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="6.413"
              height="10.828"
              viewBox="0 0 6.413 10.828"
              id="bannerCaret"
            >
              <path
                id="Path_9757"
                data-name="Path 9757"
                d="M12687.83,8954.8l4,4-4,4"
                transform="translate(-12686.416 -8953.387)"
                fill="none"
                stroke="#fff"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
              />
            </svg>
          </Link>
        )}
      </>
    )
  }
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

const HeaderStaticQuery = props => {
  return (
    <StaticQuery
      query={siteStructureQuery}
      render={withPreview(
        data => (
          <Header data={data} {...props} />
        ),
        siteStructureQuery
      )}
    />
  )
}

export default HeaderStaticQuery
