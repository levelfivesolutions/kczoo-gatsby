import { StaticQuery, graphql } from "gatsby";
import { Link } from "gatsby";
import { sortBy } from 'lodash';
import { renderAsText, linkResolver } from '../../utility.js';
import moment from 'moment';
import PropTypes from "prop-types";
import React from "react";
import Icon from "../icon/icon.js";
import Button from '../button/button-link.js';
import { withPreview } from 'gatsby-source-prismic-graphql';

import styles from "./index.module.css";

const eventsQuery = graphql`
	query EventsPanelQuery($cursor: String) {
		prismic {
			allSpecial_events(after: $cursor) {
				edges {
					node {
						_linkType
						_meta {
							id
							tags
							type
							uid
						}
						title
						activity_time {
							start_time
						}
						spot_images
					}
				}
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
				totalCount
			}
		}
	}
`

const EventsPanel = ({data}) => {
	//console.log('*** Events -->', data);
	if (data.length === 0) {
		return null;
	} 
	
	let events = data;
	//console.log('events', events);
	
	// BUILD EVENT TIMES
	let eventTimes = [];
	let today = moment();
	events.forEach(event => {
		if (event.node.activity_time) {
			event.node.activity_time.forEach(time => {
				if (time.start_time && moment(time.start_time).isSameOrAfter(today)) {
					eventTimes.push(
						Object.assign({}, time, 
							{ 
								_linkType: event.node._linkType,
								_meta: event.node._meta,
								title: event.node.title, 
								image: event.node.spot_images,
							}
						)
					);					
				}
			});
		}
	});
	//console.log('eventTimes', eventTimes);
	
	// SORT BY TIME
	let eventsByTime = sortBy(eventTimes, [(d) => d.start_time ]);
	//console.log('eventsByTime', eventsByTime);
	
	let event = eventsByTime[0];
	console.log('EVENT PANEL', event);
	
	if(!event) return null;

	return (
		<>
			<div className={`${ styles.eventImageWrapper }`}>
				<Link to={ linkResolver( event._meta ) }>
					<img src={ event.image ? event.image.url : '' } alt="" className={`${ styles.eventImage } img-fluid`} />
				</Link>
			</div>
			{ event.start_time && <p>{ moment(event.start_time).format('MMMM D') }</p> }
			<Button text="More Events" url="/events" size="small" />
		</>
	);
}

class EventsPanelBootstrap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			data: [],
			totalCount: -1,
		};
  }
	load(cursor) {
		//console.log('EVENTS prismic???', this.props.prismic);
		if (this.props.prismic) {
			this.props.prismic.load({
				variables: { cursor: cursor },
				query: eventsQuery, // (optional)
				fragments: [], // (optional)
			})
			.then(res => { 
				//console.log('EVENTS NAV DATA LOADED', res.data);
				this.setState({
					data: this.state.data.concat(res.data.allSpecial_events.edges),
					totalCount: res.data.allSpecial_events.totalCount
				});
				if (res.data.allSpecial_events.pageInfo.hasNextPage) {
					this.load(res.data.allSpecial_events.pageInfo.endCursor);
				}
			});		
		}
	}
  componentDidMount() {
		this.load(null);
	}	
	render() {
		return <EventsPanel data={ this.state.data } />;
	}
}
export default EventsPanelBootstrap;
