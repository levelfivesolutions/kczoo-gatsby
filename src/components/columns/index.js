import React from 'react';
import CampSeasonal from '../camp-seasonal/camp-seasonal.js';
import SectionBlock from '../section-block';
import { filterArrayByType } from '../../utility.js';

import styles from './index.module.css';

export default ({ title, description, body, sliceData, inset = true, classList, ...args }) => {
  let data = sliceData || filterArrayByType(body, "donation_columns")[0];
  console.log('donation_columns', data);

  if (!data || data.length === 0) {
    return null;
  }

  const sectionTitle = data.primary ? data.primary.column_section_title : null;

  return (
    <SectionBlock {...args} classList={classList}>
      <div className="container">
        {sectionTitle && <h2 className={styles.title}>{sectionTitle}</h2>}
        <div className="row">
          <div className={inset ? "col-md-10 offset-md-1" : "col-md-12"}>
            <div className="row v-20-s">
              {
                data.fields.map((d, i) => {
                  console.log('column', `${i}.`, d);
                  return (
                    <div className="col-md-4" key={i}>
                      <CampSeasonal
                        color={d.color}
                        image={d.column_image}
                        title={d.column_title}
                        description={d.column_text}
                        btnText={d.cta_label || d.column_cta_label}
                        link={d.cta_link || d.column_cta}
                      />
                    </div>
                  );
                })
              }
            </div>
          </div>
        </div>
      </div>
    </SectionBlock>
  );
}
