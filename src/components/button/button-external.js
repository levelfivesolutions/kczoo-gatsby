import React from 'react';
import { ColorToHex } from '../../utility.js';
import Icon from '../icon/icon.js';
import styles from './button.module.css';

export default ({text, url = '', color = 'Tangerine', icon, iconWidth=10, isInverse = false, onClick}) => {
	let style;
	
	if (isInverse) {
		style = {
			color: ColorToHex(color),
			backgroundColor: 'white'
		};
	} else {
		style = {
			color: 'white',
			backgroundColor: ColorToHex(color)
		};
	}
	
	let iconEl = null;
	if (icon) {
		iconEl = <Icon icon={ icon } width={ iconWidth } height="15" className="ml-2" />;
	}
	
	function handleClick(ev) {
		if (onClick) {
			ev.preventDefault();
			onClick();
		}
	}
	
  return (
    <a href={ url } className={ styles.button } style={ style } onClick={ handleClick } target="_blank">
    	<div className={ styles.buttonInner }>{ text } { iconEl }</div>
    </a>
  );
}
