import React from 'react'
import { ColorToHex } from '../../utility.js';
import styles from './button.module.css'

export default ({ children, type = 'button', size, color = 'Tangerine', isInverse = false, onClick }) => {
  // COLOR
  let style;
  if (isInverse) {
    style = {
      color: ColorToHex(color),
      backgroundColor: 'white'
    };
  } else {
    style = {
      color: 'white',
      backgroundColor: ColorToHex(color)
    };
  }

  let classes;
  switch (size) {
    case 'small':
      classes = `${styles.button} ${styles.buttonSmall}`;
      break;
    case 'large':
      classes = `${styles.button} ${styles.buttonLarge}`;
      break;
    case 'medium':
      classes = `${styles.button} ${styles.buttonMedium}`;
      break;
    default:
      classes = `${styles.button}`;
  }

  return (
    <button type={type} className={classes} style={style} onClick={onClick}>
      {children}
    </button>
  );
}
