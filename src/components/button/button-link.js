import React from 'react';
import { Link } from "gatsby";
import { ColorToHex, linkResolver } from '../../utility.js';
import Icon from '../icon/icon.js';
import styles from './button.module.css';

export default ({text, size, url = '', link, color = 'Tangerine', icon, isInverse = false, onClick, onClick2 }) => {
	
	// COLOR
	let style;
	if (isInverse) {
		style = {
			color: ColorToHex(color),
			backgroundColor: 'white'
		};
	} else {
		style = {
			color: 'white',
			backgroundColor: ColorToHex(color)
		};
	}
	
	// ICON
	let iconEl = null;
	if (icon) {
		iconEl = <Icon icon={ icon } height="18" color={ isInverse ? color : 'white' } className="ml-2" />;
	}
	
	function handleClick(ev) {
		if (onClick) {
			ev.preventDefault();
			onClick();
		}
	}
	
	let classes;
	switch(size) {
		case 'small': 
			classes = `${ styles.button } ${ styles.buttonSmall }`;
			break;
		case 'large': 
			classes = `${ styles.button } ${ styles.buttonLarge }`;
			break;
		case 'medium': 
			classes = `${ styles.button } ${ styles.buttonMedium }`;
			break;
		case 'flex': 
			classes = `${ styles.button } ${ styles.buttonFlex }`;
			break;
		default:
			classes = `${ styles.button }`;
	}
	let buttonInner = <span className={ styles.buttonInner }>{ text } { iconEl }</span>;

	console.log("linkTest", link, typeof link)
	
	if (link && typeof link === 'object') {
		console.log('LINK', text, link);
		if (link._linkType) {
			if (link._linkType === 'Link.web' || link._linkType === 'Link.file') {
				return (<a href={ link.url } className={ classes } style={ style }>{ buttonInner }</a>);
			} else if (link._linkType === 'Link.document') {
				return (<a href={ linkResolver(link._meta) } className={ classes } style={ style } onClick={ ev => onClick2 ? onClick2(ev) : null }>{ buttonInner }</a>);		 
			} else {
				return <button type="button">{ text }</button>;
			}
		} else {
			return <button type="button">{ text }</button>;
		}
	} else if (link && typeof link === 'string') {
		if (url.match(/^http/)) {
			return (<a href={ link } className={ classes } style={ style } target="_blank">{ buttonInner }</a>);
		} else {
			return (<Link to={ link } className={ classes } style={ style }>{ buttonInner }</Link>);
		}
	} else if (typeof onClick === 'function') {
		return (<button className={ classes } style={ style } onClick={ handleClick }>{ buttonInner }</button>);
	} else if (typeof url === 'string') {
		console.log('URL', text, url);
		if (url.match(/^http/)) {
			return (<a href={ url } className={ classes } style={ style } target="_blank">{ buttonInner }</a>);
		} else {
			return (<Link to={ url } className={ classes } style={ style }>{ buttonInner }</Link>);
		}
	} else return <button type="button" className={ classes } style={ style }>{ buttonInner }</button>;

}
