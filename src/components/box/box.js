import React from 'react';
import { ColorToHex } from '../../utility.js';
import styles from './box.module.css';

export default ({ children, color, className }) => {
	let style = {};
	if (color) {
		style = { backgroundColor: ColorToHex(color) };
	}
	return (
		<div className={ `${ styles.box } ${ className }` } style={ style }>
			{ children }
		</div>
	);
}
