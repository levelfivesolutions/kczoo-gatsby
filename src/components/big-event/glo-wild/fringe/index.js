import React, { useEffect, useState } from 'react';
import { renderImageUrl } from '../../../../utility.js';

import styles from './index.module.css';

export default ({ children }) => {
	return (
		<div className={styles.fringe}>
			<div className={styles.left}></div>
			{children}
			<div className={styles.right}></div>
		</div>
	);
}
