import React from "react"
import styles from "../../../../pages/memberships-landing.module.css"

const DropdownSelect = props => {
  return (
    <div className={`${styles.giftDropdown} mb-5`}>
      <div className={styles.joinGiftContainer}>
        <div className={styles.joinBox}>
          <button
            className={`btn dropdown-toggle ${styles.giftDropdownToggle}`}
            style={{ backgroundColor: "#f77c00", color: "#fff" }}
            type="button"
            id="dropdownMenuButton"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Group Size
          </button>
          <div
            className={`${styles.giftDropdownMenu} dropdown-menu`}
            aria-labelledby="dropdownMenuButton"
          >
            <a
              className="dropdown-item"
              target="_blank"
              href="https://store.kansascityzoo.org/webstore/shop/viewitems.aspx?cg=GLOW&c=GLOWGROUP15"
              style={{ color: "#f77c00" }}
            >
              Group of 15-29 guests
            </a>
            <a
              className="dropdown-item"
              target="_blank"
              href="https://www.kansascityzoo.org/form/glowild-group-ticket-request"
              style={{ color: "#f77c00" }}
            >
              Group of 30+ guests
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default DropdownSelect
