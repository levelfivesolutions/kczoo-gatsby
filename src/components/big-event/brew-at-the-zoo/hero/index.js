import React from "react"
import { StaticQuery, graphql } from "gatsby"
import styles from "./index.module.css"
import Fringe from "../fringe"
import { renderImageUrl } from "../../../../utility.js"

export default function Hero({
  wide_image,
  pageId,
  hero_foreground,
  hero_foreground_only,
}) {
  let style, foregroundStyle

  if (hero_foreground) {
    let foregroundHero = renderImageUrl(hero_foreground)
    foregroundStyle = {
      backgroundImage: `url(${foregroundHero})`,
    }
  }

  if (!hero_foreground_only) {
    let imageUrl = renderImageUrl(wide_image)
    style = {
      backgroundImage: `url(${imageUrl})`,
    }
  }

  return (
    <div className={styles.hero} style={style}>
      <div
        className={styles.heroForegroundContainer}
        style={foregroundStyle}
      ></div>
    </div>
  )
}
