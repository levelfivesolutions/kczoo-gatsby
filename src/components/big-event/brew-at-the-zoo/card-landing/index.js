import React from "react"
import { Link } from "gatsby"
import moment from "moment"
import Box from "../../../box/box"
import Icon from "../../../icon/icon"
import Button from "../../../button/button-link"
import { RichText } from "prismic-reactjs"
import {
  ColorToHex,
  renderImageUrl,
  renderAsText,
  renderAsHtml,
} from "../../../../utility"
import styles from "./index.module.css"

class Card extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(data) {
    //this.setState({ xoxo: data });
  }

  render() {
    let {
      color,
      title,
      description,
      ageRange,
      minAge,
      maxAge,
      dates,
      startDate,
      endDate,
      season,
      year,
      image,
      cta_thumbnail,
      price,
      priceQualifier,
      priceDesc,
      urlText,
      link,
      relation,
      url,
    } = this.props
    let hexColor = ColorToHex(color)

    let list = null
    let listItems = []
    if (dates) {
      listItems.push(
        <React.Fragment>
          <span className={styles.campIconsIcon}>
            <Icon icon="calendar" width="22" />
          </span>{" "}
          {dates}
        </React.Fragment>
      )
    }
    if (endDate) {
      listItems.push(
        <React.Fragment>
          <span className={styles.campIconsIcon}>
            <Icon icon="calendar" width="22" />
          </span>{" "}
          {moment(startDate).format("MMM D")} -{" "}
          {moment(endDate).format("MMM D")}
        </React.Fragment>
      )
    }
    if (season) {
      listItems.push(
        <React.Fragment>
          <span className={styles.campIconsIcon}>
            <Icon icon="calendar" width="22" />
          </span>{" "}
          {season} {year}
        </React.Fragment>
      )
    }
    if (minAge) {
      listItems.push(
        <React.Fragment>
          <span className={styles.campIconsIcon}>
            <Icon icon="caricature" width="24" />
          </span>{" "}
          Age Groups: {minAge} - {maxAge}
        </React.Fragment>
      )
    }
    if (ageRange) {
      listItems.push(
        <React.Fragment>
          <span className={styles.campIconsIcon}>
            <Icon icon="caricature" width="24" />
          </span>{" "}
          Age Groups: {ageRange}
        </React.Fragment>
      )
    }

    if (listItems.length > 0) {
      list = (
        <ul className={styles.campIcons}>
          {listItems.map(el => (
            <li>{el}</li>
          ))}
        </ul>
      )
    }

    return (
      <div className={styles.camp}>
        <div className="row">
          <div className="col-md-5">
            <div className={styles.campImageWrapper}>
              <img
                src={renderImageUrl(image)}
                className={styles.campImage}
                alt=""
              />
            </div>
          </div>
          <div className="col-md-7">
            <div className={styles.campBody}>
              <h2
                className={styles.campTitle}
                style={{ backgroundColor: hexColor }}
              >
                {renderAsText(title)}
              </h2>
              <div className={styles.campDesc}>
                {list}
                <div className={styles.campDescText}>
                  {renderAsHtml(description)}
                </div>
              </div>
              {urlText && (
                <div>
                  <Box>
                    <div className="d-flex align-items-center justify-content-center justify-content-md-start flex-md-row flex-column">
                      {cta_thumbnail && (
                        <img
                          src={cta_thumbnail.url}
                          style={{ margin: "10px 25px" }}
                          height={70}
                          alt={cta_thumbnail.alt}
                        />
                      )}
                      <Button
                        text={urlText}
                        url={url}
                        relation={relation}
                        link={link}
                        color={color}
                        size="medium"
                      />
                    </div>
                  </Box>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Card
