import React from "react"
import { chunk } from "lodash"
import { renderImageUrl } from "../../../utility"
import SmartLink from "../../../components/smart-link"
import styles from "./index.module.scss"
import { Link } from "gatsby"

export default function Hero({ small_image, footer_navigation, social_media, pageId, _meta, footer_navigation_background_color,footer_navigation_text_color, footer_visible  }) {
    const columnData = chunk(footer_navigation, Math.ceil(footer_navigation.length / 2))

    if(!footer_visible) {
        return null;
    } 
        
    return (
        <footer className={styles.footer} style={{backgroundColor: footer_navigation_background_color}}>
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-md-3" >
                        <img src={renderImageUrl(small_image)} alt={small_image?.alt} className="img-fluid" />
                    </div>
                    <div className="col-md-1" style={{marginBottom: 30}}></div>
                    <div className={`col-md-3 ${styles.linkCol}`}>
                        <ul className={styles.linkList}>
                            {
                                columnData.length > 0 &&
                                columnData[0].map(d => {
                                    return <li> {
                                        d?.page_link?._linkType === 'Link.web' &&
                                        <a href={d.page_link.url} target={d.page_link.target}>
                                          {d.page_title}
                                        </a>
                                      }
                                      {
                                        d?.page_link?._linkType === 'Link.document' &&
                                        <Link
                                        style={footer_navigation_text_color !== '#000000' && {color: footer_navigation_text_color}}
                                          to={d.page_link ? `${_meta?.uid}/${d.page_link._meta.uid !== 'overview' ? d.page_link._meta.uid : ""}` : null}
                                        >
                                          {d.page_title}
                                        </Link>
                                      }</li>
                                })
                            }
                        </ul>
                    </div>
                    <div className={`col-md-2 ${styles.linkCol}`}>
                        <ul className={styles.linkList}>
                            {
                                columnData.length > 1 &&
                                columnData[1].map(d => {
                                    return <li> {
                                        d.page_link._linkType === 'Link.web' &&
                                        <a href={d.page_link.url} target={d.page_link.target} >
                                          {d.page_title}
                                        </a>
                                      }
                                      {
                                        d.page_link._linkType === 'Link.document' &&
                                        <Link
                                        style={footer_navigation_text_color !== '#000000' && {color: footer_navigation_text_color}}

                                          to={d.page_link ? `${_meta?.uid}/${d.page_link._meta.uid !== 'overview' ? d.page_link._meta.uid : ""}` : null}
                                        >
                                          {d.page_title}
                                        </Link>
                                      }</li>
                                })
                            }
                        </ul>
                    </div>
                    <div className="col-md-3">
                        <ul className={styles.socialMedia}>
                            {
                                social_media.map(d => {
                                    console.log('d ->', d);
                                    return <li>
                                        <SmartLink link={d.social_media_link}>
                                            <img src={renderImageUrl(d.social_media_image)} alt="" className={styles.socialMediaImg} />
                                        </SmartLink>
                                    </li>
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    );
}
