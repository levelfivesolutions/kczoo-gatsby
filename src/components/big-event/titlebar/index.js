import React from "react"
import { renderAsHtml } from "../../../utility"
import Title from "../../../components/title"
import styles from "./index.module.scss"

export default function ({ title, introduction, color }) {
    return (
        <div className={styles.titlebar}>
            <div className="container">
                <Title color={color} className={styles.title}>{title}</Title>
                {
                    introduction &&
                    <div className={styles.description}>
                        <div className="row">
                            <div className="col-md-8 offset-md-2 text-center">
                                {renderAsHtml(introduction)}
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    )
}