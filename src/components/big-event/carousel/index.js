import React from "react"
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

import styles from "./index.module.css"

function NextArrow(props) {
  const { className, style, onClick } = props
  return (
    <button
      type="button"
      className={styles.carouselNext}
      style={{ ...style }}
      onClick={onClick}
    >
      Next
    </button>
  )
}

function PrevArrow(props) {
  const { className, style, onClick } = props
  return (
    <button
      type="button"
      className={styles.carouselPrev}
      style={{ ...style }}
      onClick={onClick}
    >
      Previous
    </button>
  )
}

export default function SimpleSlider({ images }) {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <NextArrow />,
    prevArrow: <PrevArrow />,
  }
  return (
    <div className={styles.carouselContainer}>
      <Slider {...settings}>
        {images.map(d => (
          <div>
            {
              <img
                style={{ height: "313px", width: "100%", objectFit: "cover" }}
                src={d.image.url}
                alt={d.image.alt}
              />
            }
          </div>
        ))}
      </Slider>
    </div>
  )
}
