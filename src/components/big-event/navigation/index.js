import React, { useState } from "react"
import {
  ColorToHex,
  renderImageUrl,
  linkResolver,
  renderAsHtml,
} from "../../../utility"
import styles from "./index.module.css"
import { StaticQuery, graphql } from "gatsby"
import { Link, navigate } from "gatsby"
import ButtonLink from "../../button/button-link"
import { Modal } from "react-bootstrap"
import Icon from "../../../components/icon/icon"

const BigEventNavigation = props => {
  console.log("proppys", props)
  const {
    primary_navigation,
    id,
    _meta,
    primary_navigation_background_color,
    get_tickets_link_text,
    get_tickets_link_option,
    mobile_navigation_description,
    small_image,
  } = props

  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  return (
    <div
      className="d-flex justify-content-center align-items-center flex-lg-row"
      style={
        primary_navigation_background_color !== "#000000" && {
          backgroundColor: primary_navigation_background_color,
        }
      }
    >
      <nav className={`navbar navbar-expand-lg ${styles.filterBar}`}>
        <div className="container d-flex justify-content-center">
          <ul className={`navbar-nav ${styles.navbarNav}`} id="mainNavbarList">
            {primary_navigation.map((page, i) => {
              if (!page.page_link) {
                return null
              }

              console.log("pageLink", page, "text", id)

              let classes = `nav-link ${styles.navLink} `
              if (
                page.page_link._linkType === "Link.document" &&
                id === page.page_link._meta.id
              ) {
                classes += styles.navLinkActive
              }

              return (
                <li key={i} className={`${styles.navItem} nav-item`}>
                  {page.page_link._linkType === "Link.web" && (
                    <a
                      href={page.page_link.url}
                      target={page.page_link.target}
                      className={classes}
                    >
                      {page.page_title}
                    </a>
                  )}
                  {page.page_link._linkType === "Link.document" && (
                    <Link
                      className={classes}
                      to={
                        page.page_link
                          ? `${_meta?.uid}/${
                              page.page_link._meta.uid !== "overview" &&
                              page.page_link._meta.uid !== _meta.uid
                                ? page.page_link.__typename ===
                                  "PRISMIC_Big_event_ticket_page"
                                  ? "tickets"
                                  : page.page_link._meta.uid
                                : ""
                            }`
                          : null
                      }
                    >
                      {page.page_title}
                    </Link>
                  )}
                </li>
              )
            })}
          </ul>
          <div
            onClick={handleShow}
            className={`d-flex d-md-none ${styles.expandBtn}`}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="47"
              height="47"
              viewBox="0 -3 47 47"
            >
              <defs>
                <filter
                  id="Ellipse_138"
                  x="0"
                  y="0"
                  width="47"
                  height="47"
                  filterUnits="userSpaceOnUse"
                >
                  <feOffset dy="3" input="SourceAlpha" />
                  <feGaussianBlur stdDeviation="3" result="blur" />
                  <feFlood floodOpacity="0.161" />
                  <feComposite operator="in" in2="blur" />
                  <feComposite in="SourceGraphic" />
                </filter>
              </defs>
              <g
                transform="matrix(1, 0, 0, 1, 0, 0)"
                filter="url(#Ellipse_138)"
              >
                <circle
                  id="Ellipse_138-2"
                  data-name="Ellipse 138"
                  cx="14.5"
                  cy="14.5"
                  r="14.5"
                  transform="translate(9 6)"
                  fill="#fff"
                />
              </g>
              <path
                id="Path_11582"
                data-name="Path 11582"
                d="M12639.027,2986l7.564,7.564,7.563-7.564"
                transform="translate(-12623.527 -2967.495)"
                fill="none"
                stroke={primary_navigation_background_color}
                strokeLinecap="round"
                strokeWidth="3"
              />
            </svg>
          </div>
        </div>
      </nav>
      <Modal
        show={show}
        onHide={handleClose}
        dialogClassName={styles.modalDialogMobile}
      >
        <Modal.Header closeButton className={styles.modalHeaderMobile}>
          <img src={small_image?.url} />
        </Modal.Header>
        <Modal.Body
          className={styles.modalBodyMobile}
          style={
            primary_navigation_background_color !== "#000000" && {
              backgroundColor: primary_navigation_background_color,
            }
          }
        >
          <div className={`mt-4 ${styles.goText}`}>GO TO SECTION</div>
          <div
            className={
              "d-flex flex-row my-4 px-5 align-items-center justify-content-around"
            }
          >
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
            <div className={styles.dividerDot}></div>
          </div>
          <nav className="navbar navbar-expand-lg p-0">
            <div className="container d-flex justify-content-center">
              <ul
                className={`navbar-nav d-flex flex-grow-1 ${styles.navbarNavMobile} `}
              >
                {primary_navigation.map((page, i) => {
                  if (!page.page_link) {
                    return null
                  }

                  console.log("pageLink", page, "text", id)

                  let classes = `nav-link d-flex flex-grow-1 ${styles.navLinkMobile} `
                  if (
                    page.page_link._linkType === "Link.document" &&
                    id === page.page_link._meta.id
                  ) {
                    classes += styles.navLinkActiveModal
                  }

                  return (
                    <li key={i} className={`${styles.navItem} nav-item`}>
                      {page.page_link._linkType === "Link.web" && (
                        <a
                          href={page.page_link.url}
                          target={page.page_link.target}
                          className={classes}
                        >
                          {page.page_title}
                        </a>
                      )}
                      {page.page_link._linkType === "Link.document" && (
                        <Link
                          className={classes}
                          to={
                            page.page_link
                              ? `${_meta?.uid}/${
                                  page.page_link._meta.uid !== "overview" &&
                                  page.page_link._meta.uid !== _meta.uid
                                    ? page.page_link.__typename ===
                                      "PRISMIC_Big_event_ticket_page"
                                      ? "tickets"
                                      : page.page_link._meta.uid
                                    : ""
                                }`
                              : null
                          }
                        >
                          {page.page_title}
                        </Link>
                      )}
                    </li>
                  )
                })}
              </ul>
            </div>
          </nav>
          <div className={`my-4 ${styles.mobileNavDescription}`}>
            {renderAsHtml(mobile_navigation_description)}
          </div>
        </Modal.Body>
      </Modal>
      <div
        style={{ padding: "15px 0" }}
        className="d-flex align-items-center justify-content-center overflow-visible"
      >
        {
          <div
            style={{ height: "100%" }}
            className="position-relative flex-grow-1 d-flex align-items-center"
          >
            {get_tickets_link_option?.length == 1 && (
              <ButtonLink
                link={get_tickets_link_option[0].get_tickets_link_option_link}
                text={get_tickets_link_text}
                isInverse
                size={"large"}
                color={primary_navigation_background_color}
              ></ButtonLink>
            )}
            {get_tickets_link_option?.length > 1 && (
              <div className={styles.joinBox}>
                <button
                  className={`btn dropdown-toggle ${styles.giftDropdownToggle}`}
                  style={{
                    color: ColorToHex(primary_navigation_background_color),
                  }}
                  isInverse
                  type="button"
                  id="dropdownMenuButton"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  {get_tickets_link_text}
                </button>
                <div
                  className={`${styles.giftDropdownMenu} dropdown-menu`}
                  aria-labelledby="dropdownMenuButton"
                  style={{
                    transform: "translate3d(-1px, -21px, 0px) !important",
                  }}
                >
                  {get_tickets_link_option.map(link => {
                    return (
                      <a
                        className="dropdown-item"
                        target="_blank"
                        href={link?.get_tickets_link_option_link?.url}
                        style={{
                          color: ColorToHex(
                            primary_navigation_background_color
                          ),
                        }}
                      >
                        {link.get_tickets_link_option_text}
                      </a>
                    )
                  })}
                </div>
              </div>
            )}
          </div>
        }
      </div>
    </div>
  )
}

export default BigEventNavigation
