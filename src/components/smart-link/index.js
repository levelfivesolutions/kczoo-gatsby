import React from "react";
import { Link } from 'gatsby';
import { linkResolver } from '../../utility.js';

export default ({ link, className, children }) => {
	console.log('SMART LINK', link)
	if (!link) {
		return <span>{children}</span>
	} else if (link._linkType === 'Link.document') {
		return (
			<Link to={linkResolver(link._meta)} className={className}>
				{children}
			</Link>
		);
	} else if (link._linkType === 'Link.web') {
		return (
			<a href={link.url} target={link.target} className={className}>
				{children}
			</a>
		);
	} else {
		return <span>{children}</span>
	}
}
