import React from 'react'
import Lottie from 'react-lottie';
import animationData from '../../../static/images/LottieJSON/ostrich-animationV1.json'
 
export default class LottieControl extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {isStopped: false, isPaused: false};
  }
 
  render() {
    
    const buttonStyle = {
      display: 'block',
      margin: '10px auto'
    };
 
    const defaultOptions = {
      loop: true,
      autoplay: true, 
      animationData: animationData,
      rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
      }
    };
 
    return <div>
      <Lottie options={defaultOptions}
              width={ this.props.width || 300 }
              isStopped={this.state.isStopped}
              isPaused={this.state.isPaused}/>
    </div>
  }
}