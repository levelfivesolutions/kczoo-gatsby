import React from "react"
import { renderImageUrl, renderAsText } from "../../utility.js"
import Button from "../button/button-link.js"
import styles from "./index.module.css"

export default ({
  title,
  subtitle,
  image,
  btnText,
  link,
  btn2Text,
  link2,
  pageId,
}) => (
  <div className={styles.hero}>
    <div className={styles.heroImageContainer}>
      <img src={renderImageUrl(image)} className={styles.heroImage} alt="" />
      <div className={styles.wholePageEditButton} data-wio-id={pageId}></div>
    </div>
    <div className={styles.heroDetails}>
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-md-12">
            <div className={styles.heroDetailsCopy}>
              <h1 className={styles.heroHeadline}>{renderAsText(title)}</h1>
              <div className={styles.heroDescription}>
                {renderAsText(subtitle)}
              </div>
              <div className={styles.heroButtonWrapper}>
                <Button
                  text={`${renderAsText(btnText)}  →`}
                  link={link}
                  color="Zoo Green"
                  size="large"
                />
                {btn2Text && link2 && (
                  <Button
                    text={btn2Text}
                    link={link2}
                    color="Zoo Green"
                    size="large"
                    isInverse={true}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
)
