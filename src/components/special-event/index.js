import React from 'react'
import moment from 'moment'
import { renderAsText, renderImageUrl } from '../../utility.js';
import styles from './index.module.css'

export default ({ title, startDate, endDate, image, url }) => {
	let startDateObj = moment(startDate);
	let dateString = startDateObj.format("MMMM D");
	if (endDate) {
		let endDateObj = moment(endDate);
		if (startDateObj.month() === endDateObj.month()) {
			dateString += ` - ${moment(endDate).format("D")}`;
		} else {
			dateString += ` - ${moment(endDate).format("MMMM D")}`;
		}
	}

	return (
		<div className={ styles.eventContainer }>
			<div className={ styles.eventImageContainer }>
				<a href={ url }><img className={ styles.eventImage } src={ renderImageUrl(image) } alt={ title } /></a>
			</div>
			<h3 className={ styles.eventTitle }><a href={ url }>{ renderAsText(title) }</a></h3>
			<div className={ styles.eventDate }><a href={ url }>{ dateString }</a></div>
		</div>
	);
}
