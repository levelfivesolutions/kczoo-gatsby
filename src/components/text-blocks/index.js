import React from 'react';
import Text from "../text";
import { filterArrayByType } from '../../utility.js';
import styles from './index.module.css';

export default ({ body }) => {
	let data = filterArrayByType(body, "body_text_paragraphs");
	console.log('body_text_paragraphs', data);
	
	if (data.length === 0) {
		return null;
	}
	
	let blocks = data.pop().fields;
	
  return (
		<div className="text-blocks">
			{
				blocks.map(d => {
					if (d.design_style == 'Highlighted') {
						return <div className={ `${ styles.box } content-box text-block` }><Text>{ d.paragraph }</Text></div>;
					} else {
						return <Text>{ d.paragraph }</Text>;
					}
				})
			}
		</div>
	);
}
