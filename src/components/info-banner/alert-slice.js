import React from 'react';
import Button from '../button/button-link.js';
import InfoBanner from './index.js';
import { ColorToHex, renderAsHtml, renderAsText, filterArrayByType } from '../../utility.js';
import styles from './index.module.css'

export default ({ body }) => {
	let data = filterArrayByType(body, "alert");
	console.log('data', data);
	
	if (data.length === 0) {
		return null;
	}
	
	let { color, alert_title, alerttext, icon_selector } = data.pop().primary;
	console.log(color, alert_title, alerttext, icon_selector);
	
	let style = {
		backgroundColor: ColorToHex(color)
	};
	
  return (
		<div className={ styles.section }>
			<div className="container">
				<div className="row">
					<div className="offset-md-1 col-md-10">
						<InfoBanner 
							color={ color } 
							icon="/images/icon-elephant.png"
							title={ alert_title } 
							text={ alerttext } 
							url=""
						/>
					</div>
				</div>
			</div>					
		</div>
	);
}
