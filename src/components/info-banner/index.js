import React from 'react';
import Button from '../button/button-link.js';
import { ColorToHex, renderAsHtml, renderAsText } from '../../utility.js';
import styles from './index.module.css'

export default ({ color, icon, title, text, url }) => {
	let style = {
		backgroundColor: ColorToHex(color)
	};
	
  return (
		<div className={ styles.cta } style={ style }>
			{
				icon &&
				<div className={ styles.ctaImage }>
					<img src={ icon } alt="" width="72" />
				</div>
			}
			<div className={ styles.ctaDetails }>
				{ title && <h3 className={ styles.ctaTitle } title={ renderAsText(title) }>{ renderAsText(title) }</h3> }
				<div className={ styles.ctaDescription }>{ renderAsHtml(text) }</div>
				{ url && <Button text="Learn More" url={ url } color={ color } isInverse={ true } /> }
			</div>
		</div>
	);
}
