import React from 'react';
import { filterArrayByType, linkResolver } from '../../utility.js';
import moment from 'moment';
import Button from '../button/button-link.js';
import styles from './index.module.css';

export default ({ body }) => {
	console.log("body >>>", body);
	let tableData = filterArrayByType(body, 'homeschool_table');
	console.log('tableData', tableData);
	
	if (tableData.length === 0) {
		return null;
	}
	
	let dataItems = tableData[0].fields;
		
	/*
		DATA ITEMS
	*/
	let tableRows = [];
	if (dataItems.length === 0) {
		tableRows.push(<tr><td colspan="4"><em>None scheduled</em></td></tr>);
	} else {
		tableRows = dataItems.map((d, i) => {
			return (
				<tr key={ i }>
					<td>
						<div className={ `${ styles.calendarInline }` }>
							<div className={ `${ styles.calendar} mr-3` }>
								<div className={ styles.calendarDay }>{ moment(d.start_date).format('D') }</div>
								<div className={ styles.calendarMonth }>{ moment(d.start_date).format('MMM') }</div>
							</div>
							{
								d.end_date && 
								<>
									-
									<div className={ `${ styles.calendar } ml-3` }>
										<div className={ styles.calendarDay }>{ moment(d.end_date).format('D') }</div>
										<div className={ styles.calendarMonth }>{ moment(d.end_date).format('MMM') }</div>
									</div>
								</>
							}
						</div>
					</td>
					<td>{ d.time_range }</td>
					<td>{ d.class_name }</td>
					<td className="text-right">
						{ d.cta_link && <Button text={ d.cta_label } link={ d.cta_link } /> }
					</td>
				</tr>
			);
		})
	}

	return (
		<div className={ styles.tableWrapper }>
			<div className="container">
				<div className="table-responsive">
					<table className={`table table-striped ${ styles.table }`}>
						<thead>
							<tr className="d-none d-sm-table-row">
								<th>DATE</th>
								<th>TIME</th>
								<th>DESCRIPTION</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{ tableRows }
						</tbody>
					</table>
				</div>
			</div>
		</div>
	)
}
