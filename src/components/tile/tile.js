import React from 'react'
import Link from 'gatsby-link'
import { ColorToHex, renderAsText, renderImageUrl, renderAsHtml, linkResolver } from '../../utility.js';
import styles from './tile.module.css'

export default ({ title, desc, link, href="", imageSrc, image, color, tall, wide }) => {
	let style = {
		backgroundColor: ColorToHex(color)
	};
	
	let classes = styles.preview;
	if (tall) {
		classes += ` ${styles.previewTall}`;
	}
	
	if (wide) {
		classes += ` ${styles.previewWide}`;
	}
	
	const card = (
		<div className={ classes }>
			<div className={styles.previewImage} style={{ backgroundImage: `url(${ renderImageUrl(imageSrc || image) })` }}></div>
				<div className={styles.previewHeader} style={ style }>
					<div className={ styles.previewWrapper }>
					<h3 className={styles.previewTitle}>
						{ renderAsText(title) }
					</h3>
					{ 
						desc &&
						<div className={ styles.previewDescription }>{ renderAsHtml() }</div>
					}
				</div>
				{ (link || href) && <span className={ styles.previewArrow }>&rarr;</span> }
			</div>
		</div>
	);
	
	if (link && typeof link === 'object') {
		return (
			<Link to={ linkResolver(link) } className={ styles.previewLink }>{ card }</Link>
		)
	} else if (href) {
		if (href.match(/^http/)) {
			return (
				<a href={ href } target="_blank" className={ styles.previewLink }>{ card }</a>
			);
		} else {
			return (
				<Link to={ href } className={ styles.previewLink }>{ card }</Link>
			);
		}		
	} else {
		return <>{ card }</>;
	}
}
