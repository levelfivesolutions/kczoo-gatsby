import React from 'react';
import Pricing from '../pricing/pricing.js';
import Box from '../box/box.js';
import { ColorToHex, renderAsText, renderImageUrl, renderAsHtml } from '../../utility.js';
import styles from './corporate-meeting-room.module.css';

class CorporateMeetingRoom extends React.Component {

	render() {
		let { color, title, description, image, price, quantifier, btnText, priceDesc, id, url, link } = this.props;

		var hexColor = (color) ? ColorToHex(color) : "#FFF";
		
		console.log('image', image);
		return (
			<div className={styles.camp}>
				<div className="row">
					<div className={ styles.wholePageEditButton } data-wio-id={ id }></div> 
					<div className="col-md-5"><div className={styles.campImageWrapper}><img src={ renderImageUrl(image) } className={styles.campImage} alt="" /></div></div>
					<div className="col-md-7">
						<div className={styles.campBody}>
							<h2 className={styles.campTitle} style={{ backgroundColor: hexColor }}>{ renderAsText(title)}</h2>
							<div className={styles.campDesc}>
								{ renderAsHtml(description) }
							</div>
							<div>
								<Box>
									<Pricing
										price={ price }
										quantifier={ quantifier }
										description={priceDesc}
										btnText={ btnText }
										btnColor={color}
										btnUrl={ url }
										btnLink={ link }
									/>
								</Box>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}


export default CorporateMeetingRoom;