import React from 'react';
import { renderAsText, filterArrayByType } from '../../utility.js';
import SectionBlock from '../section-block';
import styles from './index.module.css';
/*
import SectionBlock from '../section-block';
import Button from '../button/button-link.js';
import { ColorToHex, renderImageUrl, renderAsText, renderAsHtml, filterArrayByType } from '../../utility.js';
import styles from './call-out-block.module.css';
*/

export default ({ body }) => {
	let media = filterArrayByType(body, "media");
	
	if (media.length === 0) {
		return null;
	}
	
	return (
		<SectionBlock>
			<div className="container">
			{
				media.map(d => {
					console.log("media block", d);

					console.log(renderAsText(d.primary.text));

					return (
						<div className={ styles.mediaBlock }>
							<h4 className={ styles.title }>{ renderAsText(d.primary.text) }</h4>
							<div className="row">
								{
									d.fields.map((image, i) => {
										console.log('image', image);
										return (
											<div className="col-md-3 text-center">
												{ image.media_link && <img src={ image.media_link.url } alt="" className="img-fluid" /> }
											</div>
										);
									})
								}
							</div>
						</div>
					);
				})
			}
			</div>			
		</SectionBlock>
	);
}
