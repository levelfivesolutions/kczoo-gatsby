/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react";
import PropTypes from "prop-types";
import { StaticQuery, graphql } from "gatsby";
import { withPreview } from 'gatsby-source-prismic-graphql';

import SiteWideAlert from "../sitewide-alert";

import Header from "../header";
import Footer from "../footer";
import "./layout.css";
import styles from "./layout.module.css";

const alertQuery = graphql`
  query {
    prismic {
      allSitewide_alerts {
        edges {
          node {
            _meta {
              id
            }
            alerts {
              sitewide_alert_description
              sitewide_alert_title
              sitewide_alert_color
              sitewide_alert_learn_more_link {
                ... on PRISMIC__ExternalLink {
                  target
                  _linkType
                  url
                }
              }
              sitewide_alert_learn_more_text
            }
          }
        }
      }
    }
    prismic {
      footer: allFooters {
        edges {
          node {
            footer_contact
            footer_hours
            footer_location
            footer_links {
              footer_link {
                ... on PRISMIC__ExternalLink {
                  target
                  _linkType
                  url
                }
              }
              footer_link_text
            }
            footer_social_links {
              footer_social_link {
                ... on PRISMIC__ExternalLink {
                  target
                  _linkType
                  url
                }
              }
              footer_social_link_icon
              footer_social_link_text
            }
          }
        }
      }
    }
  }
`;

const Layout = ({ title, children, location, prismic, section, parent, id }) => {
  return (
      <StaticQuery
        query={alertQuery}
        render={ data => {
          return(
          <>
            {
            data.prismic.allSitewide_alerts.edges.length > 0  &&  
            <SiteWideAlert id={ data.prismic.allSitewide_alerts.edges.slice(0,1).pop().node._meta.id } alerts={ data.prismic.allSitewide_alerts.edges.slice(0,1).pop().node.alerts } /> 
            }
            <div className={ styles.topWrapper }>
              <Header location={ location} section={ section } prismic={ prismic } />
            </div>
            <div data-wio-id={id} >
              <main>{children}</main>
              <Footer title={ title } footer={ data.prismic.footer.edges[0].node } section={ section } parent={ parent } location={ location } />
            </div>
        </>
          );
       }}  
       />
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout;
