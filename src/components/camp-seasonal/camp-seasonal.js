import React from 'react';
import moment from 'moment';
import Tile from '../tile/tile.js';
import Icon from '../icon/icon.js';
import Button from '../button/button-link.js';
import Pricing from '../pricing/pricing.js';
import styles from './camp-seasonal.module.css';
import { RichText } from 'prismic-reactjs';
import { renderImageUrl, renderAsText } from '../../utility.js';

export default ({ color, title, description, season, price, priceQualifier, ageRange, year, priceDesc, price2, priceQualifier2, priceDesc2, minAge, maxAge, startDate, endDate, image, btnText, link, relation, url }) => {
	var list = null;
	var listItems = [];
	/*
	if (dates) {
		listItems.push(
			<React.Fragment>
				<Icon icon="ICON_CALENDAR" width="22" /> { dates }
			</React.Fragment>);
	}
	*/
	if (endDate) {
		listItems.push(
			<React.Fragment>
				<Icon icon="calendar" height="22" width="22" /> {moment(startDate).format('MMM D')} - {moment(endDate).format('MMM D')}
			</React.Fragment>);
	}
	if (season) {
		listItems.push(
			<React.Fragment>
				<Icon icon="calendar" width="22" />&nbsp;&nbsp; {season} {year}
			</React.Fragment>);
	}
	if (minAge) {
		listItems.push(
			<React.Fragment><Icon icon="caricature" height="22" width="24" />&nbsp;&nbsp;Age Groups: {minAge} - {maxAge}</React.Fragment>
		);
	}
	if (ageRange) {
		listItems.push(
			<React.Fragment><Icon icon="group of friends" width="24" />&nbsp;&nbsp;Age Groups: {ageRange}</React.Fragment>
		);
	}
	if (listItems.length > 0) {
		list =
			<ul className={`flex-column align-items-start d-flex ${styles.campIcons}`}>
				{listItems.map(el => <li>{el}</li>)}
			</ul>;
	}

	return (
		<div className={styles.camp}>
			{image && <Tile title={renderAsText(title)} color={color} imageSrc={renderImageUrl(image)} />}
			<div className={styles.campBody}>
				<div >
					{list}
					<p>{renderAsText(description)}</p>
				</div>
				{
					price &&
					<div className={styles.campPricingBlock}>
						<Pricing
							price={price}
							quantifier={priceQualifier}
							description=""
							btnText=""
							btnColor=""
							btnUrl=""
						/>
					</div>
				}
				{
					price2 &&
					<div className={styles.campPricingBlock}>
						<Pricing
							price={price2}
							quantifier={priceQualifier2}
							description=""
							btnText=""
							btnColor=""
							btnUrl=""
						/>
					</div>
				}
			</div>
			{
				btnText &&
				<div className={styles.campFooter}><Button color={color} text={btnText} url={url} relation={relation} link={link} /></div>
			}
		</div>
	);
}