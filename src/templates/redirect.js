import React from 'react';
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import {Helmet} from "react-helmet";
import BodyText from "../components/body-text";


class Camp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		};

  }

	render() {
		let { data } = this.props;
		console.log('props', this.props);
		const doc = data.prismic.allRedirects.edges.slice(0,1).pop();
		console.log('doc -->', doc);
		if (!doc) return null;
		
		let { redirect_url } = doc.node;

		return (
			<>
				<Helmet>
					<meta http-equiv="Refresh" content={`0; url='${ redirect_url.url }'"`} />
				</Helmet>
				<div style={{ padding: "40px" }}>
					<a href={redirect_url.url}>If you are not automatically redirected, click here.</a>
				</div>
			</>
		)
	}
}

export default Camp;

export const query = graphql `
	query RedirectQuery($uid: String) {
		prismic {
		  allRedirects(uid: $uid)  {
			edges {
			  node {
				_meta {
				  uid
				}
				redirect_url {
				  _linkType
				  ... on PRISMIC__ExternalLink {
					_linkType
					url
				  }
				}
			  }
			}
		  }
		}
	  }
	
`
