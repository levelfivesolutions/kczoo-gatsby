import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import Title from "../components/title"
import BodyText from "../components/body-text"
import Text from "../components/text"
import Hero from "../components/hero-detail"

class Exhibit extends React.Component {
  render() {
    let { data } = this.props
    console.log("props", this.props)
    const doc = data.prismic.allSpecial_exhibits.edges.slice(0, 1).pop()
    console.log("doc -->", doc)
    if (!doc) return null

    const { image, title, meta_title, meta_description, body_text } = doc.node

    return (
      <Layout
        title={title}
        section="animals"
        parent={{ title: "Featured Exhibits", url: "/featured-exhibits" }}
        {...this.props}
      >
        <SEO
          title={meta_title}
          canonical={this.props.location.origin + this.props.location.pathname}
          description={meta_description}
        />
        <Hero image={image} pageId={doc.node._meta.id} />
        <BodyText>
          <div className="container">
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Title>{title}</Title>
              </div>
            </div>
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Text>{body_text}</Text>
              </div>
            </div>
          </div>
        </BodyText>
      </Layout>
    )
  }
}

export default Exhibit

export const query = graphql`
  query exhibitQuery($uid: String) {
    prismic {
      allSpecial_exhibits(uid: $uid) {
        edges {
          node {
            title
            image
            body_text
            _meta {
              id
              tags
              type
              uid
            }
            meta_description
            meta_title
          }
        }
      }
    }
  }
`
