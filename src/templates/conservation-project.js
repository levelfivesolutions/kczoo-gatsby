import React from 'react';
import { graphql } from 'gatsby';
import SEO from "../components/seo";
import Layout from "../components/layout/layout";
import Title from "../components/title";
import BodyText from "../components/body-text";
import Text from "../components/text";
import Hero from "../components/hero-detail";
import RelatedLinks from '../components/related-links/index.js';
import { hasSlice, renderAsText } from '../utility';

class ConservationProject extends React.Component {
	render() {
		let { data } = this.props;
		console.log('props', this.props);
		const doc = data.prismic.allConservation_projects.edges.slice(0,1).pop();
		console.log('doc -->', doc);
		if (!doc) return null;
		
		const { meta_title, meta_description, wide_image, title, body_text, body } = doc.node;
		const hasRelatedLinks = hasSlice(body, 'related_links');
		
		let parentPage = null;
		if (doc.node._meta.tags.includes('field and research projects')) {
			parentPage = {
				title: 'Field and Research Projects',
				url: '/conservation-projects/field-research-projects'
			};
		} else if (doc.node._meta.tags.includes('green initiative')) {
			parentPage = {
				title: 'Green Initiatives',
				url: '/conservation-projects/green-initiatives'
			};
		}
		
		return (
			<Layout title={ title } section="conservation" parent={ parentPage } { ...this.props }>
				<SEO title={ meta_title || renderAsText(title) } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
				<Hero image={ wide_image } pageId={ doc.node._meta.id }/>
				<BodyText>
					<div className="container">
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<Title>{ title }</Title>
							</div>
						</div>
						{
							!hasRelatedLinks &&
							<div className="row">
								<div className="offset-md-1 col-md-10">
									{ body_text &&  <Text>{ body_text }</Text> }
								</div>
							</div>
						}
						{
							hasRelatedLinks &&
							<div className="row">
								<div className="offset-md-1 col-md-7">
									{ body_text && <Text>{ body_text }</Text> }
								</div>
								<div className="col-md-3">
									<RelatedLinks body={ body } />
								</div>
							</div>
						}
					</div>
				</BodyText>
			</Layout>
		)
	}
}

export default ConservationProject;

export const query = graphql`
	query conservationProjectQuery($uid: String) {
		prismic {
			allConservation_projects(uid: $uid) {
				edges {
					node {
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						meta_description
						title
						wide_image
						body_text
						body {
							... on PRISMIC_Conservation_projectBodyRelated_links {
								type
								label
								fields {
									link_name
									description
									exInLink {
										_linkType
										... on PRISMIC_Landing_page_hero_block {
											_linkType
											_meta {
												id
												tags
												type
												uid
											}
										}
										... on PRISMIC_Animals_landing_page {
											_linkType
											_meta {
												id
												tags
												type
												uid
											}
										}
										... on PRISMIC_Detail {
											_linkType
											_meta {
												id
												tags
												type
												uid
											}
										}
										... on PRISMIC__ExternalLink {
											_linkType
											url
										}
										... on PRISMIC__FileLink {
											_linkType
											url
										}
										... on PRISMIC_Form {
											_linkType
											_meta {
												id
												tags
												type
												uid
											}
										}
										... on PRISMIC_Conservation_project {
											_linkType
											_meta {
												id
												tags
												type
												uid
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`
