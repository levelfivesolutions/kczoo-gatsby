import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import BodyText from "../components/body-text"
import moment from "moment"
import Hero from "../components/hero-detail"
import CallOutBlock from "../components/call-out/call-out-block"
import {
  renderAsText,
  renderAsHtml,
  linkResolver,
  ColorToHex,
  hasSlice,
  filterArrayByType,
  getDayOfWeek,
} from "../utility.js"
import Button from "../components/button/button-link.js"
import styles from "./event.module.css"
import Slicer from "../components/slice-r/slice-r"
import RelatedLinks from "../components/related-links/index"

export default props => {
  const { data, location } = props
  console.log("data -->", data)
  const doc = data.prismic.allSpecial_events.edges.slice(0, 1).pop()
  console.log("doc -->", doc)
  if (!doc) return null

  const {
    wide_images,
    title,
    meta_title,
    meta_description,
    body_text,
    speaker_image,
    speaker_label,
    activity_time,
    hero_foreground,
    hero_title,
    hero_title_color,
    hero_border_color,
    body,
  } = doc.node

  let backgroundStyle = {
    backgroundImage: "url(/images/backgrounds/background-leaf.svg)",
    backgroundPosition: "right top",
    backgroundRepeat: "no-repeat",
    padding: "70px 0",
  }

  let heroImageUrl = !hero_foreground ? "" : hero_foreground.url
  let heroImageAlt = !hero_foreground ? "" : hero_foreground.alt

  const hasRelatedLinks = hasSlice(body, "related_links")

  return (
    <Layout
      title={title}
      section="activities"
      parent={{ title: "Events", url: "/events" }}
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero
        image={wide_images}
        pageId={doc.node._meta.id}
        heroForegroundUrl={heroImageUrl}
        heroForegroundAlt={heroImageAlt}
        heroTitle={hero_title}
        heroTitleColor={hero_title_color || "White"}
        heroBorderColor={hero_border_color || "White"}
      />

      {console.log(activity_time.length)}
      {activity_time.length <= 1 && (
        <div className="row no-gutters" style={backgroundStyle}>
          <div className="container">
            <div className="row">
              <div className="offset-lg-1 col-lg-7">
                <h1 className={styles.title}>{renderAsText(title)}</h1>
                {renderAsHtml(body_text)}
              </div>
              <div className="offset-lg-0 col-lg-3 offset-md-3 col-md-6 offset-sm-2 col-sm-8">
                <div className={styles.speakerWrapper}>
                  {speaker_image && (
                    <div>
                      <img
                        src={speaker_image.url}
                        alt={speaker_image.alt}
                        className="rounded mb-2 w-100"
                      />
                      <p className="text-center mb-5">{speaker_label}</p>
                    </div>
                  )}
                  <div className={`${styles.tableWrapperAlt} rounded`}>
                    <div className="container d-flex flex-column justify-content-center">
                      {activity_time.map(d => {
                        let startDayOfWeek = moment(d.start_time).day()
                        let endDayOfWeek = moment(d.end_time).day()
                        console.log(getDayOfWeek(startDayOfWeek))
                        console.log(getDayOfWeek(endDayOfWeek))
                        let endDate = null
                        if (d.number_of_days && d.number_of_days > 1) {
                          endDate = moment(d.start_time).add(
                            d.number_of_days - 1,
                            "days"
                          )
                        }
                        console.log(endDate)
                        return (
                          <div className={styles.gridRowAlt}>
                            <div className={styles.gridSection}>
                              <div className="pb-3">
                                {!endDate && (
                                  <div className={styles.calendar}>
                                    <div className={styles.calendarDay}>
                                      {d.start_time
                                        ? moment(d.start_time).format("D")
                                        : ""}
                                    </div>
                                    <div className={styles.calendarMonth}>
                                      {d.start_time
                                        ? moment(d.start_time).format("MMM")
                                        : ""}
                                    </div>
                                  </div>
                                )}
                                {endDate && (
                                  <div className={`${styles.calendarInline}`}>
                                    <div className={`${styles.calendar} mr-3`}>
                                      <div className={styles.calendarDay}>
                                        {d.start_time
                                          ? moment(d.start_time).format("D")
                                          : ""}
                                      </div>
                                      <div className={styles.calendarMonth}>
                                        {d.start_time
                                          ? moment(d.start_time).format("MMM")
                                          : ""}
                                      </div>
                                    </div>
                                    -
                                    <div className={`${styles.calendar} ml-3`}>
                                      <div className={styles.calendarDay}>
                                        {endDate.format("D")}
                                      </div>
                                      <div className={styles.calendarMonth}>
                                        {endDate.format("MMM")}
                                      </div>
                                    </div>
                                  </div>
                                )}
                              </div>
                              {!endDate && (
                                <div>{getDayOfWeek(startDayOfWeek)}</div>
                              )}
                              {endDate && (
                                <div className="d-inline-block">
                                  <span>{getDayOfWeek(startDayOfWeek)}</span>
                                  &nbsp; - &nbsp;
                                  <span>{getDayOfWeek(endDayOfWeek)}</span>
                                </div>
                              )}
                              <div>
                                {d.start_time
                                  ? moment(d.start_time).format("h:mm a")
                                  : ""}{" "}
                                {d.start_time && d.end_time ? "to" : ""}{" "}
                                {d.end_time && d.end_time
                                  ? moment(d.end_time).format("h:mm a")
                                  : ""}
                              </div>
                            </div>
                            <div className={styles.gridSection}>
                              {d.timelocation
                                ? d.timelocation ===
                                  "Other (enter in the Other Location field)"
                                  ? d.other_location
                                  : d.timelocation
                                : ""}
                            </div>
                            {(d.plu_price ||
                              d.plu_price_label ||
                              d.cta_link) && (
                              <div
                                className={`${styles.pluCtaWrapper} pt-3 pb-3`}
                              >
                                <div>
                                  {d.plu_price && <div>{d.plu_price}</div>}
                                  {d.plu_price_label && (
                                    <div className={styles.pluCtaLabel}>
                                      {d.plu_price_label}
                                    </div>
                                  )}
                                </div>
                                {d.cta_link && <div></div>}
                                {!d.cta_2_link && d.cta_label && (
                                  <div className="my-1">
                                    {d.cta_label && (
                                      <Button
                                        text={d.cta_label}
                                        link={d.cta_link}
                                      />
                                    )}
                                  </div>
                                )}
                                {d.cta_2_link && d.cta_label && (
                                  <div>
                                    <div className={`${styles.giftDropdown}`}>
                                      <a
                                        className={`button dropdown-toggle ${styles.giftDropdownToggle}`}
                                        type="button"
                                        id="dropdownMenuButton"
                                        data-toggle="dropdown"
                                        style={{
                                          backgroundColor: ColorToHex(
                                            "Tangerine"
                                          ),
                                          color: "white",
                                        }}
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                      >
                                        {d.cta_dropdown_label}
                                      </a>
                                      <div
                                        className={`${styles.giftDropdownMenu} dropdown-menu`}
                                        aria-labelledby="dropdownMenuButton"
                                      >
                                        <a
                                          className="dropdown-item"
                                          target="_blank"
                                          style={{
                                            backgroundColor: ColorToHex(
                                              "Tangerine"
                                            ),
                                            color: "#fff",
                                          }}
                                          href={d.cta_link.url}
                                        >
                                          {d.cta_label}
                                        </a>
                                        <a
                                          className="dropdown-item"
                                          target="_blank"
                                          style={{
                                            backgroundColor: ColorToHex(
                                              "Tangerine"
                                            ),
                                            color: "#fff",
                                          }}
                                          href={d.cta_2_link.url}
                                        >
                                          {renderAsText(d.cta_2_label)}
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                )}
                              </div>
                            )}
                            <div className="mt-4 p-0">
                              {hasRelatedLinks && <RelatedLinks body={body} />}
                            </div>
                          </div>
                        )
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <CallOutBlock body={body} />
        </div>
      )}

      {activity_time.length > 1 && (
        <div>
          <BodyText>
            <div className="container">
              <div className="row">
                <div className="offset-md-1 col-md-10">
                  <h1 className={styles.title}>{renderAsText(title)}</h1>
                  {renderAsHtml(body_text)}
                </div>
              </div>
            </div>
          </BodyText>
          <div className={styles.tableWrapper}>
            <div className={`container ${styles.gridTable}`}>
              <div className={styles.gridRow}>
                <div>DATE</div>
                <div>TIME</div>
                <div>LOCATION</div>
                <div>PRICE</div>
                <div></div>
              </div>
              {activity_time.map(d => {
                let endDate = null
                if (d.number_of_days && d.number_of_days > 1) {
                  endDate = moment(d.start_time).add(
                    d.number_of_days - 1,
                    "days"
                  )
                }
                return (
                  <div className={styles.gridRow}>
                    <div>
                      {!endDate && (
                        <div className={styles.calendar}>
                          <div className={styles.calendarDay}>
                            {d.start_time
                              ? moment(d.start_time).format("D")
                              : ""}
                          </div>
                          <div className={styles.calendarMonth}>
                            {d.start_time
                              ? moment(d.start_time).format("MMM")
                              : ""}
                          </div>
                        </div>
                      )}
                      {endDate && (
                        <div className={`${styles.calendarInline}`}>
                          <div className={`${styles.calendar} mr-3`}>
                            <div className={styles.calendarDay}>
                              {d.start_time
                                ? moment(d.start_time).format("D")
                                : ""}
                            </div>
                            <div className={styles.calendarMonth}>
                              {d.start_time
                                ? moment(d.start_time).format("MMM")
                                : ""}
                            </div>
                          </div>
                          -
                          <div className={`${styles.calendar} ml-3`}>
                            <div className={styles.calendarDay}>
                              {endDate.format("D")}
                            </div>
                            <div className={styles.calendarMonth}>
                              {endDate.format("MMM")}
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                    <div
                      style={{ fontWeight: "bold", fontFamily: "DINOTBold" }}
                    >
                      {d.start_time
                        ? moment(d.start_time).format("h:mm a")
                        : ""}{" "}
                      {d.start_time && d.end_time ? "to" : ""}{" "}
                      {d.end_time && d.end_time
                        ? moment(d.end_time).format("h:mm a")
                        : ""}
                    </div>
                    <div>
                      {d.timelocation
                        ? d.timelocation ===
                          "Other (enter in the Other Location field)"
                          ? d.other_location
                          : d.timelocation
                        : ""}
                    </div>
                    <div>{d.plu_price}</div>
                    {!d.cta_2_link && (
                      <div>
                        {d.cta_label && (
                          <Button text={d.cta_label} link={d.cta_link} />
                        )}
                      </div>
                    )}
                    {d.cta_2_link && d.cta_label && (
                      <div>
                        <div className={`${styles.giftDropdown}`}>
                          <a
                            className={`button dropdown-toggle ${styles.giftDropdownToggle}`}
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            style={{
                              backgroundColor: ColorToHex("Tangerine"),
                              color: "white",
                            }}
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            {d.cta_dropdown_label}
                          </a>
                          <div
                            className={`${styles.giftDropdownMenu} dropdown-menu`}
                            aria-labelledby="dropdownMenuButton"
                          >
                            <a
                              className="dropdown-item"
                              target="_blank"
                              style={{
                                backgroundColor: ColorToHex("Tangerine"),
                                color: "#fff",
                              }}
                              href={d.cta_link.url}
                            >
                              {d.cta_label}
                            </a>
                            <a
                              className="dropdown-item"
                              target="_blank"
                              style={{
                                backgroundColor: ColorToHex("Tangerine"),
                                color: "#fff",
                              }}
                              href={d.cta_2_link.url}
                            >
                              {renderAsText(d.cta_2_label)}
                            </a>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                )
              })}
            </div>
          </div>
          <CallOutBlock body={body} />
        </div>
      )}
    </Layout>
  )
}

export const query = graphql`
  query eventDetailQuery($uid: String) {
    prismic {
      allSpecial_events(uid: $uid) {
        edges {
          node {
            hero_title
            hero_title_color
            hero_border_color
            hero_foreground
            wide_images
            title
            meta_title
            meta_description
            speaker_image
            speaker_label
            body {
              ... on PRISMIC_Special_eventBodyDetailed_callout {
                type
                label
                fields {
                  body_text
                  button_label
                  color
                  cta1 {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC_Detail {
                      title
                      summary1
                      _linkType
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC_Form {
                      hero_image
                      title
                      _linkType
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                  }
                  image1
                  title1
                }
              }
              ... on PRISMIC_Special_eventBodyRelated_links {
                type
                label
                fields {
                  description
                  link_name
                  exInLink {
                    _linkType
                    ... on PRISMIC__FileLink {
                      _linkType
                      name
                      url
                    }
                  }
                }
              }
            }
            body_text
            activity_time {
              timelocation
              other_location
              start_time
              end_time
              plu_price
              plu_price_label
              number_of_days
              duration_time
              duration
              cta_label
              cta_dropdown_label
              cta_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
              }
              cta_2_label
              cta_2_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
              }
            }
            _meta {
              id
              tags
              type
              uid
            }
          }
        }
      }
    }
  }
`
