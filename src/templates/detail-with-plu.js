import React from 'react';
import { graphql } from 'gatsby';
import { filterArrayByType, getPrice } from '../utility';
import { getItems } from '../galaxy';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import Title from "../components/title";
import BodyText from "../components/body-text";
import Text from "../components/text";
import Hero from "../components/hero-detail";
import Box from '../components/box/box.js';
import Pricing from '../components/pricing/pricing.js';

class DetailWithPlu extends React.Component {
  constructor(props) {
    super(props);
		
    this.state = {
      items: []
    }
  }
	
  componentDidMount() {
		getItems( (res) => {console.log('res', res);
			this.setState( { items: res } );
		});
	} 
	
	render() {
		console.log('props', this.props);
		let { data, location } = this.props;
		
		const doc = data.prismic.allDetail_with_plus.edges.slice(0,1).pop();
		console.log('doc -->', doc);
		if (!this.state.doc) return null;
		
		const { wide_image, title, text, meta_title, meta_description, body } = this.state.doc.node;
		let plu_price, plu_price_description, banner_text, cta_label, color = '';
		const callout = filterArrayByType(body, 'banner_callout');
		console.log('banner_callout', callout);
	
		if (callout.length > 0) {
			({ plu_price, plu_price_description, banner_text, cta_label, color } = callout[0].fields[0]);
		}
		
		let parentPage = null;
		if (this.state.doc.node._meta.tags.includes('Animal Care Academy')) {
			parentPage = {
				title: 'Animal Care Academy',
				url: '/animal-care-academy'
			};
		}
		
		return (
			<Layout title={ title } section="camps" parent={ parentPage } { ...this.props }>
				<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
				<Hero image={ wide_image } />
				<BodyText>
					<div className="container">
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<Title className="mb-3">{ title }</Title>
							</div>
						</div>
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<Text>{ text }</Text>
							</div>
						</div>
						<div className="row">
							<div className="offset-md-1 col-md-10">
								{
									plu_price &&
									<Box color={ color }>
										<Pricing
										 	btnColor={ color }
											isInverse={ true }
											price={ getPrice(this.state.items, plu_price, 0) }
											quantifier={ plu_price_description }
											description={ banner_text }
											btnText={ cta_label }
											btnUrl={ '' }
										/>
									</Box>
								}
							</div>
						</div>						
					</div>
				</BodyText>
			</Layout>
		)
	}
}

export default DetailWithPlu;

export const query = graphql`
	query detailWithPluQuery($uid: String) {
		prismic {
			allDetail_with_plus(uid: $uid) {
				edges {
					node {
						wide_image
						title
						text
						meta_description
						meta_title
						body {
							... on PRISMIC_Detail_with_pluBodyBanner_callout {
								type
								label
								fields {
									banner_text
									color
									cta_label
									cta_link {
										... on PRISMIC__ExternalLink {
											_linkType
											url
										}
									}
									plu_price
									plu_price_description
								}
							}
						}
						_meta {
							id
							tags
							type
							uid
						}

					}
				}
			}
		}
	}
`
