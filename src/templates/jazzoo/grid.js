import React from "react"
import { graphql } from "gatsby"
import moment from "moment"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/jazzoo/hero"
import Slicer from "../../components/slice-r/slice-r"
import { renderAsText, renderImageUrl } from "../../utility"
import styles from "./grid.module.scss"
import SectionBlock from "../../components/section-block"
import JazzooNavigation from "../../components/jazzoo/navigation"
import Fringe from "../../components/jazzoo/fringe"
import Titlebar from "../../components/jazzoo/titlebar"
import CalendarDay from "../../components/calendar-day"
import JazzooFooter from "../../components/jazzoo/footer"

const JazzzooGrid = props => {
  const { data, location } = props
  console.log("data ->", data)
  const doc = data.prismic.allJazzoo_grid_layouts.edges.slice(0, 1).pop()

  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    title,
    introduction,
    items,
    meta_title,
    meta_description,
  } = doc.node

  const parentPage = {
    title: "Jazzoo",
    url: "/jazzoo",
  }
  return (
    <Layout
      parent={parentPage}
      title={meta_title || title}
      section="activities"
      {...props}
    >
      <SEO
        title={renderAsText(title)}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero pageId={doc.node._meta.id} />
      <JazzooNavigation id={_meta.id} location={location} />
      <Fringe>
        <Titlebar title={title} introduction={introduction} />
        <div className="container" style={{ paddingBottom: 220 }}>
          <div className="row">
            {items.map(element => {
              console.log("element", element)
              return (
                <div className="col-md-3 d-flex justify-content-center">
                  <img
                    src={renderImageUrl(element.image)}
                    alt={element.image?.alt}
                    className="img-fluid"
                  />
                </div>
              )
            })}
          </div>
        </div>
      </Fringe>
      <JazzooFooter />
    </Layout>
  )
}

export default JazzzooGrid

export const query = graphql`
  query jazzooGridTemplateQuery($uid: String) {
    prismic {
      allJazzoo_grid_layouts(uid: $uid) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            title
            introduction
            items {
              image
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
