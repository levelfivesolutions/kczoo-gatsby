import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Title from "../../components/title"
import BodyText from "../../components/body-text"
import Text from "../../components/text"
import Hero from "../../components/jazzoo/hero"
import { RichText } from "prismic-reactjs"
import styles from "../detail.module.css"
import jazzooStyles from "./jazzoo-detail.module.scss"
import {
  filterArrayByType,
  renderAsHtml,
  renderAsText,
  renderImageUrl,
  hasSlice,
  linkResolver,
} from "../../utility"
import PricingCallout from "../../components/pricing-callout"
import RelatedLinks from "../../components/related-links/index.js"
import TableDateRange from "../../components/table-date-range"
import TableOvernights from "../../components/table-overnights"
import AddOnSection from "../../components/add-on/add-on-section.js"
import TextBlocks from "../../components/text-blocks"
import CallOutBlock from "../../components/call-out/call-out-block"
import Button from "../../components/button/button-link.js"
import MediaBlock from "../../components/media-block"
import { getItems } from "../../galaxy"
import SliceR from "../../components/slice-r/slice-r"
import JazzooNavigation from "../../components/jazzoo/navigation"
import SectionBlock from "../../components/section-block"
import Fringe from "../../components/jazzoo/fringe"
import JazzooFooter from "../../components/jazzoo/footer"
import { Helmet } from "react-helmet"

class jazzooDetail extends React.Component {
  render() {
    let doc = this.props.data.prismic.allDetails.edges.slice(0, 1).pop()
    console.log("DETAIL doc -->", doc)
    if (!doc) return null
    const {
      meta_title,
      meta_description,
      wide_image,
      tall_image,
      title,
      body_text,
      optional_image,
      body,
    } = doc.node

    let headerCallout = null
    let quote
    if (body) {
      headerCallout = filterArrayByType(body, "header_callout")
      headerCallout = headerCallout.length > 0 ? headerCallout[0].primary : null
      quote = filterArrayByType(body, "under_optional_image_quote")
      console.log("QUOTE", quote)
      quote = quote.length > 0 ? quote[0].primary : null
    }

    const hasRelatedLinks = hasSlice(body, "related_links")
    const hasBannerCallout = hasSlice(body, "banner_callout")
    const hasSidebar = hasRelatedLinks || headerCallout || optional_image

    const ctaButton = filterArrayByType(body, "cta_button")
    const hasCtaButton = ctaButton.length > 0

    let section
    if (doc.node._meta.tags.includes("Animal Encounters")) {
      section = "animals"
    } else if (doc.node._meta.tags.includes("Homeschool")) {
      section = "education"
    } else if (doc.node._meta.tags.includes("Overnights")) {
      section = "camps"
    } else if (doc.node._meta.tags.includes("Volunteer")) {
      section = "support-us"
    } else if (doc.node._meta.tags.includes("Zoomobile")) {
      section = "education"
    } else if (doc.node._meta.tags.includes("Visit")) {
      section = "visit"
    } else if (doc.node._meta.tags.includes("Get Involved")) {
      section = "support-us"
    } else if (doc.node._meta.tags.includes("Aquarium")) {
      section = "aquarium"
    } else if (doc.node._meta.tags.includes("Jazzoo")) {
      section = "jazzoo"
    }

    let parentPage = null
    if (doc.node._meta.tags.includes("Animal Encounters")) {
      parentPage = {
        title: "Animal Encounters",
        url: "/animal-encounters",
      }
    } else if (doc.node._meta.tags.includes("Homeschool")) {
      parentPage = {
        title: "Homeschool",
        url: "/homeschool",
      }
    } else if (doc.node._meta.tags.includes("Overnights")) {
      parentPage = {
        title: "Overnights",
        url: "/overnights",
      }
    } else if (doc.node._meta.tags.includes("Volunteer")) {
      parentPage = {
        title: "Volunteers",
        url: "/volunteers",
      }
    } else if (doc.node._meta.tags.includes("Zoomobile")) {
      parentPage = {
        title: "Zoomobile",
        url: "/zoomobile",
      }
    } else if (doc.node._meta.tags.includes("Donations")) {
      parentPage = {
        title: "Donations",
        url: "/donate",
      }
    } else if (doc.node._meta.tags.includes("Daily Schedule")) {
      parentPage = {
        title: "Daily Schedule",
        url: "/daily-schedule",
      }
    } else if (doc.node._meta.tags.includes("Preschool")) {
      parentPage = {
        title: "Preschool",
        url: "/preschool",
      }
    } else if (doc.node._meta.tags.includes("Keeper for a Day")) {
      parentPage = {
        title: "Keeper for a Day",
        url: "/keeper-for-a-day",
      }
    } else if (doc.node._meta.tags.includes("Aquarium")) {
      parentPage = {
        title: "Aquarium",
        url: "/aquarium",
      }
    } else if (doc.node._meta.tags.includes("Jazzoo")) {
      parentPage = {
        title: "Jazzoo",
        url: "/jazzoo",
      }
    }

    return (
      <Layout
        title={renderAsText(title).length > 0 ? title : meta_title}
        section={section}
        paddingBottom={false}
        parent={parentPage}
        {...this.props}
      >
        <SEO
          title={meta_title}
          description={meta_description}
          canonical={this.props.location.origin + this.props.location.pathname}
        />
        <Hero
          image={wide_image}
          tallImage={tall_image}
          pageId={doc.node._meta.id}
        />
        <JazzooNavigation location={this.props.location} />
        <Fringe>
          <div className={jazzooStyles.container}>
            <div
              className={` ${
                !(renderAsText(title).length > 0 || body_text) ? "d-none" : ""
              } `}
            >
              <SectionBlock>
                <BodyText style={{ paddingBottom: "0px !important" }}>
                  <div className="container" style={{ marginBottom: "60px" }}>
                    <div className="row">
                      <div className="offset-md-1 col-md-10">
                        <Title className={jazzooStyles.title}>
                          test{title}
                        </Title>
                      </div>
                    </div>
                    {!hasSidebar && (
                      <div className="row">
                        <div className="offset-md-1 col-md-10">
                          {body_text &&
                            RichText.asText(body_text).length > 0 && (
                              <Text>{body_text}</Text>
                            )}
                          <TextBlocks body={body} />
                        </div>
                      </div>
                    )}
                    {hasSidebar && (
                      <div className="row">
                        <div className="offset-md-1 col-md-7">
                          {body_text &&
                            RichText.asText(body_text).length > 0 && (
                              <Text>{body_text}</Text>
                            )}
                          <TextBlocks body={body} />
                        </div>
                        <div className="col-md-3">
                          {headerCallout && (
                            <div className={`${styles.callOut}`}>
                              <div className={styles.callOutInner}>
                                {headerCallout.header_callout_title && (
                                  <p className={styles.calloutTitle}>
                                    {renderAsText(
                                      headerCallout.header_callout_title
                                    )}
                                  </p>
                                )}
                                {renderAsHtml(
                                  headerCallout.header_callout_text
                                )}
                                {headerCallout.header_callout_button_link && (
                                  <div className={styles.callOutButton}>
                                    <Button
                                      text={
                                        headerCallout.header_callout_button_label
                                      }
                                      link={
                                        headerCallout.header_callout_button_link
                                      }
                                      color="Zoo Green"
                                      size="small"
                                    />
                                  </div>
                                )}
                              </div>
                            </div>
                          )}
                          {optional_image && (
                            <figure className={styles.activityFigure}>
                              <img
                                src={renderImageUrl(optional_image)}
                                className={`${styles.activityImage} img-fluid`}
                                alt=""
                              />
                            </figure>
                          )}
                          {quote && (
                            <div>
                              <div>{renderAsHtml(quote.quote)}</div>
                              <div className={` ${styles.quotee} d-flex`}>
                                {quote.quotee_image && (
                                  <img
                                    src={quote.quotee_image.url}
                                    alt={quote.quotee_image.alt}
                                  />
                                )}
                                <div>{renderAsHtml(quote.quotee)}</div>
                              </div>
                            </div>
                          )}
                          <RelatedLinks body={body} />
                        </div>
                      </div>
                    )}
                  </div>
                  {hasCtaButton && (
                    <div className="container mt-5">
                      <div className="row">
                        <div class="offset-md-1 col-md-10">
                          <Button
                            text={ctaButton[0].primary.label}
                            link={ctaButton[0].primary.link}
                            color={ctaButton[0].primary.color}
                            size="large"
                          />
                        </div>
                      </div>
                    </div>
                  )}
                </BodyText>
              </SectionBlock>
            </div>
            {body.map(slice => {
              console.log("trying to render body slice")
              return (
                <SliceR
                  key={Math.random * 999999}
                  data={slice}
                  classList={jazzooStyles.jazzoo2022BottomPadding}
                  className={styles.jazzooSlicer}
                />
              )
            })}
            <TableDateRange body={body} />
            <TableOvernights body={body} />
            <AddOnSection body={body} />
            <CallOutBlock body={body} />
            <MediaBlock body={body} />
          </div>
        </Fringe>
        <JazzooFooter />
      </Layout>
    )
  }
}

export default jazzooDetail

export const query = graphql`
  query jazzooDetailQuery($uid: String) {
    prismic {
      allDetails(uid: $uid) {
        edges {
          node {
            body {
              ... on PRISMIC_DetailBodyCta_button {
                type
                label
                primary {
                  color
                  label
                  link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_DetailBodyBody_text_paragraphs {
                type
                label
                fields {
                  design_style
                }
              }
              ... on PRISMIC_DetailBodyHeader_callout {
                type
                primary {
                  header_callout_button_label
                  header_callout_button_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                  }
                  header_callout_text
                  header_callout_title
                }
              }
              ... on PRISMIC_DetailBodyBanner_callout {
                type
                label
                fields {
                  banner_text
                  color
                  cta_label
                  cta_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                  }
                  plu_price
                  plu_price_description
                }
              }
              ... on PRISMIC_DetailBodyRelated_links {
                type
                label
                primary {
                  related_links_header
                }
                fields {
                  link_name
                  description
                  exInLink {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                  }
                }
              }
              ... on PRISMIC_DetailBodyHomeschool_table {
                type
                label
                fields {
                  start_date
                  end_date
                  time_range
                  class_name
                  cta_label
                  cta_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                  }
                }
                primary {
                  title: title1
                }
              }
              ... on PRISMIC_DetailBodyDate_table__5_column {
                type
                label
                fields {
                  column
                  column_a
                  column_b
                  end_date_time
                  start_date_time
                  form_link {
                    _linkType
                    ... on PRISMIC_Form {
                      form_id
                      _linkType
                    }
                  }
                }
              }
              ... on PRISMIC_DetailBodyAddOns {
                type
                label
                fields {
                  icon_selector
                  addOn_title
                  addOn_text: description
                }
                primary {
                  title: title1
                }
              }
              ... on PRISMIC_DetailBodyDetailed_callout {
                type
                label
                fields {
                  image: image1
                  title: title1
                  cta: cta1 {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                  }
                  color
                  button_label
                  body_text
                }
              }
              ... on PRISMIC_DetailBodyDetailed_callout2 {
                type
                label
                fields {
                  title: title1
                  text: text1
                  second_cta_label
                  second_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                  }
                  cta_label
                  link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                  }
                  image1
                  color
                }
              }
              ... on PRISMIC_DetailBodyMedia {
                type
                label
                primary {
                  text
                }
                fields {
                  media_link {
                    _linkType
                    ... on PRISMIC__ImageLink {
                      _linkType
                      height
                      name
                      size
                      url
                      width
                    }
                  }
                }
              }
              ... on PRISMIC_DetailBodyImage_text_block {
                type
                label
                primary {
                  image_text_color
                  image_text_link {
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC_Landing_page_hero_block {
                      _meta {
                        uid
                        type
                        id
                      }
                    }
                  }
                  image_text_image
                  image_text_image_side
                  image_text_link {
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                      name
                    }
                    ... on PRISMIC_Detail {
                      _meta {
                        uid
                        id
                        type
                        tags
                      }
                    }
                    _linkType
                  }
                  image_text_text
                  image_text_title
                  image_text_link_text {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                    ... on PRISMIC_Detail {
                      _meta {
                        id
                        uid
                        type
                        tags
                      }
                      _linkType
                    }
                  }
                }
              }
              ... on PRISMIC_DetailBodyReused_image_text_block {
                type
                label
                primary {
                  inner_block {
                    _linkType
                    ... on PRISMIC_Reusable_image_text_block {
                      image_text_image
                      image_text_title
                      _linkType
                      _meta {
                        uid
                        type
                        tags
                        id
                      }
                      image_text_color
                      image_text_link {
                        ... on PRISMIC__ExternalLink {
                          target
                          _linkType
                          url
                        }
                        ... on PRISMIC_Landing_page_hero_block {
                          _meta {
                            uid
                            type
                            id
                          }
                          _linkType
                        }
                        _linkType
                      }
                      image_text_link_text
                      image_text_text
                    }
                  }
                }
              }
              ... on PRISMIC_DetailBodyUnder_optional_image_quote {
                type
                label
                primary {
                  quote
                  quotee
                  quotee_image
                }
              }
              ... on PRISMIC_DetailBodyCarousel {
                type
                label
                fields {
                  carousel_slide_accent_color
                  carousel_slide_image
                  carousel_slide_text
                }
                primary {
                  carousel_heading_text
                }
              }
              ... on PRISMIC_DetailBodyDetail_page_content {
                type
                label
                fields {
                  detail_page_content_body_text
                  detail_page_content_optional_image
                  detail_page_content_summary
                  detail_page_content_title
                  detail_page_content_under_optional_image_caption
                }
              }
            }
            _meta {
              id
              uid
              tags
            }
            meta_title
            meta_description
            wide_image
            tall_image
            title
            body_text
            optional_image
          }
        }
      }
    }
  }
`
