import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/jazzoo/hero"
import { renderImageUrl } from "../../utility"
import styles from "./photo-library.module.scss"
import JazzooNavigation from "../../components/jazzoo/navigation"
import Fringe from "../../components/jazzoo/fringe"
import Titlebar from "../../components/jazzoo/titlebar"
import JazzooFooter from "../../components/jazzoo/footer"
import Button from "../../components/button"

const PHOTOS_PER_PAGE = 20;
const UNDEFINED_IMAGE = { index: -1, selectedImage: { image: '' } };

const PhotoLibrary = props => {
    const [page, setPage] = useState(1);
    const [image, setImage] = useState(UNDEFINED_IMAGE);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        window.$('#modal-photo').on('hidden.bs.modal', () => {
            setImage(UNDEFINED_IMAGE);
        });
    }, []);

    const { data, location } = props
    console.log('data ->', data)
    const doc = data.prismic.allPhoto_librarys.edges.slice(0, 1).pop()

    console.log("DOC", doc)
    if (!doc) return null
    const {
        _meta,
        meta_title,
        meta_description,
        title,
        introduction,
        photos
    } = doc.node

    let elements = [];
    for (let i = 0; i < page * PHOTOS_PER_PAGE; i++) {
        if (i === photos.length) {
            break;
        }

        elements.push(photos[i]);
    }

    let hasButton = page * PHOTOS_PER_PAGE < photos.length;

    function selectImage(d, i) {
        console.log(i, d);
        setLoading(true);
        setImage({ index: i, selectedImage: d });
    }

    function handleImageLoaded() {
        window.$('#modal-photo').modal('show');
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }

    function handleClose(ev) {
        window.$('#modal-photo').modal('hide');
    }

    return (
        <Layout parent={{
            title: "Jazzoo",
            url: "/jazzoo",
        }} title={meta_title || meta_description} section="activities" {...props}>
            <SEO
                title={meta_title}
                description={meta_description}
                canonical={location.origin + location.pathname}
            />
            <Hero pageId={doc.node._meta.id} />
            <JazzooNavigation id={_meta.id} location={location} />
            <Fringe>
                <Titlebar title={title} introduction={introduction} />
                <div className={styles.gridContainer}>
                    <div className="container">
                        <div className="row">
                            <div className="offset-md-1 offset-sm-0 col-md-10 col-sm-12">
                                <div className="row">
                                    {
                                        elements.map((d, i) => {
                                            let showSpinner = (i === image.index) && loading;

                                            return (
                                                <div key={i} className={`col-md-3 col-sm-6 ${styles.gridCell}`}>
                                                    <img src={renderImageUrl(d.image.Thumbnail)} alt={d.image.alt} className="img-fluid" onClick={(ev) => { selectImage(d, i) }} />
                                                    {showSpinner && <div className={`spinner-border ${styles.spinner}`}></div>}
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                        {
                            hasButton &&
                            <div className={styles.buttonContainer}>
                                <Button onClick={ev => setPage(page + 1)} color="Zoo Green">More Photos</Button>
                            </div>
                        }
                    </div>
                </div>
            </Fringe>
            <JazzooFooter />
            <div id="modal-photo" className="modal" tabindex="-1">
                <div className={`modal-dialog modal-xl ${styles.modalDialog}`}>
                    <div className={`modal-content ${styles.modalContent}`}>
                        <div className={`modal-header ${styles.modalHeader}`}>
                            <button type="button" className={`${styles.modalClose}`} onClick={handleClose}></button>
                        </div>
                        <div className={`modal-body ${styles.modalBody}`}>
                            <img
                                src={renderImageUrl(image.selectedImage.image)}
                                alt=""
                                className="img-fluid"
                                onLoad={handleImageLoaded}
                            />
                        </div>
                        <div className={`modal-footer ${styles.modalFooter}`}>
                            <div>{image.selectedImage.image_title}</div>
                            <div>{image.index + 1}/{elements.length}</div>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default PhotoLibrary

export const query = graphql`
query photoLibraryQuery($uid: String) {
    prismic {
        allPhoto_librarys(uid: $uid) {
            edges {
              node {
                _meta {
                  id
                  tags
                  type
                  uid
                }
                meta_title
                meta_description
                title
                introduction
                photos {
                    image_title
                    image
                }
              }
            }
        }        
    }
}
`
