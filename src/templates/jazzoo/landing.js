import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/jazzoo/hero"
import { filterArrayByType } from "../../utility"
import styles from "./landing.module.css"
import SectionBlock from "../../components/section-block"
import JazzooNavigation from "../../components/jazzoo/navigation"
import Fringe from "../../components/jazzoo/fringe"
import Titlebar from "../../components/jazzoo/titlebar"
import JazzooFooter from "../../components/jazzoo/footer"
import CardLanding from "../../components/card-landing"

const JazzzooDetail = props => {
  const { data, location } = props
  console.log("data ->", data)
  const landingDoc = data.prismic.allJazzoo_landing_pages.edges
    .slice(0, 1)
    .pop()

  console.log("DOC", landingDoc)
  if (!landingDoc) return null
  const { _meta, meta_title, meta_description, title, body } = landingDoc.node

  let featuredItems = filterArrayByType(body, "feature_block").pop()
  featuredItems =
    typeof featuredItems !== "undefined" ? featuredItems.fields : null
  return (
    <Layout
      parent={{
        title: "Jazzoo",
        url: "/jazzoo",
      }}
      title={meta_title || title}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero pageId={landingDoc.node._meta.id} />
      <JazzooNavigation id={_meta.id} location={location} />
      <Fringe>
        <Titlebar title={title} />
        {featuredItems &&
          featuredItems.map((d, i) => {
            return (
              <SectionBlock
                key={i}
                classList={styles.jazzoo2022LayoutPadding}
                shading="even"
              >
                <div className="container">
                  <div className="row">
                    <div className="col-md-12">
                      <CardLanding
                        color={d.color_name}
                        image={d.callout_image}
                        season=""
                        year=""
                        minAge=""
                        maxAge=""
                        title={d.callout_title}
                        description={d.callout_description}
                        urlText={d.cta_text}
                        link={d.cta}
                        cta_thumbnail={d.cta_thumbnail}
                      />
                    </div>
                  </div>
                </div>
              </SectionBlock>
            )
          })}
      </Fringe>
      <JazzooFooter />
    </Layout>
  )
}

export default JazzzooDetail

export const query = graphql`
  query jazzooLandingTemplateQuery($uid: String) {
    prismic {
      allJazzoo_landing_pages(uid: $uid) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            title
            summary
            body {
              ... on PRISMIC_Jazzoo_landing_pageBodyFeature_block {
                type
                label
                fields {
                  callout_image
                  callout_title
                  callout_description
                  cta_text
                  cta_thumbnail
                  cta: cta1 {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                  }
                  color_name
                }
              }
            }
          }
        }
      }
    }
  }
`
