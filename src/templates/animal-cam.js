import React from 'react';
import { graphql } from 'gatsby';
import { renderAsText, renderAsHtml } from '../utility';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import Title from "../components/title";
import BodyText from "../components/body-text";
import Text from "../components/text";
import Hero from "../components/hero-detail";
import RelatedLinks from '../components/related-links/index.js';
import TextBlocks from '../components/text-blocks';
import AddOnSection from '../components/add-on/add-on-section.js';
import { renderImageUrl, hasSlice } from '../utility';
import styles from './camp.module.css';

class AnimalDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		};
  }

	render() {
		let { data, location } = this.props;
		
		const doc = data.prismic.allAnimal_cam_details.edges.slice(0,1).pop();
		console.log('doc -->', doc);
		if (!doc) return null;
		
		const { meta_title, meta_description, wide_image, cam, title, optional_image, body_text, body } = doc.node;
		/*
		const embed = filterArrayByType(cam, 'embed');
		const hasEmbed = embed.length > 0;
		*/
		const hasRelatedLinks = hasSlice(body, 'related_links');
		const hasSidebar = hasRelatedLinks || optional_image;
		
		return (
			<Layout title={ title } section="animals" parent={ { title: 'Animal Cameras', url: '/animal-cams' } } { ...this.props }>
				<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
				{/* ADD STUFF FOR EDIT BUTTOn */}
				<Hero image={ wide_image } />
				<BodyText>
					<div className="container">
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<Title className="mb-4">{ title }</Title>
								<div>{ renderAsHtml(cam) }</div>
							</div>
						</div>
					</div>
				</BodyText>
				<div className={ styles.tableWrapper }>
					<div className="container">
						{
							!hasSidebar &&
							<div className="row">
								<div className="offset-md-1 col-md-10">
									{ body_text &&  <Text>{ body_text }</Text> }
									<TextBlocks body={ body } />
								</div>
							</div>
						}
						{
							hasSidebar &&
							<div className="row">
								<div className="offset-md-1 col-md-7">
									{ body_text && <Text>{ body_text }</Text> }
									<TextBlocks body={ body } />
								</div>
								<div className="col-md-3">
									{ 
										optional_image && 
										<figure className={ styles.activityFigure }>
											<img src={ renderImageUrl(optional_image) } className={`${ styles.activityImage } img-fluid`} alt="" />
										</figure>
									}
									<RelatedLinks body={ body } isInverse={ true } />
								</div>
							</div>
						}
					</div>
				</div>
				<AddOnSection dark body={ body } />
			</Layout>
		)
	}
}

export default AnimalDetail;

export const query = graphql`
	query AnimalDetailQuery($uid: String) {
		prismic {
			allAnimal_cam_details(uid: $uid) {
				edges {
					node {
						wide_image
						title
						optional_image
						meta_title
						meta_description
						cam
						body_text
						body {
							... on PRISMIC_Animal_cam_detailBodyAddOns {
								type
								label
								fields {
									addOn_text
									addOn_title
									icon_selector
								}
								primary {
									subheading
									title1
								}
							}
							... on PRISMIC_Animal_cam_detailBodyBody_text_paragraphs {
								type
								label
								fields {
									design_style
									paragraph
								}
							}
							... on PRISMIC_Animal_cam_detailBodyRelated_links {
								type
								label
								fields {
									link_name
									description
									exInLink {
										... on PRISMIC_Conservation_project {
											_meta {
												id
												tags
												type
												uid
											}
										}
										... on PRISMIC_Detail {
											_meta {
												id
												tags
												type
												uid
											}
										}
										... on PRISMIC_Map_landing_page {
											_meta {
												id
												tags
												type
												uid
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
`
