import React from "react";
import { graphql } from 'gatsby';
import { filterArrayByType } from '../utility.js';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import SectionBlock from '../components/section-block';
import Columns from '../components/columns';
import Banner from '../components/banner';
import CardLanding from '../components/card-landing';
import CallOutBlock from "../components/call-out/call-out-block";
import CampExperience from "../components/camp-experience/camp-experience"
import { getPrice } from '../utility'
import { getItems } from '../galaxy'
import { linkResolver } from "gatsby-source-prismic-graphql";

export default (props) => {
	const { data, location } = props;
	console.log('>>>', data);

	const doc = data.prismic.allLanding_page_hero_blocks.edges.slice(0, 1).pop();
	console.log('doc -->', doc);
	if (!doc) return null;

  const { wide_header_size, title, feature, summary, cta, button_label, meta_title, meta_description, body, _meta } = doc.node;
	let featuredItems = filterArrayByType(body, "feature_block").pop();
	let campBlocks = filterArrayByType(body, "camp_feature_callout").pop();

	featuredItems = (typeof featuredItems !== 'undefined') ? featuredItems.fields : null;
	campBlocks = (typeof campBlocks !== 'undefined') ? campBlocks.fields : null;

	let section;
	if (doc.node._meta.tags.includes('Get Involved')) {
		section = 'support-us';
	} else if (doc.node._meta.tags.includes('Animals')) {
		section = 'animals';
	}

	return (
		<Layout title={ title } section={ section } { ...props }>
			<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
      <Hero image={ wide_header_size }  pageId={ _meta.id } />
			<TitleBar 
				title={ title } 
				link={ cta } 
				linkText={ button_label } 
				description={ summary } 
				calloutDescription={ feature } 
			/>      
      { featuredItems &&
				featuredItems.map((d, i) => {
					return (
						<SectionBlock key={i}>
							<div className="container">
								<div className="row">
									<div className="col-md-12">
										<CardLanding
											color={d.color_name}
											image={d.callout_image}
											season=''
											year=''
											minAge=''
											maxAge=''
											title={d.callout_title}
											description={d.callout_description}
											urlText={d.cta_text}
											link={d.cta}
										/>
									</div>
								</div>
							</div>
						</SectionBlock>
					);
				})
			}
      { campBlocks &&
				campBlocks.map((d, i) => {
					return (
						<SectionBlock key={i}>
							<div className="container">
								<div className="row">
									<div className="col-md-12">
                  <CampExperience
											color={ d.color_name }
											image={d.callout_image}
											title={d.callout_title}
											richDescription={d.description}
											ageRange={d.age_range}
											startDate={d.date_start}
											endDate={d.date_end}
											timeRange={ d.time_range }
											prices={[
												{
													price: d.reg_plu_price,
													description: d.reg_plu_price_description
												},
												{
													price: d.fotz_plu_price,
													description: d.fotz_plu_price_description
												}
											]}
											btnText={ d.cta_label }
											url={linkResolver(d.cta)}
											/>
									</div>
								</div>
							</div>
						</SectionBlock>
					);
				})
			}
			<Columns body={body} />
			<CallOutBlock body={body} />
			<Banner body={body} />
		</Layout>
	);
}

export const query = graphql`
query landingTemplateQuery($uid: String) {
  prismic {
    allLanding_page_hero_blocks(uid: $uid) {
      edges {
        node {
          _meta {
            id
            tags
            type
            uid
          }
          meta_title
          meta_description
          wide_header_size
          title
          summary
          feature
          button_label
          cta {
            ... on PRISMIC__ExternalLink {
              _linkType
              url
            }
          }
          body {
            ... on PRISMIC_Landing_page_hero_blockBodyCamp_feature_callout {
              type
              fields {
                age_range
                callout_image
                callout_title
                color_name
                cta_label
                description
                fotz_plu_price
                fotz_plu_price_description
                reg_plu_price
                reg_plu_price_description
                time_range
                cta {
                  ... on PRISMIC_Camp {
                    _meta {
                      uid
                      id
                      type
                      tags
                    }
                  }
                  ... on PRISMIC_Detail {
                    _meta {
                      uid
                      id
                      type
                      tags
                    }
                  }
                  _linkType
                }
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyFeature_block {
              type
              label
              fields {
                callout_description
                callout_image
                callout_title
                color_name
                cta_text
                cta: cta1 {
                  _linkType
                  ... on PRISMIC__Document {
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC__FileLink {
                    _linkType
                    url
                  }
                }
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyDetailed_callout {
              type
              label
              fields {
                title: title1
                text: text1
                image: image1
                color
                cta_label
                cta: link {
                  _linkType
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC__Document {
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                }
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyDonation_columns {
              type
              label
              fields {
                color
                column_image
                column_text
                column_title
                cta_label
                cta_link {
                  _linkType
                  ... on PRISMIC_Detail {
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                }
              }
            }
            ... on PRISMIC_Landing_page_hero_blockBodyBanner {
              type
              label
              fields {
                banner_color
                icon
                text
                title1
              }
            }
          }
        }
      }
    }
  }
}

`

