import React from "react"
import { graphql } from "gatsby"
import Layout from "../../../components/layout/layout"
import SEO from "../../../components/seo"
import Hero from "../../../components/big-event/glo-wild/hero"
import Slicer from "../../../components/slice-r/slice-r"
import { renderAsHtml, renderAsText, filterArrayByType } from "../../../utility"
import styles from "./home.module.css"
import SectionBlock from "../../../components/section-block"
import BigEventNavigation from "../../../components/big-event/glo-wild/navigation"
import TitleBar from "../../../components/big-event/glo-wild/titlebar"
import JazzooCarousel from "../../../components/big-event/glo-wild/carousel"
import CalendarDay from "../../../components/calendar-day"
import JazzooFooter from "../../../components/big-event/glo-wild/footer"

const BigEventHome = props => {
  const { data, location } = props
  const doc = data.prismic.allBig_event_homes.edges.slice(0, 1).pop()

  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    body,
    body_text,
    page_title,
    event_date,
    date_description,
    carousel,
    primary_sponsor,
    primary_sponsor_introduction,
  } = doc.node

  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node

  let bigEventDetails = filterArrayByType(body, "big_event_details")[0]
  console.log("bigEventDetails", bigEventDetails)
  return (
    <Layout title={meta_title || page_title} section="activities" {...props}>
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={doc.node._meta.id} />
      <BigEventNavigation {...event} id={_meta.id} location={location} />
      <SectionBlock classList={styles.bodySectionClass}>
        <TitleBar color={event.heading_color} title={page_title} />
        <div className="container mb-4">
          <div className="row">
            <div className="offset-md-1 col-md-5">
              <div className={styles.carouselContainer}>
                <JazzooCarousel images={carousel} />
              </div>
            </div>
            <div className="col-md-5">
              {renderAsHtml(body_text)}
              {primary_sponsor_introduction && (
                <div style={{ textAlign: "center", marginTop: 30 }}>
                  {renderAsHtml(primary_sponsor_introduction)}
                </div>
              )}
              {primary_sponsor && (
                <div className="d-flex align-items-center justify-content-center">
                  <img src={primary_sponsor?.url} alt={primary_sponsor?.alt} />
                </div>
              )}
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="offset-md-1 col-md-10">
              {body &&
                body
                  .filter(d => d.type === "cta")
                  .map(slice => {
                    return <Slicer key={slice.__typename} data={slice} />
                  })}
              {body &&
                body
                  .filter(d => d.type === "info_callout")
                  .map(slice => {
                    return <Slicer key={slice.__typename} data={slice} />
                  })}
            </div>
          </div>
        </div>
        <div className={styles.bodyWave}></div>
      </SectionBlock>
      {bigEventDetails && (
        <div
          className={`${styles.bigEventDetailsContainer}`}
          style={{
            padding: "150px 0 130px 0",
            width: "100%",
            zIndex: 0,
            position: "relative",
          }}
        >
          <div className={`container`}>
            <div className="row d-flex">
              <div className="col-12 col-md">
                <div className="d-flex flex-row flex-md-column flex-lg-row">
                  <div className="mb-3 mb-lg-0 d-flex align-items-center align-items-md-start ">
                    <div>
                      <div
                        style={{
                          width: "172px",
                          height: "172px",
                          backgroundColor: "#fff",
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "space-apart",
                        }}
                      >
                        <div
                          style={{
                            backgroundColor: "#AF4068",
                            color: "#fff",
                            fontSize: "20px",
                            lineHeight: "26px",
                            fontFamily: "DINOTBold",
                            padding: "10px 0",
                            textAlign: "center",
                          }}
                        >
                          STARTS SEPT
                        </div>
                        <div
                          style={{
                            textAlign: "center",
                            fontSize: "96px",
                            lineHeight: "96px",
                            fontFamily: "DINOTBold",
                            paddingTop: "10px",
                          }}
                        >
                          1
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="ml-4 pl-1 pl-md-0 pl-lg-1 ml-md-0 ml-lg-4">
                    <p
                      style={{
                        fontFamily: "DIN Round OT Bold",
                        fontSize: "21px",
                        fontWeight: "700",
                        color: "#222222",
                        lineHeight: "27px",
                        letterSpacing: "2px",
                        textTransform: "uppercase",
                        marginBottom: "8px",
                      }}
                      className={styles.greyText}
                    >
                      {renderAsText(
                        bigEventDetails?.primary?.big_event_details_left_heading
                      )}
                    </p>
                    <p
                      className={styles.greyText}
                      style={{
                        fontFamily: "DINOTBold",
                        fontSize: "24px",
                        fontWeight: "bold",
                        lineHeight: "31px",
                      }}
                    >
                      {renderAsHtml(
                        bigEventDetails?.primary
                          ?.big_event_details_date_description
                      )}
                    </p>
                    <p style={{ fontSize: "18px", lineHeight: "23px" }}>
                      <div className={styles.eventDetailLeftBody}>
                        {renderAsHtml(
                          bigEventDetails?.primary
                            ?.big_event_details_location_description
                        )}
                      </div>
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-12 col-md-auto d-flex flex-row my-4 my-md-0 flex-md-column align-items-center justify-content-around">
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
                <div aria-hidden="true" className={styles.dividerDot}></div>
              </div>
              <div className="col-12 col-md">
                <p
                  style={{
                    fontFamily: "DIN Round OT Bold",
                    fontSize: "21px",
                    fontWeight: "700",
                    color: "#222222",
                    lineHeight: "27px",
                    letterSpacing: "2px",
                    textTransform: "uppercase",
                    marginBottom: "8px",
                  }}
                  className={styles.greyText}
                >
                  {renderAsText(
                    bigEventDetails?.primary?.big_event_details_right_heading
                  )}
                </p>
                {renderAsHtml(
                  bigEventDetails?.primary?.big_event_details_right_body_text
                )}
              </div>
            </div>
          </div>
        </div>
      )}
      {body &&
        body
          .filter(d => d.type === "column")
          .map(slice => {
            return (
              <Slicer
                classList={styles.h2styles}
                key={slice.__typename}
                data={slice}
              />
            )
          })}
      <JazzooFooter {...event} />
    </Layout>
  )
}

export default BigEventHome

export const query = graphql`
  query GloWildBigEventHomeQuery($uid: String, $event: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
            }
            name
            wide_image
            small_image
            hero_foreground
            heading_color
            hero_foreground_only
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            primary_navigation_background_color
            footer_navigation_background_color
            footer_visible
            footer_navigation_text_color
            footer_visible
            mobile_navigation_description
          }
        }
      }
      allBig_event_homes(uid: $uid) {
        edges {
          node {
            _meta {
              id
            }
            page_title
            event_date
            date_description
            body_text
            meta_title
            meta_description
            primary_sponsor_introduction
            primary_sponsor
            carousel {
              image
            }
            body {
              ... on PRISMIC_Big_event_homeBodyInfo_callout {
                type
                label
                primary {
                  info_description
                }
                fields {
                  info_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                  }
                  info_link_title
                }
              }
              ... on PRISMIC_Big_event_homeBodyColumn {
                type
                label
                primary {
                  column_section_title
                }
                fields {
                  color
                  column_image
                  column_text
                  column_title
                  plu
                  plu_description
                  column_cta_label
                  column_cta {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                    ... on PRISMIC_Big_event_photo_library {
                      _linkType
                      _meta {
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Big_event_content_page {
                      _linkType
                      _meta {
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Big_event_ticket_page {
                      _linkType
                      _meta {
                        uid
                        id
                      }
                    }
                    ... on PRISMIC_Big_event_detail {
                      _linkType
                      _meta {
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Big_event_landing_page {
                      _linkType
                      _meta {
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Big_event_grid_layout {
                      _linkType
                      _meta {
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Big_event_home {
                      _linkType
                      _meta {
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Big_event_sponsors {
                      _linkType
                      _meta {
                        id
                        uid
                      }
                    }
                    ... on PRISMIC_Big_event_sponsor_levels {
                      _linkType
                      _meta {
                        id
                        uid
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_Big_event_homeBodyCta {
                type
                label
                fields {
                  icon
                  color
                  cta_label
                  cta_text
                  cta_title
                  cta_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC_Big_event_sponsors {
                      _meta {
                        uid
                        id
                      }
                      _linkType
                    }
                    ... on PRISMIC_Big_event_grid_layout {
                      _linkType
                      _meta {
                        uid
                        id
                      }
                    }
                    ... on PRISMIC_Big_event_landing_page {
                      _linkType
                      _meta {
                        uid
                        id
                      }
                    }
                    ... on PRISMIC_Big_event_detail {
                      _linkType
                      _meta {
                        uid
                        id
                      }
                    }
                  }
                }
              }
              ... on PRISMIC_Big_event_homeBodyBig_event_details {
                type
                label
                primary {
                  big_event_details_date_description
                  big_event_details_left_heading
                  big_event_details_location_description
                  big_event_details_right_body_text
                  big_event_details_right_heading
                }
              }
            }
          }
        }
      }
    }
  }
`
