import React from "react"
import { graphql } from "gatsby"
import Layout from "../../../components/layout/layout"
import SEO from "../../../components/seo"
import Hero from "../../../components/big-event/hero"
import Slicer from "../../../components/slice-r/slice-r"
import { renderAsHtml, renderImageUrl } from "../../../utility"
import styles from "./volunteers.module.scss"
import SectionBlock from "../../../components/section-block"
import Navigation from "../../../components/big-event/navigation"
import Fringe from "../../../components/big-event/fringe"
import Titlebar from "../../../components/big-event/titlebar"
import BodyText from "../../../components/body-text"
import Footer from "../../../components/big-event/footer"

const BigEventSponsors = props => {
  const { data, location } = props
  console.log("data ->", data)

  // GET DOCUMENT DATA
  const doc = data.prismic.allBig_event_volunteerss.edges.slice(0, 1).pop()
  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    page_title,
    body_copy,
    image,
    body,
  } = doc.node

  // GET EVENT DATA
  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log("event", event)

  const parentPage = {
    title: event.name,
    url: `/${event._meta.uid}`,
  }
  return (
    <Layout
      title={meta_title || page_title}
      parent={parentPage}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={doc.node._meta.id} />
      <Navigation {...event} id={_meta.id} location={location} />
      <Titlebar title={page_title} />
      <div className={styles.contentWrapper}>
        <div className="container">
          <div className="row">
            <div className="offset-md-1 col-md-7">
              {renderAsHtml(body_copy)}
            </div>
            <div className="col-md-3">
              <img
                src={renderImageUrl(image)}
                alt={image.alt}
                className={`img-fluid ${styles.image}`}
              />
            </div>
          </div>
        </div>
      </div>
      {body
        .filter(d => d.type === "simple_columns")
        .map(slice => {
          return (
            <Slicer
              key={slice.__typename}
              data={slice}
              inset={false}
              shading="even"
            />
          )
        })}
      <Footer {...event} />
    </Layout>
  )
}

export default BigEventSponsors

export const query = graphql`
  query GloWildBigEventVolunteersQuery($uid: String, $event: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
            }
            name
            wide_image
            small_image
            hero_foreground
            hero_foreground_only
            primary_navigation_background_color
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            footer_visible
            mobile_navigation_description
            footer_navigation_background_color
            footer_navigation_text_color
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
      allBig_event_volunteerss(uid: $uid) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            page_title
            body_copy
            image
            body {
              ... on PRISMIC_Big_event_volunteersBodySimple_columns {
                type
                label
                fields {
                  simple_columns_color
                  simple_columns_body_text
                  simple_columns_cta {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                  }
                  simple_columns_cta_label
                }
              }
            }
          }
        }
      }
    }
  }
`
