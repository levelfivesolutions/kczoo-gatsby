import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
import moment from "moment"
import Layout from "../../../components/layout/layout"
import SEO from "../../../components/seo"
import Hero from "../../../components/big-event/glo-wild/hero"
import SlicerR from "../../../components/slice-r/slice-r"
import { renderAsHtml, renderAsText, filterArrayByType } from "../../../utility"
import styles from "./detail.module.css"
import Titlebar from "../../../components/big-event/glo-wild/titlebar"
import Navigation from "../../../components/big-event/glo-wild/navigation"
import Title from "../../../components/title"
import Footer from "../../../components/big-event/glo-wild/footer"
import ticketStyles from "../../../pages/tickets-and-pricing.module.css"
import Button from "../../../components/button/button-link"
import { Modal } from "react-bootstrap"
import Dropdown from "../../../components/big-event/glo-wild/dropdown/dropdown"
import ModalDropdown from "../../../components/big-event/glo-wild/dropdown/modal-dropdown"

const BigEventTicketPage = props => {
  const { data, location } = props
  console.log("data ->", data)
  const [openModals, setOpenModals] = useState({})
  const [popupOpen, setPopupOpen] = useState(false)
  const doc = data.prismic.allBig_event_ticket_pages.edges.slice(0, 1).pop()
  console.log("DOC", doc)

  useEffect(() => {
    setPopupOpen(true)
  }, [])

  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    ticket_page_title,
    ticket_options_section,
    ticket_page_body_text,
    body,
  } = doc.node

  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log("event", event)

  let modal = filterArrayByType(body, "interstitial_modal")[0]
  const banner = filterArrayByType(body, "cta")

  /**
   * @param id - the id of the modal to show
   */
  function showDynamicModal(id) {
    let newModal = { ...openModals }
    newModal[id] = true
    setOpenModals({ ...newModal })
  }

  /**
   * @param id - the id of the modal to hide
   */
  function hideDynamicModal(id) {
    let newModal = { ...openModals }
    newModal[id] = false
    setOpenModals({ ...newModal })
  }

  const parentPage = {
    title: event.name,
    url: `/${event._meta.uid}`,
  }
  return (
    <Layout
      title={meta_title || ticket_page_title}
      parent={parentPage}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={doc.node._meta.id} />
      <Navigation {...event} id={_meta.id} location={location} />
      <Titlebar color={event.heading_color} title={ticket_page_title} />
      <div className="container">
        {banner.length > 0 && (
          <div>
            {banner.map(slice => {
              return <SlicerR data={slice} />
            })}
          </div>
        )}
        <div className="row d-flex justify-content-center mb-3">
          <div
            className="col-12 col-md-8 text-center fw-bold"
            style={{
              fontFamily: "DINOTBold",
              fontSize: "24px",
              lineHeight: "48px",
            }}
          >
            {renderAsHtml(ticket_page_body_text)}
          </div>
        </div>
        <div className="row d-flex justify-content-center">
          {ticket_options_section.map((d, i) => {
            console.log("d ->", d)
            return (
              <>
                <div
                  className={`col-lg-4 d-flex ${ticketStyles.ticketOptionBlockContainer}`}
                  key={i}
                >
                  <div
                    className={ticketStyles.ticketOptionBlock}
                    style={{ backgroundColor: d.ticket_option_color }}
                  >
                    <div
                      className={`${ticketStyles.ticketOptionBlockHeader} position-relative`}
                    >
                      {renderAsText(d.ticket_option_title)}
                      {d.ticket_option_info_popup_body &&
                        d.ticket_option_info_popup_title && (
                          <div
                            onClick={() => {
                              showDynamicModal(`ticketOption${i}`)
                            }}
                            onKeyPress={() => {
                              showDynamicModal(`ticketOption${i}`)
                            }}
                            className={styles.modalInfoButton}
                            style={{ color: d.ticket_option_color }}
                          >
                            i
                          </div>
                        )}
                    </div>
                    <div className={ticketStyles.ticketOptionBlockBody}>
                      <p className={ticketStyles.ticketPricing}>
                        {d.ticket_option_description}
                      </p>
                      <div className="d-flex row m-auto justify-content-center align-items-center">
                        <div
                          className={`col ${ticketStyles.ticketPrices}`}
                          dangerouslySetInnerHTML={{
                            __html: d?.ticket_option_price?.replace(
                              /\$([0-9]{1,2})/g,
                              "<sup style='top: -.3em'>$</sup><span>$1"
                            ),
                          }}
                        ></div>
                      </div>
                      <p>{d.ticket_option_quantity_description}</p>
                    </div>
                    <div className={ticketStyles.ticketOptionBlockFooter}>
                      {d.ticket_option_title &&
                        renderAsText(d.ticket_option_title).toUpperCase() !==
                          "GROUP TICKETS" && (
                          <Button
                            color={d.ticket_option_color}
                            isInverse
                            text={d.ticket_option_link_text}
                            url={d.ticket_option_link?.url || null}
                            size="large"
                          />
                        )}
                      {d.ticket_option_title &&
                        renderAsText(d.ticket_option_title).toUpperCase() ===
                          "GROUP TICKETS" && <Dropdown />}
                      <p
                        className={ticketStyles.ctaDetail}
                        style={{ marginTop: 10 }}
                      >
                        {renderAsHtml(d.ticket_option_link_callout) || (
                          <span style={{ opacity: 0 }} aria-hidden="true">
                            .
                          </span>
                        )}
                      </p>
                    </div>
                  </div>
                </div>
                {d.ticket_option_info_popup_body &&
                  d.ticket_option_info_popup_title && (
                    <Modal
                      show={openModals[`ticketOption${i}`]}
                      onHide={() => {
                        hideDynamicModal(`ticketOption${i}`)
                      }}
                      size="lg"
                      dialogClassName={styles.modal}
                    >
                      <Modal.Header
                        closeButton
                        className={styles.modalHeader}
                      ></Modal.Header>
                      <Modal.Body className={`${styles.modalBody} text-center`}>
                        <h2
                          style={{
                            color: d.ticket_option_color,
                          }}
                          className={styles.modalTitle}
                        >
                          {d.ticket_option_info_popup_title}
                        </h2>
                        {renderAsHtml(d.ticket_option_info_popup_body)}
                        <div className={styles.modalButton}>
                          {d.ticket_option_info_popup_title &&
                            renderAsText(
                              d.ticket_option_info_popup_title
                            ).toUpperCase() !== "GROUP TICKETS" && (
                              <Button
                                color={d.ticket_option_color}
                                text={d.ticket_option_link_text}
                                url={d.ticket_option_link?.url || null}
                                size="large"
                              />
                            )}
                          {d.ticket_option_info_popup_title &&
                            renderAsText(
                              d.ticket_option_info_popup_title
                            ).toUpperCase() === "GROUP TICKETS" && (
                              <ModalDropdown />
                            )}
                        </div>
                      </Modal.Body>
                    </Modal>
                  )}

                {renderAsText(modal?.primary?.interstitial_modal_heading)
                  ?.length > 0 && (
                  <Modal
                    show={popupOpen}
                    onHide={() => {
                      setPopupOpen(false)
                    }}
                    size="lg"
                    dialogClassName={styles.modal}
                  >
                    <Modal.Header closeButton className={styles.modalHeader}>
                      <Title
                        color={event.heading_color}
                        className={styles.dinRoundOT}
                      >
                        {renderAsText(modal.primary.interstitial_modal_heading)}
                      </Title>
                    </Modal.Header>
                    <Modal.Body className={styles.modalBody}>
                      {renderAsHtml(modal.primary.interstitial_modal_body_text)}
                    </Modal.Body>
                  </Modal>
                )}
              </>
            )
          })}
        </div>
      </div>
      <Footer {...event} />
    </Layout>
  )
}

export default BigEventTicketPage

export const query = graphql`
  query GloWildBigEventTicketPageQuery($event: String, $id: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
            }
            name
            wide_image
            heading_color
            small_image
            hero_foreground
            hero_foreground_only
            primary_navigation_background_color
            footer_navigation_background_color
            footer_visible
            mobile_navigation_description
            footer_navigation_text_color
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
      allBig_event_ticket_pages(id: $id) {
        edges {
          node {
            _meta {
              id
              uid
              tags
            }
            _linkType
            meta_description
            ticket_options_section {
              ticket_option_color
              ticket_option_description
              ticket_option_info_popup_body
              ticket_option_info_popup_title
              ticket_option_link_callout
              ticket_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  target
                  _linkType
                  url
                }
              }
              ticket_option_link_text
              ticket_option_price
              ticket_option_quantity_description
              ticket_option_title
            }
            ticket_page_body_text
            ticket_page_title
            meta_title
            body {
              ... on PRISMIC_Big_event_ticket_pageBodyInterstitial_modal {
                type
                label
                primary {
                  interstitial_modal_body_text
                  interstitial_modal_heading
                }
              }
              ... on PRISMIC_Big_event_ticket_pageBodyCta {
                type
                label
                fields {
                  cta_banner_color
                  cta_label
                  cta_text
                  cta_title
                  icon
                  cta_link {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                  }
                  cta_option_1
                  cta_option_1_link {
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                  }
                  cta_option_2
                  cta_option_2_link {
                    ... on PRISMIC__ExternalLink {
                      target
                      _linkType
                      url
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
