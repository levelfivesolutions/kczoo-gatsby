import React from "react"
import { graphql } from "gatsby"
import Layout from "../../../components/layout/layout"
import SEO from "../../../components/seo"
import Hero from "../../../components/big-event/hero"
import { filterArrayByType } from "../../../utility"
import styles from "./landing.module.css"
import SectionBlock from "../../../components/section-block"
import Navigation from "../../../components/big-event/navigation"
import Fringe from "../../../components/big-event/fringe"
import Titlebar from "../../../components/big-event/titlebar"
import Footer from "../../../components/big-event/footer"
import CardLanding from "../../../components/card-landing"

const BigEventLanding = props => {
  const { data, location } = props
  console.log("data ->", data)
  const landingDoc = data.prismic.allBig_event_landing_pages.edges
    .slice(0, 1)
    .pop()

  console.log("DOC", landingDoc)
  if (!landingDoc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    page_title,
    summary,
    body,
  } = landingDoc.node

  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log("event", event)

  const parentPage = {
    title: event.name,
    url: `/${event._meta.uid}`,
  }

  let featuredItems = filterArrayByType(body, "feature_block").pop()
  featuredItems =
    typeof featuredItems !== "undefined" ? featuredItems.fields : null
  return (
    <Layout
      parent={parentPage}
      title={meta_title || page_title}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={landingDoc.node._meta.id} />
      <Navigation {...event} id={_meta.id} location={location} />
      <Titlebar
        color={event.heading_color}
        title={page_title}
        introduction={summary}
      />
      <span aria-hidden style={{ marginBottom: "30px" }} />
      {featuredItems &&
        featuredItems.map((d, i) => {
          return (
            <SectionBlock key={i} shading="even">
              <div className="container">
                <div className="row">
                  <div className="col-md-12">
                    <CardLanding
                      color={d.color_name}
                      image={d.callout_image}
                      season=""
                      year=""
                      minAge=""
                      maxAge=""
                      title={d.callout_title}
                      description={d.callout_description}
                      urlText={d.cta_text}
                      link={d.cta}
                      cta_thumbnail={d.cta_thumbnail}
                    />
                  </div>
                </div>
              </div>
            </SectionBlock>
          )
        })}
      <Footer {...event} />
    </Layout>
  )
}

export default BigEventLanding

export const query = graphql`
  query GloWildBigEventLandingTemplateQuery($uid: String, $event: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
            }
            name
            wide_image
            small_image
            hero_foreground
            hero_foreground_only
            heading_color
            primary_navigation_background_color
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            footer_navigation_background_color
            footer_visible
            mobile_navigation_description
            footer_navigation_text_color
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
      allBig_event_landing_pages(uid: $uid) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            page_title
            summary
            body {
              ... on PRISMIC_Big_event_landing_pageBodyFeature_block {
                type
                label
                fields {
                  callout_image
                  callout_title
                  callout_description
                  cta_text
                  cta_thumbnail
                  cta: cta1 {
                    _linkType
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                  }
                  color_name
                }
              }
            }
          }
        }
      }
    }
  }
`
