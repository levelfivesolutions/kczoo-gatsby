import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/big-event/hero"
import Slicer from "../../components/slice-r/slice-r"
import {
  renderAsHtml,
  renderImageUrl,
  ColorToHex,
  linkResolver,
} from "../../utility"
import styles from "./sponsors.module.scss"
import SectionBlock from "../../components/section-block"
import Navigation from "../../components/big-event/navigation"
import Fringe from "../../components/big-event/fringe"
import Titlebar from "../../components/big-event/titlebar"
import Footer from "../../components/big-event/footer"
import Icon from "../../components/icon/icon"
import SmartLink from "../../components/smart-link"

function Separator({ icon, name, color, className }) {
  const style = {
    backgroundColor: ColorToHex(color),
  }
  return (
    <div className={`${styles.separator} ${className}`}>
      <div className={styles.separatorCenter}>
        <div className={styles.iconContainer} style={style}>
          <Icon icon={icon} width="25" color="White" />
        </div>
        <h6 className={styles.separatorTitle}>{name}</h6>
      </div>
    </div>
  )
}

const BigEventSponsors = props => {
  const { data, location } = props
  console.log("data ->", data)

  const doc = data.prismic.allBig_event_sponsorss.edges.slice(0, 1).pop()
  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    page_title,
    introduction,
    presenting_sponsor,
    presenting_sponsor_link,
    sponsors_tier_1,
    sponsors_tier_2,
    sponsors_tier_3,
    sponsors_tier_4,
    sponsors_tier_5,
    sponsors_tier_6,
    sponsors_tier_7,
  } = doc.node

  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log("event", event)

  const parentPage = {
    title: event.name,
    url: `/${event._meta.uid}`,
  }
  return (
    <Layout
      title={meta_title || page_title}
      parent={parentPage}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={doc.node._meta.id} />
      <Navigation {...event} id={_meta.id} location={location} />
      <Titlebar
        color={event.heading_color}
        title={page_title}
        introduction={introduction}
      />
      <div className="container">
        <section className={styles.section}>
          <Separator icon="hippo" name="HIPPO" color="Jazzoo 2021 Orange" />
          <div className="row">
            <div className="col-md-4 offset-md-4 d-flex justify-content-center">
              <SmartLink link={presenting_sponsor_link}>
                <img
                  src={renderImageUrl(presenting_sponsor)}
                  alt={presenting_sponsor.alt}
                  className="img-fluid"
                />
              </SmartLink>
            </div>
          </div>
        </section>
        <section className={styles.section}>
          <div className="row">
            <div className="offset-md-1 col-md-10">
              <Separator
                icon="elephant"
                name="ELEPHANT"
                color="Jazzoo 2021 Green"
              />
              <div className="row">
                {sponsors_tier_1.map(d => {
                  return (
                    <div className="col-md-4 d-flex align-items-center">
                      <SmartLink link={d.link}>
                        <img
                          src={renderImageUrl(d.logo)}
                          alt={d.logo.alt}
                          className="img-fluid"
                        />
                      </SmartLink>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </section>
        <section className={styles.section}>
          <div className="row">
            <div className="offset-md-1 col-md-10">
              <Separator
                icon="baboon"
                name="BABOON"
                color="Jazzoo 2021 Blue"
                className="mb-3"
              />
              <div className={styles.columns}>
                {sponsors_tier_2.map(d => {
                  return (
                    <div className={styles.linkWrapper}>
                      <SmartLink link={d.link}>{d.name}</SmartLink>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </section>
        <section className={styles.section}>
          <div className="row">
            <div className="offset-md-1 col-md-10">
              <Separator
                icon="lion"
                name="LION"
                color="Jazzoo 2021 Red"
                className="mb-3"
              />
              <div className={styles.columns}>
                {sponsors_tier_3.map(d => {
                  return (
                    <div className={styles.linkWrapper}>
                      <SmartLink link={d.link}>{d.name}</SmartLink>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </section>
        <section className={styles.section}>
          <div className="row">
            <div className="offset-md-1 col-md-10">
              <Separator
                icon="giraffe"
                name="GIRAFFE"
                color="Jazzoo 2021 Light Green"
                className="mb-3"
              />
              <div className={styles.columns}>
                {sponsors_tier_4.map(d => {
                  return (
                    <div className={styles.linkWrapper}>
                      <SmartLink link={d.link}>{d.name}</SmartLink>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </section>
        <section className={styles.section}>
          <div className="row">
            <div className="offset-md-1 col-md-10">
              <Separator
                icon="monkey"
                name="CHIMP"
                color="Jazzoo 2021 Orange"
                className="mb-3"
              />
              <div className={styles.columns}>
                {sponsors_tier_5.map(d => {
                  return (
                    <div className={styles.linkWrapper}>
                      <SmartLink link={d.link}>{d.name}</SmartLink>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </section>
        <section className={styles.section}>
          <div className="row">
            <div className="offset-md-1 col-md-10">
              <Separator
                icon="flamingo"
                name="FLAMINGO"
                color="Jazzoo 2021 Green"
                className="mb-3"
              />
              <div className={styles.columns}>
                {sponsors_tier_6.map(d => {
                  return (
                    <div className={styles.linkWrapper}>
                      <SmartLink link={d.link}>{d.name}</SmartLink>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </section>
        <section className={styles.section}>
          <div className="row">
            <div className="offset-md-1 col-md-10">
              <Separator
                icon="gift"
                name="IN KIND SPONSORS"
                color="Jazzoo 2021 Blue"
                className="mb-3"
              />
              <div className={styles.columns}>
                {sponsors_tier_7.map(d => {
                  return (
                    <div className={styles.linkWrapper}>
                      <SmartLink link={d.link}>{d.name}</SmartLink>
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer {...event} />
    </Layout>
  )
}

export default BigEventSponsors

export const query = graphql`
  query BigEventSponsorsQuery($uid: String, $event: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
            }
            name
            wide_image
            small_image
            hero_foreground
            hero_foreground_only
            primary_navigation_background_color
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            footer_navigation_background_color
            footer_visible
            heading_color
            footer_navigation_text_color
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
      allBig_event_sponsorss(uid: $uid) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            page_title
            introduction
            presenting_sponsor
            presenting_sponsor_link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            presenting_sponsor_name
            sponsors_tier_1 {
              logo
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_2 {
              logo
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_3 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_4 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_5 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_6 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
            sponsors_tier_7 {
              name
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
