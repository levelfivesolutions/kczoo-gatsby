import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/big-event/hero"
import Slicer from "../../components/slice-r/slice-r"
import { renderAsHtml } from "../../utility"
import styles from "./home.module.css"
import SectionBlock from '../../components/section-block'
import BigEventNavigation from "../../components/big-event/navigation"
import TitleBar from "../../components/big-event/titlebar"
import JazzooCarousel from "../../components/big-event/carousel"
import CalendarDay from "../../components/calendar-day"
import JazzooFooter from "../../components/big-event/footer"

const BigEventHome = props => {
  const { data, location } = props
  const doc = data.prismic.allBig_event_homes.edges.slice(0, 1).pop()

  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    body,
    body_text,
    page_title,
    event_date,
    date_description,
    carousel,
    primary_sponsor,
    primary_sponsor_introduction,
  } = doc.node

  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log('event', event)

  return (
    <Layout title={meta_title || page_title} section="activities" {...props}>
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={doc.node._meta.id} />
      <BigEventNavigation {...event} id={_meta.id} location={location} />
        <SectionBlock>
        <TitleBar color={event.heading_color} title={page_title}/>
          <div className="container mb-4">
            <div className="row">
              <div className="offset-md-1 col-md-5">
                <div className={styles.carouselContainer}>
                  <JazzooCarousel images={carousel} />
                </div>
              </div>
              <div className="col-md-5">
                <div className={styles.calendarEvent}>
                  <div className={styles.calendarDay}>
                    <CalendarDay date={event_date} color={event.heading_color} size='large' />
                  </div>
                  <div className={styles.calendarDesc}>
                    {renderAsHtml(date_description)}
                  </div>
                </div>
                {
                  renderAsHtml(body_text)
                }
                {
                  primary_sponsor_introduction &&
                  <div style={{ textAlign: "center", marginTop: 30 }}>{renderAsHtml(primary_sponsor_introduction)}</div>
                }
                {
                  primary_sponsor &&
                  <div className="d-flex align-items-center justify-content-center">
                    <img src={primary_sponsor?.url} alt={primary_sponsor?.alt} />
                  </div>
                }
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="offset-md-1 col-md-10">
                {
                  body && body.filter(d => d.type === 'cta').map(slice => {
                    return <Slicer key={slice.__typename} data={slice} />
                  })
                }
                {
                  body && body.filter(d => d.type === 'info_callout').map(slice => {
                    return <Slicer key={slice.__typename} data={slice} />
                  })
                }
              </div>
            </div>
          </div>
        </SectionBlock>
        {
          body && body.filter(d => d.type === 'column').map(slice => {
            return <Slicer key={slice.__typename} data={slice} />
          })
        }
      <JazzooFooter {...event} />
    </Layout>
  )
}

export default BigEventHome;

export const query = graphql`
query BigEventHomeQuery($uid: String, $event: String) {
  prismic {
    allBig_events(uid: $event) {
      edges {
        node {
          _meta {
            uid
          }
          name
          wide_image
          small_image
          hero_foreground
          heading_color
          hero_foreground_only
          primary_navigation {
            page_link {
              _linkType
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
              ... on PRISMIC__ExternalLink {
                _linkType
                url
                target
              }
            }
            page_title
          }
          get_tickets_link_text
          get_tickets_link_option {
            get_tickets_link_option_link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            get_tickets_link_option_text
          }
          footer_navigation {
            page_link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
            page_title
          }
          social_media {
            social_media_image
            social_media_link {
              _linkType
              ... on PRISMIC__ExternalLink {
                _linkType
                url
              }
              ... on PRISMIC__Document {
                _meta {
                  id
                  tags
                  type
                  uid
                }
              }
            }
          }
          primary_navigation_background_color
          footer_navigation_background_color
          footer_visible
          footer_navigation_text_color
          footer_visible
        }
      }
    }
    allBig_event_homes(uid: $uid) {
      edges {
        node {
          _meta {
            id
          }
          page_title
          event_date
          date_description
          body_text
          meta_title
          meta_description
          primary_sponsor_introduction
          primary_sponsor
          carousel {
            image
          }
          body {
            ... on PRISMIC_Big_event_homeBodyInfo_callout {
              type
              label
              primary {
                info_description
              }
              fields {
                info_link {
                  _linkType
                  ... on PRISMIC__Document {
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                    target
                  }
                }
                info_link_title
              }
            }
            ... on PRISMIC_Big_event_homeBodyColumn {
              type
              label
              primary {
                column_section_title
              }
              fields {
                color
                column_image
                column_text
                column_title
                plu
                plu_description
                column_cta_label
                column_cta {
                  _linkType
                  ... on PRISMIC__Document {
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                    target
                  }
                }
              }
            }
            ... on PRISMIC_Big_event_homeBodyCta {
              type
              label
              fields {
                icon
                color
                cta_label
                cta_text
                cta_title
                cta_link {
                  _linkType
                  ... on PRISMIC__Document {
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

`
