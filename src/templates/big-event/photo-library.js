import React, { useState, useEffect } from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/big-event/hero"
import { renderImageUrl } from "../../utility"
import styles from "./photo-library.module.scss"
import Navigation from "../../components/big-event/navigation"
import Fringe from "../../components/big-event/fringe"
import Titlebar from "../../components/big-event/titlebar"
import Footer from "../../components/big-event/footer"
import Button from "../../components/button"

const PHOTOS_PER_PAGE = 20
const UNDEFINED_IMAGE = { index: -1, selectedImage: { image: "" } }

const PhotoLibrary = props => {
  const [page, setPage] = useState(1)
  const [image, setImage] = useState(UNDEFINED_IMAGE)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    window.$("#modal-photo").on("hidden.bs.modal", () => {
      setImage(UNDEFINED_IMAGE)
    })
  }, [])

  const { data, location } = props
  console.log("data ->", data)
  const doc = data.prismic.allBig_event_photo_librarys.edges.slice(0, 1).pop()

  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    page_title,
    introduction,
    photos,
  } = doc.node

  // GET EVENT DATA
  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log("event", event)

  const parentPage = {
    title: event.name,
    url: `/${event._meta.uid}`,
  }

  let elements = []
  for (let i = 0; i < page * PHOTOS_PER_PAGE; i++) {
    if (i === photos.length) {
      break
    }

    elements.push(photos[i])
  }

  let hasButton = page * PHOTOS_PER_PAGE < photos.length

  function selectImage(d, i) {
    console.log(i, d)
    setLoading(true)
    setImage({ index: i, selectedImage: d })
  }

  function handleImageLoaded() {
    window.$("#modal-photo").modal("show")
    setTimeout(() => {
      setLoading(false)
    }, 1000)
  }

  function handleClose(ev) {
    window.$("#modal-photo").modal("hide")
  }

  return (
    <Layout
      parent={parentPage}
      title={meta_title || meta_description}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={doc.node._meta.id} />
      <Navigation {...event} id={_meta.id} location={location} />
      <Titlebar
        color={event.heading_color}
        title={page_title}
        introduction={introduction}
      />
      <div className={styles.gridContainer}>
        <div className="container">
          <div className="row">
            <div className="offset-md-1 offset-sm-0 col-md-10 col-sm-12">
              <div className="row">
                {elements.map((d, i) => {
                  let showSpinner = i === image.index && loading

                  return (
                    <div
                      key={i}
                      className={`col-md-3 col-sm-6 ${styles.gridCell}`}
                    >
                      <img
                        src={renderImageUrl(d.image.Thumbnail)}
                        alt={d.image.alt}
                        className="img-fluid"
                        onClick={ev => {
                          selectImage(d, i)
                        }}
                      />
                      {showSpinner && (
                        <div
                          className={`spinner-border ${styles.spinner}`}
                        ></div>
                      )}
                    </div>
                  )
                })}
              </div>
            </div>
          </div>
          {hasButton && (
            <div className={styles.buttonContainer}>
              <Button onClick={ev => setPage(page + 1)} color="Zoo Green">
                More Photos
              </Button>
            </div>
          )}
        </div>
      </div>
      <Footer {...event} />
      <div id="modal-photo" className="modal" tabindex="-1">
        <div className={`modal-dialog modal-xl ${styles.modalDialog}`}>
          <div className={`modal-content ${styles.modalContent}`}>
            <div className={`modal-header ${styles.modalHeader}`}>
              <button
                type="button"
                className={`${styles.modalClose}`}
                onClick={handleClose}
              ></button>
            </div>
            <div className={`modal-body ${styles.modalBody}`}>
              <img
                src={renderImageUrl(image.selectedImage.image)}
                alt=""
                className="img-fluid"
                onLoad={handleImageLoaded}
              />
            </div>
            <div className={`modal-footer ${styles.modalFooter}`}>
              <div>{image.selectedImage.image_title}</div>
              <div>
                {image.index + 1}/{elements.length}
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default PhotoLibrary

export const query = graphql`
  query BigEventPhotoLibraryQuery($uid: String, $event: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
            }
            name
            wide_image
            small_image
            hero_foreground
            hero_foreground_only
            primary_navigation_background_color
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            footer_navigation_background_color
            footer_visible
            heading_color
            footer_navigation_text_color
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
      allBig_event_photo_librarys(uid: $uid) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            page_title
            introduction
            photos {
              image_title
              image
            }
          }
        }
      }
    }
  }
`
