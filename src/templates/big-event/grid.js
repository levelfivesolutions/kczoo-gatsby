import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/big-event/hero"
import { renderImageUrl } from "../../utility"
//import styles from "./grid.module.scss"
import Navigation from "../../components/big-event/navigation"
import Fringe from "../../components/big-event/fringe"
import Titlebar from "../../components/big-event/titlebar"
import Footer from "../../components/big-event/footer"

const BigEventGrid = props => {
  const { data, location } = props
  console.log("data ->", data)

  const doc = data.prismic.allBig_event_grid_layouts.edges.slice(0, 1).pop()
  console.log("DOC", doc)
  if (!doc) return null
  const {
    _meta,
    page_title,
    introduction,
    items,
    meta_title,
    meta_description,
  } = doc.node

  // GET EVENT DATA
  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log("event", event)

  const parentPage = {
    title: event.name,
    url: `/${event._meta.uid}`,
  }
  return (
    <Layout
      parent={parentPage}
      title={meta_title || page_title}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={doc.node._meta.id} />
      <Navigation {...event} id={_meta.id} location={location} />
      <Titlebar
        color={event.heading_color}
        title={page_title}
        introduction={introduction}
      />
      <div className="container">
        <div className="row" style={{ paddingBottom: 80 }}>
          {items.map(element => {
            console.log("element", element)
            return (
              <div className="col-md-3 d-flex justify-content-center">
                <img
                  src={renderImageUrl(element.image)}
                  alt={element.image?.alt}
                  className="img-fluid"
                />
              </div>
            )
          })}
        </div>
      </div>
      <Footer {...event} />
    </Layout>
  )
}

export default BigEventGrid

export const query = graphql`
  query BigEventGridQuery($uid: String, $event: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
            }
            name
            wide_image
            small_image
            hero_foreground
            hero_foreground_only
            primary_navigation_background_color
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            footer_navigation_background_color
            heading_color
            footer_visible
            footer_navigation_text_color
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
      allBig_event_grid_layouts(uid: $uid) {
        edges {
          node {
            _meta {
              id
              tags
              type
              uid
            }
            meta_title
            meta_description
            page_title
            introduction
            items {
              image
              link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
