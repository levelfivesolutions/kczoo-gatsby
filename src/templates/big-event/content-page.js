import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/big-event/hero"
import Slicer from "../../components/slice-r/slice-r"
//import { renderAsHtml, renderImageUrl } from "../../utility"
//import styles from "./content-page.module.css"
import SectionBlock from "../../components/section-block"
import Navigation from "../../components/big-event/navigation"
import Fringe from "../../components/big-event/fringe"
import Titlebar from "../../components/big-event/titlebar"
import Footer from "../../components/big-event/footer"

const BigEventContentPage = props => {
  const { data, location } = props
  console.log("data ->", data)
  const doc = data.prismic.allBig_event_content_pages.edges.slice(0, 1).pop()
  console.log("DOC", doc)

  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    page_title,
    introduction,
    body,
  } = doc.node

  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log("event", event)

  const parentPage = {
    title: event.name,
    url: `/${event._meta.uid}`,
  }
  return (
    <Layout
      title={meta_title || page_title}
      parent={parentPage}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} id={_meta.id} pageId={doc.node._meta.id} />
      <Navigation {...event} id={_meta.id} location={location} />
      <Titlebar
        color={event.heading_color}
        title={page_title}
        introduction={introduction}
      />
      {body &&
        body.map(slice => {
          return <Slicer key={slice.__typename} data={slice} shading="none" />
        })}
        <div style={{marginBottom: 60}}></div>
      <Footer {...event} />
    </Layout>
  )
}

export default BigEventContentPage

export const query = graphql`
  query BigEventContentPageQuery($uid: String, $event: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
              id
            }
            name
            wide_image
            small_image
            hero_foreground
            hero_foreground_only
            heading_color
            primary_navigation_background_color
            footer_navigation_background_color
            footer_visible
            footer_navigation_text_color
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }

      allBig_event_content_pages(uid: $uid) {
        edges {
          node {
            _meta {
              uid
              id
            }
            page_title
            introduction
            body {
              ... on PRISMIC_Big_event_content_pageBodyDetail_page_content {
                type
                label
                fields {
                  detail_page_content_body_text
                  detail_page_content_optional_image
                  detail_page_content_summary
                  detail_page_content_title
                  detail_page_content_under_optional_image_caption
                }
              }
              ... on PRISMIC_Big_event_content_pageBodyImage_text_block {
                type
                label
                primary {
                  image_text_color
                  image_text_image
                  image_text_image_side
                  image_text_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                  }
                  image_text_link_text
                  image_text_text
                  image_text_title
                }
              }
            }
          }
        }
      }
    }
  }
`
