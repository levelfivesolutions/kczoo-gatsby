import React from "react"
import { graphql } from "gatsby"
import moment from "moment"
import Layout from "../../components/layout/layout"
import SEO from "../../components/seo"
import Hero from "../../components/big-event/hero"
import Slicer from "../../components/slice-r/slice-r"
import { renderAsHtml, renderImageUrl } from "../../utility"
import styles from "./detail.module.css"
import SectionBlock from "../../components/section-block"
import Navigation from "../../components/big-event/navigation"
import Fringe from "../../components/big-event/fringe"
import Title from "../../components/title"
import CalendarDay from "../../components/calendar-day"
import Footer from "../../components/big-event/footer"

const BigEventEventDetail = props => {
  const { data, location } = props
  console.log("data ->", data)
  const doc = data.prismic.allBig_event_details.edges.slice(0, 1).pop()
  console.log("DOC", doc)

  if (!doc) return null
  const {
    _meta,
    meta_title,
    meta_description,
    page_title,
    image,
    when,
    when_description,
    where,
    attire,
    body,
  } = doc.node

  const event = data.prismic.allBig_events.edges.slice(0, 1).pop().node
  console.log("event", event)

  const parentPage = {
    title: event.name,
    url: `/${event._meta.uid}`,
  }
  return (
    <Layout
      title={meta_title || page_title}
      parent={{
        title: "Jazzoo",
        url: "/jazzoo",
      }}
      section="activities"
      {...props}
    >
      <SEO
        title={meta_title}
        description={meta_description}
        canonical={location.origin + location.pathname}
      />
      <Hero {...event} pageId={doc.node._meta.id} />
      <Navigation {...event} id={_meta.id} location={location} />
      <SectionBlock>
        <Title color="Pumpkin" className={styles.title}>
          {page_title}
        </Title>
        <div className="container">
          <div className="row">
            <div className="offset-md-1 col-md-7">
              <h2 className={styles.sectionTitle}>WHEN</h2>
              <div className={styles.calendarEvent}>
                <div className={styles.calendarDay}>
                  <CalendarDay date={when} color="Pumpkin" size="large" />
                </div>
                <div className={styles.calendarDesc}>
                  <div>{moment(when).format("dddd, LL")}</div>
                  <div>
                    <small>{renderAsHtml(when_description)}</small>
                  </div>
                </div>
              </div>
              <div>
                <h2 className={styles.sectionTitle}>WHERE</h2>
                {renderAsHtml(where)}
                <h2 className={styles.sectionTitle}>ATTIRE</h2>
                {renderAsHtml(attire)}
              </div>
            </div>
            <div className={`col-md-3`}>
              <img
                src={renderImageUrl(image)}
                alt={image.alt}
                className="img-fluid"
              />
            </div>
          </div>
          <div className="row">
            <div className="offset-md-1 col-md-10">
              {body &&
                body
                  .filter(d => d.type === "info_callout")
                  .map(slice => {
                    return (
                      <div className="mt-3">
                        <Slicer key={slice.__typename} data={slice} />
                      </div>
                    )
                  })}
              {body &&
                body
                  .filter(d => d.type === "cta")
                  .map(slice => {
                    return (
                      <div className="mt-5">
                        <Slicer
                          key={slice.__typename}
                          data={slice}
                          className="mt-4"
                        />
                      </div>
                    )
                  })}
            </div>
          </div>
        </div>
      </SectionBlock>
      <Footer {...event} />
    </Layout>
  )
}

export default BigEventEventDetail

export const query = graphql`
  query BigEventEventDetailQuery($uid: String, $event: String) {
    prismic {
      allBig_events(uid: $event) {
        edges {
          node {
            _meta {
              uid
            }
            name
            wide_image
            small_image
            hero_foreground
            hero_foreground_only
            primary_navigation_background_color
            footer_navigation_background_color
            footer_visible
            footer_navigation_text_color
            get_tickets_link_text
            get_tickets_link_option {
              get_tickets_link_option_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              get_tickets_link_option_text
            }
            primary_navigation {
              page_link {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                  target
                }
              }
              page_title
            }
            footer_navigation {
              page_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
              page_title
            }
            social_media {
              social_media_image
              social_media_link {
                _linkType
                ... on PRISMIC__ExternalLink {
                  _linkType
                  url
                }
                ... on PRISMIC__Document {
                  _meta {
                    id
                    tags
                    type
                    uid
                  }
                }
              }
            }
          }
        }
      }
      allBig_event_details(uid: $uid) {
        edges {
          node {
            meta_title
            meta_description
            page_title
            image
            when
            when_description
            where
            attire
            body {
              ... on PRISMIC_Big_event_detailBodyInfo_callout {
                type
                label
                primary {
                  info_description
                }
                fields {
                  info_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                  }
                  info_link_title
                }
              }
              ... on PRISMIC_Big_event_detailBodyCta {
                type
                label
                fields {
                  color
                  cta_label
                  cta_link {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                      target
                    }
                  }
                  cta_text
                  cta_title
                  icon
                }
              }
            }
            _meta {
              id
              uid
              type
              tags
            }
          }
        }
      }
    }
  }
`
