import React from 'react';
import { graphql } from 'gatsby';
import { RichText } from 'prismic-reactjs';
import { filterArrayByType } from '../utility.js';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import Hero from "../components/hero-detail";
import BodyText from "../components/body-text";
import Text from "../components/text";
import moment from 'moment';
import Button from '../components/button/button-link.js';
import AddOnBlock from "../components/add-on/add-on-block";
import styles from './guided-tour.module.css';

class GuidedTour extends React.Component {
	render() {
		let { data } = this.props;
		console.log('props', this.props);
		const doc = data.prismic.allSpecial_experiences.edges.slice(0,1).pop();
		console.log('doc -->', doc);
		if (!doc) return null;
		
		const { wide_image, title, body_text, meta_title, meta_description } = doc.node;
		
		/*
			SLICES
		*/
		
		let addOnSection = null;
		let dataItems = [];
		
		if (doc.node.body) {
			
			// DATA TABLE ITEMS
			let dataTable = filterArrayByType(doc.node.body, 'date_table');
			dataItems = (dataTable.length > 0) ? dataTable.shift().fields : [];

			// ADD ONS
			let addOnArray = filterArrayByType(doc.node.body, "add-ons");
			if (addOnArray.length > 0) {
				addOnSection = 				
					<section className={ styles.section }>
						<div className="container">
							<h2 className={ styles.sectionTitle }>Additional Info.</h2>
							<div className="row">
								<AddOnBlock body={ doc.node.body } dark={ true } />
							</div>
						</div>
					</section>;
			}
			
		}
		
		/*
			DATA ITEMS
		*/
		let tableRows = [];
		if (dataItems.length === 0) {
			tableRows.push(<tr><td colspan="4"><em>None scheduled</em></td></tr>);
		} else {
			tableRows = dataItems.map((d, i) => {
				return (
					<tr key={ i }>
						<td>
							<div className={ styles.calendar } onClick={ this.handleDatePickerOpen }>
								<div className={ styles.calendarDay }>{ moment(d.start_date).format('D') }</div>
								<div className={ styles.calendarMonth }>{ moment(d.start_date).format('MMM') }</div>
							</div>
						</td>
						<td className="d-none d-sm-table-cell"><strong>{ moment(d.start_date).format('h:mm a') } - { moment(d.end_date).format('h:mm a') }</strong></td>
						<td className="d-none d-sm-table-cell">{ d.age_range }</td>
						<td className="d-table-cell d-sm-none">
							{ moment(d.start_date).format('h:mm a') } - { moment(d.end_date).format('h:mm a') }<br />Ages { d.age_range }
						</td>
						<td className="d-none d-sm-table-cell text-right"><Button text="Learn More" url={ '' } /></td>
						<td className="d-table-cell d-sm-none text-right"><Button text="&rarr;" url={ '' } /></td>
					</tr>
				);
			})
		}

		return (
			<Layout title={ title } section="camps" parent={ { title: 'Guided Tours', url: '/guided-tours' } } { ...this.props }>
				<SEO title={ meta_title } description={ meta_description } canonical={ this.props.location.origin + this.props.location.pathname } />
				<Hero image={ wide_image } pageId={doc.node._meta.id}/>
				<BodyText>
					<div className="container">
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<h1 className={ styles.title }>{ RichText.asText(title) }</h1>
								<Text>{ body_text }</Text>
								{/*
								<div className={ styles.pricing }>
									<span className={ styles.pricingPrice }>
										<span className={ styles.pricingQuantity }>$--</span>
										<span className={ styles.pricingQuantifier }>PER PERSON</span>
									</span>
									<span className={ styles.pricingDesc }>FOTZ Members receive a<br />10% discount</span>
								</div>
								*/}
							</div>
						</div>
					</div>
				</BodyText>
				<div className={ styles.tableWrapper }>
					<div className="container">
						<table className={`table table-striped ${ styles.table }`}>
							<thead>
								<tr className="d-none d-sm-table-row">
									<th>DATE</th>
									<th>TIME</th>
									<th>AGE</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{ tableRows }
							</tbody>
						</table>
					</div>
				</div>
				{ addOnSection }
			</Layout>
		)
	}
}

export default GuidedTour;

export const query = graphql`
	query GuidedTourQuery($id: String) {
		prismic {
			allSpecial_experiences(id: $id) {
				edges {
					node {
						title
						body_text
						wide_image
						body {
							... on PRISMIC_Special_experienceBodyDate_table {
								type
								label
								fields {
									age_range
									end_date
									start_date
									title1
								}
							}
							... on PRISMIC_Special_experienceBodyAddOns {
								type
								label
								fields {
									addOn_text
									addOn_title
									icon_selector
								}
							}
						}
						_meta {
							id
						}
					}
				}
			}
		}
	}
`
