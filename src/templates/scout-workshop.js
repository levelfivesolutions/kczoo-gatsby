import React from "react"
import { graphql } from "gatsby"
import {
  filterArrayByType,
  renderImageUrl,
  ColorToHex,
  renderAsText,
  linkResolver,
} from "../utility"
import { getItems } from "../galaxy"
import Layout from "../components/layout/layout"
import SEO from "../components/seo"
import BodyText from "../components/body-text"
import Text from "../components/text"
import Title from "../components/title"
import Hero from "../components/hero-detail"
import RelatedLinks from "../components/related-links/related-links.js"
import PricingCallout from "../components/pricing-callout"
import styles from "./event.module.css"
import moment from "moment"
import Button from "../components/button/button-link"

class ScoutWorkshop extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      items: [],
    }
  }

  componentDidMount() {
    getItems(res => {
      console.log("res", res)
      this.setState({ items: res })
    })
  }

  render() {
    const doc = this.props.data.prismic.allScout_details.edges.slice(0, 1).pop()
    console.log("doc", doc)
    if (!doc) return null

    const {
      meta_title,
      meta_description,
      wide_image,
      title,
      body_text,
      optional_image,
      body,
    } = doc.node
    const { items } = this.state

    const relatedLinks = filterArrayByType(body, "related_links")

    return (
      <Layout
        title={title}
        section="camps"
        parent={{ title: "Scouts", url: "/scouts" }}
        {...this.props}
      >
        <SEO
          title={meta_title}
          description={meta_description}
          canonical={this.props.location.origin + this.props.location.pathname}
        />
        <Hero image={wide_image} pageId={doc.node._meta.id} />
        <BodyText>
          <div className="container">
            <div className="row">
              <div className="offset-md-1 col-md-10">
                <Title className={styles.title}>{title}</Title>
              </div>
            </div>
            <div className="row">
              <div className="offset-md-1 col-md-6">
                <Text>{body_text}</Text>
                <PricingCallout body={body} items={items} />
              </div>
              <div className="col-md-4">
                {optional_image && (
                  <figure className={styles.activityFigure}>
                    <img
                      src={renderImageUrl(optional_image)}
                      className={`${styles.activityImage} img-fluid`}
                      alt=""
                    />
                  </figure>
                )}
                {relatedLinks.length > 0 && (
                  <RelatedLinks links={relatedLinks[0].fields} />
                )}
              </div>
            </div>
          </div>
        </BodyText>
        {filterArrayByType(body, "date_table").length > 0 && (
          <div className={styles.tableWrapper}>
            {/* The Date Table */}
            {/* TODO: This needs to be make a component along with the one in event.js   */}
            <div className={`container ${styles.gridTable}`}>
              <div className={styles.gridRow}>
                <div>DATE</div>
                <div>TIME</div>
                <div>PROGRAM</div>
                <div>AGE RANGE</div>
                <div></div>
              </div>
              {filterArrayByType(body, "date_table")[0].fields.map(d => {
                let endDate = null
                if (d.number_of_days && d.number_of_days > 1) {
                  endDate = moment(d.start_date).add(
                    d.number_of_days - 1,
                    "days"
                  )
                }
                return (
                  <div className={styles.gridRow}>
                    <div>
                      {!endDate && (
                        <div className={styles.calendar}>
                          <div className={styles.calendarDay}>
                            {d.start_date
                              ? moment(d.start_date).format("D")
                              : ""}
                          </div>
                          <div className={styles.calendarMonth}>
                            {d.start_date
                              ? moment(d.start_date).format("MMM")
                              : ""}
                          </div>
                        </div>
                      )}
                      {endDate && (
                        <div className={`${styles.calendarInline}`}>
                          <div className={`${styles.calendar} mr-3`}>
                            <div className={styles.calendarDay}>
                              {d.start_date
                                ? moment(d.start_date).format("D")
                                : ""}
                            </div>
                            <div className={styles.calendarMonth}>
                              {d.start_date
                                ? moment(d.start_date).format("MMM")
                                : ""}
                            </div>
                          </div>
                          -
                          <div className={`${styles.calendar} ml-3`}>
                            <div className={styles.calendarDay}>
                              {endDate.format("D")}
                            </div>
                            <div className={styles.calendarMonth}>
                              {endDate.format("MMM")}
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                    <div
                      style={{ fontWeight: "bold", fontFamily: "DINOTBold" }}
                    >
                      {d.start_date
                        ? moment(d.start_date).format("h:mm a")
                        : ""}{" "}
                      {d.start_date && d.end_date ? "to" : ""}{" "}
                      {d.end_date && d.end_date
                        ? moment(d.end_date).format("h:mm a")
                        : ""}
                    </div>
                    <div>{d.title1}</div>
                    <div>{d.age_range ? d.age_range : "Any"}</div>
                    <div>
                      {d.cta && (
                        <Button text={"Register"} link={linkResolver(d.cta)} />
                      )}
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
        )}
        {/* End Date Table */}
      </Layout>
    )
  }
}

export default ScoutWorkshop

export const query = graphql`
query ScoutWorkshopQuery($uid: String) {
  prismic {
    allScout_details(uid: $uid) {
      edges {
        node {
          _meta {
            id
            uid
            tags
            type
          }
          meta_title
          meta_description
          wide_image
          title
          summary
          body_text
          optional_image
          body {
            ... on PRISMIC_Scout_detailBodyBanner_callout {
              type
              label
              fields {
                plu_price_description
                plu_price
                cta_link {
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC_Form {
                    title
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                  _linkType
                }
                cta_label
                banner_text
              }
            }
            ... on PRISMIC_Scout_detailBodyRelated_links {
              type
              label
              fields {
                link_name
                description
                exInLink {
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC__FileLink {
                    _linkType
                    name
                    size
                    url
                  }
                }
              }
            }
            ... on PRISMIC_Scout_detailBodyDate_table {
              type
              label
              fields {
                age_range
                cta {
                  ... on PRISMIC__ExternalLink {
                    _linkType
                    url
                  }
                  ... on PRISMIC_Form {
                    title
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                  _linkType
                }
                end_date
                start_date
                title1
              }
            }
          }
        }
      }
    }
  }
}

`
