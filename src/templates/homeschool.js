import React from 'react';
import { graphql } from 'gatsby';
import { RichText } from 'prismic-reactjs';
import Layout from "../components/layout/layout";
import Hero from "../components/hero-detail";
import BodyText from "../components/body-text";
import styles from './detail.module.css';

class HomeschoolDetail extends React.Component {
	render() {
		let { data } = this.props;
		console.log('props', this.props);
		const doc = data.prismic.allDetails.edges.slice(0,1).pop();
		console.log('doc -->', doc);
		if (!doc) return null;
		
		const { wide_image, title, body_text } = doc.node;
		
		return (
			<Layout title={ title } section="camps" parent={ { title: 'Homeschool', url: '/homeschool' } } { ...this.props }>
				<Hero image={ wide_image } />
				<BodyText>
					<div className="container">
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<h1 className={ styles.title }>{ RichText.asText(title) }</h1>
							</div>
						</div>
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<div>{ RichText.render(body_text) }</div>
							</div>
						</div>
					</div>
				</BodyText>
			</Layout>
		)
	}
}

export default HomeschoolDetail;

export const query = graphql`
	query homeschoolDetailQuery($uid: String) {
		prismic {
			allDetails(uid: $uid) {
				edges {
					node {
						_meta {
							id
							tags
							type
							uid
						}
						wide_image
						title
						body_text
					}
				}
			}
		}
	}
`
