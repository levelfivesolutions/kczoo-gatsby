import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import BodyText from "../components/body-text"
import Title from "../components/title"
import Text from "../components/text"
import sorteduniqby from "lodash.sorteduniqby"
import Hero from "../components/hero-detail"
import moment from "moment"
import styles from "./camp.module.css"
import SEO from "../components/seo"

import "react-dates/lib/css/_datepicker.css"
import "react-dates/initialize"
import RelatedLinks from "../components/related-links/related-links"
import { filterArrayByType, renderImageUrl, renderAsText } from "../utility"

let test = [
  {
    title: "Camp Title Goes Here",
    startDate: "2020-07-01",
    endDate: "2020-07-01",
    startTime: "08:30:00-05:00",
    endTime: "11:30:00-05:00",
    age: "5-9",
    cost: "$25 per day, per session",
    registerUrl: "",
  },
  {
    title: "Camp Title Goes Here",
    startDate: "2020-07-01",
    endDate: "2020-07-01",
    startTime: "08:30:00-05:00",
    endTime: "11:30:00-05:00",
    age: "4-5",
    cost: "$25 per day, per session",
    registerUrl: "",
  },
  {
    title: "Camp Title Goes Here",
    startDate: "2020-07-01",
    endDate: "2020-07-01",
    startTime: "08:30:00-05:00",
    endTime: "11:30:00-05:00",
    age: "4-5",
    cost: "$25 per day, per session",
    registerUrl: "",
  },
  {
    title: "Camp Title Goes Here",
    startDate: "2020-07-10",
    endDate: "2020-07-14",
    startTime: "08:30:00-05:00",
    endTime: "11:30:00-05:00",
    age: "4-5",
    cost: "$25 per day, per session",
    registerUrl: "",
  },
]

/*
class ListItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
		};

    this.handleClick = this.handleClick.bind(this);
  }
	handleClick(ev) {
		ev.preventDefault();
    this.props.onChange({ label: this.props.label, value: this.props.value });
  }	
  render() {
		return (
			<a className={`dropdown-item ${ styles.dropdownItem }`} href="#" onClick={ this.handleClick }>
				{ this.props.label }
			</a>
		)
	}
}
*/

class Camp extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      focused: false,
      numberOfDays: 1,
      fullDay: true,
      age: { label: "All Ages", value: "" },
    }

    this.handleDatePickerOpen = this.handleDatePickerOpen.bind(this)
    this.handleChangeAge = this.handleChangeAge.bind(this)
    this.handleChangeFullDay = this.handleChangeFullDay.bind(this)
  }
  handleDatePickerOpen(ev) {
    ev.stopPropagation()
    if (!this.state.focused) {
      this.setState({ focused: true })
    }
  }
  handleChangeAge(obj) {
    this.setState({ age: obj })
  }
  handleChangeFullDay() {
    this.setState({ fullDay: !this.state.fullDay })
  }
  render() {
    let { data } = this.props
    console.log("props", this.props)
    const doc = data.prismic.allCamps.edges.slice(0, 1).pop()
    console.log("doc -->", doc)
    if (!doc) return null

    let { wide_image, camp_title, body_text, camp_image } = doc.node

    let links =
      filterArrayByType(doc.node.body, "related_links").length > 0
        ? filterArrayByType(doc.node.body, "related_links")[0].fields
        : null

    // PREP DATA
    const camps = []
    /*
		const camps = data.prismic.allActivity_times.edges.map(d => ({
				title: RichText.asText(d.node.time_relationship.title),
				startDate: d.node.date_time, 
				endDate: d.node.date_time,
				age: '4-5',
				cost: '$25 per day, per session',
				registerUrl: ''
			})
		);
		*/

    // BUILD AN ARRAY OF THE CAMP LENGTHS (NUMBER OF DAYS) AVAILABLE
    let days = sorteduniqby(
      test.map(d => {
        let start = moment(d.startDate)
        let end = moment(d.endDate)
        return end.diff(start, "days") + 1
      })
    )

    // BUILD AN ARRAY OF AGES
    let ages = sorteduniqby(test, d => d.age).map(d => d.age)

    return (
      <Layout
        title={camp_title}
        section="camps"
        parent={{ title: "Day Camps", url: "/day-camps" }}
        {...this.props}
      >
        <Hero image={wide_image} pageId={doc.node._meta.id} />
        <SEO
          title={doc.node.meta_title || renderAsText(doc.node.title)}
          description={doc.node.meta_description}
          canonical={this.props.location.origin + this.props.location.pathname}
        />
        <BodyText>
          <div className="container">
            <div className="row">
              <div className="offset-md-1 col-md-6">
                <Title className="mb-3">{camp_title}</Title>
                <Text>{body_text}</Text>
              </div>
              <div className="col-md-4">
                {camp_image && (
                  <figure className={styles.activityFigure}>
                    <img
                      src={renderImageUrl(camp_image)}
                      className={`${styles.activityImage} img-fluid`}
                      alt=""
                    />
                  </figure>
                )}
                {links && <RelatedLinks links={links} />}
              </div>
            </div>
          </div>
        </BodyText>

        {/* <nav className={`navbar navbar-expand-lg ${ styles.filterBar }`}>
					<div className="container">
						<ul className={`navbar-nav mr-auto ${ styles.navbarNavleft }`}>
							{
								days.map(d => {
									let text = `${ d }-Day Camps`;
									if (d === 1) {
										text = 'Single Day Camps';
									}
									
									let navLinkClasses = `${ styles.filterNavLink } nav-link `;
									if (d === this.state.numberOfDays) {
										navLinkClasses += styles.filterNavLinkActive;
									}
									return (
										<li className={ `${ styles.filterNavItem } nav-item` }>
											<a className={ navLinkClasses } onClick={ ev => { ev.preventDefault(); this.setState({ numberOfDays: d }); } } href="#">
												{ text }
											</a>
										</li>
									);
								})
							}
						</ul>
						<ul className={`navbar-nav align-items-center ${ styles.navbarNavRight }`}>
							<li className={`nav-item dropdown ${ styles.dropdown } ${ styles.navItemBorderRight }`}>
								<a className={`nav-link dropdown-toggle ${ styles.filterActivityToggle }`} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									{ this.state.age.label }<span className={ `${ styles.iconChevronDownWhite } ml-3` }></span>
								</a>
								<div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
									<ListItem key={ 99999 } label="All Ages" value="" onChange={ this.handleChangeAge } />
									{
										ages.map((d, i) => <ListItem key={ i } label={ d } value={ d } onChange={ this.handleChangeAge } />)
									}
								</div>
							</li>
							<li>
								<label className={ styles.switch }>
									<input type="checkbox" value={ this.state.fullDay } onChange={ this.handleChangeFullDay } />
									<span className={ styles.slider } style={ this.state.fullDay ? { paddingLeft: '60px' } : { paddingLeft: '20px' } }>
										{ this.state.fullDay ? 'Full Day' : 'Half Day' }
									</span>
								</label>
							</li>
						</ul>
						
					</div>
				</nav>
				
				<div className={ styles.tableWrapper }>
					<div className="container">
						<div className="table-responsive">
							<table className={`table table-striped ${ styles.table }`}>
								<thead>
									<tr>
										<th>DATE</th>
										<th>TIME</th>
										<th>TITLE</th>
										<th>AGE</th>
										<th>COST</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									{
										test
											.filter(d => {
												return (this.state.age.value === '') ? true : (d.age === this.state.age.value);
											})
											.filter(d => {
												let start = moment(d.startDate);
												let end = moment(d.endDate);
												let numberOfDays = end.diff(start, 'days') + 1;
												return this.state.numberOfDays === numberOfDays;
											})
											.map((d, i) => {
												let calendarDays = '';
												let start = moment(d.startDate);
												let end = moment(d.endDate);
												let numberOfDays = end.diff(start, 'days') + 1;
												if (numberOfDays > 1) {
													calendarDays = <div className={ styles.calendarDaySmall }>{ `${moment(d.startDate).format('D')} - ${moment(d.endDate).format('D')}` }</div>;
												} else {
													calendarDays = <div className={ styles.calendarDay }>{ moment(d.startDate).format('D') }</div>;
												}

												return (
													<tr key={ i }>
														<td>
															<div className={ styles.calendar } onClick={ this.handleDatePickerOpen }>
																{ calendarDays }
																<div className={ styles.calendarMonth }>{ moment(d.startDate).format('MMM') }</div>
															</div>
														</td>
														<td><strong>{ moment(d.startDate).format('h:mm a') } - { moment(d.endDate).format('h:mm a') }</strong></td>
														<td>{ d.title }</td>
														<td>{ d.age }</td>
														<td>{ d.cost }</td>
														<td><Button text="Learn More" className="ml-2" icon="register" url={ d.registerUrl } /></td>
													</tr>
												);
											})
									}
								</tbody>
							</table>
						</div>
					</div>
				</div> */}
      </Layout>
    )
  }
}

export default Camp

export const query = graphql`
  query CampQuery($uid: String) {
    prismic {
      allCamps(uid: $uid) {
        edges {
          node {
            _meta {
              uid
              id
            }
            body {
              ... on PRISMIC_CampBodyRelated_links {
                type
                label
                fields {
                  description
                  exInLink {
                    _linkType
                    ... on PRISMIC__Document {
                      _meta {
                        id
                        tags
                        type
                        uid
                      }
                    }
                    ... on PRISMIC__ExternalLink {
                      _linkType
                      url
                    }
                    ... on PRISMIC__FileLink {
                      _linkType
                      url
                    }
                  }
                  link_name
                }
                primary {
                  related_links_header
                }
              }
            }
            wide_image
            body_text
            camp_image
            camp_title
            meta_description
            meta_title
          }
        }
      }
    }
  }
`
