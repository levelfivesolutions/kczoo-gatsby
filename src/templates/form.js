import React from "react"
import { graphql } from "gatsby"
import Layout from "../components/layout/layout"
import styles from "./form.module.css"
import CognitoForm from "../components/cognito-form"
import { getUrlVars } from "../utility"
import Hero from "../components/hero-detail"
import SEO from "../components/seo"
import JazzooFooter from "../components/jazzoo/footer"
import { renderAsText } from "../utility"

class Form extends React.Component {
  render() {
    let { data, location } = this.props
    const doc = data.prismic.allForms.edges.slice(0, 1).pop()
    if (!doc) return null

    let urlVars = getUrlVars(this.props.location.search)

    if (urlVars["preferredLocation"]) {
      let checks = 0
      var checkForPreffered = setInterval(() => {
        var labels = document.getElementsByTagName("label")

        if (labels) {
          for (var i = 0; i < labels.length; i++) {
            if (
              labels[i].innerText.replace(/ /g, "-") ===
              urlVars["preferredLocation"]
            ) {
              labels[i].click()
            }
          }

          if (checks >= 20) {
            clearInterval(checkForPreffered)
          }
          checks++
        }
      }, 1000)
    }

    if (urlVars["plaqueType"]) {
      let checks = 0
      var checkForPlaqueType = setInterval(() => {
        let select = document.getElementsByTagName("select")[0]

        if (select) {
          let options = select.options

          for (var i = 0; i < options.length; i++) {
            if (options[i].value.replace(/ /g, "-") === urlVars["plaqueType"]) {
              options[i].selected = true
              options[i].click()
            }
          }

          if (checks >= 20) {
            clearInterval(checkForPlaqueType)
          }
          checks++
        }
      }, 1000)
    }

    let formId = doc.node.form_id
    return (
      <Layout title={doc.node.title} section="" {...this.props}>
        <SEO
          title={doc.node.meta_title || renderAsText(doc.node.title)}
          description={doc.node.meta_description}
          canonical={location.origin + location.pathname}
        />
        {doc.node.hero_image && (
          <Hero image={doc.node.hero_image} pageId={doc.node._meta.id} />
        )}
        <section
          style={{ paddingTop: doc.node.hero_image ? 0 : 150 }}
          className={styles.section}
        >
          <div className={`container ${styles.formContainer} `}>
            <div className="col">
              <CognitoForm id={formId} />
            </div>
          </div>
        </section>
        {doc.node._meta.uid === "jazzoo-contact-us" && (
          <>
            <div style={{ marginBottom: 220 }}></div>
            <JazzooFooter />
          </>
        )}
      </Layout>
    )
  }
}

export default Form

export const query = graphql`
  query FormQuery($uid: String) {
    prismic {
      allForms(uid: $uid) {
        edges {
          node {
            _meta {
              id
              uid
            }
            title
            hero_image
            form_id
            meta_title
            meta_description
          }
        }
      }
    }
  }
`
