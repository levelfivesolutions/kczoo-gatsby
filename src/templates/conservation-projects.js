import React from 'react';
import { graphql } from 'gatsby';
import { linkResolver, renderAsText } from '../utility.js';
import { getCursorFromDocumentIndex } from 'gatsby-source-prismic-graphql';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import Button from '../components/button/button-link.js';
import Hero from '../components/hero-landing/hero-landing.js';
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Project from '../components/project';
import styles from './conservation-projects.module.css';

const limit = 8;

const uidToTag = {
	'field-research-projects': 'field and research projects',
	'green-initiatives': 'green initiative',
	'partnerships': 'conservation partnerships'
};

class ConservationProjects extends React.Component {
  constructor(props) {console.log('props', props);
    super(props);
    this.state = {
			limit: limit,
			projects: props.data.prismic.allConservation_projects.edges,
			pageInfo: props.data.prismic.allConservation_projects.pageInfo,
			page: 0,
		};

		this.handleChangeFilterType = this.handleChangeFilterType.bind(this);
		this.handleClickLoadMore = this.handleClickLoadMore.bind(this);
  }
	componentDidMount() {
    this.props.prismic.load({
      variables: { tags: [uidToTag[this.props.pathContext.uid]] },
      query, // (optional)
      fragments: [], // (optional)
    })
		.then(res => { 
			console.log('LOADED', res.data); 
			this.setState({ 
				projects: res.data.allConservation_projects.edges,
				pageInfo: res.data.allConservation_projects.pageInfo
			}); 
		});	
	}
	handleClickLoadMore() {console.log('Click', this.state.pageInfo.endCursor);	
		const nextPage = this.state.page + limit;						 
												 
    this.props.prismic.load({
      variables: { tags: [uidToTag[this.props.pathContext.uid]], cursor: getCursorFromDocumentIndex(nextPage) },
      query, // (optional)
      fragments: [], // (optional)
    })
		.then(res => { 
			console.log('LOADED', res.data); 
			this.setState({ 
				page: nextPage,
				projects: this.state.projects.concat(res.data.allConservation_projects.edges),
				pageInfo: res.data.allConservation_projects.pageInfo
			}); 
		});	
	}
	handleChangeFilterType(d) {
    this.props.prismic.load({
      variables: { tags: d.value ? [d.value] : null },
      query, // (optional)
      fragments: [], // (optional)
    })
		.then(res => { 
			console.log('LOADED', res.data); 
			this.setState({ 
				projects: res.data.allConservation_projects.edges,
				pageInfo: res.data.allConservation_projects.pageInfo
			}); 
		});	
	}
  render() {
		const doc = this.props.data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
		console.log('doc -->', doc);
		if (!doc) return null;
		const { wide_header_size, title, summary, feature, cta, button_label } = doc.node;
						
		const { projects, pageInfo } = this.state;
		console.log('projects', projects);
		
		return (
			<Layout title={ title } section="conservation" { ...this.props }>
				<SEO title={ `${renderAsText(title)} | Kansas City Zoo` } description="" canonical={ this.props.location.origin + this.props.location.pathname } />
				<Hero image={ wide_header_size } pageId={ doc.node._meta.id } />
				<TitleBar 
					title={ title } 
					description={ summary } 
					calloutDescription={ feature } 
					link={ cta } 
					linkText={ button_label } 
				/>
				<div className={ styles.contentArea }>
					<div className="container">
						<div className="row">
							{
								projects.map((d, i) => {
									return (
										<div key={ i } className="col-lg-3 col-md-4 col-sm-6">
											<Project 
												image={ d.node.spot_image } 
												title={ d.node.title } 
												bodyCopy={ d.node.summary } 
												externalLink={ d.node.external_link }
												internalUrl={ linkResolver(d.node._meta) }
											/>
										</div>
									);
								})
							}
						</div>
					</div>
					{
						pageInfo.hasNextPage &&
						<div className="text-center">
							<Button text="Show More" onClick={ this.handleClickLoadMore } />
						</div>
					}
				</div>
			</Layout>
		)
	}
}

export default ConservationProjects;

export const query = graphql`
	query ConservationProjectsQuery($cursor: String, $tags: [String!], $uid: String) {
		prismic {
			allLanding_page_hero_blocks(uid: $uid) {
				edges {
					node {
						_meta {
							id
							tags
							type
							uid
						}
						title
						summary
						feature
						wide_header_size
						button_label
						cta {
							_linkType
							... on PRISMIC__ExternalLink {
								_linkType
								url
							}
						}
					}
				}
			}
			allConservation_projects(after: $cursor, tags: $tags, first: 8, sortBy: meta_title_ASC) {
				edges {
					node {
						_meta {
							id
							tags
							type
							uid
						}
						meta_title
						title
						summary
						spot_image
						external_link {
							_linkType
							... on PRISMIC__ExternalLink {
								_linkType
								url
							}
						}
					}
				}
				totalCount
				pageInfo {
					endCursor
					hasNextPage
					hasPreviousPage
					startCursor
				}
			}
		}
	}
`
