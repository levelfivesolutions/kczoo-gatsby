import React from 'react';
import { graphql } from 'gatsby';
import Layout from "../components/layout/layout";
import { RichText } from 'prismic-reactjs';
import Hero from "../components/hero-detail";
import moment from 'moment'
import Button from '../components/button/button-link.js';
import RelatedLinks from '../components/related-links/related-links.js';
import Box from '../components/box/box.js';
import styles from './page.module.css'
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';

let experiences = [
	{ startDate: '2020-06-23T06:30:00-05:00', endDate: '2020-06-23T08:30:00-05:00', registerUrl: '' },
	{ startDate: '2020-07-12T06:30:00-05:00', endDate: '2020-07-12T08:30:00-05:00', registerUrl: '' },
	{ startDate: '2020-07-21T06:30:00-05:00', endDate: '2020-07-21T08:30:00-05:00', registerUrl: '' },
	{ startDate: '2020-07-31T06:30:00-05:00', endDate: '2020-07-31T08:30:00-05:00', registerUrl: '' },
];

let relatedLinks = [
	{ title: 'Link Name', description: 'Description if needed Maecenas faucibus mollis interdum.', url: '' },
	{ title: 'Link Name', description: 'Description if needed Maecenas faucibus mollis interdum.', url: '' },
	{ title: 'Link Name', description: 'Description if needed Maecenas faucibus mollis interdum.', url: '' }
];

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			focused: false,
			filterDate: moment('20200603')
		};

		this.handleDatePickerOpen = this.handleDatePickerOpen.bind(this);
  }
	handleDatePickerOpen(ev) {
		ev.stopPropagation();
		if (!this.state.focused) {
			this.setState({ focused: true });
		}
	}
	render() {
		let { data } = this.props;
		console.log('props', this.props);
		const doc = data.prismic.allSpecial_events.edges.slice(0,1).pop();
		console.log('PAGE doc -->', doc);
		if (!doc) return null;
		
		let contentId = doc.node._meta.id;
		let title = RichText.asText(doc.node.title);
		let bodyCopy = RichText.render(doc.node.body_text);
		let heroImage = doc.node.image.url;
		let activityImage = 'http://placeimg.com/370/290/animals';
		
		let backgroundStyle = {
			backgroundImage: 'url(/images/bg-palm.jpg)'
		};

		return (
			<Layout section="" { ...this.props }>
				<Hero image={ heroImage } />
				<div className={ styles.contentArea } style={ backgroundStyle }>
					<div className="container">
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<a href={ `https://kansascityzoo.prismic.io/documents~b=working&c=published&l=en-us/${ contentId }*XhelXhUAACIAFix1/` } target="_blank">Edit</a>
								<h1 className={ styles.title }>{ title }</h1>
							</div>
						</div>
						<div className="row">
							<div className="offset-md-1 col-md-6">
								<div>{ bodyCopy }</div>
								<div className={ styles.pricing }>
									<span className={ styles.pricingPrice }>
										<span className={ styles.pricingQuantity }>$20</span>
										<span className={ styles.pricingQuantifier }>PER PERSON</span>
									</span>
									<span className={ styles.pricingDesc }>FOTZ Members receive a<br />10% discount</span>
								</div>
							</div>
							<div className="col-md-4">
								<div className={ styles.activityImage }>
									<img src={ activityImage } className="img-fluid" alt="" />
								</div>
								<RelatedLinks links={ relatedLinks } />
							</div>
						</div>
						<div className="row">
							<div className="offset-md-1 col-md-10">
								<Box>
									<div className={ styles.pricing }>
										<span className={ styles.pricingPrice }>
											<span className={ styles.pricingQuantity }>$20</span>
											<span className={ styles.pricingQuantifier }>PER PERSON</span>
										</span>
										<span className={ styles.pricingDesc }>FOTZ Members receive a<br />10% discount</span>
										<Button text="REGISTER" url="" />
									</div>
								</Box>
							</div>
						</div>
					</div>
				</div>
				{/*
				<nav className={`navbar navbar-expand-lg ${ styles.filterBar }`}>
					<div className="container">
						<ul className="navbar-nav mr-auto">
							<li className="nav-item">
								<div className={ `filterDate ${styles.filterDate}` }>
									<div className={ styles.calendar } onClick={ this.handleDatePickerOpen }>
										<div className={ styles.calendarDay }>{ moment(this.state.filterDate).format('D') }</div>
										<div className={ styles.calendarMonth }>{ moment(this.state.filterDate).format('MMM') }</div>
									</div>
									<SingleDatePicker
										readOnly={ true }
										date={this.state.filterDate} // momentPropTypes.momentObj or null
										onDateChange={date => this.setState({ filterDate: date })} // PropTypes.func.isRequired
										focused={this.state.focused} // PropTypes.bool
										onFocusChange={({ focused }) => this.setState({ focused: focused })} // PropTypes.func.isRequired
										id="filter-date" // PropTypes.string.isRequired,
									/>
									<span className={ styles.iconChevronDownWhite } onClick={ this.handleDatePickerOpen }></span>
								</div>
							</li>
						</ul>
					</div>
				</nav>
				*/}
				<div className={ styles.tableWrapper }>
					<div className="container">
						<table className={`table table-striped ${ styles.table }`}>
							<thead>
								<tr>
									<th>DATE</th>
									<th>TIME</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{
									experiences.map((d, i) => {
										return (
											<tr key={ i }>
												<td>
													<div className={ styles.calendar } onClick={ this.handleDatePickerOpen }>
														<div className={ styles.calendarDay }>{ moment(d.startDate).format('D') }</div>
														<div className={ styles.calendarMonth }>{ moment(d.startDate).format('MMM') }</div>
													</div>
												</td>
												<td><strong>{ moment(d.startDate).format('h:mm a') } - { moment(d.endDate).format('h:mm a') }</strong></td>
												<td><Button text="Learn More" url={ d.registerUrl } /></td>
											</tr>
										);
									})
								}
							</tbody>
						</table>
					</div>
				</div>
				<section className={ styles.section }>
					<div className="container">
						<h2 className={ styles.sectionTitle }>Additional Info.</h2>
						<div className="row">
							<div className="col-md-4">
								<Box>
									<h5 className={ styles.boxTitle }>Things to Know</h5>
									<ul>
										<li>Takes place from 7:30am - 9:30am</li>
										<li>There are no age limits for this program</li>
										<li>Private tours are available with a 10 person minimum</li>
									</ul>
								</Box>
							</div>
							<div className="col-md-4">
								<Box>
									<h5 className={ styles.boxTitle }>Requirements</h5>
									<ul>
										<li>Every 10 children will require 1 adult chaperone</li>
									</ul>
								</Box>
							</div>
							<div className="col-md-4">
								<Box>
									<h5 className={ styles.boxTitle }>What’s Included</h5>
									<ul>
										<li>Zoo admission</li>
									</ul>
								</Box>
							</div>
						</div>
						<div className="row mt-5">
							<div className="col-md-10 offset-md-1">
								<div className={ styles.bannerBottom }>
									If you have any questions, please call 816-595-1207.
								</div>
							</div>
						</div>
					</div>
				</section>
			</Layout>
		)
	}
}

export default Page;

export const query = graphql`
	query PageQuery($id: String) {
		prismic {
			allSpecial_events(id: $id) {
				edges {
					node {
						_meta {
							id
						}
						title
						body_text
					}
				}
			}
		}
	}
`
