import React, { useState, useEffect } from "react";
import { graphql } from 'gatsby';
import { filterArrayByType } from '../utility';
import Layout from "../components/layout/layout";
import SEO from "../components/seo";
import TitleBar from '../components/titlebar-landing/titlebar-landing.js';
import Hero from "../components/hero-landing/hero-landing";
import SectionBlock from '../components/section-block';
import CampExperience from '../components/camp-experience/camp-experience';
import { getItems } from '../galaxy';
import { getPrice } from '../utility';

const PreschoolLanding = (props) => {
  const [items, setItems] = useState(0);

  useEffect(() => {
    getItems( (res) => {
    	setItems(res) 
    });
  }, []);
	
	const { data, location } = props;
	console.log('>>>', data);
	
	const doc = data.prismic.allLanding_page_hero_blocks.edges.slice(0,1).pop();
	console.log('doc -->', doc);
	if (!doc) return null;
  const { wide_header_size, title, feature, summary, cta, button_label, meta_title, meta_description, body, _meta } = doc.node;
	
	const featuredItems = filterArrayByType(body, 'feature_block_with_plu_and_fotz')[0].fields;
	console.log('featuredItems', featuredItems);
	
	return (
		<Layout title={ title } section="camps" { ...props }>
			<SEO title={ meta_title } description={ meta_description } canonical={ location.origin + location.pathname } />
      <Hero image={ wide_header_size } pageId={ _meta.id } />
			<TitleBar 
				title={ title } 
				link={ cta } 
				linkText={ button_label } 
				description={ summary } 
				calloutDescription={ feature } 
			/>
      {
				featuredItems.map((d, i) => {
					return (
						<SectionBlock key={ i }>
							<div className="container">
								<div className="row">
									<div className="col-md-12">
										<CampExperience
											color={ d.color }
											image={ d.wide_image }
											title={ d.title }
											richDescription={ d.description }
											prices={[
												{
													price: getPrice(items, d.plu_price, 0),
													description: d.plu_description
												}
											]}
											priceDesc={ d.additional_text }
											btnText={ d.cta_label }
											btnLink={ d.cta_link }
										/>
									</div>
								</div>
							</div>
						</SectionBlock>
					);
				})
			}        
		</Layout>
	);
}

export default PreschoolLanding;

export const query = graphql`
{
  prismic {
    allLanding_page_hero_blocks(uid: "preschool") {
      edges {
        node {
          _meta {
            id
            tags
            type
            uid
          }
          title
					summary
          wide_header_size
          meta_title
          meta_description
          feature
          cta {
            _linkType
            ... on PRISMIC_Detail {
              _linkType
              _meta {
                id
                tags
                type
                uid
              }
            }
          }
          button_label
          body {
            ... on PRISMIC_Landing_page_hero_blockBodyFeature_block_with_plu_and_fotz {
              type
              label
              fields {
                wide_image
                title:title1
                plu_price
                plu_description
                description
                cta_link {
                  _linkType
                  ... on PRISMIC_Detail {
                    _linkType
                    _meta {
                      id
                      tags
                      type
                      uid
                    }
                  }
                }
                cta_label
                color
                additional_text
              }
            }
          }
        }
      }
    }
  }
}
`
