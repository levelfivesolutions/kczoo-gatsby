import { isObject, isString } from "lodash"
import { RichText } from "prismic-reactjs"
import moment from "moment"
import { sortBy } from "lodash"
import Icon from "./components/icon/icon"
import React from "react"

export function getText(d) {
  return d ? d.map(o => o.text).join("") : ""
}

/*
<div class="cognito">
<script src="https://www.cognitoforms.com/s/bIzPUHzgCkWRr3DDrj7bRg"></script>
<script>Cognito.load("forms", { id: "85" });</script>
</div>
*/
export function renderCognitoForm(formId) {
  const script = document.createElement("script")
  script.onload = () => {
    window.Cognito.load("forms", { id: formId })
  }
  script.src = `https://www.cognitoforms.com/s/bIzPUHzgCkWRr3DDrj7bRg`
  document.body.appendChild(script)
}

export function ColorToHex(s) {
  let hex = "#000000"

  const regex = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i

  switch (s) {
    case "Zoo Green":
      hex = "#40AF49"
      break
    case "Zoo Blue":
      hex = "#0082B5"
      break
    case "Cardinal":
      hex = "#DB1C0A"
      break
    case "Orchid":
      hex = "#AF4068"
      break
    case "Tangerine":
      hex = "#FBA24F"
      break
    case "Sea":
      hex = "#3BBC8E"
      break
    case "Terra Cotta":
      hex = "#C6443E"
      break
    case "Pumpkin":
      hex = "#FD6743"
      break
    case "Canary":
      hex = "#FDC843"
      break
    case "White":
      hex = "#FFF"
      break
    case "Jazzoo 2021 Red":
      hex = "#e25b1f"
      break
    case "Jazzoo 2021 Orange":
      hex = "#fba24f"
      break
    case "Jazzoo 2021 Green":
      hex = "#86c891"
      break
    case "Jazzoo 2021 Light Green":
      hex = "#b8d544"
      break
    case "Jazzoo 2021 Blue":
      hex = "#63cbe6"
      break
    case "Jazzoo 2022 Green":
      hex = "#2B4D20"
      break
    case "Jazzoo 2022 Maroon":
      hex = "#A12D3A"
      break
    case "Jazzoo 2022 Teal":
      hex = "#107977"
      break
    default:
      hex = "#000000"
  }

  if (regex.test(s)) {
    return s
  }

  return hex
}

export function getTextColor(s) {
  let hex = "#000000"

  switch (s) {
    case "Zoo Green":
      hex = "#FFF"
      break
    case "Zoo Blue":
      hex = "#FFF"
      break
    case "Cardinal":
      hex = "#FFF"
      break
    case "Orchid":
      hex = "#FFF"
      break
    case "Tangerine":
      hex = "#FFF"
      break
    case "Sea":
      hex = "#FFF"
      break
    case "Terra Cotta":
      hex = "#FFF"
      break
    case "Pumpkin":
      hex = "#FFF"
      break
    case "Canary":
      hex = "#FFF"
      break
    case "White":
      hex = "#000"
      break
    default:
      hex = "#000000"
  }

  return hex
}

/**
 * Returns the attribute of an object that matches "keyString"
 * Used to get an attribute regardless of trailing numbers
 * i.e. will return data for title, title1, title2 etc...
 * @param {object} data The object to pull the attribute from
 * @param {string} keyString the key (sans trailing nubmers) of the object to return
 */
export function getAttribute(data, keyString) {
  let foundObject
  Object.keys(data).map(key => {
    if (key.replace(/\d+$/, "") === keyString) {
      foundObject = data[key]
    }
  })
  return foundObject
}

export function renderCheckmark(value) {
  if (value === "Yes" || value === true) {
    return <Icon icon="check" width="25" color="Black" />
  } else {
    return ""
  }
}

export function filterArrayByType(body, filterString) {
  return body
    ? body.filter(d => d.type && d.type.replace(/\d+$/, "") === filterString)
    : []
}

export function hasSlice(body, filterString) {
  return filterArrayByType(body, filterString).length > 0
}

export const linkResolver = function linkResolver(doc) {
  // console.log("RESOLVING", doc)
  if (doc && "type" in doc && "tags" && doc && "uid" in doc) {
    if (doc.type === "landing_page_hero_block") {
      return `/${doc.uid}`
    } else if (doc.type === "detail" && doc.uid.includes("careers")) {
      return `/${doc.uid}`
    } else if (doc.type === "careers") {
      return `/${doc.uid}`
    } else if (doc.type === "special_events_landing_page") {
      return "/special-events"
    } else if (doc.type === "weddings_landing_page") {
      return `/${doc.uid}`
    } else if (doc.type === "corporate_meetings_landing_page") {
      return `/${doc.uid}`
    } else if (doc.type === "overnights_landing_page") {
      return `/${doc.uid}`
    } else if (doc.type === "purrfect_picnics_landing_page") {
      return `/${doc.uid}`
    } else if (doc.type === "membership_landing_page") {
      return `/memberships`
    } else if (doc.type === "map_landing_page") {
      return `/zoo-map`
    } else if (doc.type === "aquarium_home") {
      return `/aquarium/`
    } else if (doc.type === "detail" && doc.tags.includes("Aquarium")) {
      return `/aquarium/${doc.uid}`
    } else if (doc.type === "aquarium_donations") {
      return `/aquarium/donations`
    } else if (doc.type === "aquarium_site_plan") {
      return `/aquarium/site-plan`
    } else if (doc.type === "aquarium_sponsors") {
      return `/aquarium/sponsors`
    } else if (doc.type === "tickets_and_pricing_landing_page") {
      return `/tickets-and-pricing`
    } else if (
      doc.type === "detail_with_plu" &&
      doc.tags.includes("Animal Care Academy")
    ) {
      return `/animal-care-academy/${doc.uid}`
    } else if (doc.type === "form") {
      return `/form/${doc.uid}`
    } else if (doc.type === "special_exhibit") {
      return `/exhibit/${doc.uid}`
    } else if (doc.type === "camp") {
      return `/camp/${doc.uid}`
    } else if (doc.type === "special_experience") {
      return `/guided-tour/${doc.uid}`
    } else if (doc.type === "special_event" && doc.tags.includes("Jazzoo")) {
      return `/jazzoo`
    } else if (doc.type === "special_event" && doc.tags.includes("Big Event")) {
      return `/${doc.uid}`
    } else if (doc.type === "special_event") {
      return `/event/${doc.uid}`
    } else if (doc.type === "animal_cam_detail") {
      return `/animal-cam/${doc.uid}`
    } else if (doc.type === "scout_detail") {
      return `/scouts/${doc.uid}`
    } else if (doc.type === "conservation_project") {
      return `/conservation-project/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Homeschool")) {
      return `/homeschool/${doc.uid}`
    } else if (
      doc.type === "detail" &&
      doc.tags.includes("Distance Learning")
    ) {
      return `/distance-learning/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Overnights")) {
      return `/overnight/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Volunteer")) {
      return `/volunteer/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Zoomobile")) {
      return `/zoomobile/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Jazzoo")) {
      return `/jazzoo/${doc.uid}`
    } else if (
      doc.type === "detail" &&
      doc.tags.includes("Animal Encounters")
    ) {
      return `/animal-encounters/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Get Involved")) {
      return `/support-us/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Donations")) {
      return `/donate/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Visit")) {
      return `/visit/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("About")) {
      return `/about/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Daily Schedule")) {
      return `/activity/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Itinerary")) {
      return `/itinerary/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Ad Hoc Info")) {
      return `/fyi/${doc.uid}`
    } else if (doc.type === "detail" && doc.tags.includes("Preschool")) {
      return `/preschool/${doc.uid}`
    } else if (doc.type === "animal_detail") {
      return `/animals`
    } else if (doc.type === "jazzoo_hom") {
      return `/jazzoo`
    } else if (doc.type === "jazzoo_event_detail") {
      return `/jazzoo/detail`
    } else if (
      doc.type === "jazzoo_landing_page" ||
      doc.type === "jazzoo_grid_layout"
    ) {
      return `/jazzoo/${doc.uid}`
    } else if (doc.type === "jazzoo_sponsors") {
      return `/jazzoo/sponsors`
    } else if (doc.type === "jazzoo_sponsor_levels") {
      return `/jazzoo/sponsor-levels`
    } else if (doc.type === "jazzoo_volunteers") {
      return `/jazzoo/volunteers`
    } else if (doc.type === "jazzoo_steering_committee") {
      return `/jazzoo/steering-committees`
    } else if (doc.type === "photo_library" && doc.tags.includes("Jazzoo")) {
      return `/jazzoo/${doc.uid}`
    } else if (doc.type === "photo_library") {
      return `/${doc.uid}`
    }
  }

  if (
    doc &&
    doc._meta &&
    "type" in doc._meta &&
    "tags" &&
    doc &&
    "uid" in doc._meta
  ) {
    if (doc._meta.type === "landing_page_hero_block") {
      return `/${doc._meta.uid}`
    } else if (doc._meta.type === "hours_location_parking_landing_page") {
      return `/${doc._meta.uid}`
    } else if (doc._meta.type === "special_events_landing_page") {
      return "/special-events"
    } else if (doc._meta.type === "weddings_landing_page") {
      return `/${doc._meta.uid}`
    } else if (doc._meta.type === "corporate_meetings_landing_page") {
      return `/${doc._meta.uid}`
    } else if (doc._meta.type === "overnights_landing_page") {
      return `/${doc._meta.uid}`
    } else if (doc._meta.type === "purrfect_picnics_landing_page") {
      return `/${doc._meta.uid}`
    } else if (doc._meta.type === "membership_landing_page") {
      return `/memberships`
    } else if (doc._meta.type === "renewal_page") {
      return `/memberships/renew`
    } else if (doc._meta.type === "map_landing_page") {
      return `/zoo-map`
    } else if (doc._meta.type === "tickets_and_pricing_landing_page") {
      return `/tickets-and-pricing`
    } else if (
      doc._meta.type === "detail_with_plu" &&
      doc._meta.tags.includes("Animal Care Academy")
    ) {
      return `/animal-care-academy/${doc._meta.uid}`
    } else if (doc._meta.type === "form") {
      return `/form/${doc._meta.uid}`
    } else if (doc._meta.type === "special_exhibit") {
      return `/exhibit/${doc._meta.uid}`
    } else if (doc._meta.type === "camp") {
      return `/camp/${doc._meta.uid}`
    } else if (doc._meta.type === "special_experience") {
      return `/guided-tour/${doc._meta.uid}`
    } else if (
      doc._meta.type === "special_event" &&
      doc._meta.tags.includes("Jazzoo")
    ) {
      return `/jazzoo`
    } else if (doc._meta.type === "special_event") {
      return `/event/${doc._meta.uid}`
    } else if (doc._meta.type === "animal_cam_detail") {
      return `/animal-cam/${doc._meta.uid}`
    } else if (doc._meta.type === "scout_detail") {
      return `/scouts/${doc._meta.uid}`
    } else if (doc._meta.type === "conservation_project") {
      return `/conservation-project/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Homeschool")
    ) {
      return `/homeschool/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Overnights")
    ) {
      return `/overnight/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Volunteer")
    ) {
      return `/volunteer/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Zoomobile")
    ) {
      return `/zoomobile/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Animal Encounters")
    ) {
      return `/animal-encounters/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Distance Learning")
    ) {
      return `/distance-learning/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Donations")
    ) {
      return `/donate/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Visit")
    ) {
      return `/visit/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("About")
    ) {
      return `/about/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Daily Schedule")
    ) {
      return `/activity/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Jazzoo")
    ) {
      return `/jazzoo/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Itinerary")
    ) {
      return `/itinerary/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Ad Hoc Info")
    ) {
      return `/fyi/${doc._meta.uid}`
    } else if (
      doc._meta.type === "detail" &&
      doc._meta.tags.includes("Preschool")
    ) {
      return `/preschool/${doc._meta.uid}`
    } else if (doc._meta.type === "animal_detail") {
      return `/animals`
    } else if (doc._meta.type === "renewal_page") {
      return `/memberships/${doc._meta.uid}`
    } else if (
      doc._meta.type === "photo_library" &&
      doc._meta.tags.includes("Jazzoo")
    ) {
      return `/jazzoo/${doc._meta.uid}`
    } else if (doc._meta.type === "photo_library") {
      return `/${doc._meta.uid}`
    }
  }

  // Homepage route fallback
  return "/"
}

export function renderAsText(value) {
  return isObject(value) ? RichText.asText(value) : isString(value) ? value : ""
}

export function renderImageUrl(image) {
  console.log("image ->", image)
  return image && isObject(image) ? image.url : isString(image) ? image : ""
}

export function renderLinkUrl(link) {
  return isObject(link) ? link.url : isString(link) ? link : ""
}

export function renderAsHtml(value) {
  return isObject(value)
    ? RichText.render(value, linkResolver)
    : isString(value)
    ? value
    : ""
}

export function chunk(array, size) {
  const chunked_arr = []
  let copied = [...array] // ES6 destructuring
  const numOfChild = Math.ceil(copied.length / size) // Round up to the nearest integer
  for (let i = 0; i < numOfChild; i++) {
    chunked_arr.push(copied.splice(0, size))
  }
  return chunked_arr
}

/**
 * Return true if string contains a PLU.
 * @param {*} string
 */
export function isPlu(string) {
  string = renderAsText(string)
  return string.match(/\D{2}\d{5}/g) !== null
}

/**
 * Return an array containing any/all the PLUS in a string.
 * @param {*} string
 */
export function getPlus(string) {
  string = renderAsText(string)
  var plus = string.match(/\D{2}\d{5}/g)
  plus = plus.map(plu => plu.toUpperCase())
  return plus
}

/**
 * Return ONE plu from a string - the first if more than one.
 * @param {*} string
 */
export function getPlu(string) {
  string = renderAsText(string)
  string = string.match(/\D{2}\d{5}/g)[0]
  return string ? string.toUpperCase() : null
}

/**
 *
 * @param { array of items with plu as index and price as value} items
 * @param { string } string the plu to search for
 */
export function getPrice(items, string, digits) {
  console.log("DIGITS GET PRICE", digits)
  if (items) {
    if (isPlu(string)) {
      if (getPlus(string).length > 1) {
        var plus = getPlus(string)
        console.log("PLUS ", plus)
        return (
          formatPriceString(items[plus[0]], digits) +
          " - " +
          formatPriceString(items[plus[1]], digits)
        )
      } else {
        return formatPriceString(items[getPlu(string)], digits)
      }
    } else {
      return string
        ? formatPriceString(string, digits)
        : digits === 0
        ? "$--"
        : "$--.--"
    }
  } else {
    return string
      ? formatPriceString(string, digits)
      : digits === 0
      ? "$--"
      : "$--.--"
  }
}

/**
 * Takes a string and formats it to display as currency
 * @param { string } string
 */
export function formatPriceString(string, digits = 0) {
  console.log("DIGITS format", digits)
  if (string) {
    string = string.trim()
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: digits,
    })

    //Handle price ranges
    if (string.split("-").length > 1) {
      return (
        formatPriceString(string.split("-")[0], digits) +
        " - " +
        formatPriceString(string.split("-")[1], digits)
      )
    }

    var formatted =
      string == "FREE with Postcard"
        ? "FREE with Postcard"
        : string === "0"
        ? "FREE"
        : formatter.format(string.replace("$", "").replace(",", ""))
    return formatted !== "$NaN" ? formatted : digits === 0 ? "$--" : "$--.--"
  } else {
    return digits === 0 ? "$--" : "$--.--"
  }
}

/**
 * Returns an array of the url parameters
 * @param { } location
 */
export function getUrlVars(location) {
  var vars = {}

  var parts = !location
    ? {}
    : location.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value
      })

  return vars
}

// GET THE DAY OF THE WEEK
export function getDayOfWeek(n) {
  let dayOfWeek
  switch (n) {
    case 0:
      dayOfWeek = "Sunday"
      break
    case 1:
      dayOfWeek = "Monday"
      break
    case 2:
      dayOfWeek = "Tuesday"
      break
    case 3:
      dayOfWeek = "Wednesday"
      break
    case 4:
      dayOfWeek = "Thursday"
      break
    case 5:
      dayOfWeek = "Friday"
      break
    case 6:
      dayOfWeek = "Saturday"
      break
  }
  return dayOfWeek
}

// export function getActivitySeasonKey(date = moment()) {
//   const year = date.year()
//   let seasonKey

//   // WINTER
//   if (date.isBetween([year, 11, 1], [year, 2, 29], null, "[]")) {
//     seasonKey = "activity_times"
//     // SPRING
//   } else if (date.isBetween([year, 3, 1], [year, 5, 22], null, "[]")) {
//     seasonKey = "activity_times"
//     // FALL
//   } else if (date.isBetween([year, 9, 8], [year, 10, 31], null, "[]")) {
//     seasonKey = "activity_times"
//     // SUMMER
//   } else if ((date.isBetween([year, 5, 23], [year, 9, 7]), null, "[]")) {
//     seasonKey = "activity_times"
//   }

//   return seasonKey
// }

export function generateActivitiesByDate(activities, dateArg) {
  let date = isObject(dateArg) ? dateArg : moment(dateArg)
  console.log("DATE", date.format())

  let weekends = []
  let day = 1
  let month = moment(date).date(day)

  // STILL THE SAME MONTH?
  while (month.month() === date.month()) {
    let weekend = []
    let isSaturday = month.day() === 6

    // ADD SATURDAY?
    if (isSaturday) {
      weekend.push(day)
    }

    // NEXT DAY
    month.date(++day)

    if (isSaturday) {
      weekend.push(month.date()) // ADD SUNDAY
      weekends.push(weekend) // ADD WEEKEND
    }
  }
  console.log("weekends", weekends)

  // const year = date.year()
  // let seasonKey

  // // WINTER
  // if (date.isBetween([year, 11, 1], [year, 2, 29], null, "[]")) {
  //   seasonKey = "activity_times"
  //   // SPRING
  // } else if (date.isBetween([year, 3, 1], [year, 5, 22], null, "[]")) {
  //   seasonKey = "activity_times"
  //   // FALL
  // } else if (date.isBetween([year, 9, 8], [year, 10, 31], null, "[]")) {
  //   seasonKey = "activity_times"
  //   // SUMMER
  // } else if ((date.isBetween([year, 5, 23], [year, 9, 7]), null, "[]")) {
  //   seasonKey = "activity_times"
  // }

  // console.log("season key:", seasonKey)

  // BUILD ACTIVITY TIMES
  let activityTimes = []
  activities.forEach(activity => {
    //console.log('ACTIVITY -->',activity.node[seasonKey]);
    let isActive = activity.node["activity_times_are_active"]
    //console.log("active??", activity.node["activity_times_are_active"])
    if (activity.node["activity_times"] && isActive === true) {
      activity.node["activity_times"].forEach(time => {
        // THANKSGIVING OR CHRISTMAS OR NEW YEARS
        if (
          (date.month() !== 10 && date.dayOfYear() !== 26) ||
          (date.month() !== 11 && date.dayOfYear() !== 25) ||
          (date.month() !== 0 && date.dayOfYear() !== 1)
        ) {
          let hour24 =
            time.start_am_pm === "PM" && Number(time.start_hour) < 12
              ? Number(time.start_hour) + 12
              : Number(time.start_hour)
          let minute = Number(time.start_minute)

          if (time.frequency === "Daily") {
            let t = moment({
              hour:
                time.start_am_pm === "PM" && Number(time.start_hour) < 12
                  ? Number(time.start_hour) + 12
                  : Number(time.start_hour),
              minute: Number(time.start_minute),
            })
            activityTimes.push({
              title: activity.node.title,
              image: activity.node.spot_images,
              wide_images: activity.node.wide_images,
              location: time.location,
              duration: time.duration,
              duration_time_units: time.duration_time_units,
              description: activity.node.description,
              date: moment({ hour: hour24, minute: minute }),
              link: activity.node.activity_parent_page,
              id: activity.node._meta ? activity.node._meta.id : null,
              uid: activity.node._meta ? activity.node._meta.uid : "",
            })
          } else if (time.frequency === "Weekends Only") {
            weekends.forEach(d => {
              if (date.date() === d[0] || date.date() === d[1]) {
                activityTimes.push({
                  title: activity.node.title,
                  image: activity.node.spot_images,
                  location: time.location,
                  duration: time.duration,
                  duration_time_units: time.duration_time_units,
                  description: activity.node.description,
                  id: activity.node._meta ? activity.node._meta.id : null,
                  uid: activity.node._meta ? activity.node._meta.uid : "",
                  date: moment({ hour: hour24, minute: minute }),
                })
              }
            })
          } else if (time.frequency === "1st & 3rd Weekends Only") {
            if (
              date.date() === weekends[0][0] ||
              date.date() === weekends[0][1] ||
              date.date() === weekends[2][0] ||
              date.date() === weekends[2][1]
            ) {
              activityTimes.push({
                title: activity.node.title,
                image: activity.node.spot_images,
                location: time.location,
                duration: time.duration,
                duration_time_units: time.duration_time_units,
                description: activity.node.description,
                id: activity.node._meta ? activity.node._meta.id : null,
                uid: activity.node._meta ? activity.node._meta.uid : "",
                date: moment({ hour: hour24, minute: minute }),
              })
            }
          } else if (time.frequency === "2nd & 4th Weekends Only") {
            if (
              date.date() === weekends[1][0] ||
              date.date() === weekends[1][1] ||
              date.date() === weekends[3][0] ||
              date.date() === weekends[3][1]
            ) {
              activityTimes.push({
                title: activity.node.title,
                image: activity.node.spot_images,
                location: time.location,
                duration: time.duration,
                duration_time_units: time.duration_time_units,
                description: activity.node.description,
                id: activity.node._meta ? activity.node._meta.id : null,
                uid: activity.node._meta ? activity.node._meta.uid : "",
                date: moment({ hour: hour24, minute: minute }),
              })
            }
          }
        }
      })
    }
  })

  // SORT BY TIME
  activityTimes = sortBy(activityTimes, [
    function(d) {
      return d.date.valueOf()
    },
  ])

  return activityTimes
}

export function responsiveImageUrl(data) {
  if (typeof window === "undefined" || !data) return null

  console.log("Data", data)
  const width = typeof window !== "undefined" ? window.innerWidth : ""
  let retImage = data

  if (width >= 1200 && typeof data.xl !== "undefined") {
    console.log("Responsive Image XL")
    retImage = data.xl
  } else if (width >= 992 && typeof data.lg !== "undefined") {
    console.log("Responsive Image LG")
    retImage = data.lg
    return renderImageUrl(data.lg)
  } else if (width >= 768 && typeof data.md !== "undefined") {
    console.log("Responsive Image MD")
    retImage = data.md
    console.log("Responsive Image SM")
  } else if (width < 768 && typeof data.sm !== "undefined") {
    retImage = data.sm
  } else {
    retImage = data
  }

  return renderImageUrl(retImage)
}
