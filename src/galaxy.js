import axios from 'axios';
import moment from 'moment';
import { isPlu, getPlu } from './utility';


/**
 * Price retreives the price for an item from Galaxy using an item's plu.
 * This function allows: should a PLU be unavailable a price may be used instead.
 * ONLY USE WHEN QUERYING 1 (ONE) PLU ON A PAGE
 * @param {*} PLU String - the price OR PLU for a SINGLE item
 * @param {*} fn a function to set the state from the res
 */
export function priceFromPLU(PLU, fn) {

	// formats a string to display as currency
	const formatter = new Intl.NumberFormat('en-US', {
		style: 'currency',
		currency: 'USD',
		minimumFractionDigits: 2
	  })

	if( isPlu(PLU) ) {
		let xmls =
			`<Envelope>
					<Header>
						<SourceID>Level5</SourceID>
						<MessageID>0</MessageID>
						<MessageType>QueryEvents</MessageType>
						<SessionID></SessionID>
						<TimeStamp>2014-07-04 17:50:09</TimeStamp>
					</Header>
					<Body>
						<QueryEvents>
								<EventRangeBeginDate>2002-07-10 00:00:00</EventRangeBeginDate>
								<EventRangeEndDate>2102-07-11 00:00:00</EventRangeEndDate>
							<Pricing>
								<CustomerID>0</CustomerID>
								<Tickets>
									<Ticket>
										<PLU>${getPlu(PLU)}</PLU>
									</Ticket>
								</Tickets>
							</Pricing>
						</QueryEvents>
					</Body>
				</Envelope>`;

		console.log(xmls);

		return axios.post('https://kc-zoo-qa-proxy.azurewebsites.net/', xmls,
			{
				headers:
					{ 'Content-Type': 'text/xml' }
			}).then(res => {
				var XMLParser = require('react-xml-parser');
				var xml = new XMLParser().parseFromString(res.data);
				var value = xml.getElementsByTagName('Price')[0].value;
				console.log("VALUE ", value.replace(",",) )
				fn( (value) ? formatter.format(value.replace(",","")) : "--.--");
			}).catch(err => { console.log("PLU ERROR"); return "--.--"; });
	} else {		
		console.log("PLU JUNK ", PLU)
		return fn( (PLU) ? formatter.format(PLU.replace("$","").replace(",","")) : "--.--"); 
	}
}

/*  -  -  -  -  -  -  -  -  -  -  -  -  TO IMPLEMENT: -  -  -  -  -  -  -  -  -  -  

	constructor(props) {
		super(props);
		this.state = {
			price: "$--.--"
		};

	}

	componentDidMount() {
		priceFromPLU( this.props.priceOrPlu, (res) => {
			this.setState({ price: res })
		});
	}




=================================================================================================
 * * * * * * * * * * * * * * * * * * End price from PLU * * * * * * * * * * * * * * * * * * * * * 
=================================================================================================






*/
/**
 * This function should be used when more than one PLU needs pulled on a page
 * @param {*} fn a function to set the state from the res
 */
export function getItems(fn, discounted=null, customer=171) {
	let xmls =
		`<Envelope>
			<Header>
				<SourceID>Level5</SourceID>
				<MessageID>-1</MessageID>
				<MessageType>QueryCustomerItems</MessageType>
				<SessionID />
				<TimeStamp>${ moment().format() }</TimeStamp>
				<EchoData />
				<SystemFields />
			</Header>
			<Body>
				<QueryCustomerItems>
					<CustomerID>${customer}</CustomerID>
					<UseSalesProgramPricing>${ (discounted) ? `YES` : `NO` }</UseSalesProgramPricing>
				</QueryCustomerItems>
			</Body>
		</Envelope>`;

		console.log(xmls);

		return axios.post('https://egal.fotzkc.org/', xmls,
			{
				headers:
					{ 'Content-Type': 'text/xml' }
			}).then(res => {
				var XMLParser = require('react-xml-parser');
				var xml = new XMLParser().parseFromString(res.data);
				var xmlItems = xml.getElementsByTagName('Item');
				var items = {};

				xmlItems.forEach(
					( xmlItem ) => {
						items[xmlItem.children.filter( (d) =>  d.name === "PLU" ).pop().value] = xmlItem.children.filter( (d) =>  d.name === "Price" ).pop().value;
					}
				)
				fn( items );
			}).catch(err => { console.log("Item ERROR"); return fn([]); });
}

export function getOnlineItems(fn){
	return getItems(fn, true);
}

export function getJacksonClayItems(fn){
	return getItems(fn, true, 188);
}

/*  -  -  -  -  -  -  -  -  -  -  -  -  TO IMPLEMENT: -  -  -  -  -  -  -  -  -  -  

  componentDidMount() {
		getItems( (res) => {
			this.setState( { items: res } )
		});
	} 

	getPrice( this.state.items, d.plu_price)

--=--==--==-=--=-=-=-=-=-=-=-=-=-=-=-  OR WITH HOOKS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

	const [items, setItems] = useState(0);
	
	useEffect(() => {
		getItems( (res) => {
				setItems(res) 
			});
	}, []);

	getPrice(items, detail.full_price_plu)
	
=================================================================================================
 * * * * * * * * * * * * * * * * * * End price from PLU * * * * * * * * * * * * * * * * * * * * * 
=================================================================================================
*/
