import 'react-hot-loader'
import React from "react"
import PropTypes from "prop-types"

export default function HTML(props) {
	return (
		<html {...props.htmlAttributes}>
			<head>
				<meta charSet="utf-8" />
				<meta httpEquiv="x-ua-compatible" content="ie=edge" />
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1, shrink-to-fit=no"
				/>
				<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />
				<link rel="stylesheet" type="text/css" href="/mapplic/mapplic.css" />

				{/* Verification For Facebook Pixel */}
				<meta name="facebook-domain-verification" content="ryem32mvw8p55tm2l0uob2gi77rxga" />

				{/* Font Preloading */}
				<link rel="preload" as="font" href="/fonts/Lifehack/DearType - Lifehack Basic.woff" type="font/woff" crossOrigin="anonymous" />
				<link rel="preload" as="font" href="/fonts/Lifehack/DearType - Lifehack.woff" type="font/woff" crossOrigin="anonymous" />
				<link rel="preload" as="font" href="/fonts/Lifehack/DearType - Lifehack Sans.woff" type="font/woff" crossOrigin="anonymous" />

				<link rel="preload" as="font" href="/fonts/DIN/DIN Condensed Bold.woff" type="font/woff" crossOrigin="anonymous" />
				<link rel="preload" as="font" href="/fonts/DIN/FontFont - DINOT-Bold.woff" type="font/woff" crossOrigin="anonymous" />
				<link rel="preload" as="font" href="/fonts/DIN/FontFont - DINOT-Italic.woff" type="font/woff" crossOrigin="anonymous" />
				<link rel="preload" as="font" href="/fonts/DIN/FontFont - DINOT.woff" type="font/woff" crossOrigin="anonymous" />
				<link rel="preload" as="font" href="/fonts/DIN/FontFont - DINRoundOT-Bold.woff" type="font/woff" crossOrigin="anonymous" />
				<link rel="preload" as="font" href="/fonts/DIN/FontFont - DINRoundOT.woff" type="font/woff" crossOrigin="anonymous" />

				{/* for Catalpa font Jazzoo 2022 */}
				<link rel="stylesheet" href="https://use.typekit.net/nbx5ciw.css"/>
				{props.headComponents}
				{/* Facebook Pixel Code */}
				<script
					dangerouslySetInnerHTML={{
						__html: `
							!function(f, b, e, v, n, t, s) {
									if (f.fbq)
											return;
									n = f.fbq = function() {
											n.callMethod ?
											n.callMethod.apply(n, arguments) : n.queue.push(arguments)
									};
									if (!f._fbq)
											f._fbq = n;
									n.push = n;
									n.loaded = !0;
									n.version = '2.0';
									n.queue = [];
									t = b.createElement(e);
									t.async = !0;
									t.src = v;
									s = b.getElementsByTagName(e)[0];
									s.parentNode.insertBefore(t, s)
							}(window,
							document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
							fbq('init', '798966990249033'); // Insert your pixel ID here.
							fbq('track', 'PageView');
						`,
					}}
				/>
				<noscript
					dangerouslySetInnerHTML={{
						__html: `
							<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=798966990249033&ev=PageView&noscript=1"/>
						`,
					}}
				/>
				{/* DO NOT MODIFY */}
				{/* End Facebook Pixel Code */}

				{/* Global site tag (gtag.js) - Google Analytics */}
				<script async src="https://www.googletagmanager.com/gtag/js?id=UA-1419698-1"></script>
				<script dangerouslySetInnerHTML={{
					__html:
						`
							window.dataLayer = window.dataLayer || [];
							function gtag(){dataLayer.push(arguments);}
							gtag('js', new Date());

							gtag('config', 'UA-1419698-1');
						`
					}}

				/>

				{/* Facebook Pixel Code for Visit KC */}
				<script
					dangerouslySetInnerHTML={{
						__html: `
							!function(f, b, e, v, n, t, s)
							{
									if (f.fbq)
											return;
									n = f.fbq = function() {
											n.callMethod ?
											n.callMethod.apply(n, arguments) : n.queue.push(arguments)
									};
									if (!f._fbq)
											f._fbq = n;
									n.push = n;
									n.loaded = !0;
									n.version = '2.0';
									n.queue = [];
									t = b.createElement(e);
									t.async = !0;
									t.src = v;
									s = b.getElementsByTagName(e)[0];
									s.parentNode.insertBefore(t, s)
							}(window, document, 'script',
							'https://connect.facebook.net/en_US/fbevents.js');
							fbq('init', '1199689840048667');
							fbq('track', 'PageView');
						`,
					}}
				/>
				<noscript>
					<img height="1" width="1" src="https://www.facebook.com/tr?id=1199689840048667&ev=PageView
						&noscript=1"/>
				</noscript>
				{/* End Facebook Pixel Code */}

				<script src="https://js.adsrvr.org/up_loader.1.1.0.js" type="text/javascript"></script>
				<script
					dangerouslySetInnerHTML={{
						__html: `
							ttd_dom_ready(function() {
									if (typeof TTDUniversalPixelApi === 'function') {
											var universalPixelApi = new TTDUniversalPixelApi();
											universalPixelApi.init("7u4x0v8", ["o7kg2hq"], "https://insight.adsrvr.org/track/up");
									}
							});
						`,
					}}
				/>

				<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

				<script async src='https://tag.simpli.fi/sifitag/94b41200-8a77-0137-57be-06659b33d47c'></script>

				<script
					dangerouslySetInnerHTML={{
						__html: `
							var smgETrackParams = {
								subscriptionId: 'A010631E-1CFE-4BFE-9902-E4B5BCB644CF'
							};
							var smgConfigData = {};
						`,
					}}
				/>
				<script src="https://feedback.smg.com/etrackjslibrary" type="text/javascript" async></script>
				<script src="https://js.adsrvr.org/up_loader.1.1.0.js" type="text/javascript"></script>
				<script
					dangerouslySetInnerHTML={{
						__html: `
							ttd_dom_ready( function() {
									if (typeof TTDUniversalPixelApi === 'function') {
											var universalPixelApi = new TTDUniversalPixelApi();
											universalPixelApi.init("sn0fms7", ["2ihgygc"], "https://insight.adsrvr.org/track/up");
									}
							});
						`,
					}}
				/>
			</head>
			<body {...props.bodyAttributes}>
				{props.preBodyComponents}
				<div
					key={`body`}
					id="___gatsby"
					dangerouslySetInnerHTML={{ __html: props.body }}
				/>
				<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
				<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
				<script src="/mapplic/mapplic.js"></script>
				{props.postBodyComponents}
			</body>
		</html>
	)
}

HTML.propTypes = {
	htmlAttributes: PropTypes.object,
	headComponents: PropTypes.array,
	bodyAttributes: PropTypes.object,
	preBodyComponents: PropTypes.array,
	body: PropTypes.string,
	postBodyComponents: PropTypes.array,
}
