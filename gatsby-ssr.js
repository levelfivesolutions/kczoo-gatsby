import React from "react"
import { withPrefix } from "gatsby"
export const onRenderBody = (
  { setHeadComponents, setPostBodyComponents },
  pluginOptions
) => {
  setHeadComponents([
    <script
      dangerouslySetInnerHTML={{
        __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P86Z4KP');`,
      }}
    />,
    <script
      src="https://www.cognitoforms.com/f/seamless.js"
      data-key="bIzPUHzgCkWRr3DDrj7bRg"
    ></script>,
  ])
  setPostBodyComponents([
    <noscript>
      <iframe
        src="https://www.googletagmanager.com/ns.html?id=GTM-P86Z4KP"
        height="0"
        width="0"
        style={{ display: "none", visibility: "hidden" }}
      ></iframe>
    </noscript>,
  ])
}
