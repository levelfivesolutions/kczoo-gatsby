require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

/*****************************************************************************
 * * * * * * * * * * * * How to query a document ref * * * * * * * * * * * * * *
 *
 *  1. Head to https://kansascityzoo.prismic.io/api/v2
 *  2. Copy the "At this timeline ref"
 *  3. Paste it in the gatsby-source-prismic-graphql as a 'prismicRef' option:
 *
 *   {
 *     resolve: 'gatsby-source-prismic-graphql',
 *     options: {
 *       …
 *       prismicRef: '...', // optional, default: master; useful for A/B experiments
 *       …
 *   }
 *
 ******************************************************************************/

module.exports = {
  siteMetadata: {
    title: `Kansas City Zoo`,
    description: `The Kansas City Zoo connects people to each other and the natural world to promote understanding, appreciation, and conservation.`,
    author: `@KansasCityZoo`,
    siteUrl: `https://www.kansascityzoo.org`,
  },
  plugins: [
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        exclude: [`/category/*`, `/path/to/page`, `/preview/*`],
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-prismic-graphql`,
      options: {
        repositoryName: `kansascityzoo`, // required
        path: "/preview", // optional, default: /preview
        previews: true, // optional, default: false
        extraPageFields: `_meta{tags}`,
        // prismicRef: "Y1wGSRAAACEAswZH~Y1rneRAAAB8ArjF9", // optional, default: master; useful for A/B experiments
        // accessToken: `MC5YaFkzN2hVQUFDUUFEOFJC.77-9eO-_vW4QMUgs77-9b--_ve-_ve-_ve-_vTrvv73vv73vv73vv73vv73vv71dP--_vWoNRe-_vV3vv73vv73vv70`,
        pages: [
          {
            type: "Photo_library", // TypeName from prismic
            match: "/jazzoo/:uid", // Pages will be generated under this pattern
            path: "/jazzoo", // Placeholder page for unpublished documents
            component: require.resolve(
              "./src/templates/jazzoo/photo-library.js"
            ),
          },
          {
            type: "Jazzoo_landing_page", // TypeName from prismic
            match: "/jazzoo/:uid", // Pages will be generated under this pattern
            path: "/jazzoo", // Placeholder page for unpublished documents
            component: require.resolve("./src/templates/jazzoo/landing.js"),
          },
          {
            type: "Jazzoo_grid_layout", // TypeName from prismic
            match: "/jazzoo/:uid", // Pages will be generated under this pattern
            path: "/jazzoo", // Placeholder page for unpublished documents
            component: require.resolve("./src/templates/jazzoo/grid.js"),
          },
          {
            type: "Special_event", // TypeName from prismic
            match: "/event/:uid", // Pages will be generated under this pattern
            filter: data => !data.node._meta.tags.includes("Big Event"),
            path: "/event", // Placeholder page for unpublished documents
            component: require.resolve("./src/templates/event.js"),
          },
          {
            type: "Form", // TypeName from prismic
            match: "/form/:uid", // Pages will be generated under this pattern
            path: "/form", // Placeholder page for unpublished documents
            component: require.resolve("./src/templates/form.js"),
          },
          {
            type: "Special_exhibit",
            match: "/exhibit/:uid",
            path: "/exhibit",
            component: require.resolve("./src/templates/exhibit.js"),
          },
          {
            type: "Camp",
            match: "/camp/:uid",
            path: "/camp",
            component: require.resolve("./src/templates/camp.js"),
          },
          // {
          //   type: "Special_experience",
          //   match: "/guided-tour/:uid",
          //   path: "/guided-tour",
          //   component: require.resolve("./src/templates/guided-tour.js"),
          // },
          {
            type: "Redirect",
            match: "/:uid",
            path: "/redirect",
            component: require.resolve("./src/templates/redirect.js"),
          },
          {
            type: "Detail",
            match: "/aquarium/:uid",
            filter: data => data.node._meta.tags.includes("Aquarium"),
            path: "/aquarium",
            component: require.resolve("./src/templates/aquarium-detail.js"),
          },
          {
            type: "Detail",
            match: "/jazzoo/:uid",
            filter: data => data.node._meta.tags.includes("Jazzoo"),
            path: "/jazzoo",
            component: require.resolve(
              "./src/templates/jazzoo/jazzoo-detail.js"
            ),
          },
          {
            type: "Aquarium_site_plan",
            match: "/aquarium/site-plan",
            path: "/aquarium/site-plan",
            component: require.resolve("./src/pages/aquarium/site-plan.js"),
          },
          {
            type: "Renewal_page",
            match: "/memberships/renew",
            path: "/memberships/renew",
            component: require.resolve("./src/pages/renewals.js"),
          },
          {
            type: "Detail",
            match: "/preschool/:uid",
            filter: data => data.node._meta.tags.includes("Preschool"),
            path: "/preschool",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/about/:uid",
            filter: data => data.node._meta.uid.includes("Contact Us"),
            path: "/about",
            component: require.resolve("./src/pages/about/contact-us.jsx"),
          },
          {
            type: "Detail",
            match: "/:uid",
            filter: data => data.node._meta.uid.includes("careers"),
            path: "/careers",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/support-us/:uid",
            filter: data => data.node._meta.tags.includes("Get Involved"),
            path: "/support/us",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/homeschool/:uid",
            filter: data => data.node._meta.tags.includes("Homeschool"),
            path: "/homeschool",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/overnight/:uid",
            filter: data => data.node._meta.tags.includes("Overnights"),
            path: "/overnight",
            component: require.resolve("./src/templates/detail.js"),
          },

          {
            type: "Detail",
            match: "/volunteer/:uid",
            filter: data => data.node._meta.tags.includes("Volunteer"),
            path: "/volunteer",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/zoomobile/:uid",
            filter: data => data.node._meta.tags.includes("Zoomobile"),
            path: "/zoomobile",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/animal-encounters/:uid",
            filter: data => data.node._meta.tags.includes("Animal Encounters"),
            path: "/animal-encounters",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/about/:uid",
            filter: data => data.node._meta.tags.includes("About"),
            path: "/about",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/support-us/:uid",
            filter: data => data.node._meta.tags.includes("Support Us"),
            path: "/support-us",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/visit/:uid",
            filter: data => data.node._meta.tags.includes("Visit"),
            path: "/visit",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/education/:uid",
            filter: data => data.node._meta.tags.includes("Education"),
            path: "/education",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/distance-learning/:uid",
            filter: data => data.node._meta.tags.includes("Distance Learning"),
            path: "/distance-learning",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/donate/:uid",
            filter: data => data.node._meta.tags.includes("Donations"),
            path: "/donate",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/activity/:uid",
            filter: data => data.node._meta.tags.includes("Daily Schedule"),
            path: "/activity",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/itinerary/:uid",
            filter: data => data.node._meta.tags.includes("Itinerary"),
            path: "/itinerary",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/conservation/:uid",
            filter: data => data.node._meta.tags.includes("Conservation"),
            path: "/conservation",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Detail",
            match: "/fyi/:uid",
            filter: data => data.node._meta.tags.includes("Ad Hoc Info"),
            path: "/fyi",
            component: require.resolve("./src/templates/detail.js"),
          },
          {
            type: "Animal_cam_detail",
            match: "/animal-cam/:uid",
            path: "/animal-cam",
            component: require.resolve("./src/templates/animal-cam.js"),
          },
          {
            type: "Landing_page_hero_block",
            match: "/:uid",
            filter: data =>
              data.node._meta.uid === "animal-cams" ||
              data.node._meta.uid === "donate" ||
              data.node._meta.uid === "volunteers" ||
              data.node._meta.uid === "distance-learning" ||
              data.node._meta.tags.includes("Ad Hoc Info"),
            path: "/",
            component: require.resolve("./src/templates/landing.js"),
          },
          {
            type: "Landing_page_hero_block",
            match: "/support-us/:uid",
            filter: data =>
              data.node._meta.uid === "memorials-and-tributes" ||
              data.node._meta.uid === "planned-giving-and-stock",
            path: "/",
            component: require.resolve("./src/templates/landing.js"),
          },
          {
            type: "Landing_page_hero_block",
            match: "/education/:uid",
            filter: data => data.node._meta.tags.includes("Education"),
            path: "/education",
            component: require.resolve("./src/templates/landing.js"),
          },
          {
            type: "Detail_with_plu",
            match: "/animal-care-academy/:uid",
            filter: data =>
              data.node._meta.tags.includes("Animal Care Academy"),
            path: "/animal-care-academy",
            component: require.resolve("./src/templates/detail-with-plu.js"),
          },
          {
            type: "Scout_detail",
            match: "/scouts/:uid",
            path: "/scouts",
            component: require.resolve("./src/templates/scout-workshop.js"),
          },
          {
            type: "Conservation_project",
            match: "/conservation-project/:uid",
            path: "/conservation-project",
            component: require.resolve(
              "./src/templates/conservation-project.js"
            ),
          },
          {
            type: "Landing_page_hero_block",
            match: "/conservation-projects/:uid",
            filter: data => data.node._meta.tags.includes("Conservation"),
            path: "/conservation-projects",
            component: require.resolve(
              "./src/templates/conservation-projects.js"
            ),
          },
        ],
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}

//            filter: data => data.node._meta.tags.includes('Zoomobile'),
