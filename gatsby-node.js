/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

exports.createPages = async function({ actions, graphql }) {
  const { data: homeData } = await graphql(`
    {
      prismic {
        allBig_event_homes {
          edges {
            node {
              _meta {
                uid
              }
              event {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("homeData", homeData)
  homeData.prismic.allBig_event_homes.edges.forEach(data => {
    if (data.node.event._meta.uid) {
      console.log("CREATING -->", data.node.event._meta.uid)

      let specificEvent = ""
      // Can add more specific uids here
      if (data.node.event._meta.tags.includes("GloWild")) {
        specificEvent = "/glo-wild"
      }
      if (data.node.event._meta.tags.includes("Brew at the Zoo")) {
        specificEvent = "/brew-at-the-zoo"
      }

      actions.createPage({
        path: data.node.event._meta.uid,
        component: require.resolve(
          `./src/templates/big-event${specificEvent}/home.js`
        ),
        context: { uid: data.node._meta.uid, event: data.node.event._meta.uid },
      })
    }
  })

  const { data: eventDetailData } = await graphql(`
    {
      prismic {
        allBig_event_details {
          edges {
            node {
              _meta {
                uid
              }
              event {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("eventDetailData", eventDetailData)
  eventDetailData.prismic.allBig_event_details.edges.forEach(data => {
    if (data.node.event._meta.uid) {
      const path = `${data.node.event._meta.uid}/detail`
      console.log("CREATING BIG EVENT DETAIL -->", path, data.node._meta.uid)

      let specificEvent = ""
      // Can add more specific uids here
      if (data.node.event._meta.tags.includes("GloWild")) {
        specificEvent = "/glo-wild"
      }
      if (data.node.event._meta.tags.includes("Brew at the Zoo")) {
        specificEvent = "/brew-at-the-zoo"
      }

      actions.createPage({
        path: path,
        component: require.resolve(
          `./src/templates/big-event${specificEvent}/detail.js`
        ),
        context: { uid: data.node._meta.uid, event: data.node.event._meta.uid },
      })
    }
  })

  const { data: eventTicketsData } = await graphql(`
    {
      prismic {
        allBig_event_ticket_pages {
          edges {
            node {
              _meta {
                uid
                id
              }
              big_event_parent {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    id
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("eventTicketsData", eventTicketsData)
  eventTicketsData.prismic.allBig_event_ticket_pages.edges.forEach(data => {
    if (data?.node?.big_event_parent?._meta?.uid) {
      const path = `${data?.node?.big_event_parent?._meta?.uid}/tickets`
      console.log("CREATING BIG EVENT Tickets -->", path, data.node._meta.uid)

      let specificEvent = ""
      // Can add more specific uids here
      if (data.node.big_event_parent._meta.tags.includes("GloWild")) {
        specificEvent = "/glo-wild"
      }
      if (data.node.big_event_parent._meta.tags.includes("Brew at the Zoo")) {
        specificEvent = "/brew-at-the-zoo"
      }

      actions.createPage({
        path: path,
        component: require.resolve(
          `./src/templates/big-event${specificEvent}/tickets-page.js`
        ),
        context: {
          uid: data.node._meta.uid,
          id: data.node._meta.id,
          event: data.node.big_event_parent._meta.uid,
        },
      })
    }
  })

  const { data: bigEventLandingData } = await graphql(`
    {
      prismic {
        allBig_event_landing_pages {
          edges {
            node {
              _meta {
                uid
              }
              event {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("bigEventLandingData", bigEventLandingData)
  bigEventLandingData.prismic.allBig_event_landing_pages.edges.forEach(data => {
    if (data.node.event._meta.uid) {
      const path = `${data.node.event._meta.uid}/${data.node._meta.uid}`
      console.log("CREATING BIG EVENT LANDING PAGE -->", path)

      let specificEvent = ""
      // Can add more specific uids here
      if (data.node.event._meta.tags.includes("GloWild")) {
        specificEvent = "/glo-wild"
      }
      if (data.node.event._meta.tags.includes("Brew at the Zoo")) {
        specificEvent = "/brew-at-the-zoo"
      }

      actions.createPage({
        path: path,
        component: require.resolve(
          `./src/templates/big-event${specificEvent}/landing.js`
        ),
        context: { uid: data.node._meta.uid, event: data.node.event._meta.uid },
      })
    }
  })

  const { data: bigEventSponsorsData } = await graphql(`
    {
      prismic {
        allBig_event_sponsorss {
          edges {
            node {
              _meta {
                uid
                id
              }
              event {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("bigEventSponsorsData", bigEventSponsorsData)
  bigEventSponsorsData.prismic.allBig_event_sponsorss.edges.forEach(data => {
    if (data.node.event._meta.uid) {
      const path = `${data.node.event._meta.uid}/sponsors`
      console.log("CREATING BIG EVENT SPONSORS PAGE -->", path)

      let specificEvent = ""
      // Can add more specific uids here
      if (data.node.event._meta.tags.includes("GloWild")) {
        specificEvent = "/glo-wild"
      }
      if (data.node.event._meta.tags.includes("Brew at the Zoo")) {
        specificEvent = "/brew-at-the-zoo"
      }

      actions.createPage({
        path: path,
        component: require.resolve(
          `./src/templates/big-event${specificEvent}/sponsors.js`
        ),
        context: { id: data.node._meta.id, event: data.node.event._meta.uid },
      })
    }
  })

  const { data: bigEventVolunteersData } = await graphql(`
    {
      prismic {
        allBig_event_volunteerss {
          edges {
            node {
              _meta {
                uid
              }
              event {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("bigEventVolunteersData", bigEventVolunteersData)
  bigEventVolunteersData.prismic.allBig_event_volunteerss.edges.forEach(
    data => {
      if (data.node.event._meta.uid) {
        const path = `${data.node.event._meta.uid}/volunteers`
        console.log("CREATING BIG EVENT VOLUNTEERS PAGE -->", path)

        let specificEvent = ""
        // Can add more specific uids here
        if (data.node.event._meta.tags.includes("GloWild")) {
          specificEvent = "/glo-wild"
        }
        if (data.node.event._meta.tags.includes("Brew at the Zoo")) {
          specificEvent = "/brew-at-the-zoo"
        }

        actions.createPage({
          path: path,
          component: require.resolve(
            `./src/templates/big-event${specificEvent}/volunteers.js`
          ),
          context: {
            uid: data.node._meta.uid,
            event: data.node.event._meta.uid,
          },
        })
      }
    }
  )

  const { data: bigEventGridData } = await graphql(`
    {
      prismic {
        allBig_event_grid_layouts {
          edges {
            node {
              _meta {
                uid
              }
              event {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("bigEventGridData", bigEventGridData)
  bigEventGridData.prismic.allBig_event_grid_layouts.edges.forEach(data => {
    if (data.node.event._meta.uid) {
      const path = `${data.node.event._meta.uid}/${data.node._meta.uid}`
      console.log("CREATING BIG EVENT GRID PAGE -->", path)

      let specificEvent = ""
      // Can add more specific uids here
      if (data.node.event._meta.tags.includes("GloWild")) {
        specificEvent = "/glo-wild"
      }
      if (data.node.event._meta.tags.includes("Brew at the Zoo")) {
        specificEvent = "/brew-at-the-zoo"
      }

      actions.createPage({
        path: path,
        component: require.resolve(
          `./src/templates/big-event${specificEvent}/grid.js`
        ),
        context: { uid: data.node._meta.uid, event: data.node.event._meta.uid },
      })
    }
  })

  const { data: bigEventPhotoLibraryData } = await graphql(`
    {
      prismic {
        allBig_event_photo_librarys {
          edges {
            node {
              _meta {
                uid
              }
              event {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("bigEventPhotoLibraryData", bigEventPhotoLibraryData)
  bigEventPhotoLibraryData.prismic.allBig_event_photo_librarys.edges.forEach(
    data => {
      if (data.node.event._meta.uid) {
        const path = `${data.node.event._meta.uid}/${data.node._meta.uid}`
        console.log("CREATING BIG EVENT GRID PAGE -->", path)

        let specificEvent = ""
        // Can add more specific uids here
        if (data.node.event._meta.tags.includes("GloWild")) {
          specificEvent = "/glo-wild"
        }
        if (data.node.event._meta.tags.includes("Brew at the Zoo")) {
          specificEvent = "/brew-at-the-zoo"
        }

        actions.createPage({
          path: path,
          component: require.resolve(
            `./src/templates/big-event${specificEvent}/photo-library.js`
          ),
          context: {
            uid: data.node._meta.uid,
            event: data.node.event._meta.uid,
          },
        })
      }
    }
  )

  const { data: bigEventContentPagesData } = await graphql(`
    {
      prismic {
        allBig_event_content_pages {
          edges {
            node {
              _meta {
                uid
              }
              event {
                _linkType
                ... on PRISMIC__Document {
                  _meta {
                    uid
                    tags
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  console.log("bigEventContentPagesData", bigEventContentPagesData)
  bigEventContentPagesData.prismic.allBig_event_content_pages.edges.forEach(
    data => {
      if (data.node.event._meta.uid) {
        const path = `${data.node.event._meta.uid}/${data.node._meta.uid}`
        console.log("CREATING BIG EVENT CONTENT PAGE -->", path)

        let specificEvent = ""
        // Can add more specific uids here
        if (data.node.event._meta.tags.includes("GloWild")) {
          specificEvent = "/glo-wild"
        }
        if (data.node.event._meta.tags.includes("Brew at the Zoo")) {
          specificEvent = "/brew-at-the-zoo"
        }

        actions.createPage({
          path: path,
          component: require.resolve(
            `./src/templates/big-event${specificEvent}/content-page.js`
          ),
          context: {
            uid: data.node._meta.uid,
            event: data.node.event._meta.uid,
          },
        })
      }
    }
  )
}
