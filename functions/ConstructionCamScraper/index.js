module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    var request = require('request');

    async function makeReq() {
        let retHtml;
        request('https://www.p-tn.net/pwc/kczoo/recent-image.asp', function(error, result, html){
            if(error) {
                console.log('An error occurred');
                console.log(error);
                retHtml = error;
                return error;
            }
            
            console.log(html);
            retHtml = html
        })
        return retHtml;
    }

    context.res = {
        // status: 200, /* Defaults to 200 */
        body: makeReq()
    };
}