module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    const url = "https://dev.azure.com/LevelFiveUX/_apis/public/distributedtask/webhooks/PrismicZoo?api-version=6.0-preview";
    var method = "POST";
    let body = {
        "test":"Cant send with an empty body I guess"
    }

    if( !url ){
        context.res = {
            // status: 200, /* Defaults to 200 */
            body: "Please add the url for the pipeline as a parameter to the request."
        };
    } else {

        // You REALLY want shouldBeAsync = true.
        // Otherwise, it'll block ALL execution waiting for server response.
        var shouldBeAsync = true;

        var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
        
        var request = new XMLHttpRequest();
        
        // Before we send anything, we first have to say what we will do when the
        // server responds. This seems backwards (say how we'll respond before we send
        // the request? huh?), but that's how Javascript works.
        // This function attached to the XMLHttpRequest "onload" property specifies how
        // the HTTP response will be handled. 

        request.onload = function () {
            // Because of javascript's fabulous closure concept, the XMLHttpRequest "request"
            // object declared above is available in this function even though this function
            // executes long after the request is sent and long after this function is
            // instantiated. This fact is CRUCIAL to the workings of XHR in ordinary
            // applications.

            // You can get all kinds of information about the HTTP response.
            var status = request.status; // HTTP response status, e.g., 200 for "200 OK"
            var data = request.responseText; // Returned data, e.g., an HTML document.

            const responseMessage = (status === 200) ?
                "Production Build Triggered Successfully" :
                "Error triggering build: " + status;


        }

        request.open(method, url, shouldBeAsync);


         request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        // request.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
        // Or... whatever

        // Actually sends the request to the server.
        request.send(JSON.stringify(body))
    }
}